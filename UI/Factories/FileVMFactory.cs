﻿using Repository;
using Repository.Abstract;
using Repository.Entities;
using Repository.Misc;
using System;
using System.IO;

namespace UI.Factories
{
    internal class FileVMFactory
    {
        internal delegate string MapPathFunc(string path);

        internal static byte[] GetList(string url_prefix, Formats format, MapPathFunc mapPathFunc)
        {
            byte[] data = null;

            if (format == Formats.xsd)
                data = File.ReadAllBytes("");
            else
            {
                string fname = mapPathFunc(string.Format("~/Content/Files/list.{0}", format));
                if (File.Exists(fname))
                {
                    data = File.ReadAllBytes(fname);
                }
                else
                {
                    bool hasErrors = false;
                    using (var ctx = new DataContext())
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            try
                            {
                                var setPassportRepository = RepositoryFactory.Get<ISetPassportRepository>(ctx);
                                setPassportRepository.ExportList(memoryStream, url_prefix, format);
                                data = memoryStream.ToArray();
                            }
                            catch
                            {
                                hasErrors = true;
                            }
                        }
                    }

                    if (!hasErrors) WriteToFile(fname, data);
                }
            }
            return data;
        }

        internal static byte[] GetMeta(string setId, string version, string url_prefix, Formats format, MapPathFunc mapPathFunc)
        {
            byte[] data = null;

            if (format == Formats.xsd)
                data = File.ReadAllBytes("");
            else
            {
                string fname = mapPathFunc(string.Format("~/Content/Files/{0}/meta.{1}", setId, format));

                if (File.Exists(fname))
                {
                    data = File.ReadAllBytes(fname);
                }
                else
                {
                    bool hasErrors = false;
                    using (var ctx = new DataContext())
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            try
                            {
                                var setPassportRepository = RepositoryFactory.Get<ISetPassportRepository>(ctx);
                                setPassportRepository.ExportMeta(memoryStream, url_prefix, setId, null, format);
                                data = memoryStream.ToArray();
                            }
                            catch
                            {
                                hasErrors = true;
                            }
                        }
                    }

                    if (!hasErrors) WriteToFile(fname, data);
                }
            }

            return data;
        }

        internal static byte[] GetData(string setId, string versId, string structId, Formats format, MapPathFunc mapPathFunc)
        {
            byte[] data = null;

            string fname = mapPathFunc(string.Format("~/Content/Files/{0}/data-{1}-structure-{2}.{3}", setId, versId, structId, format));

            if (File.Exists(fname))
            {
                data = File.ReadAllBytes(fname);
            }
            else
            {
                bool hasErrors = false;

                using (var ctx = new DataContext())
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        var tableDataRepository = RepositoryFactory.Get<ISetDataRepository>(ctx);
                        tableDataRepository.ExportData(memoryStream, setId, versId, structId, format);
                        data = memoryStream.ToArray();
                    }
                }

                if (!hasErrors) WriteToFile(fname, data);
            }

            return data;
        }

        internal static byte[] GetStructure(string setId, string structId, Formats format, Func<string, string> mapPathFunc)
        {
            byte[] data = null;

            string fname = mapPathFunc(string.Format("~/Content/Files/{0}/structure-{1}.{2}", setId, structId, format));
            if (File.Exists(fname))
            {
                data = File.ReadAllBytes(fname);
            }
            else
            {
                bool hasErrors = false;
                using (var ctx = new DataContext())
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        try
                        {
                            var dataStructRepository = RepositoryFactory.Get<IDataStructureRepository>(ctx);
                            dataStructRepository.ExportStructure(memoryStream, setId, structId, format);
                            data = memoryStream.ToArray();
                        }
                        catch
                        {
                            hasErrors = true;
                        }
                    }
                }

                if (!hasErrors) WriteToFile(fname, data);
            }

            return data;
        }


        static void WriteToFile(string path, byte[] data)
        {
            if (data.Length == 0) return;

            var dir = Path.GetDirectoryName(path);
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

            using (var fileStream = File.Create(path))
            {
                try
                {
                    fileStream.Write(data, 0, data.Length);
                }
                catch { }
            }
        }

        internal static byte[] GetStructure(Guid setVerId, Formats format, Func<string, string> mapPathFunc)
        {
            byte[] data = null;

            string fname = mapPathFunc(string.Format("~/Content/Files/{0:N}/structure.{1}", setVerId, format));
            if (File.Exists(fname))
            {
                data = File.ReadAllBytes(fname);
            }
            else
            {
                bool hasErrors = false;
                using (var ctx = new DataContext())
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        try
                        {
                            var dataStructRepository = RepositoryFactory.Get<IDataStructureRepository>(ctx);
                            dataStructRepository.ExportStructure(memoryStream, setVerId, format);
                            data = memoryStream.ToArray();
                        }
                        catch
                        {
                            hasErrors = true;
                        }
                    }
                }

                if (!hasErrors) WriteToFile(fname, data);
            }

            return data;
        }

        internal static byte[] GetData(Guid setVerId, Formats format, Func<string, string> mapPathFunc)
        {
            byte[] data = null;

            string fname = mapPathFunc(string.Format("~/Content/Files/{0:N}/data.{1}", setVerId, format));
            if (File.Exists(fname))
            {
                data = File.ReadAllBytes(fname);
            }
            else
            {
                bool hasErrors = false;
                using (var ctx = new DataContext())
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        try
                        {
                            var tableDataRepository = RepositoryFactory.Get<ISetDataRepository>(ctx);
                            tableDataRepository.ExportData(memoryStream, setVerId, format);
                            data = memoryStream.ToArray();
                        }
                        catch
                        {
                            hasErrors = true;
                        }
                    }
                }

                if (!hasErrors) WriteToFile(fname, data);
            }

            return data;
        }
    }
}