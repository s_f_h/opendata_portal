﻿using Repository;
using Repository.Abstract;
using Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI.Models;

namespace UI.Factories
{
    internal static class StatusVMFactory
    {
        internal static StatusVM GetIndexVM()
        {
            var result = new StatusVM();
            result.TopbarVM.Current = result.TopbarVM.First(x => x.Value == "Status");
            result.Title = result.TopbarVM.Current.Key;

            using (var ctx = new DataContext())
            {
                try
                {
                    var setVerRepo = RepositoryFactory.Get<IRepositoryBase<SetVersion, Guid>>(ctx);

                    var date = DateTime.Today.AddMonths(-6);

                    var addedVers = setVerRepo.Where(x => x.CreationDate >= date, 0, 50, x => x.SetsPassports.Select(y => y.Agency), x => x.Set.Category);
                    var modifiedVers = setVerRepo.Where(x => x.SetsPassports.Any(y => y.Modified >= date), 0, 50, x => x.SetsPassports.Select(y => y.Agency), x => x.Set.Category);

                    if (addedVers.IsOK && modifiedVers.IsOK)
                    {
                        result.AddedItems = addedVers.Result.ToArray()
                            .Select(x => new RegistryOrStatusItemVM
                            {
                                CreationDate = x.CreationDate,
                                ActualTo = x.CreationDate.AddDays((int)x.Set.UpdatePeriodicity),
                                Category = x.Set.Category.Name,
                                Description = x.Set.Description,
                                Name = x.Set.Title,
                                Id = x.Id,
                                Agencies = x.SetsPassports.Select(y => new AgencyItemVM
                                {
                                    AgencyId = y.Agency_Id,
                                    Name = y.Agency.Name,
                                    SetTitle = y.Title,
                                    SetId = y.Identifier,
                                })
                            });

                        result.ModifiedItems = modifiedVers.Result.ToArray()
                            .Select(x => new RegistryOrStatusItemVM
                            {
                                CreationDate = x.CreationDate,
                                ActualTo = (x.Set.UpdatePeriodicity > 0) ? (DateTime?)x.CreationDate.AddDays((int)x.Set.UpdatePeriodicity) : null,
                                Category = x.Set.Category.Name,
                                Description = x.Set.Description,
                                Name = x.Set.Title,
                                Id = x.Id,
                                Agencies = x.SetsPassports.Select(y => new AgencyItemVM
                                {
                                    AgencyId = y.Agency_Id,
                                    Name = y.Agency.Name,
                                    SetTitle = y.Title,
                                    SetId = y.Identifier,
                                })
                            });
                    }
                    else
                    {
                        result.HasErrors = true;
                    }
                }
                catch
                {
                    result.HasErrors = true;
                }
            }

            return result;
        }

        internal static IList<RegistryOrStatusItemVM> LoadAdded(int page)
        {
            throw new NotImplementedException();
        }

        internal static IList<RegistryOrStatusItemVM> LoadModified(int page)
        {
            throw new NotImplementedException();
        }
    }
}