﻿using Repository;
using Repository.Abstract;
using Repository.Entities;
using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using UI.Models;

namespace UI.Factories
{
    public static class AgencyCategoryVMFactory
    {
        public static ClassifierItemsVM GetCategoryVM()
        {
            ClassifierItemsVM result = new ClassifierItemsVM();
            result.TopbarVM.Current = result.TopbarVM.First(x => string.Equals(x.Value, "category", StringComparison.InvariantCultureIgnoreCase));
            result.Title = result.TopbarVM.Current.Key;
            GetItems<Category>(result);

            return result;
        }

        public static ClassifierItemsVM GetAgencyVM()
        {
            ClassifierItemsVM result = new ClassifierItemsVM();
            result.TopbarVM.Current = result.TopbarVM.First(x => string.Equals(x.Value, "agency", StringComparison.InvariantCultureIgnoreCase));
            result.Title = result.TopbarVM.Current.Key;

            GetItems<Agency>(result);

            return result;
        }

        static void GetItems<T>(ClassifierItemsVM vm) where T : AgencyCategoryBase
        {
            using (var ctx = new DataContext())
            {
                try
                {
                    var items_repo = RepositoryFactory.Get<IRepositoryBase<T, Guid>>(ctx);
                    var sets_repo = RepositoryFactory.Get<IRepositoryBase<Set, Guid>>(ctx);

                    var items = items_repo.Where();
                    var sets = sets_repo.Count();

                    if (items.IsOK && sets.IsOK)
                    {
                        vm.Items = new Holder<AgencyCategoryBase>(items.Result);
                        vm.SetsTotal = sets.Result;
                    }
                    else
                    {
                        vm.HasErrors = true;
                    }
                }
                catch { }
            }
        }

        internal static SetsVM GetAgencySetsVM(Guid id, int rating)
        {
            var result = new SetsVM();
            result.TopbarVM.Current = result.TopbarVM.First(x => string.Equals(
                x.Value, "agency", StringComparison.InvariantCultureIgnoreCase));

            result.RatingWidgetVM = new RatingWidgetVM { Value = rating };

            using (var ctx = new DataContext())
            {
                try
                {
                    var agencyRepo = RepositoryFactory.Get<IRepositoryBase<Agency, Guid>>(ctx);
                    var agencies = agencyRepo.Where();

                    var setPassportsRepo = RepositoryFactory.Get<ISetPassportRepository>(ctx);

                    RepositoryOperationResult<int> passportsCount;

                    if (rating > 0)
                    {
                        var _result = setPassportsRepo.GetPassportsForAgencyAndRating(id, rating);
                        passportsCount = new RepositoryOperationResult<int>(_result.IsOK, _result.Message, _result.Result.Count());
                    }
                    else
                        passportsCount = setPassportsRepo.Count(x => x.SetVersion.IsCurrent && x.Agency_Id == id);


                    if (agencies.IsOK && passportsCount.IsOK)
                    {
                        result.SidebarVM = new Holder<AgencyCategoryBase>(agencies.Result);
                        result.SidebarVM.Current = result.SidebarVM.FirstOrDefault(x => x.Id == id);
                        result.Title = (result.SidebarVM.Current as Agency).ShortName;

                        result.SetsTotal = passportsCount.Result;
                    }
                }
                catch { }
            }

            return result;
        }

        internal static SetsVM GetCategorySetsVM(Guid id, int rating)
        {
            var result = new SetsVM();
            result.TopbarVM.Current = result.TopbarVM.First(x => string.Equals(
                x.Value, "category", StringComparison.InvariantCultureIgnoreCase));

            result.RatingWidgetVM = new RatingWidgetVM { Value = rating };

            using (var ctx = new DataContext())
            {
                try
                {
                    var setVersRepo = RepositoryFactory.Get<ISetVersionRepository>(ctx);
                    RepositoryOperationResult<int> setVersCount;

                    if (rating > 0)
                    {
                        var _resulr =  setVersRepo.GetSetVersionsForCategoryAndRating(id, rating);
                        setVersCount = new RepositoryOperationResult<int>(_resulr.IsOK, _resulr.Message, _resulr.Result.Count());
                    }
                    else
                        setVersCount = setVersRepo.Count(x => x.IsCurrent && x.Set.Category_Id == id);


                    var categoryRepo = RepositoryFactory.Get<IRepositoryBase<Category, Guid>>(ctx);
                    var categories = categoryRepo.Where();

                    if (setVersCount.IsOK && categories.IsOK)
                    {
                        result.SidebarVM = new Holder<AgencyCategoryBase>(categories.Result);
                        result.SidebarVM.Current = result.SidebarVM.FirstOrDefault(x => x.Id == id);
                        result.Title = result.SidebarVM.Current.Name;

                        result.SetsTotal = setVersCount.Result;
                    }
                }
                catch { }
            }

            return result;
        }

        internal static IList<SetsItemVM> LoadAgencySets(Guid id, int page, int rating)
        {
            IList<SetsItemVM> result = null;

            int from = Common.Configuration.Base.ItemsPerPage * (page - 1);
            int count = Common.Configuration.Base.ItemsPerPage;

            using (var ctx = new DataContext())
            {
                try
                {
                    var repo = RepositoryFactory.Get<ISetPassportRepository>(ctx);

                    RepositoryOperationResult<IQueryable<SetPassport>> passports;

                    if (rating > 0)
                        passports = repo.GetPassportsForAgencyAndRating(id, rating, x => x.SetVersion.IsCurrent,
                            from, count, x => x.Agency, x => x.SetVersion.Set.Category, x => x.ViewedBy, x => x.RatedBy);
                    else
                        passports = repo.Where(x => x.Agency_Id == id && x.SetVersion.IsCurrent,
                        from, count, x => x.Agency, x => x.SetVersion.Set.Category, x=>x.SetVersion.DataStructure,
                        x => x.ViewedBy, x => x.RatedBy);

                    if (passports.IsOK)
                    {
                        result = passports.Result.ToArray().Select(x => new SetsItemVM
                        {
                            Id = x.Identifier,
                            Name = x.Title,
                            Description = x.Description,
                            Category = x.SetVersion.Set.Category.Name,
                            Viewed = x.ViewsCount,
                            AverageRating = double.IsNaN(x.AverageRating) || double.IsInfinity(x.AverageRating) ? 0 : x.AverageRating,
                            VerName = x.SetVersion.Name,
                            VersId = x.SetVersion.CodeName,
                            StructId = x.SetVersion.DataStructure.CodeName

                        }).ToList();
                    }
                }
                catch { }
            }

            return result;
        }

        internal static IList<SetsItemVM> LoadCategorySets(Guid id, int page, int rating)
        {
            IList<SetsItemVM> result = null;

            int from = Common.Configuration.Base.ItemsPerPage * (page - 1);
            int count = Common.Configuration.Base.ItemsPerPage;

            using (var ctx = new DataContext())
            {
                try
                {
                    var repo = RepositoryFactory.Get<ISetVersionRepository>(ctx);

                    RepositoryOperationResult<IQueryable<SetVersion>> setVers;

                    if (rating > 0)
                        setVers = repo.GetSetVersionsForCategoryAndRating(id, rating,
                            x => x.IsCurrent, from, count, x => x.Set, x => x.SetsPassports
                            .Select(y => y.ViewedBy), x => x.SetsPassports.Select(y => y.RatedBy));
                    else
                        setVers = repo.Where(x => x.IsCurrent && x.Set.Category_Id == id, from, count,
                            x => x.SetsPassports.Select(y => y.Agency), x => x.Set, x => x.SetsPassports
                            .Select(y => y.ViewedBy), x => x.SetsPassports.Select(y => y.RatedBy));

                    if (setVers.IsOK)
                    {
                        result = setVers.Result.ToArray().Select(x =>
                        {
                            var res_item = new SetsItemVM
                            {
                                Id = x.Id,
                                Description = x.Set.Description,
                                Name = x.Set.Title,
                                Agencies = x.SetsPassports.ToArray().Select(y => new AgencyItemVM
                                {
                                    Name = y.Agency.Name,
                                    SetTitle = y.Title,
                                    SetId = y.Identifier,
                                    AgencyId = y.Agency_Id
                                }).ToList(),
                                VerName = x.Name,
                                AverageRating = x.SetsPassports.Sum(y => y.AverageRating) / x.SetsPassports.Count,
                                Viewed = x.SetsPassports.Sum(y => y.ViewsCount)
                            };

                            if (double.IsNaN(res_item.AverageRating) || double.IsInfinity(res_item.AverageRating))
                                res_item.AverageRating = 0;

                            return res_item;

                        }).ToList();
                    }
                }
                catch { }
            }


            return result;
        }
    }
}