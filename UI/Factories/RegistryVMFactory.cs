﻿using Repository;
using Repository.Abstract;
using Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UI.Models;

namespace UI.Factories
{
    public static class RegistryVMFactory
    {
        public static RegistryVM GetIndexPage(Expression<Func<SetVersion, bool>> searchParams = null)
        {
            var result = new RegistryVM();
            result.TopbarVM.Current = result.TopbarVM
                .First(x => string.Equals(x.Value, "registry", StringComparison.InvariantCultureIgnoreCase));

            result.Title = result.TopbarVM.Current.Key;

            using (var ctx = new DataContext())
            {
                var setVerRepo = RepositoryFactory.Get<IRepositoryBase<SetVersion, Guid>>(ctx);
                var agencyRepo = RepositoryFactory.Get<IRepositoryBase<Agency, Guid>>(ctx);
                var categoryRepo = RepositoryFactory.Get<IRepositoryBase<Category, Guid>>(ctx);

                var cnt = setVerRepo.Count(searchParams == null ? x => x.IsCurrent : searchParams);
                var categories = categoryRepo.Where(x => x.Sets.Any());
                var agencies = agencyRepo.Where(x => x.Sets.Any());

                if (cnt.IsOK && categories.IsOK && agencies.IsOK)
                {
                    result.SetsTotal = cnt.Result;

                    var _agencies = agencies.Result.OrderBy(x => x.Name).ToList();
                    var _categories = categories.Result.OrderBy(x => x.Name).ToList();

                    _agencies.Insert(0, new Agency { Id = Guid.Empty, Name = "Все" });
                    _categories.Insert(0, new Category { Id = Guid.Empty, Name = "Все" });

                    result.Categories = _categories;
                    result.Agencies = _agencies;
                }
                else
                {
                    result.HasErrors = true;
                }
            }

            return result;
        }

        public static List<RegistryOrStatusItemVM> Load(int page, Expression<Func<SetVersion, bool>> searchParams = null)
        {
            List<RegistryOrStatusItemVM> items = null;

            int from = Common.Configuration.Base.ItemsPerPage * (page - 1);
            int count = Common.Configuration.Base.ItemsPerPage;

            using (var ctx = new DataContext())
            {
                try
                {
                    var repo = RepositoryFactory.Get<IRepositoryBase<SetVersion, Guid>>(ctx);


                    var setVers = repo.Where(searchParams == null ? x => x.IsCurrent : searchParams, from, count,
                        x => x.Set.Category, x => x.SetsPassports.Select(y => y.Agency));



                    if (setVers.IsOK)
                    {
                        var _res = setVers.Result.ToArray();

                        items = new List<RegistryOrStatusItemVM>
                            (_res.Select(x => new RegistryOrStatusItemVM
                                {
                                    Id = x.Id,
                                    Category = x.Set.Category.Name,
                                    Name = x.Set.Title,
                                    Description = x.Set.Description,
                                    Agencies = new List<AgencyItemVM>(
                                        x.SetsPassports.ToArray().Select(y => new AgencyItemVM
                                        {
                                            AgencyId = y.Agency_Id,
                                            Name = y.Agency.Name,
                                            SetTitle = y.Title,
                                            SetId = y.Identifier,
                                        }))
                                }));
                    }
                }
                catch (Exception e)
                {
                }

            }
            return items;

        }
    }
}