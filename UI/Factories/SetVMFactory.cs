﻿using Repository;
using Repository.Abstract;
using Repository.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using UI.Models;

namespace UI.Factories
{
    public static class SetVMFactory
    {
        internal static SetViewVM CreateSetViewVM(Guid setVer, string userIP)
        {
            return CreateCategorySetViewVM(setVer, userIP);
        }

        internal static SetViewVM CreateSetViewVM(string passportId, string verCode, string structCode, string userIP)
        {
            return CreateAgencySetViewVM(passportId, verCode, structCode, userIP);
        }

        #region CreateSetViewVM
        static SetViewVM CreateCategorySetViewVM(Guid setVerId, string userIP)
        {
            SetViewVM result = new SetViewVM();
            using (var ctx = new DataContext())
            {
                var setVersionRepo = RepositoryFactory.Get<IRepositoryBase<SetVersion, Guid>>(ctx);
                var setPassportRepo = RepositoryFactory.Get<ISetPassportRepository>(ctx);

                var setVersion = setVersionRepo.FirstOrDefault(x => x.Id == setVerId,
                    x => x.SetsPassports.Select(y => y.DownloadedBy), x => x.Set,
                    x => x.SetsPassports.Select(y => y.RatedBy),
                    x => x.SetsPassports.Select(y => y.ViewedBy), x => x.DataStructure);

                if (setVersion.IsOK)
                {
                    if (setVersion.Result != null)
                    {
                        result.SetPassports = setVersion.Result.SetsPassports.Select(x => new SetPassportVM(x));

                        foreach (var setPassport in setVersion.Result.SetsPassports)
                        {
                            setPassportRepo.AddPassportViewdBy(setPassport.Id, userIP);
                        }

                        result.Title = setVersion.Result.Set.Title;
                        result.DownloadButtonVM = new DownloadButtonVM
                        {
                            Downloaded = setVersion.Result.SetsPassports.Sum(x => x.DownlodsCount),
                            Id = setVersion.Result.Id
                        };

                        result.Id = setVersion.Result.Id;

                        var rating = setVersion.Result.SetsPassports.Sum(x => x.AverageRating) / setVersion.Result.SetsPassports.Count;

                        result.RatingWidgetVM = new RatingWidgetVM
                        {
                            IsReadonly = setVersion.Result.SetsPassports.Any(x => x.RatedBy.Any(y => y.UserIP == userIP)),
                            Value = double.IsNaN(rating) || double.IsInfinity(rating) ? 0 : rating
                        };

                        result.DataStructureId = setVersion.Result.DataStructure_Id;
                        result.SetVersionsInfoVM = GetSetVersionInfo(setVersion.Result.Id, setVersion.Result.Name, setVersionRepo);
                        result.VersionId = setVerId;
                    }
                    else
                        result.IsEmpty = true;
                }
            }

            return result;
        }

        static SetViewVM CreateAgencySetViewVM(string passportId, string verCode, string structCode, string userIP)
        {
            SetViewVM result = new SetViewVM();

            Expression<Func<SetPassport, bool>> searchExpr;
            //= x => x.SetVersion.CodeName == verCode &&
            //    x.SetVersion.DataStructure.CodeName == structCode &&
            //    x.Identifier == passportId;

            if (string.IsNullOrWhiteSpace(verCode))
                searchExpr = x => x.Identifier == passportId && x.SetVersion.IsCurrent;
            else
                searchExpr = x => x.Identifier == passportId && x.SetVersion.IsCurrent
                    && x.SetVersion.CodeName == verCode;

            using (var ctx = new DataContext())
            {
                try
                {
                    var passportsRepo = RepositoryFactory.Get<ISetPassportRepository>(ctx);

                    var passport = passportsRepo.FirstOrDefault(searchExpr, x => x.RatedBy,
                        x => x.ViewedBy, x => x.DownloadedBy, x => x.SetVersion.DataStructure);

                    if (passport.IsOK)
                    {
                        if (passport != null)
                        {
                            result.SetPassports = new SetPassportVM[] { new SetPassportVM(passport.Result) };

                            passportsRepo.AddPassportViewdBy(passport.Result.Id, userIP);

                            result.Title = passport.Result.Title;
                            result.DownloadButtonVM = new DownloadButtonVM
                            {
                                Downloaded = passport.Result.DownlodsCount,
                                Id = passport.Result.Identifier,
                                VersionName = passport.Result.SetVersion.Name,
                                StructId = passport.Result.SetVersion.DataStructure.CodeName,
                                VersionId = passport.Result.SetVersion.CodeName
                            };

                            result.Id = passport.Result.Identifier;

                            var rating = passport.Result.AverageRating;

                            result.RatingWidgetVM = new RatingWidgetVM
                            {
                                IsReadonly = passport.Result.RatedBy.Any(x => x.UserIP == userIP),
                                Value = double.IsNaN(rating) || double.IsInfinity(rating) ? 0 : rating
                            };

                            var setVerRepo = RepositoryFactory.Get<IRepositoryBase<SetVersion, Guid>>(ctx);

                            result.SetVersionsInfoVM = GetSetVersionInfo(passportId, verCode, setVerRepo);
                            result.VersionId = passport.Result.SetVersion_Id;

                            result.DataStructureId = passport.Result.SetVersion.DataStructure_Id;
                        }
                        else
                            result.IsEmpty = true;
                    }
                    else
                    {
                        result.HasErrors = true;
                    }
                }
                catch { }
            }

            return result;
        }

        static SetVersionsInfoVM GetSetVersionInfo(Guid id, string verName, IRepositoryBase<SetVersion, Guid> repo)
        {
            var rootVer = repo.FirstOrDefault(x => x.Set.RootVersion.Any(y => y.Id == id) && x.ParentVersion_Id == null, x => x.DataStructure);

            if (rootVer.IsOK)
                return CreateSetVersionInfo(rootVer.Result, verName, repo);
            else
                return default(SetVersionsInfoVM);
        }

        static SetVersionsInfoVM GetSetVersionInfo(string passportId, string verCode, IRepositoryBase<SetVersion, Guid> repo)
        {
            var rootVer = repo.FirstOrDefault(x => x.SetsPassports.Any(y => y.Identifier == passportId) && x.ParentVersion_Id == null, x => x.DataStructure);

            if (rootVer.IsOK)
                return CreateSetVersionInfo(rootVer.Result, verCode, repo, passportId);
            else
                return default(SetVersionsInfoVM);
        }

        static SetVersionsInfoVM CreateSetVersionInfo(SetVersion root, string verCode, IRepositoryBase<SetVersion, Guid> repo, object id = null)
        {
            SetVersionsInfoVM result = new SetVersionsInfoVM
            {
                CreationDate = root.CreationDate,
                IsCurrent = root.IsCurrent,
                IsSelected = root.CodeName == verCode,
                Name = root.Name,
                Id = (id != null) ? id : root.Id,
                StructId = root.DataStructure.CodeName,
                VersId = root.CodeName
            };

            var subVersions = repo.Where(x => x.ParentVersion_Id == root.Id, 0, 0, x => x.DataStructure);
            if (subVersions.IsOK)
            {
                var _subVersions = new List<SetVersionsInfoVM>();
                foreach (var subVersion in subVersions.Result)
                    _subVersions.Add(CreateSetVersionInfo(subVersion, verCode, repo, id));

                result.SubVersions = _subVersions;
            }

            return result;
        }
        #endregion

        internal static TableDisplayTemplate LoadTemplate(Guid templateId)
        {
            TableDisplayTemplate result = null;

            using (var ctx = new DataContext())
            {
                try
                {
                    var tableRepo = RepositoryFactory.Get<ITableDisplayTemplateRepository>(ctx);
                    var table = tableRepo.TableStructure(templateId);
                    if (table.IsOK)
                        result = table.Result;
                }
                catch { }
            }

            return result;
        }

        #region LoadStructure
        //    private static TableDisplayTemplate LoadTableStructure(Guid templateId, IRepositoryBase<TableDisplayTemplate, Guid> tableRepo, IRepositoryBase<ColumnDisplayTemplate, Guid> columnRepo)
        //    {
        //        TableDisplayTemplate result = null;

        //        var template = tableRepo.FirstOrDefault(x => x.Id == templateId, x => x.Columns);

        //        if (template.IsOK)
        //        {
        //            result = new TableDisplayTemplate
        //            {
        //                Description = template.Result.Description,
        //                HasMapPOIs = template.Result.HasMapPOIs,
        //                Id = template.Result.Id,
        //                Name = template.Result.Name,
        //                Title = template.Result.Title,
        //                Type = template.Result.Type
        //            };



        //            result.Columns = template.Result.Columns.ToArray()
        //                .Select(x => LoadColumnStructure(x.Id, tableRepo, columnRepo)).ToArray();
        //        }

        //        return result;
        //    }

        //    private static ColumnDisplayTemplate LoadColumnStructure(Guid columnId, IRepositoryBase<TableDisplayTemplate, Guid> tableRepo, IRepositoryBase<ColumnDisplayTemplate, Guid> columnRepo)
        //    {
        //        ColumnDisplayTemplate result = null;

        //        var column = columnRepo.FirstOrDefault(x => x.Id == columnId, x => x.SubColumns);

        //        if (column.IsOK)
        //        {
        //            result = new ColumnDisplayTemplate
        //            {
        //                BindedCollumn = column.Result.BindedCollumn,
        //                Header = column.Result.Header,
        //                Id = column.Result.Id,
        //                IncludeIntoFilter = column.Result.IncludeIntoFilter,
        //                IsFullViewLink = column.Result.IsFullViewLink,
        //                ShowInPOIsPopup = column.Result.ShowInPOIsPopup,
        //                ShowInPreview = column.Result.ShowInPreview,
        //                Type = column.Result.Type
        //            };

        //            if (column.Result.InnerTable_Id.HasValue)
        //                result.InnerTable = LoadTableStructure(column.Result.InnerTable_Id.Value, tableRepo, columnRepo);

        //            if (result.SubColumns != null)
        //                result.SubColumns = column.Result.SubColumns.Select(x =>
        //                    LoadColumnStructure(x.Id, tableRepo, columnRepo))
        //                    .ToArray();
        //        }

        //        return result;
        //    }
        #endregion

        internal static DataStructure LoadStructure(Guid structId)
        {
            DataStructure result = null;

            using (var ctx = new DataContext())
            {
                try
                {
                    var tableRepo = RepositoryFactory.Get<ITableDisplayTemplateRepository>(ctx);
                    var structure = tableRepo.SetStructure(structId);
                    if (structure.IsOK)
                        result = structure.Result;
                }
                catch { }
            }

            return result;
        }

        internal static List<Dictionary<string, object>> LoadData(Guid verId, Guid structId, string passportId = null,
            int page = 1, string filterQuery = null, Guid? fullViewItemId = null, Guid? parentItemId = null)
        {
            List<Dictionary<string, object>> result = null;

            int from = Common.Configuration.Base.ItemsPerPage * (page - 1);
            int count = Common.Configuration.Base.ItemsPerPage;

            using (var ctx = new DataContext())
            {
                var repo = RepositoryFactory.Get<ISetDataRepository>(ctx);
                try
                {
                    if (fullViewItemId == null && parentItemId == null)
                        result = repo.GetTableData(verId, structId, passportId, from, count, filterQuery);
                    else
                        if (fullViewItemId != null && parentItemId == null)
                            result = repo.GetFullViewItemTableData(structId, fullViewItemId.Value);
                        else
                            if (fullViewItemId == null && parentItemId != null)
                                result = repo.GetAdditionalSetsItemData(structId, parentItemId.Value);
                }
                catch (Exception e)
                {
                    result = new List<Dictionary<string, object>>();

                    var _t = new Dictionary<string,object>();
                    _t.Add("Error", e);

                    result.Add(_t);
                }
            }

            return result;
        }

        internal static int LoadElementsCount(Guid verId, Guid structId, string passportId = null, string filterQuery = null)
        {
            int result = -1;
            Exception _e = null;
            using (var ctx = new DataContext())
            {
                try
                {
                    var repo = RepositoryFactory.Get<ISetDataRepository>(ctx);
                    result = repo.GetElementsCount(verId, structId, passportId, filterQuery);
                }
                catch (Exception e)
                {
                    _e = e;
                }
            }

            if (_e!=null) throw _e;

            return result;
        }

        internal static object SetRating(string id, string structId, string versId, int value, string ip)
        {
            object result = null;
            using (var ctx = new DataContext())
            {
                try
                {
                    ISetPassportRepository repo = RepositoryFactory.Get<ISetPassportRepository>(ctx);
                    var passport = repo.FirstOrDefault(x => x.Identifier == id && x.SetVersion.CodeName == versId && x.SetVersion.DataStructure.CodeName == structId);
                    if (passport.IsOK && passport.Result != null)
                    {
                        RepositoryOperationResult<bool> res = repo.AddPassportRating(passport.Result.Id, value, ip);
                        if (res.IsOK)
                        {
                            result = new { IsOk = true, value = value };
                            return result;
                        }
                    }
                }
                catch 
                {  

                }
            }

            result = new { IsOk = false };
            return result;
        }
    }
}