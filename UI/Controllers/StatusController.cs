﻿using System.Web.Mvc;
using UI.Factories;
using UI.Models;

namespace UI.Controllers
{
    public class StatusController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            StatusVM vm = StatusVMFactory.GetIndexVM();
            return View("Status", vm);
        }
    }
}
