﻿using Common;
using Repository.Entities;
using System;
using System.Linq.Expressions;
using System.Text;
using System.Linq;
using System.Web.Mvc;
using UI.Factories;
using System.Collections.Generic;

namespace UI.Controllers
{
    public class RegistryController : Controller
    {
        const string filter = "filterRegistry";
        const string filter_searchPhrase = filter + "_searchParhase";
        const string filter_AgencyId = filter + "_agencyId";
        const string filter_CategoryId = filter + "_categoryId";

        [HttpGet]
        public ActionResult Index()
        {
            var vm = RegistryVMFactory.GetIndexPage((Expression<Func<SetVersion, bool>>)Session[filter]);

            vm.SearchPhrase = Session[filter_searchPhrase] != null ? (string)Session[filter_searchPhrase] : null;
            vm.SelectedAgencyId = Session[filter_AgencyId] != null ? (Guid)Session[filter_AgencyId] : Guid.Empty;
            vm.SelectedCategoryId = Session[filter_CategoryId] != null ? (Guid)Session[filter_CategoryId] : Guid.Empty;

            return View("Registry", vm);

        }

        [HttpGet]
        [AjaxRequest]
        public JsonResult Load(int page = 1)
        {
            var data = RegistryVMFactory.Load(page, (Expression<Func<SetVersion, bool>>)Session[filter]);
            return Json(data, "text/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AjaxRequest]
        public void Filter(string searchParhase = null, Guid? agencyId = null, Guid? categoryId = null)
        {
            Expression<Func<SetVersion, bool>> searchParam = x => x.IsAproved && x.IsCurrent;

            // TODO: Переделать этот говнокод!!!! Запросы можно как-то строить динамически
            #region
            var a = !string.IsNullOrEmpty(searchParhase);
            var b = agencyId.HasValue && agencyId.Value != Guid.Empty;
            var c = categoryId.HasValue && categoryId.Value != Guid.Empty;

            if (!a && !b && c)
                searchParam = x => x.IsAproved && x.IsCurrent && x.Set.Category_Id == categoryId;
            else
                if (!a && b && !c)
                    searchParam = x => x.IsAproved && x.IsCurrent && x.SetsPassports.Any(y => y.Agency_Id == agencyId);
                else
                    if (!a && b && c)
                        searchParam = x => x.IsAproved && x.IsCurrent && x.SetsPassports.Any(y => y.Agency_Id == agencyId) && x.Set.Category_Id == categoryId;
                    else
                        if (a && !b && !c)
                            searchParam = x => x.IsAproved && x.IsCurrent && x.Set.Title.Contains(searchParhase);
                        else
                            if (a && !b && c)
                                searchParam = x => x.IsAproved && x.IsCurrent && x.Set.Title.Contains(searchParhase) && x.Set.Category_Id == categoryId;
                            else
                                if (a && b && !c)
                                    searchParam = x => x.IsAproved && x.IsCurrent && x.Set.Title.Contains(searchParhase) && x.SetsPassports.Any(y => y.Agency_Id == agencyId);
                                else
                                    if (a && b && c)
                                        searchParam = x => x.IsAproved && x.IsCurrent && x.Set.Title.Contains(searchParhase) && x.SetsPassports.Any(y => y.Agency_Id == agencyId) && x.Set.Category_Id == categoryId;

            #endregion


            Session[filter] = searchParam;
            Session[filter_searchPhrase] = searchParhase;
            Session[filter_AgencyId] = agencyId;
            Session[filter_CategoryId] = categoryId;
        }
    }
}
