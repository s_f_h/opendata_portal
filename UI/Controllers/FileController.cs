﻿using Repository.Misc;
using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using UI.Factories;

namespace UI.Controllers
{
    public class FileController : Controller
    {
        [HttpGet]
        public ActionResult List(string format)
        {
            Formats _format;
            string mimeType;
            string fname;

            switch (format)
            {
                case ".xml":
                case "xml":
                    _format = Formats.xml;
                    mimeType = "text/xml";
                    fname = "list.xml";
                    break;
                case "-schema.xsd":
                    _format = Formats.xsd;
                    mimeType = "text/xsd";
                    fname = "list-schema.xsd";
                    break;
                case ".csv":
                case "csv":
                    _format = Formats.csv;
                    mimeType = "text/csv";
                    fname = "list.csv";
                    break;
                default:
                    return new HttpNotFoundResult();
            }

            string url_prefix = string.Format("{0}://{1}/opendata", HttpContext.Request.Url.Scheme, HttpContext.Request.Url.Authority);
            byte[] fileStream = FileVMFactory.GetList(url_prefix, _format, Server.MapPath);

            return File(fileStream, mimeType, fname);
        }


        [HttpGet]
        public ActionResult Meta(string setId, string version = null, string format = "xml")
        {
            Formats _format;
            string mimeType;
            string fname;


            switch (format)
            {
                case ".xml":
                case "xml":
                    _format = Formats.xml;
                    mimeType = "text/xml";
                    fname = "meta.xml";
                    break;
                case "-schema.xsd":
                    _format = Formats.xsd;
                    mimeType = "text/xsd";
                    fname = "meta-schema.xsd";
                    break;
                case ".csv":
                case "csv":
                    _format = Formats.csv;
                    mimeType = "text/csv";
                    fname = "meta.csv";
                    break;
                default:
                    return new HttpNotFoundResult();
            }

            string url_prefix = string.Format("{0}://{1}/opendata", HttpContext.Request.Url.Scheme, HttpContext.Request.Url.Authority);
            byte[] fileStream = FileVMFactory.GetMeta(setId, version, url_prefix, _format, Server.MapPath);

            if (fileStream == null || fileStream.Length == 0)
                return new HttpNotFoundResult();

            return File(fileStream, mimeType, fname);
        }


        [HttpGet]
        public ActionResult Data(string setId, string versId, string structId, string format = "xml")
        {
            Formats _format;
            string mimeType;
            string fname;


            switch (format)
            {
                case "xml":
                case ".xml":
                    _format = Formats.xml;
                    mimeType = "text/xml";
                    fname = string.Format("data-{0}-structure-{1}.xml", versId, structId);
                    break;
                case "-schema.xsd": case ".xsd":
                    return Structure(setId, structId, format);
                case "csv":
                case ".csv":
                    _format = Formats.csv;
                    mimeType = "text/csv";
                    fname = string.Format("data-{0}-structure-{1}.csv", versId, structId);
                    break;
                default:
                    return new HttpNotFoundResult();
            }


            byte[] fileStream = FileVMFactory.GetData(setId, versId, structId, _format, Server.MapPath);
            return File(fileStream, mimeType, fname);
        }

        [HttpGet]
        public ActionResult Structure(string setId, string structId, string format = "xml")
        {
            Formats _format;
            string mimeType;
            string fname;


            switch (format)
            {
                case "xml":
                case ".xml":
                    _format = Formats.xml;
                    mimeType = "text/xml";
                    fname = string.Format("structure-{0}.xml", structId);
                    break;
                case "xsd":
                case ".xsd":
                    _format = Formats.xml;
                    mimeType = "text/xsd";
                    fname = string.Format("structure-{0}.xsd", structId);
                    break;
                case "csv":
                case ".csv":
                    _format = Formats.csv;
                    mimeType = "text/csv";
                    fname = string.Format("structure-{0}.csv", structId);
                    break;
                default:
                    return new HttpNotFoundResult();
            }


            byte[] fileStream = FileVMFactory.GetStructure(setId, structId, _format, Server.MapPath);
            return File(fileStream, mimeType, fname);
        }

        [HttpGet]
        public ActionResult SetVersionStructure(Guid setVerId, string format)
        {
            Formats _format;
            string mimeType;
            string fname;


            switch (format)
            {
                case "xml":
                case ".xml":
                    _format = Formats.xml;
                    mimeType = "text/xml";
                    fname = "structure.xml";
                    break;
                case "xsd":
                case ".xsd":
                    _format = Formats.xml;
                    mimeType = "text/xsd";
                    fname = "structure.xsd";
                    break;
                case "csv":
                case ".csv":
                    _format = Formats.csv;
                    mimeType = "text/csv";
                    fname = "structure.csv";
                    break;
                default:
                    return new HttpNotFoundResult();
            }


            byte[] fileStream = FileVMFactory.GetStructure(setVerId, _format, Server.MapPath);
            return File(fileStream, mimeType, fname);
        }

        [HttpGet]
        public ActionResult SetVersionData(Guid setVerId, string format)
        {
            Formats _format;
            string mimeType;
            string fname;


            switch (format)
            {
                case "xml":
                case ".xml":
                    _format = Formats.xml;
                    mimeType = "text/xml";
                    fname = "data.xml";
                    break;
                case "-schema.xsd":
                case ".xsd":
                    return SetVersionStructure(setVerId, format);
                case "csv":
                case ".csv":
                    _format = Formats.csv;
                    mimeType = "text/csv";
                    fname = "data";
                    break;
                default:
                    return new HttpNotFoundResult();
            }


            byte[] fileStream = FileVMFactory.GetData(setVerId, _format, Server.MapPath);
            return File(fileStream, mimeType, fname);
        }
    }
}
