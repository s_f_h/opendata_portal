﻿using Common;
using System;
using System.Text;
using System.Web.Mvc;
using UI.Factories;

namespace UI.Controllers
{
    public class AgencyController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var vm = AgencyCategoryVMFactory.GetAgencyVM();
            return View("Classifires", vm);
        }

        [HttpGet]
        public ActionResult Sets(Guid id, int rating = 0)
        {
            var vm = AgencyCategoryVMFactory.GetAgencySetsVM(id, rating);
            return View(vm);
        }

        [HttpGet]
        [AjaxRequest]
        public JsonResult Load(Guid id, int page = 1, int rating = 0)
        {
            var data = AgencyCategoryVMFactory.LoadAgencySets(id, page, rating);
            return Json(data, "text/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
    }
}
