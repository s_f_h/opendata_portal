﻿using Common;
using Repository.Entities;
using Repository.Misc;
using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using UI.Factories;
using UI.Models;

namespace UI.Controllers
{
    public class SetController : Controller
    {
        [HttpGet]
        public ActionResult View(Guid? setVer = null, string passportId = null, string versId = null, string structId = null)
        {
            var categorySet = setVer.HasValue && setVer.Value != Guid.Empty;
            var agencySet = !string.IsNullOrWhiteSpace(passportId);

            if (!(categorySet ^ agencySet)) return new HttpNotFoundResult();


            // no need to store filters data if we just load this page or reload it
            Session.Clear();

            var userIP = Request.UserHostAddress;

            SetViewVM vm;

            if (categorySet)
                vm = SetVMFactory.CreateSetViewVM(setVer.Value, userIP);
            else
                vm = SetVMFactory.CreateSetViewVM(passportId, versId, structId, userIP);

            return View(vm);
        }

        [HttpGet]
        public JsonResult LoadTemplate(Guid templId)
        {
            var vm = SetVMFactory.LoadTemplate(templId);
            return Json(vm, "text/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult LoadStructure(Guid structId)
        {
            var vm = SetVMFactory.LoadStructure(structId);
            return Json(vm, "text/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult LoadData(Guid verId, Guid structId, string passportId = null, int page = 1,
            Guid? fullViewItemId = null, Guid? parentItemId = null)
        {
            // fullViewItemId == null && parentItemId == null means what we load simple table data
            // fullViewItemId != null && parentItemId == null means what we load fullview item
            // fullViewItemId == null && parentItemId != null means what we load additional set items of fullview item
            string filterQuery = (string)Session[structId.ToString()];

            var vm = SetVMFactory.LoadData(verId, structId, passportId, page, filterQuery, fullViewItemId, parentItemId);
            return Json(vm, "text/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxRequest]
        public JsonResult LoadElementsCount(Guid verId, Guid structId, string passportId = null)
        {
            string filterQuery = (string)Session[structId.ToString()];

            try
            {
                int result = SetVMFactory.LoadElementsCount(verId, structId, passportId, filterQuery);
                return Json(new { count = result, itemsPerPage = Configuration.Base.ItemsPerPage }, "text/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { IsOk = false, reason = e }, "text/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        [AjaxRequest]
        public void ProcessFilterQuery(string structId)
        {
            // TODO: Compleate
            StringBuilder filterQuery = new StringBuilder();
            int keysCount = Request.Form.AllKeys.Count(x => x.Contains("_type"));
            foreach (var key in Request.Form.AllKeys.Where(x => x.Contains("_type")))
            {
                keysCount--;
                var type = Request.Form[key];
                var field = key.Replace("_type", "");
                var val = Request.Form[field];

                if (string.IsNullOrWhiteSpace(val)) continue;

                int type_val = -1;
                if (int.TryParse(type, out type_val) && type_val > -1)
                {
                    var _type = (ColumnDisplayTemplate.ContentType)type_val;
                    var queryItem = _type.CreateSqlQueryFilterItem(field, val);
                    if (!string.IsNullOrWhiteSpace(queryItem))
                    {
                        if (keysCount > 0)
                            filterQuery.AppendFormat("{0} AND ", queryItem);
                        else
                            filterQuery.Append(queryItem);
                    }
                }
            }

            var query = filterQuery.ToString();
            if (query.EndsWith("AND ", StringComparison.InvariantCultureIgnoreCase))
                query = query.Remove(query.Length - 4);


            if (string.IsNullOrWhiteSpace(query))
                Session.Remove(structId);
            else
                Session[structId] = query;
        }

        [HttpPost]
        [AjaxRequest]
        public JsonResult SetRating(string id, string structId, string versId, int value)
        {
            var userIP = Request.UserHostAddress;
            var vm = SetVMFactory.SetRating(id, structId, versId, value, userIP);
            return Json(vm, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
    }
}
