﻿using Common;
using Repository;
using Repository.Abstract;
using System.Web.Mvc;
using System.Net.Mail;
using System;
using UI.Models;
using System.Text;
using System.Net;

namespace UI.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Category");
        }

        [AjaxRequest]
        [HttpPost]
        //[PreventSpam]
        public JsonResult SendFeedback(FeedbackVM vm)
        {
            object result = null;
            if (ModelState.IsValid)
            {
                var fromAddress = new MailAddress(Configuration.SuppurtInformation.SMTPFrom, "OpenData Krasnodar");
                var toAddress = new MailAddress(Configuration.SuppurtInformation.EMail);
                string fromPassword = Configuration.SuppurtInformation.SMTPPassword;
                string subject = vm.Theme;
                string body = string.Format("От: {0}({1});{2}Тема: {3};{2}{2}{4}", vm.personName, vm.email, Environment.NewLine, vm.Theme, vm.message);

                var smtp = new SmtpClient
                {
                    Host = Configuration.SuppurtInformation.SMTPHost,
                    Port = Configuration.SuppurtInformation.SMTPPort,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
            }

            return Json(result, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult API()
        {
            return View();
        }

        [HttpGet]
        public ActionResult About()
        {
            return View(new BaseVM());
        }

        [HttpGet]
        public ActionResult Developers()
        {
            return View();
        }

        [HttpGet]
        public ActionResult News()
        {
            return View();
        }

        [HttpGet]
        public ActionResult FAQ()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Terms()
        {
            return View(new BaseVM());
        }
    }
}
