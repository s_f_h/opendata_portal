﻿using Common;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.RouteExistingFiles = true;

            routes.MapRoute(
                name: "default",
                url: "",
                defaults: new { controller = "Category", action = "Index" },
                namespaces: new[] { "UI.Controllers" }
            );


            #region Ссылки на скачиваемые файлы
            /* 
             * ссылка на реестр наборов для скачивания должна быть представлена в виде 
             *      ~/list.<format>
             * например,
             *      ~/list.xml
             */
            routes.MapRoute(
                name: "download_list",
                url: "list.{format}",
                defaults: new { controller = "File", action = "List", format = "xml" },
                namespaces: new[] { "UI.Controllers" }
            );

            /*
             * ссылка на пасспорт набора для скачивания должна быть представлена в виде 
             *      ~/<identifier>/meta.<format>
             * например,
             *      ~/1234567890-setNumOne/meta.xml
             */
            routes.MapRoute(
                name: "download_meta",
                url: "{setId}/meta.{format}",
                defaults: new { controller = "File", action = "Meta", format = "xml" },
                constraints: new { setId = Constants.RegExps.SetIdentifier },
                namespaces: new[] { "UI.Controllers" }
            );

            /*
              * ссылка на данные набора для скачивания должна быть представлена в виде 
              *      ~/<identifier>/data-<versionCode>-structure-<structureCode>.<format>
             *      или
             *      ~/<setVersionId>/data.<format>
              * например,
              *      ~/1234567890-setNumOne/data-20161212-structure-20051212.xml
             *      или
             *      ~/4aef15e8-4053-e611-beb6-0026180bf5cb/data.xml
              */
            routes.MapRoute(
                name: "download_data",
                url: "{setId}/data-{versId}-structure-{structId}.{format}",
                defaults: new { controller = "File", action = "Data", format = "xml" },
                constraints: new { setId = Constants.RegExps.SetIdentifier, versId = Constants.RegExps.CodeName, structId = Constants.RegExps.CodeName },
                namespaces: new[] { "UI.Controllers" }
            );

            routes.MapRoute(
                name: "download_data_for_version",
                url: "{setVerId}/data.{format}",
                defaults: new { controller = "File", action = "SetVersionData", format = "xml" },
                constraints: new { setVerId = Constants.RegExps.Guid },
                namespaces: new[] { "UI.Controllers" }
            );

            /*
              * ссылка на структуру данных набора для скачивания должна быть представлена в виде 
              *      ~/<identifier>/structure-<structureCode>.<format>
             *      или
             *      ~/<setVersionId>/structure.<format>
              * например,
              *      ~/1234567890-setNumOne/structure-20051212.xml
             *      или
             *      ~/4aef15e8-4053-e611-beb6-0026180bf5cb/structure.xml
              */
            routes.MapRoute(
                name: "download_struct",
                url: "{setId}/structure-{structId}.{format}",
                defaults: new { controller = "File", action = "Structure", format = "xml" },
                constraints: new { setId = Constants.RegExps.SetIdentifier, structId = Constants.RegExps.CodeName },
                namespaces: new[] { "UI.Controllers" }
            );

            routes.MapRoute(
                name: "download_struct_for_version",
                url: "{setVerId}/structure.{format}",
                defaults: new { controller = "File", action = "SetVersionStructure", format = "xml" },
                constraints: new { setVerId = Constants.RegExps.Guid },
                namespaces: new[] { "UI.Controllers" }
            );

            #endregion

            #region Ссылки в Footer
            routes.MapRoute(
                name: "about_page",
                url: "About",
                defaults: new { controller = "Home", action = "About" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "developers_page",
                url: "Developers",
                defaults: new { controller = "Home", action = "Developers" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "api_page",
                url: "API",
                defaults: new { controller = "Home", action = "API" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "terms_page",
                url: "Terms",
                defaults: new { controller = "Home", action = "Terms" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "news_page",
                url: "News",
                defaults: new { controller = "Home", action = "News" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "faq_page",
                url: "FAQ",
                defaults: new { controller = "Home", action = "FAQ" },
                namespaces: new[] { "UI.Controllers" });
            #endregion

            #region Ссылки на список наборов в классификаторе
            routes.MapRoute(
                name: "category_sets",
                url: "Category/{id}",
                defaults: new { controller = "Category", action = "Sets", id = UrlParameter.Optional },
                constraints: new { id = Constants.RegExps.Guid },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "agency_sets",
                url: "Agency/{id}",
                defaults: new { controller = "Agency", action = "Sets", id = UrlParameter.Optional },
                constraints: new { id = Constants.RegExps.Guid },
                namespaces: new[] { "UI.Controllers" });
            #endregion

            #region Ссылки на наборы
            routes.MapRoute(
                name: "view_set",
                url: "{setVer}",
                defaults: new { controller = "Set", action = "View", setVer = UrlParameter.Optional },
                constraints: new { setVer = Constants.RegExps.Guid },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "view_agency_set",
                url: "{passportId}",
                defaults: new { controller = "Set", action = "View" },
                constraints: new { passportId = Constants.RegExps.SetIdentifier },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "view_agency_set_version",
                url: "{passportId}/data-{versId}-structure-{structId}",
                defaults: new { controller = "Set", action = "View" },
                constraints: new { passportId = Constants.RegExps.SetIdentifier, versId = Constants.RegExps.CodeName, structId = Constants.RegExps.CodeName },
                namespaces: new[] { "UI.Controllers" });

            #endregion

            routes.MapRoute(
                name: "standart",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "UI.Controllers" }
            );
        }
    }
}