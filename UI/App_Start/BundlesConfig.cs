﻿using System.Web.Optimization;

namespace UI
{
    public class BundlesConfig
    {
        public static void Register(BundleCollection bundles)
        {
            #region ScriptBundles
            bundles.Add(new ScriptBundle("~/scripts/main")
                .Include("~/Scripts/jquery-2.1.4.js")
                .Include("~/Scripts/app/bootstrap.js")
                .Include("~/Scripts/app/domEvents.js")
                .Include("~/Scripts/app/hoverIntent.js")
                .Include("~/Scripts/app/superfish.min.js")
                .Include("~/Scripts/app/site.js"));


            bundles.Add(new ScriptBundle("~/scripts/plugins/qrcode")
                .Include("~/Scripts/app/qrCodeGenerator.js")
                .Include("~/Scripts/plugins/qrcode/jquery.qrcode.js")
                .Include("~/Scripts/plugins/qrcode/qrcode.js"));

            bundles.Add(new ScriptBundle("~/scripts/plugins/tabs")
                .Include("~/Scripts/app/tabs.js")
                .Include("~/Scripts/jquery-ui-1.11.4.js"));

            bundles.Add(new ScriptBundle("~/scripts/plugins/rateit")
                .Include("~/Scripts/plugins/rateit/jquery.rateit.js")
                .Include("~/Scripts/jquery-ui-1.11.4.js")
                .Include("~/Scripts/app/RatingWidget.js"));

            bundles.Add(new ScriptBundle("~/scripts/plugins/pager")
                .Include("~/Scripts/app/PagerWidget.js"));
            #endregion


            #region StyleBundles
            bundles.Add(new StyleBundle("~/styles/main")
                .Include("~/Content/Stylesheets/reset.css")
                .Include("~/Content/Stylesheets/screen.css")
                .Include("~/Content/Stylesheets/colorbox.css"));

            bundles.Add(new StyleBundle("~/styles/jquery-ui")
                .Include("~/Content/Stylesheets/jquery-ui.css")
                .Include("~/Content/Stylesheets/jquery-ui.structure.css")
                .Include("~/Content/Stylesheets/jquery-ui.theme.css"));


            bundles.Add(new StyleBundle("~/styles/plugin/rateit")
                .Include("~/Content/Stylesheets/plugins/rateit/rateit.css"));
            #endregion
            


            

        }
    }
}