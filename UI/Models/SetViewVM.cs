﻿using Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class SetViewVM : BaseVM
    {
        public IEnumerable<SetPassportVM> SetPassports { get; set; }
        public object Id { get; set; }
        public Guid VersionId { get; set; }
        public bool IsEmpty { get; set; }


        public DownloadButtonVM DownloadButtonVM { get; set; }
        [Display(Name="Рейтин набора:")]
        public RatingWidgetVM RatingWidgetVM { get; set; }
        public SetVersionsInfoVM SetVersionsInfoVM { get; set; }

        public Guid DataStructureId { get; set; }

        public class SetViewParams
        {
            public SetViewParams(object id, Guid verId, Guid structId)
            {
                this.Id = id;
                this.VersionId = verId;
                this.DataStructureId = structId;
            }
            public object Id { get; set; }
            public Guid VersionId { get; set; }
            public Guid DataStructureId { get; set; }
        }
    }
}