﻿using System;
using System.Collections.Generic;

namespace UI.Models
{
    public class SetVersionsInfoVM
    {
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }

        public bool IsCurrent { get; set; }
        public bool IsSelected { get; set; }

        public object Id { get; set; }

        public string VersId { get; set; }
        public string StructId{ get; set; }

        public IEnumerable<SetVersionsInfoVM> SubVersions { get; set; }
    }
}