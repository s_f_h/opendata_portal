﻿using Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class SetPassportVM
    {
        public SetPassportVM(SetPassport item)
        {
            this.Creator = item.Creator;
            this.Created = item.Created;
            this.Description = item.Description;
            this.Identifier = item.Identifier;
            this.MethodicStandartVersion = item.MethodicStandartVersion;
            this.Modified = item.Modified;
            this.PublisherMBox = item.PublisherMBox;
            this.PublisherName = item.PublisherName;
            this.PublisherPhone = item.PublisherPhone;
            this.StructCodeName = item.SetVersion.DataStructure.CodeName;
            this.Subject = item.Subject;
            this.Valid = item.Valid;
            this.VersCodeName = item.SetVersion.CodeName;
            this.Title = item.Title;
        }

        public string Title { get; private set; }
        public string Creator { get; private set; }
        public string MethodicStandartVersion { get; private set; }
        public string Identifier { get; private set; }
        public string VersCodeName { get; private set; }
        public string StructCodeName { get; private set; }
        public string Description { get; private set; }
        public string PublisherName { get; private set; }
        [DataType(DataType.PhoneNumber)]
        public string PublisherPhone { get; private set; }
        [DataType(DataType.EmailAddress)]
        public string PublisherMBox { get; set; }
        [DataType(DataType.Date)]
        public DateTime Created { get; private set; }
        [DataType(DataType.Date)]
        public DateTime Modified { get; private set; }
        public string Subject { get; private set; }
        [DataType(DataType.Date)]
        public DateTime? Valid { get; private set; }
    }
}