﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class DownloadButtonVM
    {
        public bool IsAgency { get; set; }
        public object Id { get; set; }
        public string VersionName { get; set; }
        public string VersionId { get; set; }
        public string StructId { get; set; }
        public int Downloaded { get; set; }

        private Buttons _avaliableButtons = Buttons.All;

        public Buttons AvaliableButtons { get; set; }

        public enum Buttons
        {
            Meta = 0x1,
            Structure=0x2,
            Data = 0x4,
            All = Meta | Data | Structure
        }
    }
}