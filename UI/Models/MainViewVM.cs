﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class MainViewVM : BaseVM
    {
        static Holder<KeyValuePair<string, string>> _topbarVM = new Holder<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("Категории", "Category"),
            new KeyValuePair<string, string>("Поставщики", "Agency"),
            new KeyValuePair<string, string>("Реестр", "Registry"),
            new KeyValuePair<string, string>("Состояние", "Status"),
        };

        public MainViewVM()
        {
            TopbarVM = _topbarVM;
        }

        public Holder<KeyValuePair<string, string>> TopbarVM { get; private set; }

        public int SetsTotal { get; set; }
    }
}