﻿using Repository.Entities.Base;

namespace UI.Models
{
    public class SetsVM : MainViewVM
    {
        public Holder<AgencyCategoryBase> SidebarVM { get; set; }
        public RatingWidgetVM RatingWidgetVM { get; set; }
    }
}