﻿using Repository.Entities;
using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class RegistryVM : MainViewVM
    {
        public string SearchPhrase { get; set; }
        public IEnumerable<AgencyCategoryBase> Agencies;
        public Guid SelectedAgencyId { get; set; }

        public IEnumerable<AgencyCategoryBase> Categories;
        public Guid SelectedCategoryId { get; set; }
    }
}