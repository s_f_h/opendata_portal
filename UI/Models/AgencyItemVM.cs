﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class AgencyItemVM
    {
        public Guid AgencyId { get; set; }
        public string SetTitle { get; set; }
        public string Name { get; set; }
        public string SetId { get; set; }
    }
}