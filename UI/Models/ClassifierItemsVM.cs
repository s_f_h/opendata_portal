﻿using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class ClassifierItemsVM : MainViewVM
    {
        public Holder<AgencyCategoryBase> Items { get; set; }
    }
}