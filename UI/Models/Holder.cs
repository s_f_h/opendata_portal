﻿using System.Collections.Generic;

namespace UI.Models
{
    public class Holder<T> : List<T>
    {
        public Holder() : base() { }
        public Holder(IEnumerable<T> collection) : base(collection) { }
        public Holder(int capacity) : base(capacity) { }


        public T Current { get; set; }
    }
}