﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class StatusVM : MainViewVM
    {
        public IEnumerable<RegistryOrStatusItemVM> AddedItems { get; set; }
        public IEnumerable<RegistryOrStatusItemVM> ModifiedItems { get; set; }
    }
}