﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class FeedbackVM
    {
        public string feedbackTheme { get; set; }
        public string email { get; set; }
        public string personName { get; set; }
        public string message { get; set; }

        string _theme;

        public string Theme
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_theme)) return _theme;

                switch (feedbackTheme)
                {
                    case "create_new_dataset":
                        _theme = "Создание набора данных";
                        break;
                    case "error_in_dataset":
                        _theme = "Ошибка в наборе данных";
                        break;
                    case "error_in_od":
                        _theme = "Ошибка в работе портала";
                        break;
                    default:
                        _theme = "Другое";
                        break;
                }

                return _theme;
            }
        }
    }
}