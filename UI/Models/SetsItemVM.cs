﻿using System.Collections.Generic;

namespace UI.Models
{
    public class SetsItemVM
    {
        public object Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StructId { get; set; }
        public string VersId { get; set; }


        public string VerName { get; set; }

        public List<AgencyItemVM> Agencies { get; set; }

        public string Category { get; set; }

        public int Viewed { get; set; }
        public double AverageRating { get; set; }
    }
}