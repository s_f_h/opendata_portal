﻿namespace UI.Models
{
    public class RatingWidgetVM
    {
        public double Value;
        public bool IsReadonly;
    }
}