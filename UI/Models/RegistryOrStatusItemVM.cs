﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class RegistryOrStatusItemVM
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? ActualTo { get; set; }

        public IEnumerable<AgencyItemVM> Agencies { get; set; }

        
    }
}