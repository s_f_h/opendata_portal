﻿using System.Collections.Generic;

namespace UI.Models
{
    public class BaseVM
    {
        static Holder<KeyValuePair<string, string>> _externalLinks = new Holder<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("Открытые данные", "Index"),
                new KeyValuePair<string, string>("О портале", "About"),
                //new KeyValuePair<string, string>("Поставщикам", "Agencies"),
                //new KeyValuePair<string, string>("Разработчикам", "Developers"),
                //new KeyValuePair<string, string>("API", "API"),
                new KeyValuePair<string, string>("Условия использования", "Terms"),
                //new KeyValuePair<string, string>("Поиск", "Search"),
                //new KeyValuePair<string, string>("Новости","News"),
                //new KeyValuePair<string, string>("Часто задаваемые вопросы","FAQ"),
        };

        public BaseVM()
        {
            ExternalLinks = _externalLinks;
            FeedBackVM = new FeedbackVM();
        }

        public Holder<KeyValuePair<string, string>> ExternalLinks { get; private set; }
        public FeedbackVM FeedBackVM { get; set; }

        public bool HasErrors { get; set; }

        public string Title { get; set; }
        public bool AddFront { get; set; }
    }
}