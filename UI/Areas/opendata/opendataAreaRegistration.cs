﻿using Common;
using System;
using System.Web.Mvc;

namespace UI.Areas.opendata
{
    public class opendataAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "opendata";
            }
        }

        public override void RegisterArea(AreaRegistrationContext routes)
        {
            routes.MapRoute(
                name: "opendata_default",
                url: "opendata",
                defaults: new { controller = "Category", action = "Index" },
                namespaces: new[] { "UI.Controllers" }
            );

            //TODO: Заменить на маршрутизацию статических файлов на стороне сервера
            #region Ссылки на скачиваемые файлы
            /* 
             * ссылка на реестр наборов для скачивания должна быть представлена в виде 
             *      ~/list.<format>
             * например,
             *      ~/list.xml
             */
            routes.MapRoute(
                name: "opendata_download_list",
                url: "opendata/list{format}",
                defaults: new { controller = "File", action = "List", format = "xml" },
                namespaces: new[] { "UI.Controllers" }
            );

            /*
             * ссылка на пасспорт набора для скачивания должна быть представлена в виде 
             *      ~/<identifier>/meta.<format>
             * например,
             *      ~/1234567890-setNumOne/meta.xml
             */
            routes.MapRoute(
                name: "opendata_download_meta",
                url: "opendata/{setId}/meta{format}",
                defaults: new { controller = "File", action = "Meta", format = "xml" },
                constraints: new { setId = Constants.RegExps.SetIdentifier },
                namespaces: new[] { "UI.Controllers" }
            );


            routes.MapRoute(
                name: "opendata_download_data",
                url: "opendata/{setId}/data-{versId}-structure-{structId}.{format}",
                defaults: new { controller = "File", action = "Data", format = "xml" },
                constraints: new { setId = Constants.RegExps.SetIdentifier, versId = Constants.RegExps.CodeName, structId = Constants.RegExps.CodeName },
                namespaces: new[] { "UI.Controllers" }
            );

            routes.MapRoute(
                name: "opendata_download_struct",
                url: "opendata/{setId}/structure-{structId}.{format}",
                defaults: new { controller = "File", action = "Structure", format = "xml" },
                constraints: new { setId = Constants.RegExps.SetIdentifier, structId = Constants.RegExps.CodeName },
                namespaces: new[] { "UI.Controllers" }
            );

            #endregion

            #region Ссылки в Footer
            routes.MapRoute(
                name: "opendata_about_page",
                url: "opendata/About",
                defaults: new { controller = "Home", action = "About" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "opendata_developers_page",
                url: "opendata/Developers",
                defaults: new { controller = "Home", action = "Developers" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "opendata_api_page",
                url: "opendata/API",
                defaults: new { controller = "Home", action = "API" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "opendata_terms_page",
                url: "opendata/Terms",
                defaults: new { controller = "Home", action = "Terms" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "opendata_news_page",
                url: "opendata/News",
                defaults: new { controller = "Home", action = "News" },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "opendata_faq_page",
                url: "opendata/FAQ",
                defaults: new { controller = "Home", action = "FAQ" },
                namespaces: new[] { "UI.Controllers" });
            #endregion

            #region Ссылки на список наборов в классификаторе
            routes.MapRoute(
                name: "opendata_category_sets",
                url: "opendata/Category/{id}",
                defaults: new { controller = "Category", action = "Sets", id = UrlParameter.Optional },
                constraints: new { id = Constants.RegExps.Guid },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "opendata_agency_sets",
                url: "opendata/Agency/{id}",
                defaults: new { controller = "Agency", action = "Sets", id = UrlParameter.Optional },
                constraints: new { id = Constants.RegExps.Guid },
                namespaces: new[] { "UI.Controllers" });
            #endregion

            #region Ссылки на наборы
            routes.MapRoute(
                name: "opendata_view_set",
                url: "opendata/{setVer}",
                defaults: new { controller = "Set", action = "View", setVer = UrlParameter.Optional, passportId = (string)null, verName = (string)null },
                constraints: new { setVer = Constants.RegExps.Guid },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "opendata_view_agency_set",
                url: "opendata/{passportId}",
                defaults: new { controller = "Set", action = "View" },
                constraints: new { passportId = Constants.RegExps.SetIdentifier },
                namespaces: new[] { "UI.Controllers" });

            routes.MapRoute(
                name: "opendata_view_agency_set_version",
                url: "opendata/{passportId}/data-{versId}-structure-{structId}",
                defaults: new { controller = "Set", action = "View" },
                constraints: new { passportId = Constants.RegExps.SetIdentifier, versId = Constants.RegExps.CodeName, structId = Constants.RegExps.CodeName },
                namespaces: new[] { "UI.Controllers" });

            //routes.MapRoute(
            //    name: "opendata_view_agency_set_version",
            //    url: "opendata/{passportId}-{verName}",
            //    defaults: new { controller = "Set", action = "View", setVer = (Guid?)null, passportId = UrlParameter.Optional, verName = UrlParameter.Optional },
            //    constraints: new { passportId = Constants.RegExps.SetIdentifier, verName = Constants.RegExps.VersionName },
            //    namespaces: new[] { "UI.Controllers" });

            #endregion

            routes.MapRoute(
                name: "opendata_standart",
                url: "opendata/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "UI.Controllers" }
            );
        }
    }
}
