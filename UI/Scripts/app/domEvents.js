﻿'use strict'

var oDataKrdReg = {};
oDataKrdReg.domEvents = {
    init: function () {
        // показываем подсказку
        $('.info_link').on('click', this.showTooltip);
        // закрываем окно подсказки
        $('.hide_block').on('click', this.hideTooltip);
        // ищем среди документов
        $('.b-search .button').on('click', this.searchForPhrase);
        // переключаемся между вкладками на главной странице
        //$('.front .b-power li a').on('click', this.switchTabsAtIndexPage);
        // показываем муниципальное образование
        $('.b-city > a').on('click', this.showMunicipalitiesList);
        // выбираем муниципальное образование на стартовой странице
        $('.selection li a').on('click', this.chooseMunicipality);
        // переходим к муниципальному образованию
        $('.active_selection li a').on('click', this.goToMunicipality);
        // скрываем муниципальное образование
        $('.selection .slideUp, .active_selection .slideUp').on('click', this.hideMunicipalitiesList);
        // переходим при выборе категории
        $('.menu_list a').on('click', this.showDocuments);
        // переключаемся между вкладками
        $('.module_title li a').on('click', this.switchTabs);
        // показываем окно с пояснениями api
        $('.api_btn').on('click', $.proxy(this.showApiPopup, this));
        $('.ind_api_btn').on('click', $.proxy(this.showIndApiPopup, this));
        // показываем окно обратной связи
        $('.b-feedback > a.lnk-feedback').on('click', this.showCallbackForm);
        // показываем окно обратной связи 2
        $('.js-feedback').on('click', this.showCallbackForm);
        // отправляем отзыв на сервер
        $('.popup_feedback input[type=submit]').on('click', $.proxy(this.sendFeedback, this));
        // закрываем окно обратно связи
        $('.popup_feedback .close_feedback').on('click', this.closeFeedbackForm);
        // скроллим вверх страницы
        $('.b-feedback > a.lnk-up').on('click', this.scrollToTop);
        // зыкрываем окно
        $('.haze').on('click', this.closeFeedbackForm);
    },
    showTooltip: function (event) {
        var text;
        $(this).parents('.b-info_top').find('.info_content_wrap').slideToggle(400);
        text = $(this).text();
        if (text === 'Свернуть') {
            $(this).text('Что такое «Портал отрытых данных»?');
        } else {
            $(this).text('Свернуть');
        }
        return false;
    },
    hideTooltip: function (event) {
        event.stopPropagation();
        event.preventDefault();
        $(this).parents('.b-info_top').slideUp(400);
        document.cookie = 'denyTooltip=1';
    },
    switchTabs: function (event) {
        var $buttonIndex, $parentsWrap
        event.stopPropagation();
        event.preventDefault();
        $parentsWrap = $(this).parents('.tabs_module');
        $buttonIndex = $(this).parent('li').index();
        $parentsWrap.find('.module_item').eq($buttonIndex).fadeIn(200).siblings().hide();
        $(this).parent('li').find('a').addClass('active_button');
        $(this).parent('li').siblings().find('a').removeClass('active_button');
    },
    showMunicipalitiesList: function (event) {
        var $element, $hideElement;
        event.stopPropagation();
        event.preventDefault();
        $element = $(this);
        $hideElement = $element.next('.selection, .active_selection');
        $hideElement.slideDown(300);
        $element.addClass('disable');
    },
    chooseMunicipality: function (event) {
        var $activeCity, $element, key, $selection, template, text;
        $element = $(this);
        text = $element.text();
        key = $element.data('key');
        $selection = $(this).parents('.selection');
        if ($selection.hasClass('first_click')) {
            $selection.removeClass('first_click');
            template = '<h2>Выбранное МО:</h2><div class="active_city active_button" ' + 'data-key=' + key + '>'
                       + text + '</div>';
            $selection.prev('a').text('Выбрать другое').before(template);
            $selection.find('.active').removeClass('active');
            $(this).addClass('active');
            $('.text .hide').removeClass('hide');
            $('.b-tips').hide();
        } else {
            $activeCity = $(this).parents('.b-city').find('.active_city');
            $activeCity.text(text);
            $activeCity.data('key', key);
            $selection.find('.active').removeClass('active');
            $(this).addClass('active');
        }
        return false;
    },
    hideMunicipalitiesList: function (event) {
        var $element, $parentElement;
        event.stopPropagation();
        event.preventDefault();
        $element = $(this);
        $parentElement = $element.parents('.selection, .active_selection');
        $parentElement.slideUp(300);
        $element.parents('.b-city').find('.disable').removeClass('disable');
    },
    showDocuments: function (event) {
        //var providerName, $activeMunicipality, $element, $selector;
        //event.stopPropagation();
        //event.preventDefault();
        //$element = $(this);
        //$selector = $('.b-power .active_power');
        //if ($selector.hasClass('municipalitySelector')) {
        //    $activeMunicipality = $('.active_city');
        //    window.location = 'http://' + window.location.host + '/municipality/'
        //                                + $activeMunicipality.data('key')
        //                                + '/category/' + $element.data('key');
        //}
        // else if ($selector.hasClass('agencySelector')) {
        //     window.location = 'http://' + window.location.host + '/agency/' + $element.data('key');
        //}

    },
    searchForPhrase: function (event) {
        var $element, $textElement, text;
        event.stopPropagation();
        event.preventDefault();
        $element = $(this);
        $textElement = $element.prev();
        text = $textElement.val();
        if (text !== '')
            window.location = 'http://' + window.location.host + '/Home/Search/' + text;
    },
    switchTabsAtIndexPage: function (event) {
        var buttonIndex, $parentsWrap;
        event.preventDefault();
        event.stopPropagation();
        if (!$(this).hasClass('active_power')) {
            $parentsWrap = $(this).parents('.b-power');
            buttonIndex = $(this).parent('li').index();
            $('.b-power_item').toggleClass('active');
            $(this).parent('li').find('a').addClass('active_power');
            $(this).parent('li').siblings().find('a').removeClass('active_power');
        }
        return false;
    },
    goToMunicipality: function (event) {
        var $activeCategory, $categoryKey, $element, $key;
        event.preventDefault();
        event.stopPropagation();
        $element = $(this);
        $key = $element.data('key');
        $activeCategory = $('.menu_list li.active a');
        $categoryKey = $activeCategory.data('key');
        if ($categoryKey && $key) {
            window.location = 'http://' + window.location.host + '/municipality/' + $key + '/category/' + $categoryKey;
        }
    },
    showApiPopup: function (event) {
        var id, $parentRow;
        event.preventDefault();
        event.stopPropagation();
        $parentRow = $(event.target).parents('tr');
        id = $parentRow.data('key');
        oDataKrdReg.domEvents.constructPopup(id);
    },
    showIndApiPopup: function (event) {
        var id, target;
        event.preventDefault();
        event.stopPropagation();
        target = $(event.target);
        if (target.prop('tagName') == 'A') {
            id = target.data('key');
        } else if (target.prop('tagName') == 'SPAN') {
            id = target.parent('a').data('key');
        }
        oDataKrdReg.domEvents.constructPopup(id);
    },
    constructPopup: function (id) {
        var $documentAnchor, $haze, href, $popup;
        if (!id) return;
        $haze = $('.haze');
        $popup = $('.popup_api_custom');
        $documentAnchor = $popup.find('.set a');

        try {
            href = $documentAnchor.attr('href');

            var re = /\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/;

            href = href.replace(re, id);

        } catch (err) {
        }

        $documentAnchor.attr('href', href);
        $documentAnchor.text(href);

        $haze.show()
             .on('click', function (event) {
                 event.stopPropagation();
                 $(this).off('click').hide();
                 $popup.hide();
             });
        $popup.show();
        $popup.find('.close_api').on('click', function (event) {
            event.stopPropagation();
            $popup.off('click');
            $haze.trigger('click');
            $popup.hide();
        });
    },
    showCallbackForm: function (event) {
        var popup;
        event.preventDefault();
        event.stopPropagation();
        $('.haze').show();
        popup = $('.popup_feedback');
        popup.toggleClass('closed');
        /*popup.css({
            'top'       : '0',
            'position'  : 'fixed',
        })*/
        popup.center();
    },
    //sendFeedback: function () {
    //    var feedback = this.collectFeedbackValues();
    //    if (feedback)
    //        oDataKrdReg.dataExchanger.postFeedback(feedback);
    //    this.closeFeedbackForm();
    //},
    //collectFeedbackValues: function () {
    //    var feedback, form;
    //    feedback = {};
    //    form = $('.popup_feedback form');
    //    feedback.ActiveType = form.find('select[name=feedbackTheme] option:selected').val();
    //    feedback.Name = form.find('input[name=personName]').val();
    //    if (!feedback.Name)
    //        form.find('input[name=personName]').val('Введите свое имя.');
    //    feedback.Message = form.find('textarea[name=message]').val();
    //    if (!feedback.Message)
    //        form.find('textarea[name=message]').val('Введите текст сообщения.');
    //    if (!feedback.Name || !feedback.Message)
    //        return null;
    //    feedback.Email = form.find('input[name=email]').val();
    //    return feedback;
    //},
    closeFeedbackForm: function (event) {
        if (event)
            event.stopPropagation();
        $('.popup_feedback').toggleClass('closed');
        $('.haze').hide();
        $('.popup_feedback form').trigger('reset');
    },
    scrollToTop: function (event) {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    },
};