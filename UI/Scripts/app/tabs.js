﻿$('.b-tabs').tabs();
var curPage = 1;
var pagesTotal = 1;

var groupsCount = 1;

var showGrpId = 0;

$(document).ready(function () {
    if (groupsCount > 1) {
        while (curPage > showGrpId * 10 + 11) {
            showGrpId++;
        }
        showGroup();

        $('.nav_next').click(nav_next_click);
        $('.nav_prev').click(nav_prev_click);

    } else {
        $('.nav_next').hide();
        $('.nav_prev').hide();
    }
});


function nav_next_click() {
    if (groupsCount - 1 == showGrpId) return;

    showGrpId++;
    showGroup();
}

function nav_prev_click() {
    if (showGrpId == 0) return;

    showGrpId--;
    showGroup();
}

function showGroup() {
    $('span.group[class*="id"]').hide();
    $('span.group.id' + showGrpId).show();
}