﻿///defines avaliable collumn data types
var collumn_types = {
    /// integral type
    int_t: 0,
    /// real type
    real_t: 1,
    /// money type
    decimal_t: 2,
    /// date type
    date_t: 3,
    /// time type
    time_t: 4,
    /// date and time type
    datetime_t: 5,
    /// single line text type
    string_t: 6,
    /// multiline text tipe
    text_t: 7,
    /// postall address
    postaddress_t: 8,
    /// url
    url_t: 9,
    /// e-mail
    email_t: 10,
    /// video file what stores on portals server type
    videobin_t: 11,
    /// video file url type
    videourl_t: 12,
    /// audio file what stores on portals server type
    audiobin_t: 13,
    /// audio file url type
    audiourl_t: 14,
    /// image file what stores on portals server type
    imagebin_t: 15,
    /// image file url type
    imageurl_t: 16,
    /// binary file what stores on portals server type
    filebin_t: 17,
    /// binary file url type
    fileurl_t: 18,
    /// logical type
    bool_t: 19,

    /// creates cell content of data set item table
    /// <param name="type" type="collumn_types">cell content data type</param>
    /// <param name="source">cell conten. For cell data types like Xbin_t this parametr must be object with src and type fields</param>
    /// <returns type="jQuery_object">cell content</param>
    createCollumnContent: function (type, source) {
        switch (type) {
            case this.audiobin_t: {
                source = source.split('.');
                return $('<audio controls><source src="' + source[0] + '" type="'
                    + source[1] + '">Браузер не поддерживает данный аудиоформат</audio>');
            }
                break;

            case this.audiourl_t:
                return $('<audio src="' + source + '" controls>Браузер не поддерживает данный аудиоформат</audio>');
                break;

            case this.bool_t:
                return $('<input type="checkbox" class="check-box set-cell-checkbox" disabled="disabled" ' +
                    (source == true ? 'checked="checked" ' : '') + '/>');
                break;

            case this.date_t:
            case this.datetime_t: {
                var ts = source.match(/\d+/gm)[0];
                var date = new Date(ts * 1);
                return type == this.date_t ? date.toLocaleDateString() : date.toLocaleString();
            }
                break;

            case this.email_t:
                return $('<a href="mailto:' + source + '">' + source + '</a>');
                break;

            case this.filebin_t:
            case this.fileurl_t:
                return $('<a href="' + source + '"><img src="/Content/Images/app/icon-load-active.png">Скачать</a>');
                break;

            case this.imageurl_t:
            case this.imagebin_t:
                return $('<img src="' + source + '" class="set-cell-image"/>');
                break;

            case this.decimal_t:
                return source + ' p.';
                break;

            case this.int_t:
            case this.postaddress_t:
            case this.real_t:
            case this.string_t:
                return source;
                break;

            case this.text_t:
                return $('<p>' + source + '</p>');
                break;

            case this.time_t: {
                var date = new Date(source.Ticks * 1);
                return date.toLocaleTimeString();
            }
                break;

            case this.url_t:
                return $('<a href="' + source + '">' + source + '</a>');
                break;

            case this.videobin_t: {
                source = source.split('.');
                return $('<video class="set-cell-video" controls><source src="'
                    + source[0] + '" type="' + source[1] + '">Браузер не поддерживает данный видеоформат</video>');
            }
                break;

            case this.videourl_t:
                return $('<object width="350" height="288">' +
                        '<param value="' + source + '<param name="allowFullScreen" value="true"></param>' +
                        '<embed allowfullscreen="true" width="350" height="288" type="application/x-shockwave-flash" '
                        + 'src="' + source + '" /></object>');
                break;

            default:
                return "!!ошибка!!";
                break;
        }
    },

    /// creates POI markers content element
    createPOIRowContent: function (type, title, source) {
        switch (type) {
            case this.bool_t:
                return '<tr><th>' + title + '</th><td><input type="checkbox" class="check-box set-cell-checkbox" disabled="disabled" ' +
                    (source == true ? 'checked="checked" ' : '') + '/></td></tr>';
                break;

            case this.date_t:
            case this.datetime_t: {
                var ts = source.match(/\d+/gm)[0];
                var date = new Date(ts * 1);
                return '<tr><th>' + title + '</th><td>' + (type == this.date_t ? date.toLocaleDateString() : date.toLocaleString()) + '</td></tr>';
            }
                break;

            case this.email_t:
                return '<tr><th>' + title + '</th><td><a href="mailto:' + source + '">' + source + '</a></td></tr>';
                break;

            case this.imageurl_t:
            case this.imagebin_t:
                return '<tr><th>' + title + '</th><td><img src="' + source + '" class="set-cell-image"/></td></tr>';
                break;

            case this.decimal_t:
                return '<tr><th>' + title + '</th><td>' + source + ' p.</td></tr>';
                break;

            case this.int_t:
            case this.postaddress_t:
            case this.real_t:
            case this.string_t:
                return '<tr><th>' + title + '</th><td>' + source + '</td></tr>';
                break;

            case this.text_t:
                return '<tr><th>' + title + '<td><p>' + source + '</p></td></tr>';
                break;

            case this.time_t: {
                var date = new Date(source.Ticks * 1);
                return '<tr><th>' + title + '</th><td>' + date.toLocaleTimeString() + '</td></tr>';
            }
                break;

            case this.url_t:
                return '<tr><th>' + title + '</th><td><a href="' + source + '">' + source + '</a></td></tr>';
                break;

            default:
                return '';
                break;

        }
    },

    /// creates filters content element
    createFilterItem: function (collumn) {

        var result = "";

        var createTypeField = function (_col) {
            return '<input type="hidden" name="' + _col.BindedCollumn + '_type" value="' + collumn.Type + '"/>';
        }

        switch (collumn.Type) {
            case this.bool_t:
                result = '<input type="checkbox" name="' + collumn.BindedCollumn + '" class="check-box set-cell-checkbox"/>';
                break;

            case this.date_t:
                result = '<input type="date" name="' + collumn.BindedCollumn + '" class="date set-cell-date"/>';
                break;

            case this.datetime_t:
                result = '<input type="datetime" name="' + collumn.BindedCollumn + '" class="datetime set-cell-date"/>';
                break;

            case this.decimal_t:
            case this.int_t:
            case this.real_t:
                result = '<input type="number" name="' + collumn.BindedCollumn + '" class="range set-cell-date"/>';
                break;

            case this.email_t:
            case this.postaddress_t:
            case this.string_t:
            case this.text_t:
            case this.url_t:
                result = '<input type="text" name="' + collumn.BindedCollumn + '" class="text set-cell-date"/>';
                break;

            case this.time_t:
                result = '<input type="time" name="' + collumn.BindedCollumn + '" class="time set-cell-date"/>';
                break;

            default:
                return null;
                break;
        }

        result += createTypeField(collumn);
        return result;
    }
};




// class that implement all necessary routine for display set data on page.
// use Generate function where you need to create set data representation.
// class will hold all routines for display and navigation on set data.
/// <param name="_structure" type="Repository.Entities.DataStructure">Full set structure</param>
/// <param name="_versionID" type="System.Guid">Set version</param>
/// <param name="_identifier" type="System.String">Set's identifier in passport if we display only data from given passport</param>
/// <param name="_loadDataUrl" type="System.String">The url of data provider action.</param>
/// <param name="_loadTemplateUrl" type="System.String">The url of table display template provider action.</param>
/// <param name="_loadPagerInfoUrl" type="System.String">The url of paging info provider action.</param>
/// <param name="_processFilterQueryUrl" type="System.String">The url of filter query processor action.</param>
/// <param name="_mapMarkers" type="System.String">Reference to POIs collection</param>
var TableGenerator = function (_structure, _versionID, _identifier, _loadDataUrl, _loadTemplateUrl, _loadPagerInfoUrl, _processFilterQueryUrl, _mapMarkers) {
    var currentStructureID = _structure.Id;
    var loadDataUrl = _loadDataUrl;
    var loadTemplateUrl = _loadTemplateUrl;
    var loadPagerInfoUrl = _loadPagerInfoUrl;
    var processFilterQueryUrl = _processFilterQueryUrl;
    var versionId = _versionID;
    var identifier = _identifier;
    var markers = _mapMarkers;

    var structures = [];

    var mainPalceHolder;
    var isExtendedStructureSet = false;
    var fullViewItemId = null;
    var buildAddtitonalSets = false;

    var tableGenerator = this;

    var parseStructure = function (struct, parent) {

        var result = [];

        var item = {
            id: struct.Id,
            parent: parent,
            order: struct.Order,
            templateId: struct.Template_Id,
            table: struct.TableName,
            children: null
        };

        structures[struct.Id] = item;

        if (struct.Children && struct.Children.length > 0) {
            isExtendedStructureSet = true;
            item.children = [];
            struct.Children.forEach(function (_child) {
                var child = parseStructure(_child, item);
                item.children.push(child);
            });
        }

        return item;
    }

    var getDataSync = function (_url, _data) {

        //var result = null;

        //$.ajax({
        //    type: 'GET',

        //    url: _url,
        //    data: _data,
        //    async: false,
        //    cache: false,
        //    dataType: 'json',
        //    always: function (response) {
        //        alert('contact!!!');
        //        console.log(response);
        //        result = response;
        //    }
        //});

        var result = $.ajax({
            url: _url,
            data: _data,
            async: false,
            dataType: 'json'
        }).responseText;

        //return result;

        console.log(result);

        return JSON.parse(result);
    };
    var postDataSync = function (_url, _data) {
        var result = $.ajax({
            type: 'POST',
            url: _url,
            data: _data,
            async: false,
            datatype: 'json',
            processData: false,
            contentType: false,
        }).responseText;

        if (result == '') return null;

        return JSON.parse(result);
    }



    // generates link for detailed view of set item data table item
    var createFullViewLink = function (content) {

        var fullViewClick = function () {
            // looking for row what contais this element
            var item = $(this);
            while (true) {
                if (item.attr('id'))
                    break;
                item = item.parent();
            }

            fullViewItemId = item.attr('id');

            // change currentStructureID to make build necessary structures
            item = item.parent();
            while (true) {
                if (item.attr('id'))
                    break;
                item = item.parent();
            }
            currentStructureID = item.attr('id');

            tableGenerator.Generate(mainPalceHolder);
        };

        var fullViewLink = $('<a href="#" class="full-view-link" title="Подробнее">' + content + '</a>');
        fullViewLink.click(fullViewClick);
        return fullViewLink;
    }

    // generates header for set item data table
    var generateHeader = function (placeHolder, _structId) {
        // function download set item data table structure if necessary 
        // then walk throught it and create tadle header
        var structure = structures[_structId];

        if (structure) {
            if (!structure.Template) {
                structure.Template = getDataSync(loadTemplateUrl, { templId: structure.templateId });
            }

            var thead = $('<thead></thead>');

            var template = structure.Template;

            var rowColumns = template.Columns;

            var createTableHeaderColumn = function (column, maxLvl, lvl) {
                var colspan = 0;
                var rowspan = 0;

                // if column contains subcolumns, we look depth to find how much of then
                // and then calculate necessary header's collumn row and cloumn spans
                if (!column.SubColumns || column.SubColumns.length == 0) {
                    rowspan = maxLvl - lvl;
                } else {
                    for (i = 0; i < column.SubColumns.length; i++)
                        colspan += TableGenerator.maxWidth_Column(column.SubColumns[i]);
                }

                // create header cell with calculated column and row spans and title
                var th = $('<th></th>');
                th.attr("title", column.DescriptionRu);
                if (colspan > 1) th.attr('colspan', colspan);
                if (rowspan > 1) th.attr('rowspan', rowspan);
                th.text(column.Header);

                return th;
            };

            var maxDepth = TableGenerator.maxDepth_Table(template);
            for (lvl = 0; lvl < maxDepth; lvl++) {
                // select columns of current table header level and create header elements
                var tr = $("<tr></tr>");

                rowColumns.forEach(function (column) {
                    tr.append(createTableHeaderColumn(column, maxDepth, lvl));
                });

                thead.append(tr);

                var _selectMany = function (_cols) {
                    var _res = [];
                    pos = 0;
                    // look for collumns in next level of table structure what defined as tree.
                    for (i = 0; i < _cols.length; i++) {
                        if (_cols[i].SubColumns && _cols[i].SubColumns.length > 0)
                            for (j = 0; j < _cols[i].SubColumns.length; j++) {
                                _res[pos] = _cols[i].SubColumns[j];
                                pos++;
                            }
                    }

                    return _res;
                }

                rowColumns = _selectMany(rowColumns);
            }

            placeHolder.append(thead);
        }
    };

    // generates content for set item data table
    var generateContent = function (placeHolder, structId, page) {
        if (!page) page = 1;

        var res = null;

        if (fullViewItemId) {
            if (buildAddtitonalSets) {
                res = getDataSync(loadDataUrl,
                {
                    verId: versionId,
                    passportId: identifier,
                    page: page,
                    structId: structId,
                    parentItemId: fullViewItemId
                });
            } else {
                res = getDataSync(loadDataUrl,
                {
                    verId: versionId,
                    passportId: identifier,
                    page: page,
                    structId: structId,
                    fullViewItemId: fullViewItemId
                });
            }
        } else {
            res = getDataSync(loadDataUrl,
            {
                verId: versionId,
                passportId: identifier,
                page: page,
                structId: structId
            });
        }

        var template = structures[structId].Template;

        if (res && template) {

            var tbody = $('<tbody></tbody>');

            var bindedCols = [];
            // data is represented as collection of collection of key-value pairs
            // wich represented as row with key as BindedCollumn and value as cell value.

            // mapping of template structure cells and relevant key-value pairs
            template.Columns.forEach(function (column) {
                var _columns = TableGenerator.walkDeep_Column(column);
                bindedCols = bindedCols.concat(_columns);
            });

            var createContentRow = function (row, bindedColumns) {
                var tr = $('<tr></tr>');
                tr.attr('id', row['Id']);

                var createColumnsInnerTable = function (column, row) {
                    // for inner table we have to represent our structure as 
                    // result of matrix transponition. 
                    var table = $('<table class="inner_table"></table>');
                    var tbody = $('<tbody></tbody>');

                    var maxDepth = TableGenerator.maxWidth_Table(column.InnerTable);
                    var maxWidth = 0;

                    column.InnerTable.Columns.forEach(function (_column) {
                        maxWidth += TableGenerator.maxDepth_Column(_column);
                    });

                    var analize_cells = function (columns, table, binded_cells, i, j) {
                        // this function needed for inner tables. It walks throught inner table template
                        // process template structure and put result at 2-dimensional array what looks
                        // like real table.
                        if (!columns) return 0;
                        columns.forEach(function (column) {

                            if (!table[i])
                                table[i] = [];
                            else
                                if (table[i][j]) i++;

                            table[i][j] = column;

                            if (column.SubColumns && column.SubColumns.length > 0)
                                i = analize_cells(column.SubColumns, table, binded_cells, i, j + 1);
                            else
                                binded_cells.push(column);

                            i++;
                        });

                        return i;
                    }

                    var _table = [];
                    var _binded_cells = [];

                    // the result will be inner table structure what helps us to create table
                    analize_cells(column.InnerTable.Columns, _table, _binded_cells, 0, 0);
                    _binded_cells = _binded_cells.reverse(); //make a queue

                    var createColumnsInnerTableHeaderCell = function (column, maxDepth, lvl) {
                        var th = $('<th></th>');
                        th.text(column.Header);

                        var colspan = 0;
                        var rowspan = 0;

                        if (column.SubColumns && column.SubColumns.length > 0)
                            column.SubColumns.forEach(function (_column) {
                                rowspan += TableGenerator.maxWidth_Column(_column);
                            });
                        else
                            colspan = maxDepth - lvl;

                        th.attr("title", column.DescriptionRu);

                        if (rowspan > 1) th.attr("rowspan", rowspan);
                        if (colspan > 1) th.attr("colspan", colspan);

                        return th;
                    }

                    for (i = 0; i < maxWidth; i++) {
                        var tr = $('<tr></tr>');
                        for (j = 0; j < maxDepth; j++)
                            if (_table[i][j])
                                tr.append(createColumnsInnerTableHeaderCell(_table[i][j], maxDepth, j));

                        if (tr.html().length > 0 && _binded_cells.length > 0) {
                            var bindedCell = _binded_cells.pop();

                            var cell = null;
                            var td = $('<td>test</td>');
                            val = row[bindedCell.BindedCollumn];

                            var cell_content = collumn_types.createCollumnContent(bindedCell.Type, val);

                            if (isExtendedStructureSet && bindedCell.IsFullViewLink) {
                                var item = createFullViewLink(cell_content);
                                if (item)
                                    td.append(item);
                                else
                                    td.html(cell_content);
                            } else
                                td.html(cell_content);

                            tr.append(td);
                        }
                        tbody.append(tr);
                    }

                    return table.append(tbody);
                }

                bindedColumns.forEach(function (bindedColumn) {
                    var td = $('<td></td>');
                    if (bindedColumn.InnerTable) {
                        td.append(createColumnsInnerTable(bindedColumn, row));
                    } else {
                        val = row[bindedColumn.BindedCollumn];

                        var cell_content = collumn_types.createCollumnContent(bindedColumn.Type, val);

                        if (isExtendedStructureSet && bindedColumn.IsFullViewLink) {
                            var item = createFullViewLink(cell_content);
                            if (item)
                                td.append(item);
                            else
                                td.html(cell_content);
                        } else
                            td.html(cell_content);
                    }

                    tr.append(td);
                });

                return tr;
            };

            res.forEach(function (row) {
                tbody.append(createContentRow(row, bindedCols));
            });

            placeHolder.append(tbody);
        }

        return res;
    };

    // generates POI markers on the map tab
    var generatePOIs = function (structId, poisData) {
    }

    // generates filter for current structure
    var generateFilter = function (placeHolder, structId) {

        var struct = structures[structId];
        if (struct) {
            var filter = $('<div class="clp__body filter"></div>');
            var filterForm = $('<form id="filter_' + structId + '"></form>');
            var filterFormContent = $('<div class="items form-items bottom-items"></div>');

            filterForm.append($('<input type="hidden" name="structId" value="' + structId + '"/>'));

            // table structure represented as tree. so, make a deep walk throught tree, 
            // collect it nodes into array and then we find node this BindedColumn != null
            // create filter item and appent it to filter form.
            var processCell = function (cell, items) {
                if (!items)
                    items = [];

                if (cell.SubColumns && cell.SubColumns.length > 0) {
                    for (i = 0; i < cell.SubColumns.length; i++) {
                        var subItems = items.slice(0);
                        subItems.push(cell.Header + "&rarr;");
                        processCell(cell.SubColumns[i], subItems);
                    }
                    return;
                }

                if (cell.InnerTable && cell.InnerTable.Columns && cell.InnerTable.Columns.length > 0) {
                    for (i = 0; i < cell.InnerTable.Columns.length; i++) {
                        var subItems = items.slice(0);
                        subItems.push(cell.Header + "&rarr;");
                        processCell(cell.InnerTable.Columns[i], subItems);
                    }
                    return;
                }

                if (cell.Header) {
                    items.push(cell.Header + "&rarr;");
                }

                if (cell.BindedCollumn) {
                    var filterItem = collumn_types.createFilterItem(cell);
                    if (cell.IncludeIntoFilter && filterItem) {
                        var html = '<div class="item item-50"><div class="item-content">'
                        items.forEach(function (arr_item) {
                            html += arr_item;
                        });
                        html += filterItem;
                        html += '</div></div>';

                        filterFormContent.append($(html));
                    }
                }
            };

            struct.Template.Columns.forEach(function (cell) {
                processCell(cell, null);
            });

            var filterButtonClicked = function () {
                // filtering event means creation of new pager and set item data
                // for filter's set item

                // look for a main set div, that contains this pager
                // as we can see in generateSetItem, set has class 'set-item', 
                // that contains set element structure identifier
                var set_item = $(this).parent();
                while (true) {
                    if (set_item.attr('id') && !(/filter_/i.test(set_item.attr('id'))))
                        break;
                    set_item = set_item.parent();
                }

                // post query to server
                var _form = document.getElementById('filter_' + set_item.attr('id'));

                postDataSync(processFilterQueryUrl, new FormData(_form));

                generateSetItem(set_item, set_item.attr('id'), 1, true);
            }

            filterForm.append(filterFormContent);

            var filterButton = $('<input type="button" title="Произвести фильтрацию данных таблицы набора в соответствии с параметрами фильтра" value="Фильтровать"/>');
            filterButton.click(filterButtonClicked);

            var clearFilterFormButton = $('<input type="reset" title="Отчистить содержимое полей фильтра" value="Сбросить">');
            clearFilterFormButton.click(function (event) {
                // reset filter form and trigger filterButtonClick
                event.preventDefault();
                $(this).closest('form').get(0).reset();
                filterButton.trigger('click');
            });

            filterForm.append(filterButton);
            filterForm.append(clearFilterFormButton);

            filter.append(filterForm);
            var toggleFilterVisibilityButton = $('<div class="clp__trigger js-collpasible-trigger">cвернуть фильтр</div>');
            filter.append(toggleFilterVisibilityButton);
            placeHolder.append(filter);
        }
    }

    // generates pager for set table
    var generatePager = function (placeHodlder, structId, only_return) {
        // ask pager info from server and build pager for table
        var pagerInfo = getDataSync(loadPagerInfoUrl, {
            verId: versionId,
            structId: structId,
            passportId: identifier
        });

        var changePage = function (pagerElem) {
            var page = pagerElem.data('page-num');

            // look for a main set item div, that contains this pager
            // as we can see in generateSetItem, set has class 'set-item', 
            // that contains set element structure identifier
            var set_item;
            var parent = pagerElem.parent();
            while (true) {
                if (parent.attr('id')) {
                    set_item = parent;
                    break;
                }
                parent = parent.parent();
            }

            var _structId = set_item.attr('id');

            if (page && set_item && _structId && _structId != '')
                generateSetItem(set_item, structId, page);
        }

        if (pagerInfo) {
            if (pagerInfo.itemsPerPage <= pagerInfo.count) {
                var pages_count = Math.round(pagerInfo.count / pagerInfo.itemsPerPage);
                // use functions from PagingClick.js
                var pager = generatePagerHelper(pages_count, changePage);

                if (only_return)
                    return pager;

                placeHodlder.append(pager);
            }
        }
    }

    // generates new set item element or set item element data (and pager for filter event handlerd).
    // generate full set item if was called from Generate or only part of set item overwise.
    var generateSetItem = function (placeHolder, structId, page, filterEvent) {
        // placeholder is div with filter, pager and set table inside.
        // first of all, look if we have alreade create our set item.

        var setItemsData;

        var filter = $("div.filter", placeHolder);
        if (filter && (filter.context || filter.length)) {
            // filter always exists in created set item, so this function
            // was called by paging event or filter event handlers.
            // looking for set item table, removes tbody element and 
            // call generateContent function
            var table = $('table', placeHolder);
            $('tbody', table).remove();

            
            if (filterEvent) {
                // function was called by filter event, so we have to rebuild our pager
                $.each($('*', placeHolder), function (i, j) {
                    // I can't get fuckn pager using simple $('nav.pager', placeholder) instruction
                    console.log($(j)[0].tagName);
                    if ($(j)[0].tagName == "NAV") {
                        $(j).remove()
                        return false;
                    }
                });

                var new_pager = generatePager(placeHolder, structId, true);
                if (new_pager)
                    new_pager.insertAfter(filter);
            }

            setItemsData = generateContent(table, structId, page);
        } else {
            // this function was called first time for placeHolder and
            // we have to create full DOM for this placeholder
            if (!structures[structId].Template)
                structures[structId].Template = getDataSync(loadTemplateUrl, { templId: structures[structId].templateId });
            var set = $('<div id="' + structId + '" class="set-item"></div>');
            if (!isExtendedStructureSet || (buildAddtitonalSets == (!!fullViewItemId))) { // use C-style bool conversation
                // in ExtendedStructure mode we generate pagers an filters only for preview items
                generateFilter(set, structId);
                generatePager(set, structId);
            }
            var table = $('<table></table>');
            generateHeader(table, structId);
            setItemData = generateContent(table, structId, page);
            set.append(table);

            placeHolder.append(set);
        }

        generatePOIs(structId, setItemsData);
    }

    this.Generate = function (placeHolder) {
        // this function generates full DOM with set tables, filter and 
        // pagers inside it and put generated DOM into placeholder.
        // Generated DOM has next structure:
        // <div>
        //  <div.set-item id="set item 1 structure id">  // set item 1
        //      <filter == div.filter>
        //      <pager == nav.pager> // can be missed if set data items count lower or equal some extern defined value
        //      <table with set data>
        //  </div>
        //  <div.set-item id="set item 2 structure id">  // set item 2
        //      <filter == div.filter>
        //      <pager == nav.pager> // can be missed if set data items count lower or equal some extern defined value
        //      <table with set data>
        //  </div>
        //   ...
        //  <div.set-item id="set item N structure id">  // set item N
        //      <filter == div.filter>
        //      <pager == nav.pager> // can be missed if set data items count lower or equal some extern defined value
        //      <table with set data>
        //  </div>
        // We have multiple set items on page if we entered into set item (click on link inside set item data table) and
        // first table will be detailed info of selected set item data table item.

        var sets = $("<div></div>");

        buildAddtitonalSets = false;

        var struct = structures[currentStructureID];
        if (struct) {
            // save set items placeholder for feature
            mainPalceHolder = placeHolder;

            placeHolder.empty();
            generateSetItem(sets, struct.id, 1);
            if (isExtendedStructureSet) {
                if (fullViewItemId && struct.children) {
                    // build additional set items
                    buildAddtitonalSets = true;
                    struct.children.forEach(function (item) {
                        var _struct = structures[item.id];
                        var temp_placeholder = $('<div></div>');
                        generateSetItem(temp_placeholder, _struct.id, 1);
                        temp_placeholder = $('div.set-item', temp_placeholder);
                        sets.append(temp_placeholder);
                    });

                }

            }
            placeHolder.append(sets);
        }
    }

    parseStructure(_structure, null);
};

TableGenerator.maxDepth_Column = function (column) {
    var max = 0;
    if (column.SubColumns) {
        column.SubColumns.forEach(function (_collumn) {
            var _max = TableGenerator.maxDepth_Column(_collumn);
            if (_max > max) { max = _max; }
        });
    }
    return 1 + max;
};

TableGenerator.maxWidth_Column = function (column) {
    var max = 1;
    if (column.SubColumns && column.SubColumns.length > 0) {
        max = column.SubColumns.length;
        for (i = 0; i < column.SubColumns.length; i++) {
            var _max = TableGenerator.maxWidth_Column(column.SubColumns[i]);
            if (_max > max) max = _max;
        }
    }
    return max;
};

TableGenerator.walkDeep_Column = function (column) {
    var res = [];

    if (column.SubColumns && column.SubColumns.length > 0)
        column.SubColumns.forEach(function (c) {
            res = res.concat(TableGenerator.walkDeep_Column(c));
        });
    else
        res[0] = column;

    return res;
};

TableGenerator.maxWidth_Table = function (table) {
    var max = 0;
    if (table.Columns) {
        max = 1;
        table.Columns.forEach(function (column) {
            var _max = TableGenerator.maxWidth_Column(column);
            if (_max > max) { max = _max }
        });
    }

    return max;
};

TableGenerator.maxDepth_Table = function (table) {
    var max = 0;
    if (table.Columns) {
        max = 1;
        table.Columns.forEach(function (column) {
            var _max = TableGenerator.maxDepth_Column(column);
            if (_max > max) { max = _max; }
        });
    }

    return max;
};