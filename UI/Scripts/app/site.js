var Page = (function ()
{
    var Page = {},
	max_mobile_width = 768, // максимальная ширина, при которой применяется стили и функционал для портативных устройств
	is_mobile = false // браузер мобильный или нет
    ;

    // После загрузки страницы ($(document).ready) устанавливаем некоторые важные константы
    Page.KeyVariables = function () {
        //-- (is_mobile) определяем: браузер мобильный или нет --//
        if (navigator.userAgent.match(/Android/i)
			|| navigator.userAgent.match(/webOS/i)
			|| navigator.userAgent.match(/iPhone/i)
			|| navigator.userAgent.match(/iPad/i)
			|| navigator.userAgent.match(/iPod/i)
			|| navigator.userAgent.match(/BlackBerry/i)
			|| navigator.userAgent.match(/Windows Phone/i)
			) {
            is_mobile = true;
            $("body").addClass("sp--mobile-browser");
            console.info("Mobile browser");
        }
        else {
            is_mobile = false;
            console.info("Desktop browser");
        }

    };
	
	
    // Стандартное верхнее меню 2-го уровня
    Page.HeaderMenuDesktop = function () {
        var h_menu_selector = '.h-menu';
        if (typeof ($.fn.superfish) === 'undefined') {
            console.info("HeaderMenuDesktop. required superfish(.min).js");
            return;
        }
        $(h_menu_selector).each(function (index) {
            $(this).superfish({});
        });
    };
	
    return Page;
})();


$(document).ready(function () {

	Page.KeyVariables();
	Page.HeaderMenuDesktop();
	
});