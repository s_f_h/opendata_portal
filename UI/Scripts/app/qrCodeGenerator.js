﻿function getQRcode(obj) {

    var el = '<div class="popup-qr"><div class="qr-img"></div></div>'
    
    var path = window.location.protocol + "/" + window.location.host + "/" + obj.data('key');

    console.log(path);

    $(el).appendTo(obj);

    $('.qr-img').empty();
    $('.qr-img').qrcode({
        size: "160",
        render: "canvas",
        text: path
    });

    $('.popup-qr').show();
}

$(document).mouseup(function (e) {
    if (($(".popup-qr").has(e.target).length === 0)) {
        $('.popup-qr').remove();
    }
});