﻿function generatePagerHelper(pagesTotal, callback) {
    var res = $('<nav class="pager"></nav>');
    generatePagerFor(res, pagesTotal, callback);
    return res;
}

function generatePagerFor(placeHolder, pagesTotal, callback) {
    var grpTag = $('<span class="group" data-id="0"></span>');
    var itemsInGroup = 10;

    for (var i = 0; i < pagesTotal; i++) {
        var aTag = $('<a href="#" data-page-num="' + (i + 1) + '">' + (i + 1) + '</a>');

        if (i == 0) {
            aTag.addClass("selected");
            if (pagesTotal > itemsInGroup) {
                var btn = $('<input type="button" value="\u2190" class="nav_prev" />');
                placeHolder.append(aTag);
                placeHolder.append(btn);
                continue;
            }
        }

        if ((i == pagesTotal - 1) && (pagesTotal > itemsInGroup)) {
            var btn = $('<input type="button" value="\u2192" class="nav_next" />');
            placeHolder.append(grpTag);
            placeHolder.append(btn);
            placeHolder.append(aTag);
            continue;
        }

        grpTag.append(aTag);

        if ((i != 0) && (i % 10 == 0)) {
            placeHolder.append(grpTag);
            grpTag = $('<span class="group" data-id="' + (i / 10) + '" hidden="hidden"></span>');
        }
    }

    if (pagesTotal <= itemsInGroup) placeHolder.append(grpTag);

    $("a", placeHolder).click(function () {
        $("a", placeHolder).removeClass("selected");
        $("span.current-page", placeHolder).text("Страница " + $(this).data('page-num') + " из " + pagesTotal);
        $('a[data-page-num="' + $(this).data('page-num') + '"]', placeHolder).addClass("selected");
        callback($(this));
    });

    var currentPage = $('<div class="current-page">Страница 1 из ' + pagesTotal + '</span>');
    placeHolder.append(currentPage);

    $('.nav_next', placeHolder).click(function () {
        var currentGrp = $('span:visible', placeHolder);
        var id = currentGrp.data('id');
        var nextGrp = $('span[data-id="' + (id + 1) + '"]', placeHolder);
        if (nextGrp && nextGrp.length > 0) {
            currentGrp.hide();
            nextGrp.show();
        }
    });
    $('.nav_prev', placeHolder).click(function () {
        var currentGrp = $('span:visible', placeHolder);
        var id = currentGrp.data('id');
        var nextGrp = $('span[data-id="' + (id - 1) + '"]', placeHolder);
        if (nextGrp && nextGrp.length > 0) {
            currentGrp.hide();
            nextGrp.show();
        }
    });
}