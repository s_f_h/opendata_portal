﻿function generateRatingWidgetFor(placeHolder, step, currentValue, isReadonly, callback) {
    var widget;

    if (isReadonly) {
        widget = $('<div class="rateit" id="rating_widget" data-rateit-starwidth="20" data-rateit-starheight="20"  data-rateit-value="' + currentValue + '" data-rateit-step="' +
            step + '" data-rateit-resetable="false" data-rateit-ispreset="true" data-rateit-readonly="true"></div>');
    } else {
        widget = $('<div class="rateit" id="rating_widget" data-rateit-starwidth="20" data-rateit-starheight="20" data-rateit-value="' + currentValue + '" data-rateit-step="' +
            step + '" data-rateit-resetable="true" data-rateit-ispreset="true"></div>');
    }

    var ratingVal = $('<span id="rating_widget_val">(' + currentValue + ')</span>');

    placeHolder.append(widget);
    placeHolder.append(ratingVal);

    widget.bind('rated', callback);
    widget.bind('reset', callback);
    widget.bind('over', function (event, value) {
        if (value == null) value = 0;
        ratingVal.text('(' + value + ')');
    });

    widget.rateit({ min: 0, max: 5, step: step, value: currentValue });

}

function setRatingWidgetValueFor(placeHolder, isReadonly, value) {
    if (isReadonly)
        $("span.rateit", placeHolder).rateit('readonly', true);
    else
        $("span.rateit", placeHolder).rateit('readonly', false);


    $("span#rating_widget_val", placeHolder).text('(' + value + ')');
}