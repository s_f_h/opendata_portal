﻿'use strict'

$(document).ready(function () {
    oDataKrdReg.domEvents.init();


    //toggle link up
    function linkUpUpdate() {
        var lnkToTop = $('.lnk-up');
        $(window).scroll(function () {
            if ($(this).scrollTop() > 0) {
                lnkToTop.fadeIn();
            } else {
                lnkToTop.fadeOut();
            }
        });
        lnkToTop.click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });

    }

    linkUpUpdate();
    //vertical-tabs
    $('.b-vertical-tabs').tabs();

    //collapsible
    $(document).on('click', '.js-collpasible-trigger', function () {
        var trigger = $(this);
        var content = $(this).prev();
        content.slideToggle(500, function () {
            trigger.text(function () {
                return content.is(":visible") ? "свернуть фильтр" : "развернуть фильтр";
            });
            trigger.parent().toggleClass('is-collapsed')

        });
        /*
        trigger.parent().toggleClass(function(){
            return content.is(":visible") ? "" : "";
        })*/
    })

});


$(document).ready(function () {
    //dropdown list
    $('.js-dropdown').on('mouseenter', function () {
        $(this).addClass('dropdown-is-active').find('.dropdown__sub').fadeIn(0);
    }).on('mouseleave', function () {
        $(this).removeClass('dropdown-is-active').find('.dropdown__sub').fadeOut(0);
    });

    $('select').selectmenu({
        change: function (event, data) {
            // установака selected для выбранного пользователем элемента выпадающего списка,
            // чтобы была возможность отправить его с данными формы
            var id = $(this).val();
            var elems = $('option', $(this).parent());

            $.each(elems, function (index, value) {
                var $value = $(value);
                if ($value.val() == id)
                    $value.attr('selected', true);
                else
                    $value.attr('selected', false);
            });

            // запускаем событие для других обработчиков
            $(this).trigger("change");
        }
    });
});



jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
}