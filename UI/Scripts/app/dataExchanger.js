﻿'use strict'

oDataKrdReg.dataExchanger = {
    postFeedback: function (feedback) {
        if (!feedback)
            return;
        $.post('../api/v1/homeapi/feedback', feedback, function () {
            console.log('feedback sent.');
        },
        'application/json');
    }
};