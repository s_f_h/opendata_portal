-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		arhichief
-- Create date: 28.07.2016
-- =============================================

CREATE FUNCTION [dbo].[GetAverageRatingForPassport] (@id uniqueidentifier)
RETURNS @Result TABLE (Id uniqueidentifier, Rating float)
AS
BEGIN
	INSERT @Result
	SELECT [SetPassport_Id], AVG([Rating]) FROM [dbo].[RatedBy] WHERE [SetPassport_Id] = @id GROUP BY [SetPassport_Id]
	RETURN
END
GO

CREATE FUNCTION [dbo].[GetAverageRatingForSetVersion] (@id uniqueidentifier)
RETURNS @Result TABLE (Id uniqueidentifier, Rating float)
AS
BEGIN
	INSERT @Result
	SELECT [SetPassport_Id], AVG([Rating]) FROM [dbo].[RatedBy] WHERE [SetPassport_Id] IN 
		(SELECT Id FROM [dbo].[SetsPassports] WHERE [SetVersion_Id] = @id) GROUP BY [SetPassport_Id]
	RETURN
END
GO

CREATE FUNCTION [dbo].[GetSetVersionsWithCategoryAndRating] (@category uniqueidentifier, @rating float)
RETURNS @Result TABLE (Id uniqueidentifier, Rating float)
AS
BEGIN

	DECLARE @setPassportsOfCategory TABLE (Id uniqueidentifier)
	DECLARE @avgPassports TABLE (rating float, passport uniqueidentifier)

	INSERT @setPassportsOfCategory
	SELECT DISTINCT [Id] FROM [dbo].[SetsPassports] WHERE [SetVersion_Id] IN
		(SELECT [Id] FROM [dbo].[SetVersions] WHERE [IsAproved] = 1 AND [IsCurrent] = 1 AND [Set_Id] IN 
			(SELECT [Id] FROM [dbo].[Sets] WHERE [Category_Id] = @category))

	INSERT @avgPassports
	SELECT AVG([Rating]), [SetPassport_Id] FROM [dbo].[RatedBy] WHERE [SetPassport_Id] IN
		(SELECT [Id] FROM @setPassportsOfCategory) GROUP BY [SetPassport_Id]

	INSERT @Result
	SELECT [SV].[Id], [AP].[rating] FROM [dbo].[SetVersions] [SV], @avgPassports [AP] WHERE [AP].[rating] >= @rating AND [SV].[Id] IN 
		(SELECT SetVersion_Id FROM [dbo].[SetsPassports] WHERE [Id] IN
			(SELECT [passport] FROM @avgPassports WHERE [rating] >= @rating))

	RETURN
END
GO

CREATE FUNCTION [dbo].[GetSetPassporstWithAgencyAndRating] (@agency uniqueidentifier, @rating float)
RETURNS @Result TABLE (Id uniqueidentifier, Rating float)
AS
BEGIN

	DECLARE @setPassportsOfAgency TABLE (Id uniqueidentifier)
	DECLARE @avgPassports TABLE (rating float, passport uniqueidentifier)

	INSERT @setPassportsOfAgency
	SELECT [Id] FROM [dbo].[SetsPassports] WHERE [Agency_Id] = @agency AND [SetVersion_Id] IN
		(SELECT [Id] FROM [dbo].[SetVersions] WHERE [IsCurrent] = 1 AND [IsAproved] = 1)

	INSERT @avgPassports
	SELECT AVG([Rating]), [SetPassport_Id] FROM [dbo].[RatedBy] WHERE [SetPassport_Id] IN
		(SELECT [Id] FROM @setPassportsOfAgency) GROUP BY [SetPassport_Id]

	INSERT @Result
	SELECT [SP].[Id], [AP].[rating] FROM [dbo].[SetsPassports] [SP], @avgPassports [AP] WHERE [AP].[rating] >= @rating AND [SP].[Id] IN 
		(SELECT [AP].[passport] FROM @avgPassports WHERE [AP].[rating] >= @rating)

	RETURN
END
GO