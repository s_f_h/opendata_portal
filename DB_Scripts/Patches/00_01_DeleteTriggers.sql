-- ������� � �� �������� ��� ������ dbo.CollumnDisplayTemplates, 
-- dbo.DataStructures, dbo.SetVersions, ������������ �� �������� ��������� 
-- ��������� �������. 
-- 
-- ������ �� �������� �����������, �� � ��� � ���, ������ ��� �������� �����.

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[Before_Delete_CollumnDisplayTemplate] 
   ON  [dbo].[CollumnDisplayTemplates]
   INSTEAD OF DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

END

CREATE TRIGGER [dbo].[Before_Delete_DataStructures] 
   ON  [dbo].[DataStructures]
   INSTEAD OF DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

END

CREATE TRIGGER [dbo].[Before_Delete_SetVersions] 
   ON  [dbo].[SetVersions]
   INSTEAD OF DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

END

GO