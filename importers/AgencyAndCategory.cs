﻿using Repository;
using Repository.Abstract;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers
{
    class AgencyAndCategory
    {
        public AgencyAndCategory(DataContext ctx)
        {
            agencyRepo = Repository.RepositoryFactory.Get<IAgencyRepository>(ctx);
            categoryRepo = Repository.RepositoryFactory.Get<ICategoryRepository>(ctx);
        }

        IAgencyRepository agencyRepo;
        ICategoryRepository categoryRepo;

        public static Dictionary<Agencies, Guid> agencise = new Dictionary<Agencies, Guid>();
        public static Dictionary<Categories, Guid> categorise = new Dictionary<Categories, Guid>();

        public void execute()
        {
            #region
            var agency = new Agency
            {
                ITN = "2308027369",
                Keyword = "min_soc_razv",
                Name = "Министерство социального развития и семейной политики Краснодарского края",
                ShortName = "Мин соц развития"
            };

            agencyRepo.Add(agency);
            agencise.Add(Agencies.min_soc_razv, agency.Id);

            agency = new Agency
            {
                ITN = "2308084335",
                Keyword = "dep_pechat",
                Name = "Департамент печати и средств массовых коммуникаций Краснодарского края",
                ShortName = "Департамент печати и СМИ"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.dep_pechat, agency.Id);

            agency = new Agency
            {
                ITN = "2308167704",
                Keyword = "dep_reg_contract",
                Name = "Департамент по регулированию контрактной системы Краснодарского края",
                ShortName = "Департамент рег контр"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.dep_reg_contract, agency.Id);

            agency = new Agency
            {
                ITN = "2308077190",
                Keyword = "dep_trans",
                Name = "Департамент транспорта Краснодарского края",
                ShortName = "Департамент транспорта"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.dep_trans, agency.Id);

            agency = new Agency
            {
                ITN = "2310120340",
                Keyword = "dep_trud",
                Name = "Департамент труда и занятости населения Краснодарского края",
                ShortName = "Департамент труда"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.dep_trud, agency.Id);

            agency = new Agency
            {
                ITN = "2309053058",
                Keyword = "min_zdrav",
                Name = "Министерство здравоохранения Краснодарского края ",
                ShortName = "Мин Здрав"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.min_zdrav, agency.Id);


            agency = new Agency
            {
                ITN = "2308040000",
                Keyword = "min_fin",
                Name = "Министерство финансов Краснодарского края",
                ShortName = "Мин Фин"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.min_fin, agency.Id);

            agency = new Agency
            {
                ITN = "2308120720",
                Keyword = "min_ek",
                Name = "Министерство экономики Краснодарского края",
                ShortName = "Мин Эк"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.min_ek, agency.Id);

            agency = new Agency
            {
                ITN = "2310069623",
                Keyword = "upr_arhiv",
                Name = "Управление по делам архивов Краснодарского края",
                ShortName = "Управление по делам архивов"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.upr_arhiv, agency.Id);

            agency = new Agency
            {
                ITN = "2351007922",
                Keyword = "adm_tbilisi",
                Name = "Администрация муниципального образования Тбилисский район",
                ShortName = "Адм Тбилисский район"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.adm_tbilisi, agency.Id);

            agency = new Agency
            {
                ITN = "2308178382",
                Keyword = "diskk",
                Name = "Департамент информатизации и связи Краснодарского края",
                ShortName = "ДИС"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.diskk, agency.Id);

            agency = new Agency
            {
                ITN = "2308041155",
                Keyword = "dep_potreb",
                Name = "Департамент потребительской сферы Краснодарского края",
                ShortName = "Деп потреб"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.dep_potreb, agency.Id);

            agency = new Agency
            {
                ITN = "2308077553",
                Keyword = "dep_imuszh",
                Name = "Департамент Имущественных Отношений Краснодарского Края",
                ShortName = "Деп имуществ"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.dep_imuszh, agency.Id);

            agency = new Agency
            {
                ITN = "2308060599",
                Keyword = "min_kult",
                Name = "Министерство культуры Краснодарского края",
                ShortName = "Мин культ"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.min_kult, agency.Id);

            agency = new Agency
            {
                ITN = "2308027802",
                Keyword = "min_obr",
                Name = "Министерство образования и науки Краснодарского края",
                ShortName = "Мин обр"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.min_obr, agency.Id);

            agency = new Agency
            {
                ITN = "2312161984",
                Keyword = "min_prir",
                Name = "Министерство природных ресурсов Краснодарского края",
                ShortName = "Мин прир ресурс"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.min_prir, agency.Id);

            agency = new Agency
            {
                ITN = "2308085811",
                Keyword = "reg_energ_kom",
                Name = "Региональная энергетическая комиссия",
                ShortName = "Рег энерг"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.reg_energ_kom, agency.Id);

            agency = new Agency
            {
                ITN = "2309105980",
                Keyword = "upr_gos_ohr",
                Name = "Управление государственной охраны объектов культурного наследия Краснодарского края",
                ShortName = "Гос охрана"
            };
            agencyRepo.Add(agency);
            agencise.Add(Agencies.upr_gos_ohr, agency.Id);
            #endregion
            #region
            var category = new Category
            {
                Keyword = "kult",
                Name = "Культура"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.kult, category.Id);

            category = new Category
            {
                Keyword = "obraz",
                Name = "Образование"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.obraz, category.Id);

            category = new Category
            {
                Keyword = "ekon",
                Name = "Экономика и финансы"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.ekon, category.Id);

            category = new Category
            {
                Keyword = "potreb",
                Name = "Потребительская сфера"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.potreb, category.Id);

            category = new Category
            {
                Keyword = "priroda",
                Name = "Природные ресурсы и земельные отношения"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.priroda, category.Id);

            category = new Category
            {
                Keyword = "admin",
                Name = "Администрации муниципальных образований"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.admin, category.Id);

            category = new Category
            {
                Keyword = "stroitelstvo",
                Name = "Строительство и надзор"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.stroitelstvo, category.Id);

            category = new Category
            {
                Keyword = "promishlenost",
                Name = "Промышленность и энергетика"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.promishlenost, category.Id);

            category = new Category
            {
                Keyword = "soc_ravitie",
                Name = "Социальное развитие"
            };
            categoryRepo.Add(category);
            categorise.Add(Categories.soc_ravitie, category.Id);
            #endregion
        }

        public enum Agencies
        {
            /// <summary>
            ///"Министерство социального развития и семейной политики Краснодарского края",    
            /// <summary>
            min_soc_razv,
            /// <summary>
            /// Департамент печати и средств массовых коммуникаций Краснодарского края",
            /// </summary>
            dep_pechat,
            /// <summary>
            /// Департамент по регулированию контрактной системы Краснодарского края
            /// </summary>
            dep_reg_contract,
            /// <summary>
            /// Департамент транспорта Краснодарского края
            /// </summary>
            dep_trans,
            /// <summary>
            /// Департамент труда и занятости населения Краснодарского края
            /// </summary>
            dep_trud,
            /// <summary>
            /// Министерство здравоохранения Краснодарского края
            /// </summary>
            min_zdrav,
            /// <summary>
            /// Министерство финансов Краснодарского края
            /// </summary>
            min_fin,
            /// <summary>
            /// Министерство экономики Краснодарского края
            /// </summary>
            min_ek,
            /// <summary>
            /// Управление по делам архивов Краснодарского края
            /// </summary>
            upr_arhiv,
            /// <summary>
            /// Администрация муниципального образования Тбилисский район
            /// </summary>
            adm_tbilisi,
            /// <summary>
            /// Департамент информатизации и связи Краснодарского края
            /// </summary>
            diskk,
            /// <summary>
            /// Департамент потребительской сферы Краснодарского края
            /// </summary>
            dep_potreb,
            /// <summary>
            /// Департамент Имущественных Отношений Краснодарского Края
            /// </summary>
            dep_imuszh,
            /// <summary>
            /// Министерство культуры Краснодарского края
            /// </summary>
            min_kult,
            /// <summary>
            /// Министерство образования и науки Краснодарского края
            /// </summary>
            min_obr,
            /// <summary>
            /// Министерство природных ресурсов Краснодарского края
            /// </summary>
            min_prir,
            /// <summary>
            /// Региональная энергетическая комиссия
            /// </summary>
            reg_energ_kom,
            /// <summary>
            /// Управление государственной охраны объектов культурного наследия Краснодарского края
            /// </summary>
            upr_gos_ohr,
        }
        public enum Categories
        {
            /// <summary>
            /// Культура
            /// </summary>
            kult,
            /// <summary>
            /// Образование
            /// </summary>
            obraz,
            /// <summary>
            /// Экономика и финансы
            /// </summary>
            ekon,
            /// <summary>
            /// Потребительская сфера
            /// </summary>
            potreb,
            /// <summary>
            /// Природные ресурсы и земельные отношения
            /// </summary>
            priroda,
            /// <summary>
            /// Администрации муниципальных образований
            /// </summary>
            admin,
            /// <summary>
            /// Строительство и надзор
            /// </summary>
            stroitelstvo,
            /// <summary>
            /// Промышленность и энергетика
            /// </summary>
            promishlenost,
            /// <summary>
            /// Социальное развитие
            /// </summary>
            soc_ravitie,
        }
    }
}
