﻿using importers.Importers;
using Repository;
using System;

namespace importers
{
    class Program
    {
        static void Main(string[] args)
        {
            
            using (DataContext ctx = new DataContext())
            {
                AgencyAndCategory imain = new AgencyAndCategory(ctx);
                imain.execute();

                ImporterBase[] importers = new ImporterBase[]
                {
                    new AnimalsObject(ctx),
                    new CultureObject(ctx),
                    new diokk_actions(ctx),
                    new diokk_info(ctx),
                    new diokk_podved(ctx),
                    new diokk_rukovod(ctx),
                    new diokk_struct(ctx),
                    new ElectroPrice(ctx),
                    new GasPriceObject(ctx),
                    new GEOObjects(ctx),
                    new HuntResourcesInfo(ctx),
                    new HuntResourcesInfoForm1_3(ctx),
                    new HuntResourcesInfoForm1_4(ctx),
                    new HuntResourcesInfoForm1_5(ctx),
                    new HuntResourcesInfoForm2_1(ctx),
                    new HuntResourcesInfoForm3_1(ctx),
                    new HuntResourcesInfoForm4_2(ctx),
                    new HuntResourcesInfoForm4_4(ctx),
                    new HuntResourcesInfoForm4_5(ctx),
                    new HuntResourcesInfoForm5_1(ctx),
                    new HuntResourcesInfoForm5_2(ctx),
                    new HuntResourcesInfoForm5_3(ctx),
                    new HuntResourcesInfoForm5_4(ctx),
                    new HuntResourcesInfoForm5_4_3(ctx),
                    new HuntResourcesInfoForm5_4_4(ctx),
                    new HuntResourcesInfoForm5_4_5(ctx),
                    new HuntResourcesInfoForm5_4_6(ctx),
                    new HuntResourcesInfoForm5_5(ctx),
                    new HuntResourcesInfoForm5_6(ctx),
                    new HuntResourcesInfoForm6_1(ctx),
                    new HuntResourcesInfoFrom4_3(ctx),
                    new HuntResourcesInfoLand_1(ctx),
                    new IncomeRegional(ctx),
                    new MarketObject(ctx),
                    new MinSocialOrganizations(ctx),
                    new Parks(ctx),
                    new RegistryEducation(ctx),
                    new RegistryEducationApproved(ctx),
                    new TheaterObject(ctx),
                    new TotalPivot(ctx)
                };

                foreach (var importer in importers)
                {
                    try
                    {
                        importer.Execute();
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }
    }
}
