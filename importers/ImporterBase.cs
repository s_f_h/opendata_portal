﻿using importers.ImporterParametrs;
using Repository;
using Repository.Abstract;
using Repository.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;

namespace importers
{
    internal abstract class ImporterBase
    {
        const string request_template = "http://od.krasnodar.ru/{0}/data.xml";

        static DataContext _ctx;
        static IRepositoryBase<Agency, Guid> agencyRepository;
        static IRepositoryBase<Category, Guid> categoryRepository;
        static IRepositoryExtend<TableDisplayTemplate, Guid> tableDisplayTemplateRepository;
        static IRepositoryExtend<DataStructure, Guid> dataStructureRepository;
        static IRepositoryExtend<SetPassport, Guid> setPassportRepository;
        static IRepositoryExtend<Set, Guid> setRepository;
        static IRepositoryExtend<SetVersion, Guid> setVersionRepository;

        ImportingSetSettings[] importingSetsSettings;

        protected bool asIs = false;

        private ImporterBase(params ImportingSetSettings[] importingSetsSettings)
        {
            // Check importing sets settings
            foreach (var importingSetSettings in importingSetsSettings)
            {

            }

            this.importingSetsSettings = importingSetsSettings;
        }

        internal ImporterBase(DataContext ctx, params ImportingSetSettings[] importingSetsSettings)
            : this(importingSetsSettings)
        {
            if (_ctx == null)
            {
                _ctx = ctx;
                agencyRepository = RepositoryFactory.Get<IRepositoryBase<Agency, Guid>>(ctx);
                categoryRepository = RepositoryFactory.Get<IRepositoryBase<Category, Guid>>(ctx);
                tableDisplayTemplateRepository = RepositoryFactory.Get<IRepositoryExtend<TableDisplayTemplate, Guid>>(ctx);
                dataStructureRepository = RepositoryFactory.Get<IRepositoryExtend<DataStructure, Guid>>(ctx);
                setPassportRepository = RepositoryFactory.Get<IRepositoryExtend<SetPassport, Guid>>(ctx);
                setRepository = RepositoryFactory.Get<IRepositoryExtend<Set, Guid>>(ctx);
                setVersionRepository = RepositoryFactory.Get<IRepositoryExtend<SetVersion, Guid>>(ctx);
            }
        }

        public void Execute()
        {
            RepositoryOperationResult<bool> result;
            // create table display templates
            Dictionary<string, Guid> _templates = new Dictionary<string, Guid>();
            var templates = CreateTableDisplayTemplate();
            foreach (var template in templates)
            {
                result = tableDisplayTemplateRepository.Add(template.Value);
                if (!result.IsOK || !result.Result) throw new Exception(result.Message);
                _templates.Add(template.Key, template.Value.Id);
            }

            // create structure
            var dataStruct = CreateDataStructure(_templates);
            result = dataStructureRepository.Add(dataStruct);
            if (!result.IsOK || !result.Result) throw new Exception(result.Message);

            foreach (var importSetSettings in importingSetsSettings)
            {
                var agency = agencyRepository.FirstOrDefault(x => x.Id == importSetSettings.AgencyId).Result;
                var category = categoryRepository.FirstOrDefault(x => x.Id == importSetSettings.CategoryId).Result;
                DateTime creationDate = importSetSettings.CreationDate;
                var updatePeriodicity = importSetSettings.UpdatePeriodicity;

                // create set
                var set = CreateSet(category.Id, importSetSettings.Description, importSetSettings.Title, updatePeriodicity, agency);
                result = setRepository.Add(set);
                if (!result.IsOK || !result.Result) throw new Exception(result.Message);

                // create set versions, their passport and load data into set
                Guid? parentVersion_id = null;
                var lastImportingSetVersionSettings = importSetSettings.SetVersionSettings[0];
                int verId = 0;
                foreach (var importingSetVersionSettings in importSetSettings.SetVersionSettings)
                {
                    updatelastImportingSetVersionSettings(lastImportingSetVersionSettings, importingSetVersionSettings);
                    var setVer = CreateSetVersion(set.Id, dataStruct.Id, parentVersion_id, creationDate, verId, importingSetVersionSettings.IsCurrent);
                    result = setVersionRepository.Add(setVer);
                    if (!result.IsOK || !result.Result) throw new Exception(result.Message);

                    // create passport
                    var passport = CreateSetPassport(setVer.Id, agency, importSetSettings.CreationDate, creationDate, updatePeriodicity,
                        lastImportingSetVersionSettings.Creator,
                        lastImportingSetVersionSettings.Title, 
                        importingSetVersionSettings.Description,
                        importSetSettings.SetIdentifier,
                        lastImportingSetVersionSettings.PublisherName ,
                        lastImportingSetVersionSettings.PublisherPhone ,
                        lastImportingSetVersionSettings.PublisherMBox,
                        importingSetVersionSettings.Provinance,
                        string.IsNullOrEmpty(importingSetVersionSettings.Subject) ? lastImportingSetVersionSettings.Subject : importingSetVersionSettings.Subject);
                    //passport.dummy = importingSetVersionSettings.SourceId.ToString();
                    result = setPassportRepository.Add(passport);
                    if (!result.IsOK || !result.Result) throw new Exception(result.Message);

                    // import data
                    importData(importingSetVersionSettings.SourceId, passport.Id, dataStruct.TableName);

                    parentVersion_id = setVer.Id;
                    if (updatePeriodicity > Set.UpdatingPeriodicity.MoreTimesPerDay)
                        creationDate = creationDate.AddDays((int)updatePeriodicity);
                }
            }
        }

        private void updatelastImportingSetVersionSettings(ImportingSetVersionSettings lastImportingSetVersionSettings, ImportingSetVersionSettings importingSetVersionSettings)
        {
            if (!string.IsNullOrWhiteSpace(importingSetVersionSettings.Creator))
                lastImportingSetVersionSettings.Creator = importingSetVersionSettings.Creator;

            if (!string.IsNullOrWhiteSpace(importingSetVersionSettings.Description))
                lastImportingSetVersionSettings.Description = importingSetVersionSettings.Description;

            if (!string.IsNullOrWhiteSpace(importingSetVersionSettings.PublisherMBox))
                lastImportingSetVersionSettings.PublisherMBox = importingSetVersionSettings.PublisherMBox;

            if (!string.IsNullOrWhiteSpace(importingSetVersionSettings.PublisherName))
                lastImportingSetVersionSettings.PublisherName = importingSetVersionSettings.PublisherName;

            if (!string.IsNullOrWhiteSpace(importingSetVersionSettings.PublisherPhone))
                lastImportingSetVersionSettings.PublisherPhone = importingSetVersionSettings.PublisherPhone;

            if (!string.IsNullOrWhiteSpace(importingSetVersionSettings.Subject))
                lastImportingSetVersionSettings.Subject = importingSetVersionSettings.Subject;

            if (!string.IsNullOrWhiteSpace(importingSetVersionSettings.Title))
                lastImportingSetVersionSettings.Title = importingSetVersionSettings.Title;
        }

        protected abstract Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate();
        protected abstract DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates);
        protected abstract string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id);

        private Set CreateSet(Guid cat_id, string description, string title, Set.UpdatingPeriodicity updatePeriod, params Agency[] agencies)
        {
            return new Set
            {
                Agencies = agencies,
                Category_Id = cat_id,
                Description = description,
                Title = title,
                UpdatePeriodicity = updatePeriod
            };
        }
        private SetVersion CreateSetVersion(Guid set_Id, Guid dataStruct_Id, Guid? parentVer_Id, DateTime creationDate, int verId, bool isCurrent)
        {
            return new SetVersion
            {
                CodeName = creationDate.ToString("yyyyMMdd"),
                CreationDate = creationDate,
                DataStructure_Id = dataStruct_Id,
                IsAproved = true,
                IsCurrent = isCurrent,
                Name = string.Format("ver1{0:d3}", verId),
                ParentVersion_Id = parentVer_Id,
                Set_Id = set_Id
            };
        }
        private SetPassport CreateSetPassport(Guid ver_id, Agency agency, DateTime creationDate, DateTime modified, Set.UpdatingPeriodicity updatePeriod, string creator, string title,
            string description, string identifier, string publisher, string phone, string _mbox, string provinance, string subject)
        {
            return new SetPassport
            {
                Agency_Id = agency.Id,
                Created = creationDate,
                Creator = creator,
                Description = description,
                Identifier = string.Format("{0}-{1}", agency.ITN, identifier),
                MethodicStandartVersion = "3.1",
                Provinance = provinance ?? "",
                PublisherMBox = _mbox,
                PublisherName = publisher,
                PublisherPhone = phone,
                SetVersion_Id = ver_id,
                Subject = subject ?? "",
                Title = title,
                Valid = updatePeriod > Set.UpdatingPeriodicity.MoreTimesPerDay ? (DateTime?)modified.AddDays((int)updatePeriod) : null,
                Modified = modified
            };
        }


        object tryGetValue(string val)
        {
            if (string.IsNullOrWhiteSpace(val)) return (object)null;

            if (asIs) return val;

            int _int = int.MinValue;
            double _double = double.MinValue;
            decimal _decimal = decimal.MinValue;
            DateTime _datetime = DateTime.MinValue;
            TimeSpan _timeSpan = TimeSpan.MinValue;
            bool _bool = false;

            if (int.TryParse(val, out _int) && _int != int.MinValue)
                return _int;

            if (double.TryParse(val, out _double) && _double != double.MinValue)
                return _double;

            if (decimal.TryParse(val, out _decimal) && _decimal != decimal.MinValue)
                return _double;

            if (TimeSpan.TryParse(val, out _timeSpan) && _timeSpan != TimeSpan.MinValue)
                return _timeSpan;

            if (DateTime.TryParse(val, out _datetime) && _datetime != DateTime.MinValue)
                return _datetime;

            if (bool.TryParse(val, out _bool))
                return _bool ? 1 : 0;

            return val;
        }
        void parseDocument(XmlNodeList rows, List<Dictionary<string, object>> result)
        {

            foreach (var row in rows)
            {
                Dictionary<string, object> _row = new Dictionary<string, object>();
                foreach (XmlNode node in (XmlNode)row)
                    parseCollumn(node, _row);
                result.Add(_row);
            }
        }
        void parseCollumn(XmlNode node, Dictionary<string, object> result, string parent = null)
        {
            if (node.Name == "Set" || node.Name == "Parametrs")
                if (node.ChildNodes.Count > 2)
                {
                    foreach (XmlNode _node in node.ChildNodes)
                    {
                        if (_node.Name != "Parametrs") continue;
                        var _parent = string.Format("{0}{1}_", parent, node["Name"].InnerText.Replace('(', '_').Replace(')', '_'));
                        parseCollumn(_node, result, _parent);
                    }
                }
                else
                {
                    var key = string.Format("{0}{1}", parent, node["Name"].InnerText.Replace('(', '_').Replace(')', '_'));
                    if (key.EndsWith("_")) key = key.Remove(key.Length - 1);

                    result.Add(key, tryGetValue(node["Value"].InnerText));
                }
        }
        void importData(Guid key, Guid passportId, string table)
        {
            string url = string.Format(request_template, key);
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Exception ex = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            XmlDocument doc = new XmlDocument();

            using (var response = request.GetResponse())
            {
                try
                {
                    using (var stream = response.GetResponseStream())
                    {
                        try
                        {
                            doc.Load(stream);
                        }
                        catch (Exception e)
                        {
                            ex = e;
                        }
                    }
                }
                catch (Exception e)
                {
                    ex = e;
                }
            }

            if (ex != null) throw ex;

            parseDocument(doc.ChildNodes[0].ChildNodes, rows);

            if (asIs)
            {
                foreach (var row in rows)
                {

                    try
                    {
                        string cmd = GenerateInsertQuery(table, row, passportId);
                        _ctx.Database.ExecuteSqlCommand(cmd);
                    }
                    catch(Exception e)
                    {
                    }
                }
            }
            else
            {
                foreach (var row in rows)
                {

                    if (table == "CultureObject")
                    {
                        try
                        {
                            string cmd = GenerateInsertQuery(table, row, passportId);
                            _ctx.Database.ExecuteSqlCommand(cmd);
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    else
                    {
                        string cmd = GenerateInsertQuery(table, row, passportId);
                        _ctx.Database.ExecuteSqlCommand(cmd);
                    }
                }
            }
        }
    }
}
