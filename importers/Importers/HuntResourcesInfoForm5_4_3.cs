﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_4_3 : ImporterBase
    {
        public HuntResourcesInfoForm5_4_3(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_narusheniya_categorii",
                Title = "Сведения о категориях нарушений",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о категориях нарушений",
                        SourceId = Guid.Parse("9F9E746A-3D14-47EC-A531-DD2E70C2133A")
                    }
                }
            }) { }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm5_4_3", new TableDisplayTemplate
            {
                Description = "Сведения о категориях нарушений",
                Name = "HuntResourcesInfoForm5_4_3",
                Title = "Сведения о категориях нарушений",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "TerritoryNameF5_4_3",
                         DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },

                     new ColumnDisplayTemplate
                     {
                        #region
                         Header = "СТАТЬЯ 7.11 АДМИНИСТРАТИВНОГО КОДЕКСА",
                         Order = 1,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения о категориях нарушений. СТАТЬЯ 7.11",
                             Name = "HuntResourcesInfoForm5_4_3_st_7_11",
                             Title = "Сведения о категориях нарушений. СТАТЬЯ 7.11",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry7_11F5_4_3__NumberOfCasesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 7.11 АДМИНИСТРАТИВНОГО КОДЕКСА	Кол-во случаев, ед.",
                                     DescritpionEn = "",
                                     Header = "Кол-во случаев, ед.",
                                     IncludeIntoFilter = true,
                                     Order = 2,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry7_11F5_4_3__ExactFeesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 7.11 АДМИНИСТРАТИВНОГО КОДЕКСА	Наложено/взыскано штрафов тыс. руб.",
                                     DescritpionEn = "",
                                     Header = "Наложено/взыскано штрафов тыс. руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 3,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry7_11F5_4_3__NumberOfClaimsF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 7.11 АДМИНИСТРАТИВНОГО КОДЕКСА	Предъявлено/взыскано исков, тыс. руб.",
                                     DescritpionEn = "Предъявлено/взыскано исков, тыс. руб.",
                                     Header = "Предъявлено/взыскано исков, тыс. руб.",
                                     IncludeIntoFilter = true,
                                     Order = 4,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                        #endregion
                     },

                     new ColumnDisplayTemplate
                     {
                        #region
                         Header = "СТАТЬЯ 8.35 АДМИНИСТРАТИВНОГО КОДЕКСА",
                         Order = 5,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения о категориях нарушений. СТАТЬЯ 8.35",
                             Name = "HuntResourcesInfoForm5_4_3_st_8_35",
                             Title = "Сведения о категориях нарушений. СТАТЬЯ 8.35",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_35F5_4_3__NumberOfCasesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.35 АДМИНИСТРАТИВНОГО КОДЕКСА	Кол-во случаев, ед.",
                                     DescritpionEn = "",
                                     Header = "Кол-во случаев, ед.",
                                     IncludeIntoFilter = true,
                                     Order = 6,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_35F5_4_3__ExactFeesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.35 АДМИНИСТРАТИВНОГО КОДЕКСА	Наложено/взыскано штрафов тыс. руб.",
                                     DescritpionEn = "",
                                     Header = "Наложено/взыскано штрафов тыс. руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 7,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_35F5_4_3__NumberOfClaimsF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.35 АДМИНИСТРАТИВНОГО КОДЕКСА	Предъявлено/взыскано исков, тыс. руб.",
                                     DescritpionEn = "Предъявлено/взыскано исков, тыс. руб.",
                                     Header = "Предъявлено/взыскано исков, тыс. руб.",
                                     IncludeIntoFilter = true,
                                     Order = 8,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                        #endregion
                     },

                     new ColumnDisplayTemplate
                     {
                        #region
                         Header = "СТАТЬЯ 8.36 АДМИНИСТРАТИВНОГО КОДЕКСА",
                         Order = 9,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения о категориях нарушений. СТАТЬЯ 8.36",
                             Name = "HuntResourcesInfoForm5_4_3_st_8_36",
                             Title = "Сведения о категориях нарушений. СТАТЬЯ 8.36",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_36F5_4_3__NumberOfCasesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.36 АДМИНИСТРАТИВНОГО КОДЕКСА	Кол-во случаев, ед.",
                                     DescritpionEn = "",
                                     Header = "Кол-во случаев, ед.",
                                     IncludeIntoFilter = true,
                                     Order = 10,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_36F5_4_3__ExactFeesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.36 АДМИНИСТРАТИВНОГО КОДЕКСА	Наложено/взыскано штрафов тыс. руб.",
                                     DescritpionEn = "",
                                     Header = "Наложено/взыскано штрафов тыс. руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 11,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_36F5_4_3__NumberOfClaimsF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.36 АДМИНИСТРАТИВНОГО КОДЕКСА	Предъявлено/взыскано исков, тыс. руб.",
                                     DescritpionEn = "Предъявлено/взыскано исков, тыс. руб.",
                                     Header = "Предъявлено/взыскано исков, тыс. руб.",
                                     IncludeIntoFilter = true,
                                     Order = 12,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                        #endregion
                     },

                     new ColumnDisplayTemplate
                     {
                        #region
                         Header = "СТАТЬЯ 8.37 АДМИНИСТРАТИВНОГО КОДЕКСА",
                         Order = 13,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения о категориях нарушений. СТАТЬЯ 8.37",
                             Name = "HuntResourcesInfoForm5_4_3_st_8_37",
                             Title = "Сведения о категориях нарушений. СТАТЬЯ 8.37",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_37F5_4_3__NumberOfCasesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.37 АДМИНИСТРАТИВНОГО КОДЕКСА	Кол-во случаев, ед.",
                                     DescritpionEn = "",
                                     Header = "Кол-во случаев, ед.",
                                     IncludeIntoFilter = true,
                                     Order = 14,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_37F5_4_3__ExactFeesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.37 АДМИНИСТРАТИВНОГО КОДЕКСА	Наложено/взыскано штрафов тыс. руб.",
                                     DescritpionEn = "",
                                     Header = "Наложено/взыскано штрафов тыс. руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 15,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_37F5_4_3__NumberOfClaimsF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.37 АДМИНИСТРАТИВНОГО КОДЕКСА	Предъявлено/взыскано исков, тыс. руб.",
                                     DescritpionEn = "Предъявлено/взыскано исков, тыс. руб.",
                                     Header = "Предъявлено/взыскано исков, тыс. руб.",
                                     IncludeIntoFilter = true,
                                     Order = 16,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                        #endregion
                     },

                     new ColumnDisplayTemplate
                     {
                        #region
                         Header = "СТАТЬЯ 19.4 АДМИНИСТРАТИВНОГО КОДЕКСА",
                         Order = 17,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения о категориях нарушений. СТАТЬЯ 7.11",
                             Name = "HuntResourcesInfoForm5_4_3_st_19_4",
                             Title = "Сведения о категориях нарушений. СТАТЬЯ 19.4",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry19_4F5_4_3__NumberOfCasesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 19.4 АДМИНИСТРАТИВНОГО КОДЕКСА	Кол-во случаев, ед.",
                                     DescritpionEn = "",
                                     Header = "Кол-во случаев, ед.",
                                     IncludeIntoFilter = true,
                                     Order = 18,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry19_4F5_4_3__ExactFeesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 19.4 АДМИНИСТРАТИВНОГО КОДЕКСА	Наложено/взыскано штрафов тыс. руб.",
                                     DescritpionEn = "",
                                     Header = "Наложено/взыскано штрафов тыс. руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 19,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry19_4F5_4_3__NumberOfClaimsF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 19.4 АДМИНИСТРАТИВНОГО КОДЕКСА	Предъявлено/взыскано исков, тыс. руб.",
                                     DescritpionEn = "Предъявлено/взыскано исков, тыс. руб.",
                                     Header = "Предъявлено/взыскано исков, тыс. руб.",
                                     IncludeIntoFilter = true,
                                     Order = 20,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                        #endregion
                     },

                     new ColumnDisplayTemplate
                     {
                        #region
                         Header = "СТАТЬЯ 258 УГОЛОВНОГО КОДЕКСА",
                         Order = 21,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения о категориях нарушений. СТАТЬЯ 258",
                             Name = "HuntResourcesInfoForm5_4_3_st_258",
                             Title = "Сведения о категориях нарушений. СТАТЬЯ 258",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry258F5_4_3__NumberOfCasesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 258 УГОЛОВНОГО КОДЕКСА	Кол-во случаев, ед.",
                                     DescritpionEn = "",
                                     Header = "Кол-во случаев, ед.",
                                     IncludeIntoFilter = true,
                                     Order = 22,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry258F5_4_3__ExactFeesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 258 УГОЛОВНОГО КОДЕКСА	Наложено/взыскано штрафов тыс. руб.",
                                     DescritpionEn = "",
                                     Header = "Наложено/взыскано штрафов тыс. руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 23,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry258F5_4_3__NumberOfClaimsF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 258 УГОЛОВНОГО КОДЕКСА	Предъявлено/взыскано исков, тыс. руб.",
                                     DescritpionEn = "Предъявлено/взыскано исков, тыс. руб.",
                                     Header = "Предъявлено/взыскано исков, тыс. руб.",
                                     IncludeIntoFilter = true,
                                     Order = 24,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                        #endregion
                     },

                     new ColumnDisplayTemplate
                     {
                        #region
                         Header = "СТАТЬЯ 7.2 АДМИНИСТРАТИВНОГО КОДЕКСА",
                         Order = 25,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения о категориях нарушений. СТАТЬЯ 7.2",
                             Name = "HuntResourcesInfoForm5_4_3_st_7_2",
                             Title = "Сведения о категориях нарушений. СТАТЬЯ 7.2",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry7_2F5_4_3__NumberOfCasesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 7.2 АДМИНИСТРАТИВНОГО КОДЕКСА Кол-во случаев, ед.",
                                     DescritpionEn = "",
                                     Header = "Кол-во случаев, ед.",
                                     IncludeIntoFilter = true,
                                     Order = 26,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry7_2F5_4_3__ExactFeesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 7.2 АДМИНИСТРАТИВНОГО КОДЕКСА Наложено/взыскано штрафов тыс. руб.",
                                     DescritpionEn = "",
                                     Header = "Наложено/взыскано штрафов тыс. руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 27,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry7_2F5_4_3__NumberOfClaimsF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 7.2 АДМИНИСТРАТИВНОГО КОДЕКСА Предъявлено/взыскано исков, тыс. руб.",
                                     DescritpionEn = "Предъявлено/взыскано исков, тыс. руб.",
                                     Header = "Предъявлено/взыскано исков, тыс. руб.",
                                     IncludeIntoFilter = true,
                                     Order = 28,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                        #endregion
                     },

                     new ColumnDisplayTemplate
                     {
                        #region
                         Header = "СТАТЬЯ 8.33 АДМИНИСТРАТИВНОГО КОДЕКСА",
                         Order = 29,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения о категориях нарушений. СТАТЬЯ 8.33",
                             Name = "HuntResourcesInfoForm5_4_3_st_8_33",
                             Title = "Сведения о категориях нарушений. СТАТЬЯ 8.33",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_33F5_4_3__NumberOfCasesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.33 АДМИНИСТРАТИВНОГО КОДЕКСА	Кол-во случаев, ед.",
                                     DescritpionEn = "",
                                     Header = "Кол-во случаев, ед.",
                                     IncludeIntoFilter = true,
                                     Order = 30,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_33F5_4_3__ExactFeesF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.33 АДМИНИСТРАТИВНОГО КОДЕКСА Наложено/взыскано штрафов тыс. руб.",
                                     DescritpionEn = "",
                                     Header = "Наложено/взыскано штрафов тыс. руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 31,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "Entry8_33F5_4_3__NumberOfClaimsF5_4_3",
                                     DescriptionRu = "СТАТЬЯ 8.33 АДМИНИСТРАТИВНОГО КОДЕКСА Предъявлено/взыскано исков, тыс. руб.",
                                     DescritpionEn = "Предъявлено/взыскано исков, тыс. руб.",
                                     Header = "Предъявлено/взыскано исков, тыс. руб.",
                                     IncludeIntoFilter = true,
                                     Order = 32,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                        #endregion
                     }
                }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_4_3"],
                Name = "HuntResourcesInfoForm5_4_3",
                Description = "Сведения о категориях нарушений",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_4_3"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{26}] (TerritoryNameF5_4_3, " +
                "Entry7_2F5_4_3__NumberOfCasesF5_4_3,  Entry7_2F5_4_3__ExactFeesF5_4_3,  Entry7_2F5_4_3__NumberOfClaimsF5_4_3, " +
                "Entry7_11F5_4_3__NumberOfCasesF5_4_3, Entry7_11F5_4_3__ExactFeesF5_4_3, Entry7_11F5_4_3__NumberOfClaimsF5_4_3, " +
                "Entry8_33F5_4_3__NumberOfCasesF5_4_3, Entry8_33F5_4_3__ExactFeesF5_4_3, Entry8_33F5_4_3__NumberOfClaimsF5_4_3, " +
                "Entry8_35F5_4_3__NumberOfCasesF5_4_3, Entry8_35F5_4_3__ExactFeesF5_4_3, Entry8_35F5_4_3__NumberOfClaimsF5_4_3, " +
                "Entry8_36F5_4_3__NumberOfCasesF5_4_3, Entry8_36F5_4_3__ExactFeesF5_4_3, Entry8_36F5_4_3__NumberOfClaimsF5_4_3, " +
                "Entry8_37F5_4_3__NumberOfCasesF5_4_3, Entry8_37F5_4_3__ExactFeesF5_4_3, Entry8_37F5_4_3__NumberOfClaimsF5_4_3, " +
                "Entry19_4F5_4_3__NumberOfCasesF5_4_3, Entry19_4F5_4_3__ExactFeesF5_4_3, Entry19_4F5_4_3__NumberOfClaimsF5_4_3, " +
                "Entry258F5_4_3__NumberOfCasesF5_4_3,  Entry258F5_4_3__ExactFeesF5_4_3,  Entry258F5_4_3__NumberOfClaimsF5_4_3, SetsPassports_Id) " +
                "VALUES ('{0}', {1}, '{2}', '{3}', {4}, '{5}', '{6}', {7}, '{8}', '{9}', {10}, '{11}', '{12}', {13}, '{14}', '{15}', {16}, '{17}', '{18}', " +
                "{19}, '{20}', '{21}', {22}, '{23}', '{24}', '{25}')", row["TerritoryNameF5_4_3"],
                row["Entry7_2F5_4_3__NumberOfCasesF5_4_3"], row["Entry7_2F5_4_3__ExactFeesF5_4_3"], row["Entry7_2F5_4_3__NumberOfClaimsF5_4_3"],
                row["Entry7_11F5_4_3__NumberOfCasesF5_4_3"], row["Entry7_11F5_4_3__ExactFeesF5_4_3"], row["Entry7_11F5_4_3__NumberOfClaimsF5_4_3"],
                row["Entry8_33F5_4_3__NumberOfCasesF5_4_3"], row["Entry8_33F5_4_3__ExactFeesF5_4_3"], row["Entry8_33F5_4_3__NumberOfClaimsF5_4_3"],
                row["Entry8_35F5_4_3__NumberOfCasesF5_4_3"], row["Entry8_35F5_4_3__ExactFeesF5_4_3"], row["Entry8_35F5_4_3__NumberOfClaimsF5_4_3"],
                row["Entry8_36F5_4_3__NumberOfCasesF5_4_3"], row["Entry8_36F5_4_3__ExactFeesF5_4_3"], row["Entry8_36F5_4_3__NumberOfClaimsF5_4_3"],
                row["Entry8_37F5_4_3__NumberOfCasesF5_4_3"], row["Entry8_37F5_4_3__ExactFeesF5_4_3"], row["Entry8_37F5_4_3__NumberOfClaimsF5_4_3"],
                row["Entry19_4F5_4_3__NumberOfCasesF5_4_3"], row["Entry19_4F5_4_3__ExactFeesF5_4_3"], row["Entry19_4F5_4_3__NumberOfClaimsF5_4_3"],
                row["Entry258F5_4_3__NumberOfCasesF5_4_3"], row["Entry258F5_4_3__ExactFeesF5_4_3"], row["Entry258F5_4_3__NumberOfClaimsF5_4_3"],
                passport_id, tableName);
        }
    }
}
