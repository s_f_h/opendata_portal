﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm6_1 : ImporterBase
    {
        public HuntResourcesInfoForm6_1(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_ohot_uslugi",
                Title = "Сведения об оказываемых услугах в сфере охотничьего хозяйства",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения об оказываемых услугах в сфере охотничьего хозяйства",
                        SourceId = Guid.Parse("cdc38400-99ad-4457-a114-4443ad3f0076")
                    }
                }
            }) { }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm6_1", new TableDisplayTemplate
            {
                Description = "Сведения об особо охраняемых природных территориях регионального значения",
                Name = "HuntResourcesInfoForm6_1",
                Title = "Сведения об особо охраняемых природных территориях регионального значения",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "OrgNameF6_1",
                         DescriptionRu = "НАИМЕНОВАНИЕ ЮРИДИЧЕСКОГО ЛИЦА, ИНДИВИДУАЛЬНОГО ПРЕДПРИНИМАТЕЛЯ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ ЮРИДИЧЕСКОГО ЛИЦА, ИНДИВИДУАЛЬНОГО ПРЕДПРИНИМАТЕЛЯ",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "OrgFormF6_1",
                         DescriptionRu = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА ПО ОКОПФ",
                         DescritpionEn = "",
                         Header = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА ПО ОКОПФ",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "SrevicesF6_1",
                         DescriptionRu = "ОКАЗЫВАЕМЫЕ УСЛУГИ В СООТВЕТСТВИИ С ОБЩЕРОССИЙСКИМ КЛАССИФИКАТОРОМ ВИДОВ ЭКОНОМИЧЕСКОЙ ДЕЯТЕЛЬНОСТИ, ПРОДУКЦИИ И УСЛУГ (ОКДП)",
                         DescritpionEn = "",
                         Header = "ОКАЗЫВАЕМЫЕ УСЛУГИ В СООТВЕТСТВИИ С ОБЩЕРОССИЙСКИМ КЛАССИФИКАТОРОМ ВИДОВ ЭКОНОМИЧЕСКОЙ ДЕЯТЕЛЬНОСТИ, ПРОДУКЦИИ И УСЛУГ (ОКДП)",
                         IncludeIntoFilter = true,
                         Order = 2,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
                     new ColumnDisplayTemplate
                     {
                         Header = "ОБЪЕМЫ",
                         Order = 3,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Сведения об особо охраняемых природных территориях регионального значения. Объемы",
                             Name = "HuntResourcesInfoForm6_1_values",
                             Title = "Сведения об особо охраняемых природных территориях регионального значения. Объемы",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "AmountF6_1_UnitMesF6_1",
                                     DescriptionRu = "ОБЪЕМЫ единица измерения",
                                     DescritpionEn = "",
                                     Header = "единица измерения	",
                                     IncludeIntoFilter = true,
                                     Order = 4,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "AmountF6_1_TotalF6_1",
                                     DescriptionRu = "ОБЪЕМЫ всего единиц",
                                     DescritpionEn = "",
                                     Header = "всего единиц	",
                                     IncludeIntoFilter = true,
                                     Order = 5,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "AmountF6_1_PricelF6_1",
                                     DescriptionRu = "ОБЪЕМЫ цена за единицу, руб.",
                                     DescritpionEn = "",
                                     Header = "цена за единицу, руб.	",
                                     IncludeIntoFilter = true,
                                     Order = 6,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "AmountF6_1_CostF6_1",
                                     DescriptionRu = "ОБЪЕМЫ стоимость всего, руб.",
                                     DescritpionEn = "",
                                     Header = "стоимость всего, руб.",
                                     IncludeIntoFilter = true,
                                     Order = 7,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 }
                             }
                         }
                     }
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm6_1"],
                Name = "HuntResourcesInfoForm6_1",
                Description = "Сведения об оказываемых услугах в сфере охотничьего хозяйства",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm6_1"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (OrgNameF6_1, OrgFormF6_1, SrevicesF6_1, AmountF6_1_UnitMesF6_1, AmountF6_1_TotalF6_1, AmountF6_1_PricelF6_1, AmountF6_1_CostF6_1, SetsPassports_Id) " +
                "VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')", tableName, row["OrgNameF6_1"], row["OrgFormF6_1"], row["SrevicesF6_1"], row["AmountF6_1_UnitMesF6_1"],
                row["AmountF6_1_TotalF6_1"], row["AmountF6_1_PricelF6_1"], row["AmountF6_1_CostF6_1"], passport_id);
        }
    }
}
