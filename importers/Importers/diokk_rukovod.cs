﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class diokk_rukovod : ImporterBase
    {
        public diokk_rukovod(DataContext ctx)
            : base(ctx,
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.dep_imuszh],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.stroitelstvo],
                    CreationDate = DateTime.Now,
                    Description = "Сведения о руководителе департамента и его заместителях с указанием должности, фамилии, имени, отчества, номера телефона приемной, адреса электронной почты приемной",
                    SetIdentifier = "rukovod",
                    Title = "Руководство департамента имущественных отношений Краснодарского края",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Департамент имущественных отношений Краснодарского края",
                            Description= "Сведения о руководителе департамента и его заместителях с указанием должности, фамилии, имени, отчества, номера телефона приемной, адреса электронной почты приемной",
                            IsCurrent = true,
                            PublisherMBox = "ok.post@diok.ru",
                            PublisherName ="Е.С. Новикова",
                            PublisherPhone = "+7-861-2682954",
                            SourceId = Guid.Parse("02268D35-A85F-43E1-A15A-9B10ECD59C93"),
                            Subject = "Руководство департамента имущественных отношений Краснодарского края",
                            Title = "Руководство департамента имущественных отношений Краснодарского края",
                            #endregion
                        }
                    }
                    #endregion
                })
        {
            asIs = true;
        }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("diokk_rukovod",
                new TableDisplayTemplate
                {
                    Description =
                        "Сведения о руководителе департамента и его заместителях с указанием должности, фамилии, имени, отчества, номера телефона приемной, адреса электронной почты приемной",
                    Title = "Руководство департамента имущественных отношений Краснодарского края",
                    Name = "diokk_rukovod",
                    Type = TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                    {
                        #region
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "LastName",
                            DescriptionRu = "ФАМИЛИЯ",
                            DescritpionEn = "",
                            Header = "ФАМИЛИЯ",
                            IncludeIntoFilter = true,
                            Order = 0,
                            Type = ColumnDisplayTemplate.ContentType.String,
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "FirstName",
                            DescriptionRu = "ИМЯ",
                            DescritpionEn = "",
                            Header = "ИМЯ",
                            IncludeIntoFilter = true,
                            Order = 1,
                            Type = ColumnDisplayTemplate.ContentType.String,
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "MidleName",
                            DescriptionRu = "ОТЧЕСТВО",
                            DescritpionEn = "",
                            Header = "ОТЧЕСТВО",
                            IncludeIntoFilter = true,
                            Order = 2,
                            Type = ColumnDisplayTemplate.ContentType.String,
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "Post",
                            DescriptionRu = "ДОЛЖНОСТЬ",
                            DescritpionEn = "",
                            Header = "ДОЛЖНОСТЬ",
                            IncludeIntoFilter = true,
                            Order = 3,
                            Type = ColumnDisplayTemplate.ContentType.String,
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "Phone",
                            DescriptionRu = "НОМЕР ТЕЛЕФОНА ПРИЕМНОЙ",
                            DescritpionEn = "",
                            Header = "НОМЕР ТЕЛЕФОНА ПРИЕМНОЙ",
                            IncludeIntoFilter = true,
                            Order = 4,
                            Type = ColumnDisplayTemplate.ContentType.String,
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "Email",
                            DescriptionRu = "АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ ПРИЕМНОЙ",
                            DescritpionEn = "",
                            Header = "АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ ПРИЕМНОЙ",
                            IncludeIntoFilter = true,
                            Order = 5,
                            Type = ColumnDisplayTemplate.ContentType.String,
                        }
                        #endregion
                    }
                });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["diokk_rukovod"],
                Name = "diokk_rukovod",
                Description = "Руководство департамента имущественных отношений Краснодарского края",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "diokk_rukovod"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return
                string.Format(
                    "INSERT INTO [dbo].[{0}] (LastName, FirstName, MidleName, Post, Phone, Email, SetsPassports_Id) " +
                    "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')",
                    tableName, row["LastName"] ?? "null", row["FirstName"] ?? "null",
                    row["MidleName"] ?? "null", row["Post"] ?? "null",
                    row["Phone"] ?? "null", row["Email"] ?? "null",
                    passport_id);
        }
    }
}
