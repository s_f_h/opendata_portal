﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm1_3 : ImporterBase
    {
        public HuntResourcesInfoForm1_3(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2015, 1, 1),
                Description =
                    "Документированная информация о плодовитости копытных животных, отнесенных к охотничьим ресурсам",
                SetIdentifier = "gosohotrestr_plodovitost_kopitnih",
                Title =
                    "Документированная информация о плодовитости копытных животных, отнесенных к охотничьим ресурсам",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description =
                            "Документированная информация о плодовитости копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014  г.",
                            IsCurrent = true,
                        Provinance = "Обновление данных",
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName =
                            "Белоусов Игорь Валерьевич, ведущий консультант отдела охраны, воспроизводства и использования объектов животного мира и среды их обитания",
                        PublisherPhone = "+7-861-2591971",
                        SourceId = Guid.Parse("CD5D19F8-0389-4F24-8F9A-1B4E5F15265E"),
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title =
                            "Документированная информация о плодовитости копытных животных, отнесенных к охотничьим ресурсам в сезоне 2013 - 2014 гг."
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm1_3", new TableDisplayTemplate
            {
                Description =
                    "Документированная информация о плодовитости копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014  г.",
                Name = "HuntResourcesInfoForm1_3",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "HuntResourceType",
                        DescriptionRu = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                        DescritpionEn = "",
                        Header = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FemalesHuntedTotal",
                        DescriptionRu = "ВСЕГО ДОБЫТО САМОК, ОСОБЕЙ",
                        DescritpionEn = "",
                        Header = "ВСЕГО ДОБЫТО САМОК, ОСОБЕЙ",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "YoungerOneYear",
                        DescriptionRu = "ДО 1 ГОДА",
                        DescritpionEn = "",
                        Header = "ДО 1 ГОДА",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ПОЛУТОРОГОДОВАЛЫХ",
                        Order = 3,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Полуторогодовалых добыто",
                            Name = "OneYearAndHalf",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OneYearAndHalf_Total",
                                    DescriptionRu =
                                        "Всего",
                                    DescritpionEn = "",
                                    Header =
                                        "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OneYearAndHalf_Farrow",
                                    DescriptionRu =
                                        "Яловых",
                                    DescritpionEn = "",
                                    Header =
                                        "Яловых",
                                    IncludeIntoFilter = true,
                                    Order = 6,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OneYearAndHalf_PregnantOne",
                                    DescriptionRu =
                                        "Стельных 1 эмбрион",
                                    DescritpionEn = "",
                                    Header =
                                        "Стельных 1 эмбрион",
                                    IncludeIntoFilter = true,
                                    Order = 7,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OneYearAndHalf_PregnantTwo",
                                    DescriptionRu =
                                        "Стельных 2 эмбриона",
                                    DescritpionEn = "",
                                    Header =
                                        "Стельных 2 эмбриона",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OneYearAndHalf_PregnantThree",
                                    DescriptionRu =
                                        "Стельных 3 эмбриона",
                                    DescritpionEn = "",
                                    Header =
                                        "Стельных 3 эмбриона",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ВЗРОСЛЫХ",
                        Order = 9,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Взрослых добыто",
                            Name = "Adult",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Adult_Total",
                                    DescriptionRu =
                                        "Всего",
                                    DescritpionEn = "",
                                    Header =
                                        "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 10,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Adult_Farrow",
                                    DescriptionRu =
                                        "Яловых",
                                    DescritpionEn = "",
                                    Header =
                                        "Яловых",
                                    IncludeIntoFilter = true,
                                    Order = 11,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Adult_PregnantOne",
                                    DescriptionRu =
                                        "Стельных 1 эмбрион",
                                    DescritpionEn = "",
                                    Header =
                                        "Стельных 1 эмбрион",
                                    IncludeIntoFilter = true,
                                    Order = 12,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Adult_PregnantTwo",
                                    DescriptionRu =
                                        "Стельных 2 эмбриона",
                                    DescritpionEn = "",
                                    Header =
                                        "Стельных 2 эмбриона",
                                    IncludeIntoFilter = true,
                                    Order = 13,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Adult_PregnantThree",
                                    DescriptionRu =
                                        "Стельных 3 эмбриона",
                                    DescritpionEn = "",
                                    Header =
                                        "Стельных 3 эмбриона",
                                    IncludeIntoFilter = true,
                                    Order = 14,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                        }
                    },

                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm1_3"],
                Name = "HuntResourcesInfoForm1_3",
                Description =
                    "Документированная информация о плодовитости копытных животных, отнесенных к охотничьим ресурсам",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm1_3"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (HuntResourceType, FemalesHuntedTotal, " +
                                 "YoungerOneYear, OneYearAndHalf_Total, " +
                                 "OneYearAndHalf_Farrow, OneYearAndHalf_PregnantOne, " +
                                 "OneYearAndHalf_PregnantTwo, OneYearAndHalf_PregnantThree, " +
                                 "Adult_Total, Adult_Farrow, " +
                                 "Adult_PregnantOne, Adult_PregnantTwo, " +
                                 "Adult_PregnantThree, SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}')",
                                 tableName,
                                 row["HuntResourceType"], row["FemalesHuntedTotal"],
                                 row["YoungerOneYear"], row["OneYearAndHalf_Total"],
                                 row["OneYearAndHalf_Farrow"], row["OneYearAndHalf_PregnantOne"],
                                 row["OneYearAndHalf_PregnantTwo"], row["OneYearAndHalf_PregnantThree"],
                                 row["Adult_Total"], row["Adult_Farrow"],
                                 row["Adult_PregnantOne"], row["Adult_PregnantTwo"],
                                 row["Adult_PregnantThree"], passport_id);
        }
    }
}