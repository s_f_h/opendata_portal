﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm2_1 : ImporterBase
    {
        public HuntResourcesInfoForm2_1(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2015, 1, 1),
                Description =
                    "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotrestr_ohot_ugodiya_2",
                Title =
                    "Сведения об охотничьих угодьях в субъекте Российской Федерации",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description =
                            "Документированная информация об охотничьих угодьях в субъекте российской федерации по состоянию на 1 мая 2014 года",
                            IsCurrent = true,
                        Provinance = "Обновление данных",
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName =
                            "Белоусов Игорь Валерьевич, ведущий консультант отдела охраны, воспроизводства и использования объектов животного мира и среды их обитания",
                        PublisherPhone = "+7-861-2591971",
                        SourceId = Guid.Parse("BC8C91CB-FDC8-4B05-B3FC-0012370AE518"),
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title =
                            "Сведения об охотничьих угодьях в субъекте Российской Федерации"
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm2_1", new TableDisplayTemplate
            {
                Description =
                    "Сведения об охотничьих угодьях в субъекте Российской Федерации",
                Name = "HuntResourcesInfoForm2_1",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "MO_NameF2_1",
                        DescriptionRu = "НАИМЕНОВАНИЕ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TotalSpaceF2_1",
                        DescriptionRu = "ОБЩАЯ ПЛОЩАДЬ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ, ТЫС. ГА",
                        DescritpionEn = "",
                        Header = "ОБЩАЯ ПЛОЩАДЬ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ, ТЫС. ГА",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ОБЩАЯ ПЛОЩАДЬ ОХОТНИЧЬИХ УГОДИЙ",
                        Order = 2,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "HuntTotalSpaceF2_1_FousandsGaF2_1",
                                DescriptionRu = "Тыс. га",
                                DescritpionEn = "",
                                Header = "Тыс. га",
                                IncludeIntoFilter = true,
                                Order = 3,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "HuntTotalSpaceF2_1_Mo_TotalSpacePercentF2_1",
                                DescriptionRu = "% от общей площади муниципального образования",
                                DescritpionEn = "",
                                Header = "% от общей площади муниципального образования",
                                IncludeIntoFilter = true,
                                Order = 4,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ПЛОЩАДЬ ОБЩЕДОСТУПНЫХ ОХОТНИЧЬИХ УГОДИЙ",
                        Order = 5,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "FreeHuntSpaceF2_1_FousandsGaF2_1",
                                DescriptionRu = "Тыс. га",
                                DescritpionEn = "",
                                Header = "Тыс. га",
                                IncludeIntoFilter = true,
                                Order = 6,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "FreeHuntSpaceF2_1_HuntTotalSpacePercentF2_1",
                                DescriptionRu = "% от общей площади муниципального образования",
                                DescritpionEn = "",
                                Header = "% от общей площади муниципального образования",
                                IncludeIntoFilter = true,
                                Order = 7,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ВЫДЕЛЕННАЯ КВОТА, ОСОБЕЙ",
                        Order = 8,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "ClippedHuntSpaceF2_1_FousandsGaF2_1",
                                DescriptionRu = "Тыс. га",
                                DescritpionEn = "",
                                Header = "Тыс. га",
                                IncludeIntoFilter = true,
                                Order = 9,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "ClippedHuntSpaceF2_1_HuntTotalSpacePercentF2_1",
                                DescriptionRu = "% от общей площади муниципального образования",
                                DescritpionEn = "",
                                Header = "% от общей площади муниципального образования",
                                IncludeIntoFilter = true,
                                Order = 10,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                        }
                    }
                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm2_1"],
                Name = "HuntResourcesInfoForm2_1",
                Description =
                    "Сведения об охотничьих угодьях в субъекте Российской Федерации",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm2_1"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (MO_NameF2_1, TotalSpaceF2_1, " +
                                 "HuntTotalSpaceF2_1_FousandsGaF2_1, HuntTotalSpaceF2_1_Mo_TotalSpacePercentF2_1, " +
                                 "FreeHuntSpaceF2_1_FousandsGaF2_1, FreeHuntSpaceF2_1_HuntTotalSpacePercentF2_1, " +
                                 "ClippedHuntSpaceF2_1_FousandsGaF2_1, ClippedHuntSpaceF2_1_HuntTotalSpacePercentF2_1, " +
                                 " SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')",
                                 tableName,
                                 row["MO_NameF2_1"], row["TotalSpaceF2_1"],
                                 row["HuntTotalSpaceF2_1_FousandsGaF2_1"], row["HuntTotalSpaceF2_1_Mo_TotalSpacePercentF2_1"],
                                 row["FreeHuntSpaceF2_1_FousandsGaF2_1"], row["FreeHuntSpaceF2_1_HuntTotalSpacePercentF2_1"],
                                 row["ClippedHuntSpaceF2_1_FousandsGaF2_1"], row["ClippedHuntSpaceF2_1_HuntTotalSpacePercentF2_1"], 
                                 passport_id);
        }
    }
}