﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm3_1 : ImporterBase
    {
        public HuntResourcesInfoForm3_1(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2015, 1, 1),
                Description =
                    "Документированная информация о юридических лицах и индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничьего хозяйства, а также организациях, осуществляющих деятельности по закупке, производству и продаже продукции охоты",
                SetIdentifier = "gosohotrestr_ur_litsa",
                Title =
                    "Сведения о юридических лицах, осуществляющих виды деятельности в сфере охотничьего хозяйства",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description =
                            "Документированная информация о юридических лицах и индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничьего хозяйства, а также организациях, осуществляющих деятельности по закупке, производству и продаже продукции охоты",
                        Provinance = "Обновление данных",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName =
                            "Белоусов Игорь Валерьевич, ведущий консультант отдела охраны, воспроизводства и использования объектов животного мира и среды их обитания",
                        PublisherPhone = "+7-861-2591971",
                        SourceId = Guid.Parse("B7F614F3-1D64-4622-A115-83388181FCB9"),
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title =
                            "Сведения о юридических лицах, осуществляющих виды деятельности в сфере охотничьего хозяйства"
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm3_1", new TableDisplayTemplate
            {
                Description =
                    "Сведения о юридических лицах, осуществляющих виды деятельности в сфере охотничьего хозяйства",
                Name = "HuntResourcesInfoForm3_1",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "NameF3_1",
                        DescriptionRu = "НАИМЕНОВАНИЕ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FormOfOrganization3_1",
                        DescriptionRu = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА (ОКОПФ)",
                        DescritpionEn = "",
                        Header = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА (ОКОПФ)",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "AddressesF3_1",
                        DescriptionRu = "ПОЧТОВЫЙ АДРЕС, ТЕЛЕФОН, E-MAIL",
                        DescritpionEn = "",
                        Header = "ПОЧТОВЫЙ АДРЕС, ТЕЛЕФОН, E-MAIL",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "INN_AndDataF3_1",
                        DescriptionRu = "ИНН, ДАТА ПОСТАНОВКИ НА УЧЕТ В НАЛОГОВОМ ОРГАНЕ",
                        DescritpionEn = "",
                        Header = "ИНН, ДАТА ПОСТАНОВКИ НА УЧЕТ В НАЛОГОВОМ ОРГАНЕ",
                        IncludeIntoFilter = true,
                        Order = 3,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "RegisterNumberAndDataF3_1",
                        DescriptionRu = "ГОСУДАРСТВЕННЫЙ РЕГИСТРАЦИОННЫЙ НОМЕР ЗАПИСИ И ДАТА ВНЕСЕНИЯ ЕЕ В ЕГРЮЛ",
                        DescritpionEn = "",
                        Header = "ГОСУДАРСТВЕННЫЙ РЕГИСТРАЦИОННЫЙ НОМЕР ЗАПИСИ И ДАТА ВНЕСЕНИЯ ЕЕ В ЕГРЮЛ",
                        IncludeIntoFilter = true,
                        Order = 4,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ClippedHuntSpaceF3_1",
                        DescriptionRu = "ПЛОЩАДЬ ЗАКРЕПЛЕННЫХ ОХОТНИЧЬИХ УГОДИЙ, ТЫС. ГА",
                        DescritpionEn = "",
                        Header = "ПЛОЩАДЬ ЗАКРЕПЛЕННЫХ ОХОТНИЧЬИХ УГОДИЙ, ТЫС. ГА",
                        IncludeIntoFilter = true,
                        Order = 5,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ЧИСЛЕННОСТЬ И ШТАТ РАБОТНИКОВ ЗАНЯТЫХ В ОХОТНИЧЬЕМ ХОЗЯЙСТВЕ",
                        Order = 6,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "StaffF3_1_Huntsmen",
                                DescriptionRu = "егеря",
                                DescritpionEn = "",
                                Header = "егеря",
                                IncludeIntoFilter = true,
                                Order = 7,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "StaffF3_1_Experts",
                                DescriptionRu = "охотоведы",
                                DescritpionEn = "",
                                Header = "охотоведы",
                                IncludeIntoFilter = true,
                                Order = 8,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "StaffF3_1_HuntersInState",
                                DescriptionRu = "штатные охотники",
                                DescritpionEn = "",
                                Header = "штатные охотники",
                                IncludeIntoFilter = true,
                                Order = 9,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ОХОТХОЗЯЙСТВЕННОЕ СОГЛАШЕНИЕ, ДОЛГОСРОЧНАЯ ЛИЦЕНЗИЯ",
                        Order = 10,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "LicenseF3_1_NumberF3_1",
                                DescriptionRu = "Номер",
                                DescritpionEn = "",
                                Header = "Номер",
                                IncludeIntoFilter = true,
                                Order = 11,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "LicenseF3_1_DateF3_1",
                                DescriptionRu = "Дата",
                                DescritpionEn = "",
                                Header = "Дата",
                                IncludeIntoFilter = true,
                                Order = 12,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "LicenseF3_1_TimeValidF3_1",
                                DescriptionRu = "Время действия",
                                DescritpionEn = "",
                                Header = "Время действия",
                                IncludeIntoFilter = true,
                                Order = 13,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ВНУТРИХОЗЯЙСТВЕННОЕ ОХОТУСТРОЙСТВО",
                        Order = 14,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "InternalHuntOrganizationF3_1_YearF3_1",
                                DescriptionRu = "год проведения",
                                DescritpionEn = "",
                                Header = "год проведения",
                                IncludeIntoFilter = true,
                                Order = 15,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "InternalHuntOrganizationF3_1_TerritoryPercentF3_1",
                                DescriptionRu = "% охвата территории",
                                DescritpionEn = "",
                                Header = "% охвата территории",
                                IncludeIntoFilter = true,
                                Order = 16,
                                Type = ColumnDisplayTemplate.ContentType.String,
                            }
                        }
                    },
                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm3_1"],
                Name = "HuntResourcesInfoForm3_1",
                Description =
                    "Сведения об охотничьих угодьях в субъекте Российской Федерации",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm3_1"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (NameF3_1, FormOfOrganization3_1, " +
                                 "AddressesF3_1, INN_AndDataF3_1, " +
                                 "RegisterNumberAndDataF3_1, ClippedHuntSpaceF3_1," +
                                 "LicenseF3_1_NumberF3_1, LicenseF3_1_DateF3_1," +
                                 "LicenseF3_1_TimeValidF3_1, InternalHuntOrganizationF3_1_YearF3_1," +
                                 "InternalHuntOrganizationF3_1_TerritoryPercentF3_1, StaffF3_1_Huntsmen, " +
                                 "StaffF3_1_Experts, StaffF3_1_HuntersInState, " +
                                 "SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}','{10}', '{11}', '{12}', '{13}', '{14}', '{15}')",
                                 tableName,
                                 row["NameF3_1"], row["FormOfOrganization3_1"],
                                 row["AddressesF3_1"], row["INN_AndDataF3_1"],
                                 row["RegisterNumberAndDataF3_1"], row["ClippedHuntSpaceF3_1"],
                                 row["LicenseF3_1_NumberF3_1"], row["LicenseF3_1_DateF3_1"],
                                 row["LicenseF3_1_TimeValidF3_1"], row["InternalHuntOrganizationF3_1_YearF3_1"],
                                 row["InternalHuntOrganizationF3_1_TerritoryPercentF3_1"], row["StaffF3_1_Huntsmen"],
                                 row["StaffF3_1_Experts"], row["StaffF3_1_HuntersInState"],
                                  passport_id);
        }
    }
}