﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm4_5 : ImporterBase
    {
        public HuntResourcesInfoForm4_5(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now.AddYears(-2),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_medved_2",
                Title = "Сведения о добыче бурого медведя",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    //Документированная информация о добыче медведя",
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Документированная информация о добыче медведя по состоянию на 1 сентября  2014  г.",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        SourceId = Guid.Parse("4A7A6881-2F29-4C6B-8012-9EFF9AA73C15"),
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче бурого медведя в 2013 - 2014 гг."
                        #endregion
                    }
                }
                #endregion
            }) { }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var rsult = new Dictionary<string, Repository.Entities.TableDisplayTemplate>();

            rsult.Add("HuntResourcesInfoForm4_5",
                new Repository.Entities.TableDisplayTemplate
                {
                    Description = "Сведения о добыче бурого медведя",
                    Name = "HuntResourcesInfoForm4_5",
                    Title = "Сведения о добыче бурого медведя",
                    Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TerritoryNameF4_5",
                                    DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ApprovedQuotaF4_5",
                                    DescriptionRu = "УТВЕРЖДЕННАЯ КВОТА	",
                                    DescritpionEn = "",
                                    Header = "УТВЕРЖДЕННАЯ КВОТА",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.Integer
                                },


                                new ColumnDisplayTemplate
                                {
                                    Header = "ДОБЫТО, ОСОБЕЙ",
                                    Order = 2,
                                    SubColumns = new ColumnDisplayTemplate[]
                                    {
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedF4_5_TotalF4_5",
                                            DescriptionRu = "ДОБЫТО, ОСОБЕЙ Всего",
                                            DescritpionEn = "",
                                            Header = "Всего",
                                            IncludeIntoFilter = true,
                                            Order = 3,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedF4_5_MaleF4_5",
                                            DescriptionRu = "ДОБЫТО, ОСОБЕЙ самцов",
                                            DescritpionEn = "",
                                            Header = "самцов",
                                            IncludeIntoFilter = true,
                                            Order = 4,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedF4_5_FemaleF4_5",
                                            DescriptionRu = "ДОБЫТО, ОСОБЕЙ самок",
                                            DescritpionEn = "",
                                            Header = "самок",
                                            IncludeIntoFilter = true,
                                            Order = 5,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        }
                                    }
                                }
                            }
                });

            return rsult;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm4_5"],
                Name = "HuntResourcesInfoForm4_5",
                Description = "Документированная информация о добыче медведя",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm4_5"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (TerritoryNameF4_5, ApprovedQuotaF4_5, HuntedF4_5_TotalF4_5, HuntedF4_5_MaleF4_5, HuntedF4_5_FemaleF4_5, SetsPassports_Id) " +
                "VALUES ('{1}', {2}, {3}, {4}, {5}, '{6}')", tableName, row["TerritoryNameF4_5"], row["ApprovedQuotaF4_5"], row["HuntedF4_5_TotalF4_5"], row["HuntedF4_5_MaleF4_5"],
                row["HuntedF4_5_FemaleF4_5"], passport_id);
        }
    }
}
