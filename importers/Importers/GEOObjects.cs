﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class GEOObjects : ImporterBase
    {
        public GEOObjects(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                SetIdentifier = "perechenGEEobjects",
                Title = "Перечень объектов государственной экологической экспертизы краевого уровня: поступившие объекты ГЭЭ",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края Отдел государственной экологической экспертизы",
                        Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Шуденкова Янина Вячеславовна",
                        PublisherPhone = "+7-861-2518289",
                        SourceId = Guid.Parse("B1F52301-31BD-49F2-A44E-4511FCB2E46F"),
                        Subject = "Экспертиза, государственная экологическая экспертиза, общественная экологическая экспертиза, эксперты",
                        Title = "Перечень объектов государственной экологической экспертизы краевого уровня"
                        #endregion
                    }
                }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                SetIdentifier = "perechenGEEobjects_returned",
                Title = "Перечень объектов государственной экологической экспертизы краевого уровня: объекты ГЭЭ, возвращенные без проведения ГЭЭ в связи с некомплектностью/неоплатой",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края Отдел государственной экологической экспертизы",
                        Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Шуденкова Янина Вячеславовна",
                        PublisherPhone = "+7-861-2518289",
                        SourceId = Guid.Parse("F2A3FA1A-03FC-48C5-BC5D-5569999D36E7"),
                        Subject = "Экспертиза, государственная экологическая экспертиза, общественная экологическая экспертиза, эксперты",
                        Title = "Перечень объектов государственной экологической экспертизы краевого уровня: объекты ГЭЭ, возвращенные без проведения ГЭЭ в связи с некомплектностью/неоплатой"
                        #endregion
                    }
                }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Title = "Перечень объектов государственной экологической экспертизы краевого уровня: объекты, по которым проведение ГЭЭ завершено",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetIdentifier = "perechenGEEobjects_compleate",
                Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края Отдел государственной экологической экспертизы",
                        Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Шуденкова Янина Вячеславовна",
                        PublisherPhone = "+7-861-2518289",
                        SourceId = Guid.Parse("EA2B0029-089B-40BF-9536-215E15A5C38D"),
                        Subject = "Экспертиза, государственная экологическая экспертиза, общественная экологическая экспертиза, эксперты",
                        Title = "Перечень объектов государственной экологической экспертизы краевого уровня: объекты, по которым проведение ГЭЭ завершено",
                        #endregion
                    }
                }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Title = "Перечень объектов государственной экологической экспертизы краевого уровня: текущие объекты ГЭЭ",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetIdentifier = "perechenGEEobjects_current",
                Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края Отдел государственной экологической экспертизы",
                        Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Шуденкова Янина Вячеславовна",
                        PublisherPhone = "+7-861-2518289",
                        SourceId = Guid.Parse("890952A4-730B-4864-A91C-4028D6020AB3"),
                        Subject = "Экспертиза, государственная экологическая экспертиза, общественная экологическая экспертиза, эксперты",
                        Title = "Перечень объектов государственной экологической экспертизы краевого уровня: текущие объекты ГЭЭ"
                        #endregion
                    }
                }
                #endregion
            }) { }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("GEOObjects", new TableDisplayTemplate
            {
                Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                Name = "GEOObjects",
                Title = "Перечень объектов государственной экологической экспертизы краевого уровня",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "StatementNum",
                                DescriptionRu = "ВХ. № ЗАЯВЛЕНИЯ ЗАЯВИТЕЛЯ, ДАТА",
                                DescritpionEn = "",
                                Header = "ВХ. № ЗАЯВЛЕНИЯ ЗАЯВИТЕЛЯ, ДАТА",
                                IncludeIntoFilter = true,
                                Order = 0,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "ObjectName",
                                DescriptionRu = "НАИМЕНОВАНИЕ ОБЪЕКТА ГЭЭ. ЗАКАЗЧИК",
                                DescritpionEn = "",
                                Header = "НАИМЕНОВАНИЕ ОБЪЕКТА ГЭЭ. ЗАКАЗЧИК",
                                IncludeIntoFilter = true,
                                Order = 1,
                                Type = ColumnDisplayTemplate.ContentType.Text
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "MaterialsLetter",
                                DescriptionRu = "ПИСЬМО О СОСТАВЕ МАТЕРИАЛОВ, ПРЕДСТАВЛЕННЫХ НА ГЭЭ (СРОК ПРЕДСТАВЛЕНИЯ)",
                                DescritpionEn = "",
                                Header = "ПИСЬМО О СОСТАВЕ МАТЕРИАЛОВ, ПРЕДСТАВЛЕННЫХ НА ГЭЭ (СРОК ПРЕДСТАВЛЕНИЯ)",
                                IncludeIntoFilter = true,
                                Order = 2,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "LowMaterialsNum",
                                DescriptionRu = "ВХ. № ПИСЬМА ЗАЯВИТЕЛЯ О ПРЕДОСТАВЛЕНИИ НЕДОСТАЮЩИХ МАТЕРИАЛОВ",
                                DescritpionEn = "",
                                Header = "ВХ. № ПИСЬМА ЗАЯВИТЕЛЯ О ПРЕДОСТАВЛЕНИИ НЕДОСТАЮЩИХ МАТЕРИАЛОВ",
                                IncludeIntoFilter = true,
                                Order = 3,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "ReturnLetter",
                                DescriptionRu = "ПИСЬМО О ВОЗВРАТЕ МАТЕРИАЛОВ, ПРЕДСТАВЛЕННЫХ НА ГЭЭ, В СВЯЗИ С НЕКОМПЛЕКТНОСТЬЮ",
                                DescritpionEn = "",
                                Header = "ПИСЬМО О ВОЗВРАТЕ МАТЕРИАЛОВ, ПРЕДСТАВЛЕННЫХ НА ГЭЭ, В СВЯЗИ С НЕКОМПЛЕКТНОСТЬЮ",
                                IncludeIntoFilter = true,
                                Order = 4,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "PayForMaterialsDate",
                                DescriptionRu = "ПИСЬМО О КОМПЛЕКТНОСТИ МАТЕРИАЛОВ И НЕОБХОДИМОСТИ ОПЛАТЫ РАБОТ ПО ГЭЭ (СРОК ОПЛАТЫ)",
                                DescritpionEn = "",
                                Header = "ПИСЬМО О КОМПЛЕКТНОСТИ МАТЕРИАЛОВ И НЕОБХОДИМОСТИ ОПЛАТЫ РАБОТ ПО ГЭЭ (СРОК ОПЛАТЫ)",
                                IncludeIntoFilter = true,
                                Order = 5,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "PayDate",
                                DescriptionRu = "П/П ОБ ОПЛАТЕ СЧЕТА (ДАТА ПОСТУПЛЕНИЯ)",
                                DescritpionEn = "",
                                Header = "П/П ОБ ОПЛАТЕ СЧЕТА (ДАТА ПОСТУПЛЕНИЯ)",
                                IncludeIntoFilter = true,
                                Order = 6,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "NopayLetter",
                                DescriptionRu = "ПИСЬМО О ВОЗВРАТЕ МАТЕРИАЛОВ, ПРЕДСТАВЛЕННЫХ НА ГЭЭ, В СВЯЗИ С НЕОПЛАТОЙ",
                                DescritpionEn = "",
                                Header = "ПИСЬМО О ВОЗВРАТЕ МАТЕРИАЛОВ, ПРЕДСТАВЛЕННЫХ НА ГЭЭ, В СВЯЗИ С НЕОПЛАТОЙ",
                                IncludeIntoFilter = true,
                                Order = 7,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "Order",
                                DescriptionRu = "ПРИКАЗ ОБ ОРГАНИЗАЦИИ И ПРОВЕДЕНИИ ГЭЭ (СРОК РАБОТЫ КОМИССИИ)",
                                DescritpionEn = "",
                                Header = "ПРИКАЗ ОБ ОРГАНИЗАЦИИ И ПРОВЕДЕНИИ ГЭЭ (СРОК РАБОТЫ КОМИССИИ)",
                                IncludeIntoFilter = true,
                                Order = 8,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "OrderDate",
                                DescriptionRu = "ПРИКАЗ О ПРОДЛЕНИИ СРОКА ПРОВЕДЕНИЯ ГЭЭ",
                                DescritpionEn = "",
                                Header = "ПРИКАЗ О ПРОДЛЕНИИ СРОКА ПРОВЕДЕНИЯ ГЭЭ",
                                IncludeIntoFilter = true,
                                Order = 9,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },

                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "OrderResult",
                                DescriptionRu = "РЕЗУЛЬТАТ ПРОВЕДЕНИЯ ГЭЭ (ПРИКАЗ)",
                                DescritpionEn = "",
                                Header = "РЕЗУЛЬТАТ ПРОВЕДЕНИЯ ГЭЭ (ПРИКАЗ)",
                                IncludeIntoFilter = true,
                                Order = 9,
                                Type = ColumnDisplayTemplate.ContentType.Text
                            }
                        }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["GEOObjects"],
                Name = "GEOObjects",
                Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "GEOObjects"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] ([StatementNum], [ObjectName], [MaterialsLetter], [LowMaterialsNum], [ReturnLetter], [PayForMaterialsDate], [PayDate], [NopayLetter], " +
                "[Order], [OrderDate], [OrderResult], [SetsPassports_Id]) VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}')", tableName,
                row["StatementNum"], row["ObjectName"], row["MaterialsLetter"], row["LowMaterialsNum"], row["ReturnLetter"], row["PayForMaterialsDate"], row["PayDate"],
                row["NopayLetter"], row["Order"], row["OrderDate"], row["OrderResult"], passport_id);
        }
    }

   
}
