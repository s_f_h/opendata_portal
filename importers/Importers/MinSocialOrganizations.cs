﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class MinSocialOrganizations : ImporterBase
    {
        public MinSocialOrganizations(DataContext ctx)
            : base(ctx,
            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_soc_razv],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.soc_ravitie],
                CreationDate = DateTime.Now,
                Description = "Информация о специализированных учреждениях для несовершеннолетних, нуждающихся в социальной реабилитации министерства социального развития и семейной политики Краснодарского края - Наименование учреждений; - Адреса; - Контактные данные; - Задачи и функции",
                SetIdentifier = "SRCNkk",
                Title = "Перечень специализированных учреждений для несовершеннолетних, нуждающихся в социальной реабилитации",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство социального развития и семейной политики Краснодарского края",
                        Description = "Информация о специализированных учреждениях для несовершеннолетних, нуждающихся в социальной реабилитации министерства социального развития и семейной политики Краснодарского края - Наименование учреждений; - Адреса; - Контактные данные; - Задачи и функции",
                        IsCurrent = true,
                        PublisherMBox = "efashkin_ai@msrsp.krasnodar.ru",
                        PublisherName = "Новик Галина Викторовна",
                        PublisherPhone = "+7-861-2592312",
                        Subject = "поддержка, социальная, защита, население, учреждения, реабилитация, несовершеннолетние",
                        Title = "Перечень учреждений для детей-сирот и детей, оставшихся без попечения родителей",
                        SourceId = Guid.Parse("FEE755BB-4A3D-4DF7-A9B0-5BCAFE94F1DF")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_soc_razv],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.soc_ravitie],
                CreationDate = DateTime.Now,
                Description = "Информация по учреждениям для детей-сирот и детей, оставшихся без попечения родителей министерства социального развития и семейной политики Краснодарского края - Наименование учреждений; - Адреса; - Контактные данные; - Задачи и функции учреждений.",
                SetIdentifier = "minsocuch08kk",
                Title = "Перечень специализированных учреждений для несовершеннолетних, нуждающихся в социальной реабилитации",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство социального развития и семейной политики Краснодарского края",
                        Description = "Информация по учреждениям для детей-сирот и детей, оставшихся без попечения родителей министерства социального развития и семейной политики Краснодарского края - Наименование учреждений; - Адреса; - Контактные данные; - Задачи и функции учреждений.",
                        IsCurrent = true,
                        PublisherMBox = "efashkin_ai@msrsp.krasnodar.ru",
                        PublisherName = "Новик Галина Викторовна",
                        PublisherPhone = "+7-861-2592312",
                        Subject = "поддержка, социальная, защита, население, учреждения, реабилитация, несовершеннолетние",
                        Title = "Перечень учреждений для детей-сирот и детей, оставшихся без попечения родителей",
                        SourceId = Guid.Parse("72626B83-07E3-4F6E-911B-23FCE37FEDF9")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_soc_razv],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.soc_ravitie],
                CreationDate = DateTime.Now,
                Description = "Информация по Централизованным бухгалтериям министерства социального развития и семейной политики Краснодарского края - Наименование учреждений; - Адреса; - Контактные данные; - Задачи и функции учреждений.",
                SetIdentifier = "minsoccb01kk",
                Title = "Информация по Централизованным бухгалтериям",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство социального развития и семейной политики Краснодарского края",
                        Description = "Информация по Централизованным бухгалтериям министерства социального развития и семейной политики Краснодарского края - Наименование учреждений; - Адреса; - Контактные данные; - Задачи и функции учреждений.",
                        IsCurrent = true,
                        PublisherMBox = "efashkin_ai@msrsp.krasnodar.ru",
                        PublisherName = "Новик Галина Викторовна",
                        PublisherPhone = "+7-861-2592312",
                        Subject = "поддержка, социальная, защита, население, учреждения, реабилитация, несовершеннолетние",
                        Title = "Информация по Централизованным бухгалтериям",
                        SourceId = Guid.Parse("7DAB4E5C-6AC2-4EFA-B523-927B2185F703")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("MinSocialOrganizations", new TableDisplayTemplate
            {
                Description = "Перечень специализированных учреждений",
                Name = "MinSocialOrganizations",
                Title = "Перечень специализированных учреждений",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                 {
                     new ColumnDisplayTemplate
                     {
                         Header = "НАИМЕНОВАНИЕ УЧРЕЖДЕНИЯ",
                         Order = 0,
                         SubColumns = new ColumnDisplayTemplate[]
                         {
                             new ColumnDisplayTemplate
                             {
                                 BindedCollumn = "ShortName",
                                 DescriptionRu = "НАИМЕНОВАНИЕ УЧРЕЖДЕНИЯ КРАТКОЕ",
                                 DescritpionEn = "",
                                 Header = "КРАТКОЕ",
                                 IncludeIntoFilter = true,
                                 Order = 1,
                                 Type = ColumnDisplayTemplate.ContentType.String
                             },
                             new ColumnDisplayTemplate
                             {
                                 BindedCollumn = "FullName",
                                 DescriptionRu = "НАИМЕНОВАНИЕ УЧРЕЖДЕНИЯ ПОЛНОЕ",
                                 DescritpionEn = "",
                                 Header = "ПОЛНОЕ",
                                 IncludeIntoFilter = true,
                                 Order = 2,
                                 Type = ColumnDisplayTemplate.ContentType.Text
                             },
                         }
                     },
            
                     new ColumnDisplayTemplate
                     {
                         Header = "АДРЕС",
                         Order = 3,
                         SubColumns = new ColumnDisplayTemplate[]
                         {
                             new ColumnDisplayTemplate
                             {
                                 BindedCollumn = "ActualAddress",
                                 DescriptionRu = "АДРЕС ФАКТИЧЕСКИЙ",
                                 DescritpionEn = "",
                                 Header = "ФАКТИЧЕСКИЙ",
                                 IncludeIntoFilter = true,
                                 Order = 4,
                                 Type = ColumnDisplayTemplate.ContentType.PostAddress
                             },
                             new ColumnDisplayTemplate
                             {
                                 BindedCollumn = "LegalAddress",
                                 DescriptionRu = "АДРЕС ЮРИДИЧЕСКИЙ",
                                 DescritpionEn = "",
                                 Header = "ЮРИДИЧЕСКИЙ",
                                 IncludeIntoFilter = true,
                                 Order = 5,
                                 Type = ColumnDisplayTemplate.ContentType.PostAddress
                             },
                         }
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Telephones",
                         DescriptionRu = "НОМЕРА ТЕЛЕФОНОВ",
                         DescritpionEn = "",
                         Header = "НОМЕРА ТЕЛЕФОНОВ",
                         IncludeIntoFilter = true,
                         Order = 6,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Email",
                         DescriptionRu = "ЭЛЕКТРОННЫЙ АДРЕС",
                         DescritpionEn = "",
                         Header = "ЭЛЕКТРОННЫЙ АДРЕС",
                         IncludeIntoFilter = true,
                         Order = 7,
                         Type = ColumnDisplayTemplate.ContentType.EMail
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "ManagerName",
                         DescriptionRu = "Ф.И.О. РУКОВОДИТЕЛЯ",
                         DescritpionEn = "",
                         Header = "Ф.И.О. РУКОВОДИТЕЛЯ",
                         IncludeIntoFilter = true,
                         Order = 8,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
                   
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "OrganizationFunctions",
                         DescriptionRu = "ФУНКЦИИ И ЗАДАЧИ УЧРЕЖДЕНИЯ",
                         DescritpionEn = "",
                         Header = "ФУНКЦИИ И ЗАДАЧИ УЧРЕЖДЕНИЯ",
                         IncludeIntoFilter = true,
                         Order = 9,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     }
                 }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["MinSocialOrganizations"],
                Name = "MinSocialOrganizations",
                Description = "Перечень специализированных учреждений",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "MinSocialOrganizations"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (ShortName, FullName, ActualAddress, LegalAddress, Telephones, Email, ManagerName, OrganizationFunctions, SetsPassports_Id) " +
                "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')", tableName, row["ShortName"], row["FullName"], row["ActualAddress"],
                row["LegalAddress"], row["Telephones"], row["Email"], row["ManagerName"], row["OrganizationFunctions"], passport_id);
        }
    }
}
