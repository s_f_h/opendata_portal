﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class TheaterObject : ImporterBase
    {
        public TheaterObject(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_kult],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.kult],
                CreationDate = DateTime.Now,
                Description = "Информация о театрах",
                SetIdentifier = "theatres",
                Title = "Подведомственные учреждения министерства культуры Краснодарского края (теотрально-концертные учреждения)",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство культуры Краснодарского края",
                        Description = "Информация о театрах",
                        IsCurrent = true,
                        PublisherMBox = "uorn@krasnodar.ru",
                        PublisherName = "Куропатов С.С.",
                        PublisherPhone = "+7-861-9926027",
                        Subject = "Информация о театрах",
                        Title = "Подведомственные учреждения министерства культуры Краснодарского края (теотрально-концертные учреждения)",
                        SourceId = Guid.Parse("F6B5E444-5359-4517-AC95-EDF8682D189C")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("TheaterObject", new TableDisplayTemplate
            {
                Description = "Перечень объектов государственной экологической экспертизы краевого уровня",
                Name = "GEOObjects",
                Title = "Перечень объектов государственной экологической экспертизы краевого уровня",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    new ColumnDisplayTemplate
                    {
                        Header = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА",
                        Order = 0,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "LFFully",
                                DescriptionRu = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА ПОЛНОСТЬЮ",
                                DescritpionEn = "",
                                Header = "ПОЛНОСТЬЮ",
                                IncludeIntoFilter = true,
                                Order = 1,
                                Type = ColumnDisplayTemplate.ContentType.Text
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "LFBriefly",
                                DescriptionRu = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА СОКРАЩЕННАЯ",
                                DescritpionEn = "",
                                Header = "СОКРАЩЕННАЯ",
                                IncludeIntoFilter = true,
                                Order = 2,
                                Type = ColumnDisplayTemplate.ContentType.String
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "НАЗВАНИЕ",
                        Order = 3,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "BriefName",
                                DescriptionRu = "НАЗВАНИЕ СОКРАЩЕННОЕ (ЕСЛИ ЕСТЬ)",
                                DescritpionEn = "",
                                Header = "СОКРАЩЕННОЕ (ЕСЛИ ЕСТЬ)",
                                IncludeIntoFilter = true,
                                Order = 4,
                                Type = ColumnDisplayTemplate.ContentType.String
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "FullName",
                                DescriptionRu = "НАЗВАНИЕ ПОЛНОСТЬЮ",
                                DescritpionEn = "",
                                Header = "ПОЛНОСТЬЮ",
                                IncludeIntoFilter = true,
                                Order = 5,
                                Type = ColumnDisplayTemplate.ContentType.Text
                            }
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "АДРЕСС(А)",
                        Order = 6,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "FactAdres",
                                DescriptionRu = "АДРЕСС(А) ФАКТИЧЕСКИЙ(Е)",
                                DescritpionEn = "",
                                Header = "ФАКТИЧЕСКИЙ(Е)",
                                IncludeIntoFilter = true,
                                Order = 7,
                                Type = ColumnDisplayTemplate.ContentType.PostAddress
                            },
                            new ColumnDisplayTemplate
                            {
                                BindedCollumn = "JuristicAdres",
                                DescriptionRu = "АДРЕСС(А) ЮРИДИЧЕСКИЙ",
                                DescritpionEn = "",
                                Header = "ЮРИДИЧЕСКИЙ",
                                IncludeIntoFilter = true,
                                Order = 8,
                                Type = ColumnDisplayTemplate.ContentType.PostAddress
                            }
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "Numbers",
                        DescriptionRu = "ТЕЛЕФОН(Ы)",
                        DescritpionEn = "",
                        Header = "ТЕЛЕФОН(Ы)",
                        IncludeIntoFilter = true,
                        Order = 9,
                        Type = ColumnDisplayTemplate.ContentType.Text
                    },

                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "InternetAdres",
                        DescriptionRu = "АДРЕСС ИНТЕРНЕТ-САЙТА",
                        DescritpionEn = "",
                        Header = "АДРЕСС ИНТЕРНЕТ-САЙТА",
                        IncludeIntoFilter = true,
                        Order = 10,
                        Type = ColumnDisplayTemplate.ContentType.Url
                    },

                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FPInstitution",
                        DescriptionRu = "ФУНКЦИИ И ЗАДАЧИ УЧРЕЖДЕНИЯ",
                        DescritpionEn = "",
                        Header = "ФУНКЦИИ И ЗАДАЧИ УЧРЕЖДЕНИЯ",
                        IncludeIntoFilter = true,
                        Order = 11,
                        Type = ColumnDisplayTemplate.ContentType.Text
                    },

                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "RegularStaffing",
                        DescriptionRu = "ШТАТНАЯ ЧИСЛЕННОСТЬ",
                        DescritpionEn = "",
                        Header = "ШТАТНАЯ ЧИСЛЕННОСТЬ",
                        IncludeIntoFilter = true,
                        Order = 12,
                        Type = ColumnDisplayTemplate.ContentType.Integer
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "Repertory",
                        DescriptionRu = "РЕПЕРТУАР ТЕАТРОВ И КОНЦЕРТНЫХ УЧРЕЖДЕНИЙ КРАСНОДАРСКОГО КРАЯ ПОДВЕДОМСТВЕННЫХ МИНИСТЕРСТВУ КУЛЬТУРЫ КРАСНОДАРСКОГО КРАЯ",
                        DescritpionEn = "",
                        Header = "РЕПЕРТУАР ТЕАТРОВ И КОНЦЕРТНЫХ УЧРЕЖДЕНИЙ КРАСНОДАРСКОГО КРАЯ ПОДВЕДОМСТВЕННЫХ МИНИСТЕРСТВУ КУЛЬТУРЫ КРАСНОДАРСКОГО КРАЯ",
                        IncludeIntoFilter = true,
                        Order = 13,
                        Type = ColumnDisplayTemplate.ContentType.Text
                    }
                }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["TheaterObject"],
                Name = "TheaterObject",
                Description = "Подведомственные учреждения министерства культуры Краснодарского края",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "TheaterObject"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (LFFully, LFBriefly, BriefName, FullName, FactAdres, JuristicAdres, " +
                "Numbers, InternetAdres, FPInstitution, RegularStaffing, Repertory, SetsPassports_Id) " +
                "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', {10}, '{11}', '{12}')", tableName,
                row["LFFully"], row["LFBriefly"], row["BriefName"], row["FullName"], row["FactAdres"], row["JuristicAdres"],
                row["Numbers"], row["InternetAdres"], row["FPInstitution"], row["RegularStaffing"], row["Repertory"], passport_id);
        }
    }
}
