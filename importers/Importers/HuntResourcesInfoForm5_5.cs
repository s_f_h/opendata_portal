﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_5 : ImporterBase
    {
        public HuntResourcesInfoForm5_5(DataContext ctx)
            : base(ctx, new ImportingSetSettings
             {
                 AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                 CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                 CreationDate = DateTime.Now,
                 Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                 SetIdentifier = "gosohotreestr_ogranicheniya",
                 Title = "Ограничения на использование охотничьих ресурсов",
                 UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                 SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                         Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                         IsCurrent = true,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Ограничения на использование охотничьих ресурсов",
                         SourceId = Guid.Parse("4254C799-CF97-470E-AE65-EBBC922AF760")
                     }
                 }
             }) { }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm5_5", new TableDisplayTemplate
            {
                Description = "Ограничения на использование охотничьих ресурсов",
                Name = "HuntResourcesInfoForm5_5",
                Title = "Ограничения на использование охотничьих ресурсов",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "RestrictionTypeF5_5",
                         DescriptionRu = "ВИД ОГРАНИЧЕНИЙ",
                         DescritpionEn = "",
                         Header = "ВИД ОГРАНИЧЕНИЙ",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "HunterResourcesTypeF5_5",
                         DescriptionRu = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                         DescritpionEn = "",
                         Header = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "TerritoryNameF5_5",
                         DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                         IncludeIntoFilter = true,
                         Order = 2,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "TimeF5_5",
                         DescriptionRu = "СРОКИ",
                         DescritpionEn = "",
                         Header = "СРОКИ",
                         IncludeIntoFilter = true,
                         Order = 3,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "OrderF5_5",
                         DescriptionRu = "РЕШЕНИЕ",
                         DescritpionEn = "",
                         Header = "РЕШЕНИЕ",
                         IncludeIntoFilter = true,
                         Order = 4,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "DateF5_5",
                         DescriptionRu = "ДАТА СОГЛАСОВАНИЯ С ФЕДЕРАЛЬНЫМ ОРГАНОМ",
                         DescritpionEn = "",
                         Header = "ДАТА СОГЛАСОВАНИЯ С ФЕДЕРАЛЬНЫМ ОРГАНОМ",
                         IncludeIntoFilter = true,
                         Order = 5,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     }
                }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_5"],
                Name = "HuntResourcesInfoForm5_5",
                Description = "Ограничения на использование охотничьих ресурсов",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_5"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (RestrictionTypeF5_5, HunterResourcesTypeF5_5, TerritoryNameF5_5, TimeF5_5, OrderF5_5, DateF5_5, SetsPassports_Id) " +
                "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')", tableName, row["RestrictionTypeF5_5"], row["HunterResourcesTypeF5_5"], row["TerritoryNameF5_5"],
                row["TimeF5_5"], row["OrderF5_5"], row["DateF5_5"], passport_id);
        }
    }
}
