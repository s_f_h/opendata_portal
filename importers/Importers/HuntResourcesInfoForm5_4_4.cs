﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_4_4 : ImporterBase
    {
        public HuntResourcesInfoForm5_4_4(DataContext ctx)
            : base(ctx, new ImportingSetSettings{
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId =  AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate =  DateTime.Now.AddYears(-2).AddMonths(-4),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничьего хозяйства",
                SetIdentifier = "gosohotreestr_nezakonnaya_dobicha",
                Title = "Сведения о незаконной добыче охотничьих ресурсов",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничьего хозяйства",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о незаконной добыче охотничьих ресурсов",
                        SourceId = Guid.Parse("257ACE58-4769-4D3E-8431-134D3493CE6C")
                    }
                }
            }){}

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (TerritoryNameF5_4_4, " +
            "IllegalHuntedF5_4_4__1_TypeF5_4_4, IllegalHuntedF5_4_4__1_KnownViolatorF5_4_4, IllegalHuntedF5_4_4__1_UnknownViolatorF5_4_4, " +
            "IllegalHuntedF5_4_4__2_TypeF5_4_4, IllegalHuntedF5_4_4__2_KnownViolatorF5_4_4, IllegalHuntedF5_4_4__2_UnknownViolatorF5_4_4, " +
            "IllegalHuntedF5_4_4__3_TypeF5_4_4, IllegalHuntedF5_4_4__3_KnownViolatorF5_4_4, IllegalHuntedF5_4_4__3_UnknownViolatorF5_4_4, " +
            "IllegalHuntedF5_4_4__4_TypeF5_4_4, IllegalHuntedF5_4_4__4_KnownViolatorF5_4_4, IllegalHuntedF5_4_4__4_UnknownViolatorF5_4_4, " +
            "IllegalHuntedF5_4_4__5_TypeF5_4_4, IllegalHuntedF5_4_4__5_KnownViolatorF5_4_4, IllegalHuntedF5_4_4__5_UnknownViolatorF5_4_4, " +
            "SetsPassports_Id) VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', " +
            "'{14}', '{15}', '{16}', '{17}')", tableName, row["TerritoryNameF5_4_4"],
            row["IllegalHuntedF5_4_4__1_TypeF5_4_4"], row["IllegalHuntedF5_4_4__1_KnownViolatorF5_4_4"], row["IllegalHuntedF5_4_4__1_UnknownViolatorF5_4_4"],
            row["IllegalHuntedF5_4_4__2_TypeF5_4_4"], row["IllegalHuntedF5_4_4__2_KnownViolatorF5_4_4"], row["IllegalHuntedF5_4_4__2_UnknownViolatorF5_4_4"],
            row["IllegalHuntedF5_4_4__3_TypeF5_4_4"], row["IllegalHuntedF5_4_4__3_KnownViolatorF5_4_4"], row["IllegalHuntedF5_4_4__3_UnknownViolatorF5_4_4"],
            row["IllegalHuntedF5_4_4__4_TypeF5_4_4"], row["IllegalHuntedF5_4_4__4_KnownViolatorF5_4_4"], row["IllegalHuntedF5_4_4__4_UnknownViolatorF5_4_4"],
            row["IllegalHuntedF5_4_4__5_TypeF5_4_4"], row["IllegalHuntedF5_4_4__5_KnownViolatorF5_4_4"], row["IllegalHuntedF5_4_4__5_UnknownViolatorF5_4_4"],
            passport_id);
        }
        protected override Dictionary<string, Repository.Entities.TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("HuntResourcesInfoForm5_4_4", new TableDisplayTemplate
            {
                Title = "Сведения о незаконной добыче охотничьих ресурсов",
                Name = "HuntResourcesInfoForm5_4_4",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TerritoryNameF5_4_4",
                        DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String,
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                        Order = 1,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения о незаконной добыче охотничьих ресурсов, ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                            Name = "HuntResourcesInfoForm5_4_4_Nezakonno_1",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__1_TypeF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Вид",
                                    DescritpionEn = "",
                                    Header = "Вид",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__1_KnownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__1_UnknownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Не выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Не выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                            }
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                        Order = 5,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения о незаконной добыче охотничьих ресурсов, ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                            Name = "HuntResourcesInfoForm5_4_4_Nezakonno_2",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__2_TypeF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Вид",
                                    DescritpionEn = "",
                                    Header = "Вид",
                                    IncludeIntoFilter = true,
                                    Order = 6,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__2_KnownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 7,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__2_UnknownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Не выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Не выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                            }
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                        Order = 9,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения о незаконной добыче охотничьих ресурсов, ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                            Name = "HuntResourcesInfoForm5_4_4_Nezakonno_3",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__3_TypeF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Вид",
                                    DescritpionEn = "",
                                    Header = "Вид",
                                    IncludeIntoFilter = true,
                                    Order = 10,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__3_KnownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 11,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__3_UnknownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Не выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Не выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 12,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                            }
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                        Order = 13,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения о незаконной добыче охотничьих ресурсов, ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                            Name = "HuntResourcesInfoForm5_4_4_Nezakonno_4",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__4_TypeF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Вид",
                                    DescritpionEn = "",
                                    Header = "Вид",
                                    IncludeIntoFilter = true,
                                    Order = 14,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__4_KnownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 15,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__4_UnknownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Не выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Не выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 16,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                            }
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                        Order = 17,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения о незаконной добыче охотничьих ресурсов, ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ",
                            Name = "HuntResourcesInfoForm5_4_4_Nezakonno_5",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__5_TypeF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Вид",
                                    DescritpionEn = "",
                                    Header = "Вид",
                                    IncludeIntoFilter = true,
                                    Order = 18,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__5_KnownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 19,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "IllegalHuntedF5_4_4__5_UnknownViolatorF5_4_4",
                                    DescriptionRu = "ДОБЫТО НЕЗАКОННО ОХОТНИЧЬИХ РЕСУРСОВ, ОСОБЕЙ Не выявленными нарушителями",
                                    DescritpionEn = "",
                                    Header = "Не выявленными нарушителями",
                                    IncludeIntoFilter = true,
                                    Order = 20,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                            }
                        }
                    }
                }
            });

            return result;
        }
        protected override Repository.Entities.DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {

            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_4_4"],
                Name = "HuntResourcesInfoForm5_4_4",
                Description = "Сведения о незаконной добыче охотничьих ресурсов",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_4_4"
            };
        }
    }
}
