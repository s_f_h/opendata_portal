﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers
{
    internal class EconomicsMultiset : ImporterBase
    {
        public EconomicsMultiset(DataContext ctx)
            : base(ctx,
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                    CreationDate = new DateTime(2015, 1, 1),
                    Description = "Годовой отчет по расходам",
                    SetIdentifier = "report_year_expenditure",
                    Title = "Годовой отчет по расходам",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Yearly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство финансов Краснодарского края",
                            Description = "Годовой отчет по расходам на 01.01.2015",
                            IsCurrent = true,
                            PublisherMBox = "minfin@krasnodar.ru",
                            PublisherName = "Кудинов Василий Петрович",
                            PublisherPhone = "+7-861-2145771",
                            SourceId = Guid.Parse("023BCB6A-0C2A-4A27-8C9D-FAC331C49DB2"),
                            Subject = "Годовой отчет по расходам на 01.01.2015",
                            Title = "Годовой отчет по расходам на 01.01.2015"
                            #endregion
                        }
                    }
                    #endregion
                },
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                    CreationDate = new DateTime(2015, 4, 1),
                    Description = "Месячный отчет по расходам",
                    SetIdentifier = "report_monthly_expenditure",
                    Title = "Месячный отчет по расходам",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Monthly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство финансов Краснодарского края",
                            Description = "Месячный отчет по расходам на 01.04.2015",
                            IsCurrent = true,
                            PublisherMBox = "minfin@krasnodar.ru",
                            PublisherName = "Кудинов Василий Петрович",
                            PublisherPhone = "+7-861-2145771",
                            SourceId = Guid.Parse("696F33B3-2D75-4EDD-96DC-062E3DAC5498"),
                            Subject = "Месячный отчет по расходам на 01.04.2015",
                            Title = "Месячный отчет по расходам на 01.04.2015"
                            #endregion
                        },
                    }
                    #endregion
                }
                )
        {
            asIs = true;
        }


        protected override Dictionary<string, Repository.Entities.TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("ReportOnExpenditure", new TableDisplayTemplate
            {
                Description = "Отчет по расходам",
                Name = "ReportOnExpenditure",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "NameOfIndicator",
                        DescriptionRu = "НАИМЕНОВАНИЕ ПОКАЗАТЕЛЯ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ ПОКАЗАТЕЛЯ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "StringCode",
                        DescriptionRu = "КОД СТРОКИ",
                        DescritpionEn = "",
                        Header = "КОД СТРОКИ",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "BudgetCode",
                        DescriptionRu = "КОД РАСХОДА ПО БЮДЖЕТНОЙ КЛАССИФИКАЦИИ",
                        DescritpionEn = "",
                        Header = "КОД РАСХОДА ПО БЮДЖЕТНОЙ КЛАССИФИКАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_1",
                        DescriptionRu =
                            "УТВЕРЖДЕНО; КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        DescritpionEn = "",
                        Header =
                            "УТВЕРЖДЕНО; КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        IncludeIntoFilter = true,
                        Order = 3,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_2",
                        DescriptionRu =
                            "УТВЕРЖДЕНО; СУММЫ, ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ И БЮДЖЕТА ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        DescritpionEn = "",
                        Header =
                            "УТВЕРЖДЕНО; СУММЫ, ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ И БЮДЖЕТА ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        IncludeIntoFilter = true,
                        Order = 4,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_3",
                        DescriptionRu = "УТВЕРЖДЕНО; КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        DescritpionEn = "",
                        Header = "УТВЕРЖДЕНО; КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 5,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_4",
                        DescriptionRu =
                            "УТВЕРЖДЕНО; СУММЫ, ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        DescritpionEn = "",
                        Header =
                            "УТВЕРЖДЕНО; СУММЫ, ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 6,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_5",
                        DescriptionRu = "УТВЕРЖДЕНО; БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        DescritpionEn = "",
                        Header = "УТВЕРЖДЕНО; БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 7,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_6",
                        DescriptionRu =
                            "УТВЕРЖДЕНО; БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                        DescritpionEn = "",
                        Header =
                            "УТВЕРЖДЕНО; БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                        IncludeIntoFilter = true,
                        Order = 8,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_7",
                        DescriptionRu = "УТВЕРЖДЕНО; БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                        DescritpionEn = "",
                        Header = "УТВЕРЖДЕНО; БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                        IncludeIntoFilter = true,
                        Order = 9,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_8",
                        DescriptionRu = "УТВЕРЖДЕНО; БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                        DescritpionEn = "",
                        Header = "УТВЕРЖДЕНО; БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                        IncludeIntoFilter = true,
                        Order = 10,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_9",
                        DescriptionRu = "УТВЕРЖДЕНО; БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                        DescritpionEn = "",
                        Header = "УТВЕРЖДЕНО; БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                        IncludeIntoFilter = true,
                        Order = 11,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedColumn_10",
                        DescriptionRu = "УТВЕРЖДЕНО; БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        DescritpionEn = "",
                        Header = "УТВЕРЖДЕНО; БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        IncludeIntoFilter = true,
                        Order = 12,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_1",
                        DescriptionRu =
                            "ИСПОЛНЕНО; КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        DescritpionEn = "",
                        Header =
                            "ИСПОЛНЕНО; КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        IncludeIntoFilter = true,
                        Order = 13,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_2",
                        DescriptionRu =
                            "ИСПОЛНЕНО; СУММЫ, ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ И БЮДЖЕТА ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        DescritpionEn = "",
                        Header =
                            "ИСПОЛНЕНО; СУММЫ, ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ И БЮДЖЕТА ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        IncludeIntoFilter = true,
                        Order = 14,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_3",
                        DescriptionRu = "ИСПОЛНЕНО; КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        DescritpionEn = "",
                        Header = "ИСПОЛНЕНО; КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 15,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_4",
                        DescriptionRu =
                            "ИСПОЛНЕНО; СУММЫ, ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        DescritpionEn = "",
                        Header =
                            "ИСПОЛНЕНО; СУММЫ, ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 16,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_5",
                        DescriptionRu = "ИСПОЛНЕНО; БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        DescritpionEn = "",
                        Header = "ИСПОЛНЕНО; БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 17,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_6",
                        DescriptionRu =
                            "ИСПОЛНЕНО; БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                        DescritpionEn = "",
                        Header =
                            "ИСПОЛНЕНО; БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                        IncludeIntoFilter = true,
                        Order = 18,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_7",
                        DescriptionRu = "ИСПОЛНЕНО; БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                        DescritpionEn = "",
                        Header = "ИСПОЛНЕНО; БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                        IncludeIntoFilter = true,
                        Order = 19,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_8",
                        DescriptionRu = "ИСПОЛНЕНО; БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                        DescritpionEn = "",
                        Header = "ИСПОЛНЕНО; БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                        IncludeIntoFilter = true,
                        Order = 20,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_9",
                        DescriptionRu = "ИСПОЛНЕНО; БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                        DescritpionEn = "",
                        Header = "ИСПОЛНЕНО; БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                        IncludeIntoFilter = true,
                        Order = 21,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FulfiledColumn_10",
                        DescriptionRu = "ИСПОЛНЕНО; БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        DescritpionEn = "",
                        Header = "ИСПОЛНЕНО; БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                        IncludeIntoFilter = true,
                        Order = 22,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["ReportOnExpenditure"],
                Name = "ReportOnExpenditure",
                Description = "Отчет по расходам",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "ReportOnExpenditure"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return
                string.Format(
                    "INSERT INTO [dbo].[{0}] (NameOfIndicator, StringCode, BudgetCode, ApprovedColumn_1, ApprovedColumn_2, " +
                    "ApprovedColumn_3, ApprovedColumn_4, ApprovedColumn_5, ApprovedColumn_6, ApprovedColumn_7, ApprovedColumn_8, ApprovedColumn_9, " +
                    "ApprovedColumn_10, FulfiledColumn_1, FulfiledColumn_2, FulfiledColumn_3, FulfiledColumn_4, FulfiledColumn_5, FulfiledColumn_6, " +
                    "FulfiledColumn_7, FulfiledColumn_8, FulfiledColumn_9, FulfiledColumn_10, SetsPassports_Id) VALUES ('{1}', '{2}', '{3}', '{4}', " +
                    "'{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', " +
                    "'{21}', '{22}', '{23}', '{24}')", tableName, row["NameOfIndicator"], row["StringCode"],
                    row["BudgetCode"],
                    row["ApprovedColumn_1"], row["ApprovedColumn_2"], row["ApprovedColumn_3"], row["ApprovedColumn_4"],
                    row["ApprovedColumn_5"],
                    row["ApprovedColumn_6"], row["ApprovedColumn_7"], row["ApprovedColumn_8"], row["ApprovedColumn_9"],
                    row["ApprovedColumn_10"],
                    row["FulfiledColumn_1"], row["FulfiledColumn_2"], row["FulfiledColumn_3"], row["FulfiledColumn_4"],
                    row["FulfiledColumn_5"],
                    row["FulfiledColumn_6"], row["FulfiledColumn_7"], row["FulfiledColumn_8"], row["FulfiledColumn_9"],
                    row["FulfiledColumn_10"],
                    passport_id);
        }
    }
}