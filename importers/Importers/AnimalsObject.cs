﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class AnimalsObject : ImporterBase
    {
        public AnimalsObject(DataContext ctx)
            : base(ctx, 
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now, 
                Title = "Министерство природных ресурсов Краснодарского края - полномочия в области охраны и использования объектов животного мира",
                SetIdentifier = "faunaprotection", 
                Description = "Сведения о министерстве природных ресурсов Краснодарского края, осуществляющего полномочия в области охраны и использования объектов животного мира",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Сведения о министерстве природных ресурсов Краснодарского края, осуществляющего полномочия в области охраны и использования объектов животного мира",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Терещук Наталья Владимировна",
                        PublisherPhone = "+7-861-2591971",
                        SourceId = Guid.Parse("23DB450B-E82E-494B-9C42-CDF4F7A2FF02"),
                        Subject = "объекты животного мира; среда обитания; охрана; воспроизводство; федеральный государственный надзор; Краснодарский край",
                        Title = "Министерство природных ресурсов Краснодарского края - полномочия в области охраны и использования объектов животного мира"
                        #endregion
                    }
                }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now, 
                Title ="Полномочия в области проведения экологической экспертизы объектов краевого уровня",
                Description = "Полномочия в области проведения экологической экспертизы объектов краевого уровня",
                SetIdentifier = "gosexpertiza",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края Отдел государственной экологической экспертизы",
                        Description ="Полномочия в области проведения экологической экспертизы объектов краевого уровня",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Шуденкова Янина Вячеславовна",
                        PublisherPhone = "+7-861-2518289",
                        SourceId = Guid.Parse("2E0DCBEC-7917-48E3-B195-0D4BA5F0D905"),
                        Subject = "Экспертиза, государственная экологическая экспертиза, общественная экологическая экспертиза, эксперты",
                        Title ="Полномочия в области проведения экологической экспертизы объектов краевого уровня"
                        #endregion
                    }
                }
                #endregion
            }){ }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (OrgFormShort, OrgFormFull, ShortName, Fullname, FactAdress, Juristicdress, Phone, Site, Functions, RegularStaffing, SetsPassports_Id)" +
                " VALUES({1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, '{11}')", 
                tableName,
                row["OrgFormShort"] != null ? "'" + row["OrgFormShort"] + "'" : null,
                row["OrgFormFull"] != null ? "'" + row["OrgFormFull"] + "'" : null,
                row["ShortName"] != null ? "'" + row["ShortName"] + "'" : null,
                row["Fullname"] != null ? "'" + row["Fullname"] + "'" : null,
                row["FactAdress"] != null ? "'" + row["FactAdress"] + "'" : null,
                row["Juristicdress"] != null ? "'" + row["Juristicdress"] + "'" : null,
                row["Phone"] != null ? "'" + row["Phone"] + "'" : null,
                row["Site"] != null ? "'" + row["Site"] + "'" : null,
                row["Functions"] != null ? "'" + row["Functions"] + "'" : null,
                row["RegularStaffing"] != null ? "'" + row["RegularStaffing"] + "'" : null,
                passport_id);
        }


        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, Repository.Entities.TableDisplayTemplate>();
            result.Add("MinPrirodi",
                new Repository.Entities.TableDisplayTemplate
                {
                    Description = "Полномочия в области охраны и использования объектов животного мира",
                    Name = "MinPrirodi",
                    Title = "Полномочия в области охраны и использования объектов животного мира",
                    Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                    {
                        #region
                        new ColumnDisplayTemplate
                        {
                            #region
                            Header = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА",
                            Order = 0,
                            IncludeIntoFilter = true,
                            SubColumns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OrgFormFull",
                                    DescriptionRu = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА ПОЛНОСТЬЮ",
                                    DescritpionEn = "",
                                    Header = "ПОЛНОСТЬЮ",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.Text
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OrgFormShort",
                                    DescriptionRu = "ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА СОКРАЩЕННАЯ",
                                    DescritpionEn = "",
                                    Header = "СОКРАЩЕННАЯ",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.Text
                                }
                                #endregion
                            }
                            #endregion
                        },

                        new ColumnDisplayTemplate
                        {
                            #region
                            Header = "Название",
                            Order = 3,
                            IncludeIntoFilter = true,
                            SubColumns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Fullname",
                                    DescriptionRu = "НАЗВАНИЕ ПОЛНОСТЬЮ",
                                    DescritpionEn = "",
                                    Header = "ПОЛНОСТЬЮ",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.Text
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ShortName",
                                    DescriptionRu = "НАЗВАНИЕ СОКРАЩЕННОЕ (ЕСЛИ ЕСТЬ)",
                                    DescritpionEn = "",
                                    Header = "СОКРАЩЕННАЯ",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.Text
                                }
                                #endregion
                            }
                            #endregion
                        },

                        new ColumnDisplayTemplate
                        {
                            #region
                            Header = "Аддресс",
                            Order = 6,
                            IncludeIntoFilter = true,
                            SubColumns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FactAdress",
                                    DescriptionRu = "АДРЕС ФАКТИЧЕСКИЙ",
                                    DescritpionEn = "",
                                    Header = "Фактический",
                                    IncludeIntoFilter = true,
                                    Order = 7,
                                    Type = ColumnDisplayTemplate.ContentType.Text
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Juristicdress",
                                    DescriptionRu = "АДРЕС ЮРИДИЧЕСКИЙ",
                                    DescritpionEn = "",
                                    Header = "Юредический",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.Text
                                }
                                #endregion
                            }
                            #endregion
                        },

                        new ColumnDisplayTemplate
                        {
                            #region
                            BindedCollumn = "Phone",
                            DescriptionRu = "Телефоны",
                            DescritpionEn = "",
                            Header = "Телефоны",
                            IncludeIntoFilter = true,
                            Order = 8,
                            Type = ColumnDisplayTemplate.ContentType.Text
                            #endregion
                        },

                        new ColumnDisplayTemplate
                        {
                            #region
                            BindedCollumn = "Site",
                            DescriptionRu = "АДРЕС ИНТЕРНЕТ-САЙТА",
                            DescritpionEn = "",
                            Header = "АДРЕС ИНТЕРНЕТ-САЙТА",
                            IncludeIntoFilter = true,
                            Order = 9,
                            Type = ColumnDisplayTemplate.ContentType.Url
                            #endregion
                        },

                        new ColumnDisplayTemplate
                        {
                            #region
                            BindedCollumn = "Functions",
                            DescriptionRu = "ОСНОВНЫЕ ФУНКЦИИ И ЗАДАЧИ МИНИСТЕРСТВА (В ОБЛАСТИ ОХРАНЫ И ИСПОЛЬЗОВАНИЯ ОБЪЕКТОВ ЖИВОТНОГО МИРА)",
                            DescritpionEn = "",
                            Header = "ОСНОВНЫЕ ФУНКЦИИ И ЗАДАЧИ МИНИСТЕРСТВА (В ОБЛАСТИ ОХРАНЫ И ИСПОЛЬЗОВАНИЯ ОБЪЕКТОВ ЖИВОТНОГО МИРА)",
                            IncludeIntoFilter = true,
                            Order = 10,
                            Type = ColumnDisplayTemplate.ContentType.Text
                            #endregion
                        },

                        new ColumnDisplayTemplate
                        {
                            #region
                            BindedCollumn = "RegularStaffing",
                            DescriptionRu = "ШТАТНАЯ ЧИСЛЕННОСТЬ",
                            DescritpionEn = "",
                            Header = "ШТАТНАЯ ЧИСЛЕННОСТЬ",
                            IncludeIntoFilter = true,
                            Order = 11,
                            Type = ColumnDisplayTemplate.ContentType.String
                            #endregion
                        }
                        #endregion
                    }
                });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["MinPrirodi"],
                Name = "MinPrirodi",
                Description = "Министерство природных ресурсов Краснодарского края - полномочия в области охраны и использования объектов животного мира",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "MinPrirodi"
            };
        }
    }
}
