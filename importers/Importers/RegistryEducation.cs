﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class RegistryEducation : ImporterBase
    {
        public RegistryEducation(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_obr],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.obraz],
                CreationDate = DateTime.Now,
                Description = "Реестр образовательных организаций Краснодарского края",
                SetIdentifier = "sch003",
                Title = "Реестр образовательных организаций Краснодарского края",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство образования и науки Краснодарского края",
                        Description = "Реестр образовательных организаций Краснодарского края",
                        IsCurrent= true,
                        PublisherMBox = "nadzor@des.kubannet.ru",
                        PublisherName = "Акиньшин Вадим Валерьевич",
                        PublisherPhone = "+7-861-23165703",
                        Subject = "-;-",
                        Title = "Реестр образовательных организаций Краснодарского края",
                        SourceId = Guid.Parse("6C2BF28F-5F49-4E8E-B86F-C1CB87532295")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("RegistryEducation", new TableDisplayTemplate
            {
                Description = "Реестр образовательных организаций Краснодарского края",
                Name = "RegistryEducation",
                Title = "Реестр образовательных организаций Краснодарского края",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                 {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Region",
                         DescriptionRu = "МО",
                         DescritpionEn = "",
                         Header = "МО",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "INN",
                         DescriptionRu = "ИНН",
                         DescritpionEn = "",
                         Header = "ИНН",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Name",
                         DescriptionRu = "НАИМЕНОВАНИЕ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ",
                         IncludeIntoFilter = true,
                         Order = 2,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "ShortName",
                         DescriptionRu = "СОКРАЩЕННОЕ НАИМЕНОВАНИЕ",
                         DescritpionEn = "",
                         Header = "СОКРАЩЕННОЕ НАИМЕНОВАНИЕ",
                         IncludeIntoFilter = true,
                         Order = 3,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Address",
                         DescriptionRu = "АДРЕСС",
                         DescritpionEn = "",
                         Header = "АДРЕСС",
                         IncludeIntoFilter = true,
                         Order = 4,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Manager",
                         DescriptionRu = "РУКОВОДИТЕЛЬ",
                         DescritpionEn = "",
                         Header = "РУКОВОДИТЕЛЬ",
                         IncludeIntoFilter = true,
                         Order = 5,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "TelephoneNumber",
                         DescriptionRu = "ТЕЛЕФОН",
                         DescritpionEn = "",
                         Header = "ТЕЛЕФОН",
                         IncludeIntoFilter = true,
                         Order = 6,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Email",
                         DescriptionRu = "EMAIL",
                         DescritpionEn = "",
                         Header = "EMAIL",
                         IncludeIntoFilter = true,
                         Order = 7,
                         Type = ColumnDisplayTemplate.ContentType.EMail
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Url",
                         DescriptionRu = "САЙТ",
                         DescritpionEn = "",
                         Header = "САЙТ",
                         IncludeIntoFilter = true,
                         Order = 8,
                         Type = ColumnDisplayTemplate.ContentType.Url
                     }
                 }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["RegistryEducation"],
                Name = "RegistryEducation",
                Description = "Реестр образовательных организаций Краснодарского края",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "RegistryEducation"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (Region, INN, Name, ShortName, Address, Manager, TelephoneNumber, Email, Url, SetsPassports_Id) " +
                "VALUES ('{1}', '{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')", tableName, row["Region"], row["INN"], row["Name"], row["ShortName"],
                row["Address"], row["Manager"], row["TelephoneNumber"], row["Email"], row["Url"], passport_id);
        }
    }
}
