﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class diokk_info : ImporterBase
    {
        public diokk_info(DataContext ctx)
            : base(ctx,
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.dep_imuszh],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.stroitelstvo],
                    CreationDate = DateTime.Now,
                    Description = "Полное и сокращенное наименование, фактический и юридический адреса, адрес электронной почты, адрес официального интернет-сайта (в формате http://сайт.домен), номера телефонов, сведения о функциях и задачах, реквизиты нормативных правовых актов, определяющ",
                    SetIdentifier = "info",
                    Title = "Информация о департаменте имущественных отношений Краснодарского края",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Департамент имущественных отношений Краснодарского края",
                            Description = "Полное и сокращенное наименование, фактический и юридический адреса, адрес электронной почты, адрес официального интернет-сайта (в формате http://сайт.домен), номера телефонов, сведения о функциях и задачах, реквизиты нормативных правовых актов, определяющ",
                            IsCurrent = true,
                            PublisherMBox = "ok.post@diok.ru",
                            PublisherName = "Г.А. Битюков, Е.С. Новикова",
                            PublisherPhone = "+7-861-2673674",
                            SourceId = Guid.Parse("697461F9-9ACE-4741-8BAA-E07A3E19CFD2"),
                            Subject = "Информация о департаменте имущественных отношений Краснодарского края",
                            Title = "Информация о департаменте имущественных отношений Краснодарского края",
                            #endregion
                        }
                    }
                    #endregion
                }) { }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("diokk_info",
                new TableDisplayTemplate
                {
                    Description =
                        "Информация о департаменте имущественных отношений Краснодарского края",
                    Title = "Информация о департаменте имущественных отношений Краснодарского края",
                    Name = "diokk_info",
                    Type = TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FULLNAME",
                                    DescriptionRu = "ПОЛНОЕ НАИМЕНОВАНИЕ",
                                    DescritpionEn = "",
                                    Header = "ПОЛНОЕ НАИМЕНОВАНИЕ",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "SNAME",
                                    DescriptionRu = "СОКРАЩЕННОЕ НАИМЕНОВАНИЕ",
                                    DescritpionEn = "",
                                    Header = "СОКРАЩЕННОЕ НАИМЕНОВАНИЕ",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ADRES_UR",
                                    DescriptionRu = "ЮРЕДИЧЕСКИЙ АДРЕСС",
                                    DescritpionEn = "",
                                    Header = "ЮРЕДИЧЕСКИЙ АДРЕСС",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ADRES_FACT",
                                    DescriptionRu = "ФАКТИЧЕСКИЙ АДРЕС",
                                    DescritpionEn = "",
                                    Header = "ФАКТИЧЕСКИЙ АДРЕС",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "EMAIL",
                                    DescriptionRu = "АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ",
                                    DescritpionEn = "",
                                    Header = "АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "WEBSITE",
                                    DescriptionRu = "АДРЕС ОФИЦИАЛЬНОГО ИНТЕРНЕТ-САЙТА",
                                    DescritpionEn = "",
                                    Header = "АДРЕС ОФИЦИАЛЬНОГО ИНТЕРНЕТ-САЙТА",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "PHONE",
                                    DescriptionRu = "НОМЕРА ТЕЛЕФОНОВ",
                                    DescritpionEn = "",
                                    Header = "НОМЕРА ТЕЛЕФОНОВ",
                                    IncludeIntoFilter = true,
                                    Order = 6,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FUNCTIONS",
                                    DescriptionRu = "ФУНКЦИИ",
                                    DescritpionEn = "",
                                    Header = "ФУНКЦИИ",
                                    IncludeIntoFilter = true,
                                    Order = 7,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TASKS",
                                    DescriptionRu = "ЗАДАЧИ",
                                    DescritpionEn = "",
                                    Header = "ЗАДАЧИ",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "NPA",
                                    DescriptionRu = "РЕКВИЗИТЫ НОРМАТИВНЫХ ПРАВОВЫХ АКТОВ, ОПРЕДЕЛЯЮЩИХ ФУНКЦИИ И ЗАДАЧИ",
                                    DescritpionEn = "",
                                    Header = "РЕКВИЗИТЫ НОРМАТИВНЫХ ПРАВОВЫХ АКТОВ, ОПРЕДЕЛЯЮЩИХ ФУНКЦИИ И ЗАДАЧИ",
                                    IncludeIntoFilter = true,
                                    Order = 9,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                }
                                #endregion
                            }
                });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["diokk_info"],
                Name = "diokk_info",
                Description = "Информация о департаменте имущественных отношений Краснодарского края",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "diokk_info"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return
                string.Format(
                    "INSERT INTO [dbo].[{0}] (FULLNAME, SNAME, ADRES_UR, ADRES_FACT, EMAIL, WEBSITE, PHONE, FUNCTIONS, TASKS, NPA, SetsPassports_Id) " +
                    "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')",
                    tableName, row["FULLNAME"] ?? "null", row["SNAME"] ?? "null",
                    row["ADRES_UR"] ?? "null", row["ADRES_FACT"] ?? "null",
                    row["EMAIL"] ?? "null", row["WEBSITE"] ?? "null",
                    row["PHONE"] ?? "null", row["FUNCTIONS"] ?? "null",
                    row["TASKS"] ?? "null", row["NPA"] ?? "null", passport_id);
        }
    }
}