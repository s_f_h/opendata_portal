﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm1_5 : ImporterBase
    {
        public HuntResourcesInfoForm1_5(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2015, 1, 1),
                Description =
                    "Документированная информация о млекопитающих и птицах занесенных в красную книгу субъекта российской федерации",
                
                SetIdentifier = "gosohotrestr_vidi_ohot_resursov",
                Title = "Сведения о видах охотничьих ресурсов, занесенных в красную книгу субъекта Российской Федерации",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description =
                            "Документированная информация о млекопитающих и птицах занесенных в красную книгу субъекта российской федерации по состоянию на 1 мая 2014 г.",
                        Provinance = "Обновление данных",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName =
                            "Белоусов Игорь Валерьевич, ведущий консультант отдела охраны, воспроизводства и использования объектов животного мира и среды их обитания",
                        PublisherPhone = "+7-861-2591971",
                        SourceId = Guid.Parse("DD99B1E6-9DF8-4576-A9F4-E9DD526F845B"),
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title =
                            "Сведения о видах охотничьих ресурсов, занесенных в красную книгу субъекта Российской Федерации"
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm1_5", new TableDisplayTemplate
            {
                Description = "Сведения о видах охотничьих ресурсов, занесенных в красную книгу субъекта Российской Федерации",
                Name = "HuntResourcesInfoForm1_5",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "HuntResourceTypeF1_5",
                        DescriptionRu = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                        DescritpionEn = "",
                        Header = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "RedBookF1_5",
                        DescriptionRu = "КРАСНАЯ КНИГА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        DescritpionEn = "",
                        Header = "КРАСНАЯ КНИГА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "RequisitesF1_5",
                        DescriptionRu = "РЕКВИЗИТЫ НОРМАТИВНОГО ПРАВОВОГО АКТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        DescritpionEn = "",
                        Header = "РЕКВИЗИТЫ НОРМАТИВНОГО ПРАВОВОГО АКТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    }
                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm1_5"],
                Name = "HuntResourcesInfoForm1_5",
                Description = "Сведения о видах охотничьих ресурсов, занесенных в красную книгу субъекта Российской Федерации",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm1_5"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (HuntResourceTypeF1_5, RedBookF1_5, RequisitesF1_5, SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}')",
                                 tableName, row["HuntResourceTypeF1_5"], row["RedBookF1_5"],
                                 row["RequisitesF1_5"], passport_id);
        }
    }
}