﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm1_4 : ImporterBase
    {
        public HuntResourcesInfoForm1_4(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2015, 1, 1),
                Description =
                    "Документированная информация о гибели охотничьих ресурсов",
                SetIdentifier = "gosohotrestr_gibel_ohot_resursov",
                Title =
                    "Сведения о гибели охотничьих ресурсов",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description =
                            "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                            IsCurrent = true,
                        Provinance = "Обновление данных",
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName =
                            "Белоусов Игорь Валерьевич, ведущий консультант отдела охраны, воспроизводства и использования объектов животного мира и среды их обитания",
                        PublisherPhone = "+7-861-2591971",
                        SourceId = Guid.Parse("589D95FB-87A9-4D79-B198-F18D5DCA2A7C"),
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title =
                            "Сведения о гибели охотничьих ресурсов в 2014 г."
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm1_4", new TableDisplayTemplate
            {
                Description =
                    "Документированная информация о гибели охотничьих ресурсов",
                Name = "HuntResourcesInfoForm1_4",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "HuntResourceTypeF41",
                        DescriptionRu = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                        DescritpionEn = "",
                        Header = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ГИБЕЛЬ, ОСОБЕЙ",
                        Order = 1,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Погибло особей",
                            Name = "AnimalsDiedF41",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "AnimalsDiedF41_TotalF41",
                                    DescriptionRu =
                                        "Всего",
                                    DescritpionEn = "",
                                    Header =
                                        "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "AnimalsDiedF41_AdultF41",
                                    DescriptionRu =
                                        "Взрослых",
                                    DescritpionEn = "",
                                    Header =
                                        "Взрослых",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "AnimalsDiedF41_YoungerOneYearF41",
                                    DescriptionRu =
                                        "До 1 года",
                                    DescritpionEn = "",
                                    Header =
                                        "До 1 года",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ОТ БОЛЕЗНЕЙ",
                        Order = 5,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Погибло от болезней",
                            Name = "FromIllnessF41",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromIllnessF41_TotalF41",
                                    DescriptionRu =
                                        "Всего",
                                    DescritpionEn = "",
                                    Header =
                                        "Взрослых",
                                    IncludeIntoFilter = true,
                                    Order = 6,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromIllnessF41_AdultF41",
                                    DescriptionRu =
                                        "Взрослых",
                                    DescritpionEn = "",
                                    Header =
                                        "Взрослых",
                                    IncludeIntoFilter = true,
                                    Order = 7,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromIllnessF41_YoungerOneYearF41",
                                    DescriptionRu =
                                        "До 1 года",
                                    DescritpionEn = "",
                                    Header =
                                        "До 1 года",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ОТ НЕЗАКОННОЙ ОХОТЫ",
                        Order = 9,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Погибло от незаконной охоты",
                            Name = "FromPoachingF41",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromPoachingF41_TotalF41",
                                    DescriptionRu =
                                        "Всего",
                                    DescritpionEn = "",
                                    Header =
                                        "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 10,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromPoachingF41_AdultF41",
                                    DescriptionRu =
                                        "Взрослых",
                                    DescritpionEn = "",
                                    Header =
                                        "Взрослых",
                                    IncludeIntoFilter = true,
                                    Order = 11,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromPoachingF41_YoungerOneYearF41",
                                    DescriptionRu =
                                        "До 1 года",
                                    DescritpionEn = "",
                                    Header =
                                        "До 1 года",
                                    IncludeIntoFilter = true,
                                    Order = 12,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ПО ДРУГИМ ПРИЧИНАМ",
                        Order = 13,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Погибло по другим причинам",
                            Name = "FromAnotherReasonsF41",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromAnotherReasonsF41_TotalF41",
                                    DescriptionRu =
                                        "Всего",
                                    DescritpionEn = "",
                                    Header =
                                        "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 14,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromAnotherReasonsF41_AdultF41",
                                    DescriptionRu =
                                        "Взрослых",
                                    DescritpionEn = "",
                                    Header =
                                        "Взрослых",
                                    IncludeIntoFilter = true,
                                    Order = 15,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromAnotherReasonsF41_YoungerOneYearF41",
                                    DescriptionRu =
                                        "До 1 года",
                                    DescritpionEn = "",
                                    Header =
                                        "До 1 года",
                                    IncludeIntoFilter = true,
                                    Order = 16,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "CommentsF41",
                        DescriptionRu =
                            "ПРИМЕЧАНИЯ",
                        DescritpionEn = "",
                        Header =
                            "ПРИМЕЧАНИЯ",
                        IncludeIntoFilter = true,
                        Order = 17,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "В ДТП",
                        Order = 18,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Погибло в ДТП",
                            Name = "FromTrafficAccidentF41",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromTrafficAccidentF41_TotalF41",
                                    DescriptionRu =
                                        "Всего",
                                    DescritpionEn = "",
                                    Header =
                                        "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 19,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromTrafficAccidentF41_AdultF41",
                                    DescriptionRu =
                                        "Взрослых",
                                    DescritpionEn = "",
                                    Header =
                                        "Взрослых",
                                    IncludeIntoFilter = true,
                                    Order = 20,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FromTrafficAccidentF41_YoungerOneYearF41",
                                    DescriptionRu =
                                        "До 1 года",
                                    DescritpionEn = "",
                                    Header =
                                        "До 1 года",
                                    IncludeIntoFilter = true,
                                    Order = 21,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                        }
                    },
                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm1_4"],
                Name = "HuntResourcesInfoForm1_4",
                Description =
                    "Документированная информация о гибели охотничьих ресурсов",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm1_4"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (HuntResourceTypeF41, AnimalsDiedF41_TotalF41, " +
                                 "AnimalsDiedF41_AdultF41, AnimalsDiedF41_YoungerOneYearF41, " +
                                 "FromIllnessF41_TotalF41, FromIllnessF41_AdultF41, " +
                                 "FromIllnessF41_YoungerOneYearF41, FromPoachingF41_TotalF41, " +
                                 "FromPoachingF41_AdultF41, FromPoachingF41_YoungerOneYearF41, " +
                                 "FromAnotherReasonsF41_TotalF41, FromAnotherReasonsF41_AdultF41, " +
                                 "FromAnotherReasonsF41_YoungerOneYearF41, CommentsF41, " +
                                 "FromTrafficAccidentF41_TotalF41, FromTrafficAccidentF41_AdultF41," +
                                 "FromTrafficAccidentF41_YoungerOneYearF41, SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', " +
                                 "'{17}', '{18}')",
                                 tableName,
                                 row["HuntResourceTypeF41"], row["AnimalsDiedF41_TotalF41"],
                                 row["AnimalsDiedF41_AdultF41"], row["AnimalsDiedF41_YoungerOneYearF41"],
                                 row["FromIllnessF41_TotalF41"], row["FromIllnessF41_AdultF41"],
                                 row["FromIllnessF41_YoungerOneYearF41"], row["FromPoachingF41_TotalF41"],
                                 row["FromPoachingF41_AdultF41"], row["FromPoachingF41_YoungerOneYearF41"],
                                 row["FromAnotherReasonsF41_TotalF41"], row["FromAnotherReasonsF41_AdultF41"],
                                 row["FromAnotherReasonsF41_YoungerOneYearF41"], row["CommentsF41"],
                                 row["FromTrafficAccidentF41_TotalF41"], row["FromTrafficAccidentF41_AdultF41"],
                                 row["FromTrafficAccidentF41_YoungerOneYearF41"], passport_id);
        }
    }
}