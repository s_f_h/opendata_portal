﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers
{
    internal class TotalPivot : ImporterBase
    {
        public TotalPivot(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                CreationDate = new DateTime(2015, 1, 1),
                Description = "Сводная таблица",
                SetIdentifier = "report_pivot_table",
                Title = "Сводная таблица",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство финансов Краснодарского края",
                        Description = "Сводная таблица на 01.01.2015",
                        Provinance = "Начальная версия",
                        PublisherMBox = "minfin@krasnodar.ru",
                        PublisherName = "Кудинов Василий Петрович",
                        PublisherPhone = "+7-861-2145771",
                        SourceId = Guid.Parse("CF0114F2-CFF1-4A00-B814-DB1ABFE54D4A"),
                        Subject = "Сводная таблица",
                        Title = "Сводная таблица на 01.01.2015"
                        #endregion
                    },

                    new ImportingSetVersionSettings
                    {
                        #region
                        Description = "Сводная таблица на 01.04.2015",
                        Provinance = "Обновление данных",
                        SourceId = Guid.Parse("EC57B3AA-27F8-46DA-A8EE-87EAF098C700"),
                        Title = "Сводная таблица на 01.04.2015",
                        IsCurrent = true,
                        #endregion
                    }
                }
                #endregion
            }) { }

        protected override Dictionary<string, Repository.Entities.TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("PivotTable", new TableDisplayTemplate
            {
                Description = "Сводная таблица",
                Name = "PivotTable",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                 {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "NameOfIndicator",
                         DescriptionRu = "НАИМЕНОВАНИЕ ПОКАЗАТЕЛЯ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ ПОКАЗАТЕЛЯ",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },

                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "StringCode",
                         DescriptionRu = "КОД СТРОКИ",
                         DescritpionEn = "",
                         Header = "КОД СТРОКИ",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },

                     new ColumnDisplayTemplate
                     {
                         Header = "ПОСТУПЛЕНИЯ:",
                         Order = 2,
                         InnerTable = new TableDisplayTemplate
                         {
                              Description = "Сводная таблица Поступления",
                              Name = "PivotTable_Incomes",
                              Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                              Columns = new ColumnDisplayTemplate[]
                              {
                                  new ColumnDisplayTemplate
                                  {
                                      BindedCollumn = "IncomeColumn_1",
                                      DescriptionRu = "ПОСТУПЛЕНИЯ: БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                      DescritpionEn = "",
                                      Header = "БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                      IncludeIntoFilter = true,
                                      Order = 3,
                                      Type = ColumnDisplayTemplate.ContentType.String
                                  },
                                  new ColumnDisplayTemplate
                                  {
                                      BindedCollumn = "IncomeColumn_2",
                                      DescriptionRu = "ПОСТУПЛЕНИЯ: БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                                      DescritpionEn = "",
                                      Header = "БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                                      IncludeIntoFilter = true,
                                      Order = 4,
                                      Type = ColumnDisplayTemplate.ContentType.String
                                  },
                                  new ColumnDisplayTemplate
                                  {
                                      BindedCollumn = "IncomeColumn_3",
                                      DescriptionRu = "ПОСТУПЛЕНИЯ: БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                                      DescritpionEn = "",
                                      Header = "БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                                      IncludeIntoFilter = true,
                                      Order = 5,
                                      Type = ColumnDisplayTemplate.ContentType.String
                                  },
                                  new ColumnDisplayTemplate
                                  {
                                      BindedCollumn = "IncomeColumn_4",
                                      DescriptionRu = "ПОСТУПЛЕНИЯ: БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                                      DescritpionEn = "",
                                      Header = "БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                                      IncludeIntoFilter = true,
                                      Order = 6,
                                      Type = ColumnDisplayTemplate.ContentType.String
                                  },
                                  new ColumnDisplayTemplate
                                  {
                                      BindedCollumn = "IncomeColumn_5",
                                      DescriptionRu = "ПОСТУПЛЕНИЯ: БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                                      DescritpionEn = "",
                                      Header = "БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                                      IncludeIntoFilter = true,
                                      Order = 7,
                                      Type = ColumnDisplayTemplate.ContentType.String
                                  },
                                  new ColumnDisplayTemplate
                                  {
                                      BindedCollumn = "IncomeColumn_6",
                                      DescriptionRu = "ПОСТУПЛЕНИЯ: БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                      DescritpionEn = "",
                                      Header = "БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                      IncludeIntoFilter = true,
                                      Order = 8,
                                      Type = ColumnDisplayTemplate.ContentType.String
                                  },
                                  new ColumnDisplayTemplate
                                  {
                                      BindedCollumn = "Summ",
                                      DescriptionRu = "ПОСТУПЛЕНИЯ: ИТОГО",
                                      DescritpionEn = "",
                                      Header = "ИТОГО",
                                      IncludeIntoFilter = true,
                                      Order = 9,
                                      Type = ColumnDisplayTemplate.ContentType.String
                                  }
                              }
                         }
                     }
                 }
            });
            return result;
        }

        protected override Repository.Entities.DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["PivotTable"],
                Name = "PivotTable",
                Description = "Сводная таблица",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "PivotTable"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (NameOfIndicator, StringCode, IncomeColumn_1, IncomeColumn_2, IncomeColumn_3, IncomeColumn_4, " +
                "IncomeColumn_5, IncomeColumn_6, Summ, SetsPassports_Id) VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')",
                tableName, row["NameOfIndicator"], row["StringCode"], row["IncomeColumn_1"], row["IncomeColumn_2"], row["IncomeColumn_3"], row["IncomeColumn_4"],
                row["IncomeColumn_5"], row["IncomeColumn_6"], row["Summ"], passport_id);
        }
    }
}
