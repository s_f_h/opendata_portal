﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class diokk_struct : ImporterBase
    {
        public diokk_struct(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.dep_imuszh],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.stroitelstvo],
                CreationDate = new DateTime(2015, 1, 1),
                Description =
                    "Наименование структурного подразделения, функции и задачи структурного подразделения, юридический и фактический адреса, номера телефонов",
                SetIdentifier = "structure",
                Title = "Структура департамента имущественных отношений Краснодарского края",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Департамент имущественных отношений Краснодарского края",
                        Description = "ДИОКК. Структура департамента",
                        Provinance = "Обновление данных",
                        PublisherMBox = "ok.post@diok.ruu",
                        PublisherName = "Е.С. Новикова",
                        PublisherPhone = "+7-8612-682954",
                        SourceId = Guid.Parse("27D6EA62-E5F2-46E5-998F-211160A887D4"),
                        Subject = "Структура департамента",
                        Title = "Структура департамента имущественных отношений Краснодарского края",
                        IsCurrent = true
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("diokk_struct", new TableDisplayTemplate
            {
                Description = "Структура департамента имущественных отношений",
                Name = "diokk_struct",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "NAME",
                        DescriptionRu = "НАИМЕНОВАНИЕ СТРУКТУРНОГО ПОДРАЗДЕЛЕНИЯ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ СТРУКТУРНОГО ПОДРАЗДЕЛЕНИЯ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "PARENT",
                        DescriptionRu = "ВХОДИТ В СОСТАВ",
                        DescritpionEn = "",
                        Header = "ВХОДИТ В СОСТАВ",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "FUNCTIONS",
                        DescriptionRu = "ФУНКЦИИ",
                        DescritpionEn = "",
                        Header = "ФУНКЦИИ",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TASKS",
                        DescriptionRu = "ЗАДАЧИ",
                        DescritpionEn = "",
                        Header = "ЗАДАЧИ",
                        IncludeIntoFilter = true,
                        Order = 3,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ADRES_UR",
                        DescriptionRu = "ЮРИДИЧЕСКИЙ АДРЕС",
                        DescritpionEn = "",
                        Header = "ЮРИДИЧЕСКИЙ АДРЕС",
                        IncludeIntoFilter = true,
                        Order = 4,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ADRES_FACT",
                        DescriptionRu = "ФАКТИЧЕСКИЙ АДРЕС",
                        DescritpionEn = "",
                        Header = "ФАКТИЧЕСКИЙ АДРЕС",
                        IncludeIntoFilter = true,
                        Order = 5,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "PHONE",
                        DescriptionRu = "НОМЕРА ТЕЛЕФОНОВ",
                        DescritpionEn = "",
                        Header = "НОМЕРА ТЕЛЕФОНОВ",
                        IncludeIntoFilter = true,
                        Order = 6,
                        Type = ColumnDisplayTemplate.ContentType.String
                    }
                    #endregion
                }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["diokk_struct"],
                Name = "diokk_struct",
                Description = "Структура департамента имущественных отношений",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "diokk_struct"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (NAME, PARENT, FUNCTIONS, TASKS, ADRES_UR, ADRES_FACT, PHONE, SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')",
                                 tableName, row["NAME"], row["PARENT"],
                                 row["FUNCTIONS"], row["TASKS"],
                                 row["ADRES_UR"], row["ADRES_FACT"],
                                 row["PHONE"], passport_id);
        }
    }
}