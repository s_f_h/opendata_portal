﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class CultureObject : ImporterBase
    {
        public CultureObject(DataContext ctx)
            : base(ctx,
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.upr_gos_ohr],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.kult],
                    CreationDate = DateTime.Now,
                    Description = "Информирование заинтересованных лиц об объектах культурного наследия федерального значения, расположенных на территории Краснодарского края",
                    SetIdentifier = "kultnasl",
                    Title = "Перечень объектов культурного наследия федерального значения, расположенных на территории Краснодарского края",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Управление государственной охраны объектов культурного наследия Краснодарского края",
                            Description = "Информирование заинтересованных лиц об объектах культурного наследия федерального значения, расположенных на территории Краснодарского края",
                            IsCurrent = true,
                            PublisherMBox = "uorn@krasnodar.ru",
                            PublisherName = "Гончаров Алексей Владимирович",
                            PublisherPhone = "+7-861-2680393",
                            SourceId = Guid.Parse("90EF9750-432C-48CB-8E57-2A25318F4656"),
                            Subject = "Информирование заинтересованных лиц об объектах культурного наследия федерального значения, расположенных на территории Краснодарского края",
                            Title = "Перечень объектов культурного наследия федерального значения, расположенных на территории Краснодарского края",
                            #endregion
                        }
                    }
                    #endregion
                }) { }


        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("CultureObject",
                new TableDisplayTemplate
                {
                    Description = "Информирование заинтересованных лиц об объектах культурного наследия",
                    Title = "Культурные объекты",
                    Name = "CultureObject",
                    Type = TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                    {
                        #region
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "OKNNum",
                            DescriptionRu = "№ ОКН ПО ГОСУДАРСТВЕННОМУ СПИСКУ ПАМЯТНИКОВ",
                            DescritpionEn = "",
                            Header = "№ ОКН ПО ГОСУДАРСТВЕННОМУ СПИСКУ ПАМЯТНИКОВ",
                            IncludeIntoFilter = true,
                            Order = 0,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "OKNName",
                            DescriptionRu = "НАИМЕНОВАНИЕ ОКН ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ (СОГЛАСНО НОРМАТИВНО-ПРАВОВОМУ АКТУ ПОСТАНОВКИ НА ГОСУДАСРТВЕННУЮ ОХРАНУ)",
                            DescritpionEn = "",
                            Header = "НАИМЕНОВАНИЕ ОКН ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ (СОГЛАСНО НОРМАТИВНО-ПРАВОВОМУ АКТУ ПОСТАНОВКИ НА ГОСУДАСРТВЕННУЮ ОХРАНУ)",
                            IncludeIntoFilter = true,
                            Order = 1,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "OKNAddres",
                            DescriptionRu = "АДРЕС (МЕСТОНАХОЖДЕНИЕ) ОКН ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ (СОГЛАСНО НОРМАТИВНО-ПРАВОВОМУ АКТУ ПОСТАНОВКИ НА ГОСУДАРСТВЕННУЮ ОХРАНУ)",
                            DescritpionEn = "",
                            Header = "АДРЕС (МЕСТОНАХОЖДЕНИЕ) ОКН ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ (СОГЛАСНО НОРМАТИВНО-ПРАВОВОМУ АКТУ ПОСТАНОВКИ НА ГОСУДАРСТВЕННУЮ ОХРАНУ)",
                            IncludeIntoFilter = true,
                            Order = 2,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "OKNAdjustedAddres",
                            DescriptionRu = "УТОЧНЕННЫЙ АДРЕС (МЕСТОНАХОЖДЕНИЕ) ПО ДОКУМЕНТАМ ТЕХНИЧЕСКОЙ ИНВЕНТАРИЗАЦИИ, ОБСЛЕДОВАНИЙ И Т.Д. (ФАКТИЧЕСКОЕ МЕСТОРАСПОЛОЖЕНИЕ)",
                            DescritpionEn = "",
                            Header = "УТОЧНЕННЫЙ АДРЕС (МЕСТОНАХОЖДЕНИЕ) ПО ДОКУМЕНТАМ ТЕХНИЧЕСКОЙ ИНВЕНТАРИЗАЦИИ, ОБСЛЕДОВАНИЙ И Т.Д. (ФАКТИЧЕСКОЕ МЕСТОРАСПОЛОЖЕНИЕ)",
                            IncludeIntoFilter = true,
                            Order = 3,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "AreaName",
                            DescriptionRu = "НАИМЕНОВАНИЕ РАЙОНА, ГОРОДСКОГО ОКРУГА",
                            DescritpionEn = "",
                            Header = "НАИМЕНОВАНИЕ РАЙОНА, ГОРОДСКОГО ОКРУГА",
                            IncludeIntoFilter = true,
                            Order = 4,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "SubjectName",
                            DescriptionRu = "НАИМЕНОВАНИЕ СУБЪЕКТА РФ",
                            DescritpionEn = "",
                            Header = "НАИМЕНОВАНИЕ СУБЪЕКТА РФ",
                            IncludeIntoFilter = true,
                            Order = 5,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "OKNVIew",
                            DescriptionRu = "ВИД ОКН (ПАМЯТНИК, АНСАМБЛЬ, ДОСТОПРИМЕЧАТЕЛЬНОЕ МЕСТО)",
                            DescritpionEn = "",
                            Header = "ВИД ОКН (ПАМЯТНИК, АНСАМБЛЬ, ДОСТОПРИМЕЧАТЕЛЬНОЕ МЕСТО)",
                            IncludeIntoFilter = true,
                            Order = 6,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "OKNAccessory",
                            DescriptionRu = "ВИДОВАЯ ПРИНАДЛЕЖНОСТЬ ОКН (АРХЕОЛОГИЯ, ИСТОРИЯ, ГРАДОСТРОИТЕЛЬСТВО, АРХИТЕКТУРА, МОНУМЕНТАЛЬНОЕ ИСКУССТВО)",
                            DescritpionEn = "",
                            Header = "ВИДОВАЯ ПРИНАДЛЕЖНОСТЬ ОКН (АРХЕОЛОГИЯ, ИСТОРИЯ, ГРАДОСТРОИТЕЛЬСТВО, АРХИТЕКТУРА, МОНУМЕНТАЛЬНОЕ ИСКУССТВО)",
                            IncludeIntoFilter = true,
                            Order = 7,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        },
                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "NormativeLegalAct",
                            DescriptionRu = "НОРМАТИВНО-ПРАВОВОЙ АКТ ПОСТАНОВКИ НА ГОСУДАРСТВЕННУЮ ОХРАНУ ",
                            DescritpionEn = "",
                            Header = "НОРМАТИВНО-ПРАВОВОЙ АКТ ПОСТАНОВКИ НА ГОСУДАРСТВЕННУЮ ОХРАНУ ",
                            IncludeIntoFilter = true,
                            Order = 7,
                            Type = ColumnDisplayTemplate.ContentType.String,       
                        }
                        #endregion
                    }
                });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["CultureObject"],
                Name = "CultureObject",
                Description = "Информирование заинтересованных лиц об объектах культурного наследия федерального значения, расположенных на территории Краснодарского края",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "CultureObject"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format(
                    "INSERT INTO [dbo].[{0}] (OKNNum, OKNName, OKNAddres, OKNAdjustedAddres, AreaName, SubjectName, OKNVIew, OKNAccessory, " +
                    "NormativeLegalAct, SetsPassports_Id) VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')",
                    tableName, row["OKNNum"] ?? "null",
                    row["OKNName"] ?? (row["OKNName"].ToString().Contains("'") ? row["OKNName"].ToString().Replace("'", "''") : row["OKNName"].ToString()),
                    row["OKNAddres"] ?? (row["OKNAddres"].ToString().Contains("'") ? row["OKNAddres"].ToString().Replace("'", "''") : row["OKNAddres"].ToString()),
                    row["OKNAdjustedAddres"] ?? (row["OKNAdjustedAddres"].ToString().Contains("'") ? row["OKNAdjustedAddres"].ToString().Replace("'", "''") : row["OKNAdjustedAddres"].ToString()),
                    row["AreaName"] ?? (row["AreaName"].ToString().Contains("'") ? row["AreaName"].ToString().Replace("'", "''") : row["AreaName"].ToString()),
                    row["SubjectName"] ?? (row["SubjectName"].ToString().Contains("'") ? row["SubjectName"].ToString().Replace("'", "''") : row["SubjectName"].ToString()),
                    row["OKNVIew"] ?? (row["OKNVIew"].ToString().Contains("'") ? row["OKNVIew"].ToString().Replace("'", "''") : row["OKNVIew"].ToString()),
                    row["OKNAccessory"] ?? (row["OKNAccessory"].ToString().Contains("'") ? row["OKNAccessory"].ToString().Replace("'", "''") : row["OKNAccessory"].ToString()),
                    row["NormativeLegalAct"] ?? (row["NormativeLegalAct"].ToString().Contains("'") ? row["NormativeLegalAct"].ToString().Replace("'", "''") : row["NormativeLegalAct"].ToString()), 
                    passport_id);
        }
    }
}