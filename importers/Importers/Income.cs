﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers
{
    internal class Income : ImporterBase
    {

        public Income(DataContext ctx)
            : base(ctx,
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                    CreationDate = new DateTime(2015, 1, 1),
                    Description = "Годовой отчет по доходам",
                    SetIdentifier = "report_year_incomes",
                    Title = "Годовой отчет по доходам",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Yearly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство финансов Краснодарского края",
                            Description = "Годовой отчет по доходам на 01.01.2015",
                            IsCurrent = true,
                            PublisherMBox = "minfin@krasnodar.ru",
                            PublisherName = "Кудинов Василий Петрович",
                            PublisherPhone = "+7-861-2145771",
                            SourceId = Guid.Parse("F36491FF-D9E2-4E4B-B291-E054C2B9B118"),
                            Subject = "Годовой отчет по доходам на 01.01.2015",
                            Title = "Годовой отчет по доходам на 01.01.2015"
                            #endregion
                        }
                    }
                    #endregion
                },
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                    CreationDate = new DateTime(2015, 4, 1),
                    Description = "Месячный отчет по доходам",
                    SetIdentifier = "report_month_incomes",
                    Title = "Месячный отчет по доходам",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Monthly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство финансов Краснодарского края",
                            Description = "Месячный отчет по доходам на 01.04.2015",
                            IsCurrent = true,
                            PublisherMBox = "minfin@krasnodar.ru",
                            PublisherName = "Кудинов Василий Петрович",
                            PublisherPhone = "+7-861-2145771",
                            SourceId = Guid.Parse("1B2C3C9C-9D24-4B54-A6CC-74647DBF8A4F"),
                            Subject = "Месячный отчет по доходам на 01.04.2015",
                            Title = "Месячный отчет по доходам на 01.04.2015"
                            #endregion
                        },
                    }
                    #endregion
                },
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                    CreationDate = new DateTime(2015, 4, 1),
                    Description = "Месячный отчет по источникам",
                    SetIdentifier = "report_month_sources",
                    Title = "Месячный отчет по источникам",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Monthly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство финансов Краснодарского края",
                            Description = "Месячный отчет по источникам на 01.04.2015",
                            IsCurrent = true,
                            PublisherMBox = "minfin@krasnodar.ru",
                            PublisherName = "Кудинов Василий Петрович",
                            PublisherPhone = "+7-861-2145771",
                            SourceId = Guid.Parse("A70E4BC2-AA23-4AF8-8449-BBEFFC5F35CC"),
                            Subject = "Месячный отчет по источникам на 01.04.2015",
                            Title = "Месячный отчет по источникам на 01.04.2015"
                            #endregion
                        },
                    }
                    #endregion
                },
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                    CreationDate = new DateTime(2015, 1, 1),
                    Description = "Годовой отчет по источникам",
                    SetIdentifier = "report_year_sources",
                    Title = "Годовой отчет по источникам",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Yearly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство финансов Краснодарского края",
                            Description = "Годовой отчет по источникам на 01.01.2015",
                            IsCurrent = true,
                            PublisherMBox = "minfin@krasnodar.ru",
                            PublisherName = "Кудинов Василий Петрович",
                            PublisherPhone = "+7-861-2145771",
                            SourceId = Guid.Parse("F36491FF-D9E2-4E4B-B291-E054C2B9B118"),
                            Subject = "Годовой отчет по источникам на 01.01.2015",
                            Title = "Годовой отчет по источникам на 01.01.2015"
                            #endregion
                        }
                    }
                    #endregion
                }
            ) { asIs = true; }


        protected override Dictionary<string, Repository.Entities.TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("ReportOnIncome", new TableDisplayTemplate
            {
                Description = "Отчет по доходам",
                Name = "ReportOnIncome",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                 {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "NameOfIndicator",
                         DescriptionRu = "НАИМЕНОВАНИЕ ПОКАЗАТЕЛЯ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ ПОКАЗАТЕЛЯ",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },

                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "StringCode",
                         DescriptionRu = "КОД СТРОКИ",
                         DescritpionEn = "",
                         Header = "КОД СТРОКИ",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },

                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "BudgetCode",
                         DescriptionRu = "КОД ДОХОДА ПО БЮДЖЕТНОЙ КЛАССИФИКАЦИИ",
                         DescritpionEn = "",
                         Header = "КОД ДОХОДА ПО БЮДЖЕТНОЙ КЛАССИФИКАЦИИ",
                         IncludeIntoFilter = true,
                         Order = 2,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },

                     new ColumnDisplayTemplate
                     {
                         Header = "УТВЕРЖДЕНО",
                         Order = 3,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Отчет по доходам, раздел Утверждено",
                             Name = "ReportOnIncome_Approved",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_1",
                                     DescriptionRu = "УТВЕРЖДЕНО КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     DescritpionEn = "",
                                     Header = "КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     IncludeIntoFilter = true,
                                     Order = 4,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_2",
                                     DescriptionRu = "УТВЕРЖДЕНО ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ И БЮДЖЕТА ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     DescritpionEn = "",
                                     Header = "ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ И БЮДЖЕТА ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     IncludeIntoFilter = true,
                                     Order = 5,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_3",
                                     DescriptionRu = "УТВЕРЖДЕНО КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     DescritpionEn = "",
                                     Header = "КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     IncludeIntoFilter = true,
                                     Order = 6,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_4",
                                     DescriptionRu = "УТВЕРЖДЕНО ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     DescritpionEn = "",
                                     Header = "ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     IncludeIntoFilter = true,
                                     Order = 7,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_5",
                                     DescriptionRu = "УТВЕРЖДЕНО БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     IncludeIntoFilter = true,
                                     Order = 8,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_6",
                                     DescriptionRu = "УТВЕРЖДЕНО БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                                     IncludeIntoFilter = true,
                                     Order = 9,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_7",
                                     DescriptionRu = "УТВЕРЖДЕНО БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                                     IncludeIntoFilter = true,
                                     Order = 10,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_8",
                                     DescriptionRu = "УТВЕРЖДЕНО БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                                     IncludeIntoFilter = true,
                                     Order = 11,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_9",
                                     DescriptionRu = "УТВЕРЖДЕНО БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                                     IncludeIntoFilter = true,
                                     Order = 12,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "ApprovedColumn_10",
                                     DescriptionRu = "УТВЕРЖДЕНО БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     IncludeIntoFilter = true,
                                     Order = 13,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
                             }
                         }
                     },

                     new ColumnDisplayTemplate
                     {
                         Header = "ИСПОЛНЕНО",
                         Order = 14,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Отчет по доходам, раздел Исполнено",
                             Name = "ReportOnIncome_Fulfilled",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_1",
                                     DescriptionRu = "ИСПОЛНЕНО КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     DescritpionEn = "",
                                     Header = "КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     IncludeIntoFilter = true,
                                     Order = 15,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_2",
                                     DescriptionRu = "ИСПОЛНЕНО ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ И БЮДЖЕТА ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     DescritpionEn = "",
                                     Header = "ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ И БЮДЖЕТА ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     IncludeIntoFilter = true,
                                     Order = 16,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_3",
                                     DescriptionRu = "ИСПОЛНЕНО КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     DescritpionEn = "",
                                     Header = "КОНСОЛИДИРОВАННЫЙ БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     IncludeIntoFilter = true,
                                     Order = 17,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_4",
                                     DescriptionRu = "ИСПОЛНЕНО ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     DescritpionEn = "",
                                     Header = "ПОДЛЕЖАЩИЕ ИСКЛЮЧЕНИЮ В РАМКАХ КОНСОЛИДИРОВАННОГО БЮДЖЕТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     IncludeIntoFilter = true,
                                     Order = 18,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_5",
                                     DescriptionRu = "ИСПОЛНЕНО БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТ СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                                     IncludeIntoFilter = true,
                                     Order = 19,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_6",
                                     DescriptionRu = "ИСПОЛНЕНО БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТЫ ВНУТРИГОРОДСКИХ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ ГОРОДОВ ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ МОСКВЫ И САНКТ-ПЕТЕРБУРГА",
                                     IncludeIntoFilter = true,
                                     Order = 20,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_7",
                                     DescriptionRu = "ИСПОЛНЕНО БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТЫ ГОРОДСКИХ ОКРУГОВ",
                                     IncludeIntoFilter = true,
                                     Order = 21,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_8",
                                     DescriptionRu = "ИСПОЛНЕНО БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТЫ МУНИЦИПАЛЬНЫХ РАЙОНОВ",
                                     IncludeIntoFilter = true,
                                     Order = 22,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_9",
                                     DescriptionRu = "ИСПОЛНЕНО БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТЫ ГОРОДСКИХ И СЕЛЬСКИХ ПОСЕЛЕНИЙ",
                                     IncludeIntoFilter = true,
                                     Order = 23,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },

                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "FulfiledColumn_10",
                                     DescriptionRu = "ИСПОЛНЕНО БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     DescritpionEn = "",
                                     Header = "БЮДЖЕТ ТЕРРИТОРИАЛЬНОГО ГОСУДАРСТВЕННОГО ВНЕБЮДЖЕТНОГО ФОНДА",
                                     IncludeIntoFilter = true,
                                     Order = 24,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
                             }
                         }
                     }
                 }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["ReportOnIncome"],
                Name = "ReportOnIncome",
                Description = "Отчет по доходам",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "ReportOnIncome"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (NameOfIndicator, StringCode, BudgetCode, ApprovedColumn_1, ApprovedColumn_2, " +
                "ApprovedColumn_3, ApprovedColumn_4, ApprovedColumn_5, ApprovedColumn_6, ApprovedColumn_7, ApprovedColumn_8, ApprovedColumn_9, " +
                "ApprovedColumn_10, FulfiledColumn_1, FulfiledColumn_2, FulfiledColumn_3, FulfiledColumn_4, FulfiledColumn_5, FulfiledColumn_6, " +
                "FulfiledColumn_7, FulfiledColumn_8, FulfiledColumn_9, FulfiledColumn_10, SetsPassports_Id) VALUES ('{1}', '{2}', '{3}', '{4}', " +
                "'{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', " +
                "'{21}', '{22}', '{23}', '{24}')", tableName, row["NameOfIndicator"], row["StringCode"], row["BudgetCode"],
                row["ApprovedColumn_1"], row["ApprovedColumn_2"], row["ApprovedColumn_3"], row["ApprovedColumn_4"], row["ApprovedColumn_5"],
                row["ApprovedColumn_6"], row["ApprovedColumn_7"], row["ApprovedColumn_8"], row["ApprovedColumn_9"], row["ApprovedColumn_10"],
                row["FulfiledColumn_1"], row["FulfiledColumn_2"], row["FulfiledColumn_3"], row["FulfiledColumn_4"], row["FulfiledColumn_5"],
                row["FulfiledColumn_6"], row["FulfiledColumn_7"], row["FulfiledColumn_8"], row["FulfiledColumn_9"], row["FulfiledColumn_10"],
                passport_id);
        }
    }
}
