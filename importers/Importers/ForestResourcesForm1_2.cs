﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers
{
    internal class ForestResourcesForm1_2 : ImporterBase
    {
        public ForestResourcesForm1_2(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2015, 1, 1),
                Description =
                    "Информация государственного лесного реестра о составе земель лесного фонда и земель иных категорий на которых расположены леса",
                SetIdentifier = "goslesreestr",
                Title = "Характеристика лесничества (лесопарка)",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Информация государственного лесного реестра о составе земель лесного фонда и земель иных категорий на которых расположены леса",
                        IsCurrent = true,
                        Provinance = "Обновление данных",
                        PublisherMBox = "dlhkk@krasnodar.ru",
                        PublisherName = "Скворцов Александр Витальевич ведущий консультант отдела государственного лесного реестра и государственной экспертизы проектов освоения лесов министерства природных ресурсов Краснодарского края",
                        PublisherPhone = "+7-861-2980368",
                        SourceId = Guid.Parse("834CDD20-2F3D-4BDD-8F78-1878B4982B8F"),
                        Subject = "Государственный лесной реестр, состав земель лесного фонда, государственные реестры",
                        Title = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014  г."
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("ForestResourcesForm1_2", new TableDisplayTemplate
            {
                Description = "Характеристика лесничества (лесопарка)",
                Name = "ForestResourcesForm1_2",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "NameLesF1_2",
                        DescriptionRu = "НАИМЕНОВАНИЕ ЛЕСНИЧЕСТВ, ЛЕСОПАРКОВ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ ЛЕСНИЧЕСТВ, ЛЕСОПАРКОВ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TerritoryNameLesF1_2",
                        DescriptionRu = "НАИМЕНОВАНИЕ УЧАСТКОВОГО ЛЕСНИЧЕСТВА",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ УЧАСТКОВОГО ЛЕСНИЧЕСТВА",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "SquareLesF1_2",
                        DescriptionRu = "ПЛОЩАДЬ, ГА",
                        DescritpionEn = "",
                        Header = "ПЛОЩАДЬ, ГА",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "NumberOneLesF1_2",
                        DescriptionRu = "КОЛИЧЕСТВО ЛЕСНЫХ КВАРТАЛОВ",
                        DescritpionEn = "",
                        Header = "КОЛИЧЕСТВО ЛЕСНЫХ КВАРТАЛОВ",
                        IncludeIntoFilter = true,
                        Order = 3,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "NumberTwoLesF1_2",
                        DescriptionRu = "КОЛИЧЕСТВО ЛЕСОТАКСАЦИОН-НЫХ ВЫДЕЛОВ",
                        DescritpionEn = "",
                        Header = "КОЛИЧЕСТВО ЛЕСОТАКСАЦИОН-НЫХ ВЫДЕЛОВ",
                        IncludeIntoFilter = true,
                        Order = 4,
                        Type = ColumnDisplayTemplate.ContentType.String
                    }
                    #endregion
                }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["ForestResourcesForm1_2"],
                Name = "ForestResourcesForm1_2",
                Description = "Характеристика лесничества (лесопарка)",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "ForestResourcesForm1_2"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (NameLesF1_2, TerritoryNameLesF1_2, SquareLesF1_2, NumberOneLesF1_2, NumberTwoLesF1_2, SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}')",
                                 tableName, row["NameLesF1_2"], row["TerritoryNameLesF1_2"],
                                 row["SquareLesF1_2"], row["NumberOneLesF1_2"],
                                 row["NumberTwoLesF1_2"], passport_id);
        }
    }
}
