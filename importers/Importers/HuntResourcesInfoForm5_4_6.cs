﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_4_6 : ImporterBase
    {
        public HuntResourcesInfoForm5_4_6(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_prestupleniya",
                Title = "Сведения о переданых в следственные органы материалах по уголовным преступлениям",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о переданых в следственные органы материалах по уголовным преступлениям",
                        SourceId = Guid.Parse("5116A3C4-D1D4-406A-8EE2-54FEB8E5FB08")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm5_4_6", new TableDisplayTemplate
            {
                Description = "Сведения о категориях нарушений",
                Name = "HuntResourcesInfoForm5_4_6",
                Title = "Сведения о категориях нарушений",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                        {
                             new ColumnDisplayTemplate
                             {
                                 BindedCollumn = "GivenToTotalF5_4_6",
                                 DescriptionRu = "ПЕРЕДАНО В СЛЕДСТВЕННЫЕ ОРГАНЫ МАТЕРИАЛОВ ПО УГОЛОВНЫМ ПРЕСТУПЛЕНИЯМ, ВСЕГО",
                                 DescritpionEn = "",
                                 Header = "ПЕРЕДАНО В СЛЕДСТВЕННЫЕ ОРГАНЫ МАТЕРИАЛОВ ПО УГОЛОВНЫМ ПРЕСТУПЛЕНИЯМ, ВСЕГО",
                                 IncludeIntoFilter = true,
                                 Order = 0,
                                 Type = ColumnDisplayTemplate.ContentType.Integer
                             },

                             new ColumnDisplayTemplate
                             {
                                #region
                                 Header = "ИЗ НИХ",
                                 Order = 1,
                                 InnerTable = new TableDisplayTemplate
                                 {
                                     Description = "Сведения о категориях нарушений. Из них",
                                     Name = "HuntResourcesInfoForm5_4_6_from_them",
                                     Title = "Сведения о категориях нарушений. Из них",
                                     Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                                     Columns = new ColumnDisplayTemplate[]
                                     {
                                          new ColumnDisplayTemplate
                                         {
                                             BindedCollumn = "AmongOthersF5_4_6__RejectedF5_4_6",
                                             DescriptionRu = "ИЗ НИХ Отказано в возбуждении по уголовным делам",
                                             DescritpionEn = "",
                                             Header = "Отказано в возбуждении по уголовным делам",
                                             IncludeIntoFilter = true,
                                             Order = 2,
                                             Type = ColumnDisplayTemplate.ContentType.Integer
                                         },

                                         new ColumnDisplayTemplate
                                         {
                                             BindedCollumn = "AmongOthersF5_4_6__StopedF5_4_6",
                                             DescriptionRu = "ИЗ НИХ Прекращено",
                                             DescritpionEn = "",
                                             Header = "Прекращено",
                                             IncludeIntoFilter = true,
                                             Order = 3,
                                             Type = ColumnDisplayTemplate.ContentType.Integer
                                         },

                                         new ColumnDisplayTemplate
                                         {
                                             BindedCollumn = "AmongOthersF5_4_6__NoDecisionF5_4_6",
                                             DescriptionRu = "ИЗ НИХ Не принято решения",
                                             DescritpionEn = "",
                                             Header = "Не принято решения",
                                             IncludeIntoFilter = true,
                                             Order = 4,
                                             Type = ColumnDisplayTemplate.ContentType.Integer
                                         },

                                         new ColumnDisplayTemplate
                                         {
                                             BindedCollumn = "AmongOthersF5_4_6__CondemnedF5_4_6",
                                             DescriptionRu = "ИЗ НИХ Осуждено, чел",
                                             DescritpionEn = "",
                                             Header = "Осуждено, чел",
                                             IncludeIntoFilter = true,
                                             Order = 5,
                                             Type = ColumnDisplayTemplate.ContentType.Integer
                                         }
                                     }
                                 }
                                #endregion
                             }
                        }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_4_6"],
                Name = "HuntResourcesInfoForm5_4_6",
                Description = "Сведения о переданых в следственные органы материалах по уголовным преступлениям",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_4_6"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (GivenToTotalF5_4_6, AmongOthersF5_4_6__RejectedF5_4_6, AmongOthersF5_4_6__StopedF5_4_6, " +
                "AmongOthersF5_4_6__NoDecisionF5_4_6, AmongOthersF5_4_6__CondemnedF5_4_6, SetsPassports_Id) VALUES('{1}', {2}, {3}, {4}, {5}, '{6}')",
                tableName, row["GivenToTotalF5_4_6"], row["AmongOthersF5_4_6__RejectedF5_4_6"], row["AmongOthersF5_4_6__StopedF5_4_6"],
                row["AmongOthersF5_4_6__NoDecisionF5_4_6"], row["AmongOthersF5_4_6__CondemnedF5_4_6"], passport_id);
        }
    }
}
