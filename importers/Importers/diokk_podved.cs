﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class diokk_podved : ImporterBase
    {
        public diokk_podved(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.dep_imuszh],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.stroitelstvo],
                CreationDate = DateTime.Now,
                Description = "Полное и сокращенное наименование, юридический и фактический адреса, номера телефонов, адрес интернет-сайта (в формате http://сайт.домен), функции и задачи учреждения, предприятияв",
                SetIdentifier = "podved",
                Title = "Перечень подведомственных учреждений, предприятий",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Департамент имущественных отношений Краснодарского края",
                        Description = "Полное и сокращенное наименование, юридический и фактический адреса, номера телефонов, адрес интернет-сайта (в формате http://сайт.домен), функции и задачи учреждения, предприятияв",
                        IsCurrent = true,
                        PublisherMBox = "dk.post@diok.ru",
                        PublisherName = "Д.К. Малицкий",
                        PublisherPhone = "+7-861-2621213",
                        SourceId = Guid.Parse("7237208E-2F4C-4A33-BCC7-60C179B17556"),
                        Subject = "Подведомственные учреждения, предприятия",
                        Title = "Перечень подведомственных учреждений, предприятий"
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("diokk_podved",
                new TableDisplayTemplate
                {
                    Description =
                        "Полное и сокращенное наименование, юридический и фактический адреса, номера телефонов и пр.",
                    Title = "Перечень подведомственных учреждений, предприятий",
                    Name = "diokk_podved",
                    Type = TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FULLNAME",
                                    DescriptionRu = "ПОЛНОЕ НАИМЕНОВАНИЕ",
                                    DescritpionEn = "",
                                    Header = "ПОЛНОЕ НАИМЕНОВАНИЕ",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "SNAME",
                                    DescriptionRu = "СОКРАЩЕННОЕ НАИМЕНОВАНИЕ",
                                    DescritpionEn = "",
                                    Header = "СОКРАЩЕННОЕ НАИМЕНОВАНИЕ",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ADRES_UR",
                                    DescriptionRu = "ЮРЕДИЧЕСКИЙ АДРЕСС",
                                    DescritpionEn = "",
                                    Header = "ЮРЕДИЧЕСКИЙ АДРЕСС",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ADRES_FACT",
                                    DescriptionRu = "ФАКТИЧЕСКИЙ АДРЕС",
                                    DescritpionEn = "",
                                    Header = "ФАКТИЧЕСКИЙ АДРЕС",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "EMAIL",
                                    DescriptionRu = "АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ",
                                    DescritpionEn = "",
                                    Header = "АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "WEBSITE",
                                    DescriptionRu = "АДРЕС ОФИЦИАЛЬНОГО ИНТЕРНЕТ-САЙТА",
                                    DescritpionEn = "",
                                    Header = "АДРЕС ОФИЦИАЛЬНОГО ИНТЕРНЕТ-САЙТА",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "PHONE",
                                    DescriptionRu = "НОМЕРА ТЕЛЕФОНОВ",
                                    DescritpionEn = "",
                                    Header = "НОМЕРА ТЕЛЕФОНОВ",
                                    IncludeIntoFilter = true,
                                    Order = 6,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "FUNCTIONS",
                                    DescriptionRu = "ФУНКЦИИ",
                                    DescritpionEn = "",
                                    Header = "ФУНКЦИИ",
                                    IncludeIntoFilter = true,
                                    Order = 7,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TASKS",
                                    DescriptionRu = "ЗАДАЧИ",
                                    DescritpionEn = "",
                                    Header = "ЗАДАЧИ",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                }
                                #endregion
                            }
                });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["diokk_podved"],
                Name = "diokk_podved",
                Description = "Перечень подведомственных учреждений, предприятий",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "diokk_podved"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return
                string.Format(
                    "INSERT INTO [dbo].[{0}] (FULLNAME, SNAME, ADRES_UR, ADRES_FACT, EMAIL, WEBSITE, PHONE, FUNCTIONS, TASKS, SetsPassports_Id) " +
                    "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')",
                    tableName, row["FULLNAME"] ?? "null", row["SNAME"] ?? "null",
                    row["ADRES_UR"] ?? "null", row["ADRES_FACT"] ?? "null",
                    row["EMAIL"] ?? "null", row["WEBSITE"] ?? "null",
                    row["PHONE"] ?? "null", row["FUNCTIONS"] ?? "null",
                    row["TASKS"] ?? "null", passport_id);
        }
    }
}
