﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_1 : ImporterBase
    {
        public HuntResourcesInfoForm5_1(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_measures",
                Title = "Сведения о проведенных биотехнических мероприятиях",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о проведенных биотехнических мероприятиях",
                        SourceId = Guid.Parse("7F3F70ED-20FC-4294-B7A3-205DDA7CAAD1")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var rsult = new Dictionary<string, Repository.Entities.TableDisplayTemplate>();

            rsult.Add("HuntResourcesInfoForm5_1",
                new Repository.Entities.TableDisplayTemplate
                {
                    Description = "Сведения о проведенных биотехнических мероприятиях",
                    Name = "HuntResourcesInfoForm5_1",
                    Title = "Сведения о проведенных биотехнических мероприятиях",
                    Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "HuntSpaceInfoF5_1",
                                    DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ И ИХ ВИД (ЗАКРЕПЛЕННЫЕ/ОБЩЕДОСТУПНЫЕ)",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ И ИХ ВИД (ЗАКРЕПЛЕННЫЕ/ОБЩЕДОСТУПНЫЕ)",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "MeasureName",
                                    DescriptionRu = "НАИМЕНОВАНИЕ МЕРОПРИЯТИЯ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ МЕРОПРИЯТИЯ",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "UnitOfMeasure",
                                    DescriptionRu = "ЕДИНИЦА ИЗМЕРЕНИЯ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Quantity",
                                    DescriptionRu = "КОЛИЧЕСТВО",
                                    DescritpionEn = "",
                                    Header = "КОЛИЧЕСТВО",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                });

            return rsult;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_1"],
                Name = "HuntResourcesInfoForm5_1",
                Description = "Сведения о проведенных биотехнических мероприятиях",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_1"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (HuntSpaceInfoF5_1, MeasureName, UnitOfMeasure, Quantity, SetsPassports_Id) " +
                "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}')", tableName, row["HuntSpaceInfoF5_1"], row["MeasureName"], row["UnitOfMeasure"], row["Quantity"], passport_id);
        }
    }
}
