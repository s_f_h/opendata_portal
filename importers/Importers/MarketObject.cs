﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class MarketObject : ImporterBase
    {
        public MarketObject(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.dep_potreb],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.potreb],
                CreationDate = DateTime.Now,
                Description = "Реестр розничных рынков, расположенных на территории Краснодарского края",
                SetIdentifier = "marketreestr",
                Title = "Реестр розничных рынков, расположенных на территории Краснодарского края",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Департамент потребительской сферы Краснодарского края",
                        Description = "Реестр розничных рынков, расположенных на территории Краснодарского края",
                        IsCurrent = true,
                        PublisherMBox = Common.Constants.Watermarks.Email,
                        PublisherName = "-",
                        PublisherPhone = Common.Constants.Watermarks.PhoneNumber,
                        Subject = "-",
                        Title = "Реестр розничных рынков, расположенных на территории Краснодарского края",
                        SourceId =  Guid.Parse("4C7D8650-94B9-44D1-8D3D-B19963B1AAA6")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("MarketObject", new TableDisplayTemplate
            {
                Description = "Реестр розничных рынков, расположенных на территории Краснодарского края",
                Name = "MarketObject",
                Title = "Реестр розничных рынков, расположенных на территории Краснодарского края",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "OrgName",
                        DescriptionRu = "НАИМЕНОВАНИЕНИЕ ОРГАНА МЕСТНОГО САМОУПРАВЛЕНИЯ, ВЫДАВШЕГО РАЗРЕШЕНИЕ НА ПРАВО ОРГАНИЗАЦИИ РОЗНИЧНОГО РЫНКА",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕНИЕ ОРГАНА МЕСТНОГО САМОУПРАВЛЕНИЯ, ВЫДАВШЕГО РАЗРЕШЕНИЕ НА ПРАВО ОРГАНИЗАЦИИ РОЗНИЧНОГО РЫНКА",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ShortName",
                        DescriptionRu = "ПОЛНОЕ И СОКРАЩЕННОЕ НАМЕНОВАНИЕ, В ТОМ ЧИСЛЕ ФИРМЕННОЕ НАИМЕНОВАНИЕ, ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА ЮРИДИЧЕСКОГО ЛИЦА (УПРАВЛЯЮЩЕЙ РЫНКОМ КОМПАНИИ)",
                        DescritpionEn = "",
                        Header = "ПОЛНОЕ И СОКРАЩЕННОЕ НАМЕНОВАНИЕ, В ТОМ ЧИСЛЕ ФИРМЕННОЕ НАИМЕНОВАНИЕ, ОРГАНИЗАЦИОННО-ПРАВОВАЯ ФОРМА ЮРИДИЧЕСКОГО ЛИЦА (УПРАВЛЯЮЩЕЙ РЫНКОМ КОМПАНИИ)",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "INN",
                        DescriptionRu = "ИНН",
                        DescritpionEn = "",
                        Header = "ИНН",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "UrAddress",
                        DescriptionRu = "МЕСТО НАХОЖДЕНИЯ ЮРИДИЧЕСКОГО ЛИЦА",
                        DescritpionEn = "",
                        Header = "МЕСТО НАХОЖДЕНИЯ ЮРИДИЧЕСКОГО ЛИЦА",
                        IncludeIntoFilter = true,
                        Order = 3,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "MarketAdress",
                        DescriptionRu = "МЕСТО НАХОЖДЕНИЯ РЫНКА",
                        DescritpionEn = "",
                        Header = "МЕСТО НАХОЖДЕНИЯ РЫНКА",
                        IncludeIntoFilter = true,
                        Order = 4,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "MarketType",
                        DescriptionRu = "ТИП РЫНКА",
                        DescritpionEn = "",
                        Header = "ТИП РЫНКА",
                        IncludeIntoFilter = true,
                        Order = 5,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "AcceptNum",
                        DescriptionRu = "НОМЕР РАЗРЕШЕНИЯ",
                        DescritpionEn = "",
                        Header = "НОМЕР РАЗРЕШЕНИЯ",
                        IncludeIntoFilter = true,
                        Order = 6,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "Reasons",
                        DescriptionRu = "ОСНОВАНИЕ ВЫДАЧИ РАЗРЕШЕНИЯ (НОМЕР И ДАТА СООТВЕТСТВУЮЩЕГО АКТА УПОЛНОМОЧЕННОГО ОРГАНА МЕСТНОГО САМОУПРАВЛЕНИЯ)",
                        DescritpionEn = "",
                        Header = "ОСНОВАНИЕ ВЫДАЧИ РАЗРЕШЕНИЯ (НОМЕР И ДАТА СООТВЕТСТВУЮЩЕГО АКТА УПОЛНОМОЧЕННОГО ОРГАНА МЕСТНОГО САМОУПРАВЛЕНИЯ)",
                        IncludeIntoFilter = true,
                        Order = 7,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "AcceptDateBegin",
                        DescriptionRu = "ДАТА ВЫДАЧИ РАЗРЕШЕНИЯ",
                        DescritpionEn = "",
                        Header = "ДАТА ВЫДАЧИ РАЗРЕШЕНИЯ",
                        IncludeIntoFilter = true,
                        Order = 8,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "AcceptDateEnd",
                        DescriptionRu = "ДАТА ОКОНЧАНИЯ СРОКА ДЕЙСТВИЯ РАЗРЕШЕНИЯ",
                        DescritpionEn = "",
                        Header = "ДАТА ОКОНЧАНИЯ СРОКА ДЕЙСТВИЯ РАЗРЕШЕНИЯ",
                        IncludeIntoFilter = true,
                        Order = 9,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "RegisterMarketNum",
                        DescriptionRu = "РЕЕСТРОВЫЙ НОМЕР РЫНКА",
                        DescritpionEn = "",
                        Header = "РЕЕСТРОВЫЙ НОМЕР РЫНКА",
                        IncludeIntoFilter = true,
                        Order = 10,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "StopReasons",
                        DescriptionRu = "ОСНОВАНИЕ ПРИОСТАНОВЛЕНИЯ, ВОЗОБНОВЛЕНИЯ, АННУЛИРОВАНИЯ,ПРОДЛЕНИЯ,ПРЕКРАЩЕНИЯ, ПЕРЕОФОРМЛЕНИЯ ДЕЙСТВИЯ РАЗРЕШЕНИЯ",
                        DescritpionEn = "",
                        Header = "ОСНОВАНИЕ ПРИОСТАНОВЛЕНИЯ, ВОЗОБНОВЛЕНИЯ, АННУЛИРОВАНИЯ,ПРОДЛЕНИЯ,ПРЕКРАЩЕНИЯ, ПЕРЕОФОРМЛЕНИЯ ДЕЙСТВИЯ РАЗРЕШЕНИЯ",
                        IncludeIntoFilter = true,
                        Order = 11,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "dateStopReasons",
                        DescriptionRu = "СРОК ПРИОСТАНОВЛЕНИЯ ИЛИ ДАТА ВОЗОБНОВЛЕНИЯ, АННУЛИРОВАНИЯ, ПРОДЛЕНИЯ, ПРЕКРАЩЕНИЯ, ПЕРЕОФОРМЛЕНИЯ ДЕЙСТВИЯ РАЗРЕШЕНИЯ",
                        DescritpionEn = "",
                        Header = "СРОК ПРИОСТАНОВЛЕНИЯ ИЛИ ДАТА ВОЗОБНОВЛЕНИЯ, АННУЛИРОВАНИЯ, ПРОДЛЕНИЯ, ПРЕКРАЩЕНИЯ, ПЕРЕОФОРМЛЕНИЯ ДЕЙСТВИЯ РАЗРЕШЕНИЯ",
                        IncludeIntoFilter = true,
                        Order = 12,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "MarketInfo",
                        DescriptionRu = "СВЕДЕНИЯ О ВКЛЮЧЕНИИ, ИСКЛЮЧЕНИИ ИЗ РЕЕСТРА РЫНКОВ",
                        DescritpionEn = "",
                        Header = "СВЕДЕНИЯ О ВКЛЮЧЕНИИ, ИСКЛЮЧЕНИИ ИЗ РЕЕСТРА РЫНКОВ",
                        IncludeIntoFilter = true,
                        Order = 13,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {

                Template_Id = tableDisplayTemplates["MarketObject"],
                Name = "MarketObject",
                Description = "Реестр розничных рынков, расположенных на территории Краснодарского края",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "MarketObject"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (OrgName, ShortName, INN, UrAddress, MarketAdress, MarketType, AcceptNum, Reasons, " +
                "AcceptDateBegin, AcceptDateEnd, RegisterMarketNum, StopReasons, dateStopReasons, MarketInfo,SetsPassports_Id) VALUES (" +
                "'{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}')", tableName,
                row["OrgName"], row["ShortName"], row["INN"], row["UrAddress"], row["MarketAdress"], row["MarketType"], row["AcceptNum"], row["Reasons"],
                row["AcceptDateBegin"], row["AcceptDateEnd"], row["RegisterMarketNum"], row["StopReasons"], row["dateStopReasons"], row["MarketInfo"],
                passport_id);
        }
    }
}
