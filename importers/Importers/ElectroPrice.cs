﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class ElectroPrice : ImporterBase
    {
        public ElectroPrice(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.reg_energ_kom],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.promishlenost],
                CreationDate = new DateTime(2015, 4, 1),
                Description =
                    "Тарифы на электроэнергию для населения",
                SetIdentifier = "liquidgasprice",
                Title =
                    "Тарифы на электроэнергию для населения",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Региональная энергетическая комиссия",
                        Description =
                            "Тарифы на электроэнергию для населения на 01.04.2015",
                            IsCurrent = true,
                        Provinance = "Обновление данных",
                        PublisherMBox = "not@given.ru",
                        PublisherName = "unknown",
                        PublisherPhone = "+7-861-1111111",
                        SourceId = Guid.Parse("A924E4D7-1566-4788-88C1-4143EF088F38"),
                        Subject = "Розничные цены на сжиженный газ, реализуемый населению",
                        Title =
                            "Тарифы на электроэнергию для населения на 01.04.2015",
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("ElectroPrice", new TableDisplayTemplate
            {
                Description =
                    "Перечень цен на электричество",
                Name = "ElectroPrice",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "Index",
                        DescriptionRu = "ПОКАЗАТЕЛЬ (ГРУППЫ ПОТРЕБИТЕЛЕЙ С РАЗБИВКОЙ ПО СТАВКАМ И ДИФФЕРЕНЦИАЦИЕЙ ПО ЗОНАМ СУТОК)",
                        DescritpionEn = "",
                        Header = "ПОКАЗАТЕЛЬ (ГРУППЫ ПОТРЕБИТЕЛЕЙ С РАЗБИВКОЙ ПО СТАВКАМ И ДИФФЕРЕНЦИАЦИЕЙ ПО ЗОНАМ СУТОК)",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "Unit",
                        DescriptionRu =
                            "ЕДИНИЦА ИЗМЕРЕНИЯ",
                        DescritpionEn = "",
                        Header =
                            "ЕДИНИЦА ИЗМЕРЕНИЯ",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "Price",
                        DescriptionRu = "ТАРИФЫ С 01.01.2015",
                        DescritpionEn = "",
                        Header = "ТАРИФЫ С 01.01.2015",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },

                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["ElectroPrice"],
                Name = "ElectroPrice",
                Description =
                    "Перечень цен на электричество",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "ElectroPrice"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] ([Index], Unit, Price, SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}')",
                                 tableName,
                                 row["Index"], row["Unit"],
                                 row["Price"], passport_id);
        }
    }
}