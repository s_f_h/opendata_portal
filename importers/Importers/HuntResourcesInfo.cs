﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfo : ImporterBase
    {
        public HuntResourcesInfo(DataContext ctx)
            : base(ctx,
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                    CreationDate = new DateTime(2014, 5, 1),
                    Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                    SetIdentifier = "gosohotreestr_tur",
                    Title = "Сведения о добыче тура",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство природных ресурсов Краснодарского края",
                            Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014  г.",
                            IsCurrent = false,
                            PublisherMBox = "mprkk@krasnodar.ru",
                            PublisherName = "Белоусов Игорь Валерьевич",
                            PublisherPhone = "+7-861-2591971",
                            SourceId = Guid.Parse("4E40F239-CA64-4182-883B-6D8CC2F09408"),
                            Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                            Title = "Сведения о добыче  тура в период с 1 августа 2013 г. по 28 февраля - 2014 г.",
                            #endregion
                        },
                        new ImportingSetVersionSettings
                        {
                            #region
                            IsCurrent = true,
                            Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014  г.",
                            Title = "Сведения о добыче  тура в период с 1 августа 2013 г. по 31 июля 2014 г.",
                            SourceId = Guid.Parse("E72C6E37-97D2-41CF-BCDC-8D85FE1588DE")
                            #endregion
                        },
                        
                    }
                    #endregion
                },
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                    CreationDate = new DateTime(2014, 5, 1),
                    Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                    SetIdentifier = "gosohotreestr_olen",
                    Title = "Сведения о добыче  оленя благородного",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                        {
                            new ImportingSetVersionSettings
                            {
                                #region
                                Creator = "Министерство природных ресурсов Краснодарского края",
                                Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014  г.",
                                IsCurrent = false,
                                PublisherMBox = "mprkk@krasnodar.ru",
                                PublisherName = "Белоусов Игорь Валерьевич",
                                PublisherPhone = "+7-861-2591971",
                                SourceId = Guid.Parse("F1439997-CFBB-4588-8770-6F85FABBA57A"),
                                Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                                Title = "Сведения о добыче  оленя благородного в период с 1 августа 2013 г. по 28 февраля - 2014 г.",
                                #endregion
                            },
                            new ImportingSetVersionSettings
                            {
                                #region
                                IsCurrent = true,
                                Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014  г.",
                                Title = "Сведения о добыче  оленя благородного в период с 1 августа 2013 г. по 31 июля 2014 г.",
                                SourceId = Guid.Parse("A8FA56AE-A8BD-4E55-BA08-00E2C46DD588")
                                #endregion
                            },
                        
                        }
                    #endregion
                },
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                    CreationDate = new DateTime(2014, 5, 1),
                    Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                    SetIdentifier = "gosohotreestr_olen_pyatn",
                    Title = "Сведения о добыче оленя пятнистого",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                        {
                            new ImportingSetVersionSettings
                            {
                                #region
                                Creator = "Министерство природных ресурсов Краснодарского края",
                                Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014  г.",
                                IsCurrent = false,
                                PublisherMBox = "mprkk@krasnodar.ru",
                                PublisherName = "Белоусов Игорь Валерьевич",
                                PublisherPhone = "+7-861-2591971",
                                SourceId = Guid.Parse("E2AEA8D7-93AC-4748-974E-4B0D0E90B7DF"),
                                Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                                Title = "Сведения о добыче оленя пятнистого в период с 1 августа 2013 г. по 28 февраля - 2014 г.",
                                #endregion
                            },
                            new ImportingSetVersionSettings
                            {
                                #region
                                IsCurrent = true,
                                Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014  г.",
                                Title = "Сведения о добыче оленя пятнистого в период с 1 августа 2013 г. по 31 июля 2014 г.",
                                SourceId = Guid.Parse("CC94D020-53C4-4CCB-B81A-95CE4A6C97B8")
                                #endregion
                            },
                        
                        }
                    #endregion
                },
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                    CreationDate = new DateTime(2014, 5, 1),
                    Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                    SetIdentifier = "gosohotreestr_kosulya",
                    Title = "Сведения о добыче косули",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                        {
                            new ImportingSetVersionSettings
                            {
                                #region
                                Creator = "Министерство природных ресурсов Краснодарского края",
                                Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014  г.",
                                IsCurrent = false,
                                PublisherMBox = "mprkk@krasnodar.ru",
                                PublisherName = "Белоусов Игорь Валерьевич",
                                PublisherPhone = "+7-861-2591971",
                                SourceId = Guid.Parse("5E2B38F5-560C-4981-824B-5AADC456B94F"),
                                Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                                Title = "Сведения о добыче косули в период с 1 августа 2013 г. по 28 февраля - 2014 г.",
                                #endregion
                            },
                            new ImportingSetVersionSettings
                            {
                                #region
                                IsCurrent = true,
                                Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014  г.",
                                Title = "Сведения о добыче косули в период с 1 августа 2013 г. по 31 июля 2014 г.",
                                SourceId = Guid.Parse("B2D854BC-5741-4129-B910-41B66F7572D1")
                                #endregion
                            },
                        
                        }
                    #endregion
                },
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                    CreationDate = new DateTime(2014, 5, 1),
                    Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                    SetIdentifier = "gosohotreestr_kaban",
                    Title = "Сведения о добыче кабана",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                            {
                                new ImportingSetVersionSettings
                                {
                                    #region
                                    Creator = "Министерство природных ресурсов Краснодарского края",
                                    Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014  г.",
                                    IsCurrent = false,
                                    PublisherMBox = "mprkk@krasnodar.ru",
                                    PublisherName = "Белоусов Игорь Валерьевич",
                                    PublisherPhone = "+7-861-2591971",
                                    SourceId = Guid.Parse("27B94E0E-F76A-44C2-803D-2AA10312C456"),
                                    Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                                    Title = "Сведения о добыче кабана в период с 1 августа 2013 г. по 28 февраля - 2014 г.",
                                    #endregion
                                },
                                new ImportingSetVersionSettings
                                {
                                    #region
                                    IsCurrent = true,
                                    Description = "Документированная информация о добыче копытных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014  г.",
                                    Title = "Сведения о добыче кабана в период с 1 августа 2013 г. по 31 июля 2014 г.",
                                    SourceId = Guid.Parse("CC814AF7-3AF5-43BC-A79D-A251A47CC478")
                                    #endregion
                                },
                        
                            }
                    #endregion
                }) { asIs = true; }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("HuntResourcesInfo",
                new TableDisplayTemplate
                {
                    Description = "Систематизированный свод документированной информации об охотничьих ресурсах",
                    Title = "Сведения о добыче",
                    Name = "HuntResourcesInfo",
                    Type = TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TerritoryName",
                                    DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalHunted",
                                    DescriptionRu = "ВСЕГО ДОБЫТО ОСОБЕЙ",
                                    DescritpionEn = "",
                                    Header = "ВСЕГО ДОБЫТО ОСОБЕЙ",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                                new ColumnDisplayTemplate
                                {
                                    Header = "ВЫДЕЛЕННАЯ КВОТА, ОСОБЕЙ",
                                    Order = 2,
                                    SubColumns = new ColumnDisplayTemplate[]
                                    {
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdmitedQuota_Total",
                                            DescriptionRu = "Всего",
                                            DescritpionEn = "",
                                            Header = "Всего",
                                            IncludeIntoFilter = true,
                                            Order = 3,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdmitedQuota_OlderOneYear",
                                            DescriptionRu = "Взрослые",
                                            DescritpionEn = "",
                                            Header = "Взрослые",
                                            IncludeIntoFilter = true,
                                            Order = 4,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdmitedQuota_YoungerOneYear",
                                            DescriptionRu = "Младше 1 года",
                                            DescritpionEn = "",
                                            Header = "Младше 1 года",
                                            IncludeIntoFilter = true,
                                            Order = 5,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                    }
                                },
                                new ColumnDisplayTemplate
                                {
                                    Header = "ВЫДАНО РАЗРЕШЕНИЙ, ШТ.",
                                    Order = 6,
                                    SubColumns = new ColumnDisplayTemplate[]
                                    {
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdmitedPermisions_Total",
                                            DescriptionRu = "Всего",
                                            DescritpionEn = "",
                                            Header = "Всего",
                                            IncludeIntoFilter = true,
                                            Order = 7,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdmitedPermisions_OlderOneYear",
                                            DescriptionRu = "Взрослые",
                                            DescritpionEn = "",
                                            Header = "Взрослые",
                                            IncludeIntoFilter = true,
                                            Order = 8,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdmitedPermisions_YoungerOneYear",
                                            DescriptionRu = "Младше 1 года",
                                            DescritpionEn = "",
                                            Header = "Младше 1 года",
                                            IncludeIntoFilter = true,
                                            Order = 9,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                    }
                                },
                                new ColumnDisplayTemplate
                                {
                                    Header = "ДОБЫТО КОПЫТНЫХ ЖИВОТНЫХ ПО ВОЗРАСТНЫМИ ПОЛОВЫМ КАТЕГОРИЯМ, ОСОБЕЙ",
                                    Order = 10,
                                    SubColumns = new ColumnDisplayTemplate[]
                                    {
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_YoungerOneYearTotal",
                                            DescriptionRu = "До 1 года всего",
                                            DescritpionEn = "",
                                            Header = "До 1 года всего",
                                            IncludeIntoFilter = true,
                                            Order = 11,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_YoungerOneYearMale",
                                            DescriptionRu = "До 1 года самцы",
                                            DescritpionEn = "",
                                            Header = "До 1 года самцы",
                                            IncludeIntoFilter = true,
                                            Order = 12,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_YoungerOneYearFemale",
                                            DescriptionRu = "До 1 года самки",
                                            DescritpionEn = "",
                                            Header = "До 1 года самки",
                                            IncludeIntoFilter = true,
                                            Order = 13,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_OneYearAndHalfTotal",
                                            DescriptionRu = "Полутарогодовалые всего",
                                            DescritpionEn = "",
                                            Header = "Полутарогодовалые всего",
                                            IncludeIntoFilter = true,
                                            Order = 14,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_OneYearAndHalfMale",
                                            DescriptionRu = "Полутарогодовалые самцы",
                                            DescritpionEn = "",
                                            Header = "Полутарогодовалые самцы",
                                            IncludeIntoFilter = true,
                                            Order = 15,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_OneYearAndHalfFemale",
                                            DescriptionRu = "Полутарогодовалые самки",
                                            DescritpionEn = "",
                                            Header = "Полутарогодовалые самки",
                                            IncludeIntoFilter = true,
                                            Order = 16,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_GrownTotal",
                                            DescriptionRu = "Взрослые всего",
                                            DescritpionEn = "",
                                            Header = "Взрослые всего",
                                            IncludeIntoFilter = true,
                                            Order = 17,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_GrownMale",
                                            DescriptionRu = "Взрослые самцы",
                                            DescritpionEn = "",
                                            Header = "Взрослые самцы",
                                            IncludeIntoFilter = true,
                                            Order = 18,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "HuntedGenderAge_GrownFemale",
                                            DescriptionRu = "Взрослые самки",
                                            DescritpionEn = "",
                                            Header = "Взрослые самки",
                                            IncludeIntoFilter = true,
                                            Order = 19,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                    }
                                },

                                #endregion
                            }
                });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfo"],
                Name = "HuntResourcesInfo",
                Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfo"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return
                string.Format(
                    "INSERT INTO [dbo].[{0}] (TerritoryName, TotalHunted, AdmitedQuota_Total, AdmitedQuota_OlderOneYear, AdmitedQuota_YoungerOneYear, " +
                    "AdmitedPermisions_Total, AdmitedPermisions_OlderOneYear, AdmitedPermisions_YoungerOneYear, HuntedGenderAge_YoungerOneYearTotal, " +
                    "HuntedGenderAge_YoungerOneYearMale, HuntedGenderAge_YoungerOneYearFemale, HuntedGenderAge_OneYearAndHalfTotal, HuntedGenderAge_OneYearAndHalfMale, " +
                    "HuntedGenderAge_OneYearAndHalfFemale, HuntedGenderAge_GrownTotal, HuntedGenderAge_GrownMale, HuntedGenderAge_GrownFemale, SetsPassports_Id) " +
                    "VALUES ('{1}', {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, '{18}')",
                    tableName, row["TerritoryName"] ?? "null", row["TotalHunted"] ?? "null",
                    row["AdmitedQuota_Total"] ?? "null",
                    row["AdmitedQuota_OlderOneYear"] ?? "null", row["AdmitedQuota_YoungerOneYear"] ?? "null",
                    row["AdmitedPermisions_Total"] ?? "null", row["AdmitedPermisions_OlderOneYear"] ?? "null",
                    row["AdmitedPermisions_YoungerOneYear"] ?? "null",
                    row["HuntedGenderAge_YoungerOneYearTotal"] ?? "null",
                    row["HuntedGenderAge_YoungerOneYearMale"] ?? "null",
                    row["HuntedGenderAge_YoungerOneYearFemale"] ?? "null",
                    row["HuntedGenderAge_OneYearAndHalfTotal"] ?? "null",
                    row["HuntedGenderAge_OneYearAndHalfMale"] ?? "null",
                    row["HuntedGenderAge_OneYearAndHalfFemale"] ?? "null", row["HuntedGenderAge_GrownTotal"] ?? "null",
                    row["HuntedGenderAge_GrownMale"] ?? "null", row["HuntedGenderAge_GrownFemale"] ?? "null", passport_id);
        }
    }

   
}