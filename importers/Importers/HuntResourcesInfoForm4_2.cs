﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm4_2 : ImporterBase
    {
        public HuntResourcesInfoForm4_2(DataContext ctx)
            : base(ctx,
            new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                    CreationDate = new DateTime(2014, 5, 1),
                    Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                    SetIdentifier = "gosohotreestr_lisica",
                    Title = "Сведения о добыче лисицы",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство природных ресурсов Краснодарского края",
                            Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                            IsCurrent = false,
                            PublisherMBox = "mprkk@krasnodar.ru",
                            PublisherName = "Белоусов Игорь Валерьевич",
                            PublisherPhone = "+7-861-2591971",
                            SourceId = Guid.Parse("83287A89-E3EC-4A89-A78F-EC7C24BEE1F7"),
                            Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                            Title = "Сведения о добыче лисицы в период с 1 ноября  2013 г. по 28 февраля  2014г.",
                            #endregion
                        },
                        new ImportingSetVersionSettings
                        {
                            #region
                            IsCurrent = true,
                            Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                            Title = "Сведения о добыче лисицы в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                            SourceId = Guid.Parse("373C2CC6-23E2-40CC-AA7C-9510B0A734F5")
                            #endregion
                        }
                    }
                    #endregion
                },
             new ImportingSetSettings
             {
                 #region
                 AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                 CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                 CreationDate = new DateTime(2014, 5, 1),
                 Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                 SetIdentifier = "gosohotreestr_kunica",
                 Title = "Сведения о добыче куницы",
                 UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                 SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = false,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("678EB907-372D-453D-A198-CCB1343FDB34"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче куницы в период с 1 ноября  2013 г. по 28 февраля  2014г.",
                         #endregion
                     },
                     new ImportingSetVersionSettings
                     {
                         #region
                         IsCurrent = true,
                         Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                         Title = "Сведения о добыче куницы в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                         SourceId = Guid.Parse("A22363E3-C202-4FC8-A27F-732FD86CC562")
                         #endregion
                     }
                 }
                 #endregion
             },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 5, 1),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_enot_sobaka",
                Title = "Сведения о добыче енотовидной собаки",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = false,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("EC19E293-7360-480D-92D7-6DBBA179EB84"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче енотовидной собаки в период с 1 ноября  2013 г. по 28 февраля  2014г.",
                         #endregion
                     },
                     new ImportingSetVersionSettings
                     {
                         #region
                         IsCurrent = true,
                         Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                         Title = "Сведения о добыче енотовидной собаки в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                         SourceId = Guid.Parse("5EA119BC-E255-4003-952B-275FD4F828D4")
                         #endregion
                     }
                 }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 5, 1),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_enot_poloskun",
                Title = "Сведения о добыче енота-полоскуна",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = false,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("9AA42247-1AC0-4A67-BB25-CABF8A30D6EE"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче енота-полоскуна в период с 1 ноября  2013 г. по 28 февраля  2014г.",
                         #endregion
                     },
                     new ImportingSetVersionSettings
                     {
                         #region
                         IsCurrent = true,
                         Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                         Title = "Сведения о добыче енота-полоскуна в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                         SourceId = Guid.Parse("3E151A00-4B39-4289-A271-47AF6667CE45")
                         #endregion
                     }
                 }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 5, 1),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_ondatra",
                Title = "Сведения о добыче ондатры",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = false,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("4435DBDD-AA40-45EE-9E94-07CCE155BD2F"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче ондатры  в период с 15 сентября 2013 г. по 28 февраля 2014 г.",
                         #endregion
                     },
                     new ImportingSetVersionSettings
                     {
                         #region
                         IsCurrent = true,
                         Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                         Title = "Сведения о добыче ондатры в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                         SourceId = Guid.Parse("75BCA324-E636-4847-806F-989AC85188C0")
                         #endregion
                     }
                 }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 5, 1),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_barsuk",
                Title = "Сведения о добыче барсука",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = false,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("5FBD8F34-40C7-452A-A75A-09656D39BCA9"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче барсука  в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                         #endregion
                     },
                     new ImportingSetVersionSettings
                     {
                         #region
                         IsCurrent = true,
                         Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                         Title = "Сведения о добыче барсука  в период с 1 сентября 2013 г. по 31 октября 2014 г.",
                         SourceId = Guid.Parse("6BA095A3-1741-46F8-A7AC-ED943C072D30")
                         #endregion
                     }
                 }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 5, 1),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_volk",
                Title = "Сведения о добыче волка",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = true,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("4AF1A232-DBB0-4A34-BC00-39D04BA4AAFB"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче волка в период с 1 ноября  2013 г. по 28 февраля  2014 г.",
                         #endregion
                     }
                 }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 5, 1),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_belka",
                Title = "Сведения о добыче белки",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = false,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("268FDCFF-4016-4346-A5BD-E071C65FB46D"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче белки  в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                         #endregion
                     },
                     new ImportingSetVersionSettings
                     {
                         #region
                         IsCurrent = true,
                         Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                         Title = "Сведения о добыче белки  в период с 1 сентября 2013 г. по 31 октября 2014 г.",
                         SourceId = Guid.Parse("CECC5665-49DF-4B12-BCB6-AD215E97A470")
                         #endregion
                     }
                 }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 5, 1),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_zayac",
                Title = "Сведения о добыче зайца-русака",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = false,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("E8C2B901-3DE7-4500-9054-D804084C5DCD"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче зайца-русака в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                         #endregion
                     },
                     new ImportingSetVersionSettings
                     {
                         #region
                         IsCurrent = true,
                         Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                         Title = "Сведения о добыче зайца-русака в период с 1 ноября 2013 г. по 31 января 2014 г.",
                         SourceId = Guid.Parse("A078714A-CD9C-4D70-AB8F-C32FEAAC56DB")
                         #endregion
                     }
                 }
                #endregion
            },
            new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 5, 1),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_shakal",
                Title = "Сведения о добыче шакала",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Quaterly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         #region
                         Creator = "Министерство природных ресурсов Краснодарского края",
                         Description = "Документированная информация о добыче пушных животных,  отнесенных к охотничьим ресурсам по состоянию на 1 мая  2014 г.",
                         IsCurrent = false,
                         PublisherMBox = "mprkk@krasnodar.ru",
                         PublisherName = "Белоусов Игорь Валерьевич",
                         PublisherPhone = "+7-861-2591971",
                         SourceId = Guid.Parse("3A832D8A-64BF-482F-BD09-C68D23E3CD54"),
                         Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                         Title = "Сведения о добыче шакала в период с 1 вгуста 2013 г. по 31 июля  2014 г.",
                         #endregion
                     },
                     new ImportingSetVersionSettings
                     {
                         #region
                         IsCurrent = true,
                         Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам по состоянию на 1 сентября  2014 г.",
                         Title = "Сведения о добыче шакала в период с 1 ноября 2013г. по 28 февраля  2014 г.",
                         SourceId = Guid.Parse("5BA4FF39-6917-4ED0-B830-DA9976DEBAFF")
                         #endregion
                     }
                 }
                #endregion
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("HuntResourcesInfoForm4_2",
                new TableDisplayTemplate
                {
                    Description = "Систематизированный свод документированной информации об охотничьих ресурсах",
                    Title = "Сведения о добыче",
                    Name = "HuntResourcesInfoForm4_2",
                    Type = TableDisplayTemplate.TemplateType.RootTable,

                    #region
                    Columns = new ColumnDisplayTemplate[]

                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TerritoryName",
                                    DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "AdmitedQuota",
                                    DescriptionRu = "УТВЕРЖДЕННАЯ КВОТА, ОСОБЕЙ",
                                    DescritpionEn = "",
                                    Header = "УТВЕРЖДЕННАЯ КВОТА, ОСОБЕЙ",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                                new ColumnDisplayTemplate
                                {
                                    Header = "КОЛИЧЕСТВО РАЗРЕШЕНИЙ НА ДОБЫЧУ, ШТ.",
                                    Order = 2,
                                    SubColumns = new ColumnDisplayTemplate[]
                                    {
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "NumberOfPermisions_Gived",
                                            DescriptionRu = "КОЛИЧЕСТВО РАЗРЕШЕНИЙ НА ДОБЫЧУ, ШТ. ВЫДАНО",
                                            DescritpionEn = "",
                                            Header = "Выдано",
                                            IncludeIntoFilter = true,
                                            Order = 3,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "NumberOfPermisions_Returned",
                                            DescriptionRu = "КОЛИЧЕСТВО РАЗРЕШЕНИЙ НА ДОБЫЧУ, ШТ. Возвращено",
                                            DescritpionEn = "",
                                            Header = "Возвращено",
                                            IncludeIntoFilter = true,
                                            Order = 4,
                                            Type = ColumnDisplayTemplate.ContentType.Integer,
                                        },
                                    }
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "HuntedWithReturnedPermisions",
                                    DescriptionRu = "ДОБЫТО НА ВОЗВРАЩЕННЫЕ РАЗРЕШЕНИЯ, ОСОБЕЙ",
                                    DescritpionEn = "",
                                    Header = "ДОБЫТО НА ВОЗВРАЩЕННЫЕ РАЗРЕШЕНИЯ, ОСОБЕЙ",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                            }
                    #endregion
                });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm4_2"],
                Name = "HuntResourcesInfoForm4_2",
                Description = "Документированная информация о добыче пушных животных, отнесенных к охотничьим ресурсам",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm4_2"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return
                string.Format(
                    "INSERT INTO [dbo].[{0}] (TerritoryName, AdmitedQuota, NumberOfPermisions_Gived, NumberOfPermisions_Returned, HuntedWithReturnedPermisions, SetsPassports_Id) " +
                    "VALUES ('{1}', {2}, {3}, {4}, {5}, '{6}')", tableName, row["TerritoryName"] ?? "null",
                    row["AdmitedQuota"] ?? "null", row["NumberOfPermisions_Gived"] ?? "null",
                    row["NumberOfPermisions_Returned"] ?? "null", row["HuntedWithReturnedPermisions"] ?? "null",
                    passport_id);
        }
    }
}
