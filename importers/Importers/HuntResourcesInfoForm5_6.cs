﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_6 : ImporterBase
    {
        public HuntResourcesInfoForm5_6(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_osobo_ohranayemie",
                Title = "Сведения об особо охраняемых природных территориях регионального значения",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения об особо охраняемых природных территориях регионального значения",
                        SourceId = Guid.Parse("77ABBE65-300A-456B-A445-CF8FB6838205")
                    }
                }
            }) { }


        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm5_6", new TableDisplayTemplate
            {
                Description = "Сведения об особо охраняемых природных территориях регионального значения",
                Name = "HuntResourcesInfoForm5_6",
                Title = "Сведения об особо охраняемых природных территориях регионального значения",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "NameF5_6",
                         DescriptionRu = "НАИМЕНОВАНИЕ ООПТ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ ООПТ",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "StatusF5_6",
                         DescriptionRu = "СТАТУС",
                         DescritpionEn = "",
                         Header = "СТАТУС",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "SpaceF5_6",
                         DescriptionRu = "ПЛОЩАДЬ, ТЫС. ГА	",
                         DescritpionEn = "",
                         Header = "ПЛОЩАДЬ, ТЫС. ГА",
                         IncludeIntoFilter = true,
                         Order = 2,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "SpecializationF5_6",
                         DescriptionRu = "ВИДОВАЯ СПЕЦИАЛИЗАЦИЯ",
                         DescritpionEn = "",
                         Header = "ВИДОВАЯ СПЕЦИАЛИЗАЦИЯ",
                         IncludeIntoFilter = true,
                         Order = 3,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "DocumentF5_6",
                         DescriptionRu = "РЕКВИЗИТЫ НОРМАТИВНОГО ПРАВОВОГО АКТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                         DescritpionEn = "",
                         Header = "РЕКВИЗИТЫ НОРМАТИВНОГО ПРАВОВОГО АКТА СУБЪЕКТА РОССИЙСКОЙ ФЕДЕРАЦИИ",
                         IncludeIntoFilter = true,
                         Order = 4,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     }
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_6"],
                Name = "HuntResourcesInfoForm5_6",
                Description = "Сведения об особо охраняемых природных территориях регионального значения",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_6"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (NameF5_6, StatusF5_6, SpaceF5_6, SpecializationF5_6, DocumentF5_6, SetsPassports_Id) " +
                "VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", tableName, row["NameF5_6"], row["StatusF5_6"], row["SpaceF5_6"],
                row["SpecializationF5_6"], row["DocumentF5_6"], passport_id);
        }
    }
}
