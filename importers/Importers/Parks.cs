﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class Parks : ImporterBase
    {
        public Parks(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.diskk],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.kult],
                CreationDate = DateTime.Now,
                Description = "Парки и парковые зоны",
                SetIdentifier = "parki",
                Title = "Парки и парковые зоны",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Департамент информатизации и связи Краснодарского края",
                        Description = "Парки и парковые зоны",
                        IsCurrent = true,
                        PublisherMBox = "k.shatobin@dis.krasnodar.ru",
                        PublisherName = "Шатобин Константин Иванович",
                        PublisherPhone = "+7-861-2142214",
                        Subject = "Парки и парковые зоны",
                        Title = "Парки и парковые зоны",
                        SourceId = Guid.Parse("BA1FBBB4-9812-4881-81BE-F5AC80128995")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("Parks", new TableDisplayTemplate
            {
                Description = "Парки и парковые зоны",
                Name = "Parks",
                Title = "Парки и парковые зоны",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    new ColumnDisplayTemplate
                    {
                        Header = "НАИМЕНОВАНИЕ",
                        Order = 0,
                        InnerTable = new TableDisplayTemplate
                        {
                            #region
                            Description = "Парки и парковые зоны НАИМЕНОВАНИЕ",
                            Name = "Parks_Name",
                            Title = "Парки и парковые зоны НАИМЕНОВАНИЕ",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Name_FullName",
                                    DescriptionRu = "НАИМЕНОВАНИЕ Полное наименование",
                                    DescritpionEn = "",
                                    Header = "Полное наименование",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Name_ShortName",
                                    DescriptionRu = "НАИМЕНОВАНИЕ Краткое наименование",
                                    DescritpionEn = "",
                                    Header = "Краткое наименование",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Name_Documents",
                                    DescriptionRu = "НАИМЕНОВАНИЕ Реквизиты документа, по которому присвоено наименование",
                                    DescritpionEn = "",
                                    Header = "Реквизиты документа, по которому присвоено наименование",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Name_Type",
                                    DescriptionRu = "НАИМЕНОВАНИЕ Тип",
                                    DescritpionEn = "",
                                    Header = "Тип",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                            }
                            #endregion
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "ХАРАКТЕРИСТИКИ",
                        Order = 5,
                        InnerTable = new TableDisplayTemplate
                        {
                            #region
                            Description = "Парки и парковые зоны ХАРАКТЕРИСТИКИ",
                            Name = "Parks_Details",
                            Title = "Парки и парковые зоны ХАРАКТЕРИСТИКИ",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_PondsNumAndSquare",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Количество водоёмов на территории объекта и их площадь",
                                    DescritpionEn = "",
                                    Header = "Количество водоёмов на территории объекта и их площадь",
                                    IncludeIntoFilter = true,
                                    Order = 6,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_PlaygroundsNum",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Количество детских площадок",
                                    DescritpionEn = "",
                                    Header = "Количество детских площадок",
                                    IncludeIntoFilter = true,
                                    Order = 7,
                                    Type = ColumnDisplayTemplate.ContentType.Integer
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_SportsgroundsNum",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Количество спортивных площадок",
                                    DescritpionEn = "",
                                    Header = "Количество спортивных площадок",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.Integer
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_HasAttractions",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Наличие аттракционов",
                                    DescritpionEn = "",
                                    Header = "Наличие аттракционов",
                                    IncludeIntoFilter = true,
                                    Order = 9,
                                    Type = ColumnDisplayTemplate.ContentType.Boolean
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_HasAquaparks",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Наличие аквапарка",
                                    DescritpionEn = "",
                                    Header = "Наличие аквапарка",
                                    IncludeIntoFilter = true,
                                    Order = 10,
                                    Type = ColumnDisplayTemplate.ContentType.Boolean
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_HasCaterings",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Наличие пунктов общественного питания",
                                    DescritpionEn = "",
                                    Header = "Наличие пунктов общественного питания",
                                    IncludeIntoFilter = true,
                                    Order = 11,
                                    Type = ColumnDisplayTemplate.ContentType.Boolean
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_HasSanitaryFacilities",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Наличие санитарно-гигиенического сооружения",
                                    DescritpionEn = "",
                                    Header = "Наличие санитарно-гигиенического сооружения",
                                    IncludeIntoFilter = true,
                                    Order = 12,
                                    Type = ColumnDisplayTemplate.ContentType.Boolean
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_IsSecured",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Охраняемая территория",
                                    DescritpionEn = "",
                                    Header = "Охраняемая территория",
                                    IncludeIntoFilter = true,
                                    Order = 13,
                                    Type = ColumnDisplayTemplate.ContentType.Boolean
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_HasPublicWiFi",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Наличие публичного Wi-Fi",
                                    DescritpionEn = "",
                                    Header = "Наличие публичного Wi-Fi",
                                    IncludeIntoFilter = true,
                                    Order = 14,
                                    Type = ColumnDisplayTemplate.ContentType.Boolean
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_WiFiOperator",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Оператор публичного Wi-Fi",
                                    DescritpionEn = "",
                                    Header = "Оператор публичного Wi-Fi",
                                    IncludeIntoFilter = true,
                                    Order = 15,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_SheduleTime",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Часы работы (если предусмотрено)",
                                    DescritpionEn = "",
                                    Header = "Часы работы (если предусмотрено)",
                                    IncludeIntoFilter = true,
                                    Order = 16,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Details_PoliceStations",
                                    DescriptionRu = "ХАРАКТЕРИСТИКИ Объекты охраны правопорядка, в том числе точки экстренного вызова (гражданин-полиция)",
                                    DescritpionEn = "",
                                    Header = "Объекты охраны правопорядка, в том числе точки экстренного вызова (гражданин-полиция)",
                                    IncludeIntoFilter = true,
                                    Order = 17,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                            #endregion
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "РАСПОЛОЖЕНИЕ",
                        Order = 18,
                        InnerTable = new TableDisplayTemplate
                        {
                            #region
                            Description = "Парки и парковые зоны РАСПОЛОЖЕНИЕ",
                            Name = "Parks_Location",
                            Title = "Парки и парковые зоны РАСПОЛОЖЕНИЕ",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Location_Area",
                                    DescriptionRu = "РАСПОЛОЖЕНИЕ Район",
                                    DescritpionEn = "",
                                    Header = "Район",
                                    IncludeIntoFilter = true,
                                    Order = 19,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Location_Division",
                                    DescriptionRu = "РАСПОЛОЖЕНИЕ Административный округ",
                                    DescritpionEn = "",
                                    Header = "Административный округ",
                                    IncludeIntoFilter = true,
                                    Order = 20,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Location_Locality",
                                    DescriptionRu = "РАСПОЛОЖЕНИЕ Населённый пункт",
                                    DescritpionEn = "",
                                    Header = "Населённый пункт",
                                    IncludeIntoFilter = true,
                                    Order = 21,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Location_GPSCoords",
                                    DescriptionRu = "РАСПОЛОЖЕНИЕ Границы с координатной привязкой (МСК-23, WGS-84)",
                                    DescritpionEn = "",
                                    Header = "Границы с координатной привязкой (МСК-23, WGS-84)",
                                    IncludeIntoFilter = true,
                                    Order = 22,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Location_Square",
                                    DescriptionRu = "РАСПОЛОЖЕНИЕ Площадь",
                                    DescritpionEn = "",
                                    Header = "Площадь",
                                    IncludeIntoFilter = true,
                                    Order = 23,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                            #endregion
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "КОНТАКТНАЯ ИНФОРМАЦИЯ",
                        Order = 24,
                        InnerTable = new TableDisplayTemplate
                        {
                            #region
                            Description = "Парки и парковые зоны КОНТАКТНАЯ ИНФОРМАЦИЯ",
                            Name = "Parks_Contacts",
                            Title = "Парки и парковые зоны КОНТАКТНАЯ ИНФОРМАЦИЯ",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Contacts_OpertingOrgFullName",
                                    DescriptionRu = "КОНТАКТНАЯ ИНФОРМАЦИЯ Полное наименование эксплуатирующей организации",
                                    DescritpionEn = "",
                                    Header = "Полное наименование эксплуатирующей организации",
                                    IncludeIntoFilter = true,
                                    Order = 25,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Contacts_OperatingOrgAddress",
                                    DescriptionRu = "КОНТАКТНАЯ ИНФОРМАЦИЯ Юридический адрес эксплуатирующей организации",
                                    DescritpionEn = "",
                                    Header = "Юридический адрес эксплуатирующей организации",
                                    IncludeIntoFilter = true,
                                    Order = 26,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Contacts_OperatingOrgPhone",
                                    DescriptionRu = "КОНТАКТНАЯ ИНФОРМАЦИЯ Телефон эксплуатирующей организации",
                                    DescritpionEn = "",
                                    Header = "Телефон эксплуатирующей организации",
                                    IncludeIntoFilter = true,
                                    Order = 27,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Contacts_OperatingOrgURL",
                                    DescriptionRu = "КОНТАКТНАЯ ИНФОРМАЦИЯ Сайт эксплуатирующей организации",
                                    DescritpionEn = "",
                                    Header = "Сайт эксплуатирующей организации",
                                    IncludeIntoFilter = true,
                                    Order = 28,
                                    Type = ColumnDisplayTemplate.ContentType.Url
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Contacts_OperatingOrgEmail",
                                    DescriptionRu = "КОНТАКТНАЯ ИНФОРМАЦИЯ Адрес электронной почты эксплуатирующей организации",
                                    DescritpionEn = "",
                                    Header = "Адрес электронной почты эксплуатирующей организации",
                                    IncludeIntoFilter = true,
                                    Order = 29,
                                    Type = ColumnDisplayTemplate.ContentType.EMail
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "Contacts_Mentor",
                                    DescriptionRu = "КОНТАКТНАЯ ИНФОРМАЦИЯ Балансодержатель",
                                    DescritpionEn = "",
                                    Header = "Балансодержатель",
                                    IncludeIntoFilter = true,
                                    Order = 30,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                            #endregion
                        }
                    }
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["Parks"],
                Name = "Parks",
                Description = "Парки и парковые зоны",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "Parks"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (Location_Area, Location_Division, Location_Locality, Location_GPSCoords, Location_Square, " +
                "Name_FullName, Name_ShortName, Name_Documents, Name_Type, Contacts_OpertingOrgFullName, Contacts_OperatingOrgAddress, " +
                "Contacts_OperatingOrgPhone, Contacts_OperatingOrgURL, Contacts_OperatingOrgEmail, Contacts_Mentor," +
                "Details_PondsNumAndSquare, Details_PlaygroundsNum, Details_SportsgroundsNum, Details_HasAttractions, Details_HasAquaparks, " +
                "Details_HasCaterings, Details_HasSanitaryFacilities, Details_IsSecured, Details_HasPublicWiFi, Details_WiFiOperator, Details_SheduleTime, " +
                "Details_PoliceStations, SetsPassports_Id) VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', " +
                "'{13}', '{14}', '{15}', {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, '{25}', '{26}', '{27}', '{28}')", tableName, row["Location_Area"],
                row["Location_Division"], row["Location_Locality"], row["Location_GPSCoords"], row["Location_Square"], row["Name_FullName"], row["Name_ShortName"],
                row["Name_Documents"], row["Name_Type"], row["Contacts_OpertingOrgFullName"], row["Contacts_OperatingOrgAddress"], row["Contacts_OperatingOrgPhone"],
                row["Contacts_OperatingOrgURL"], row["Contacts_OperatingOrgEmail"], row["Contacts_Mentor"], row["Details_PondsNumAndSquare"], row["Details_PlaygroundsNum"],
                row["Details_SportsgroundsNum"], row["Details_HasAttractions"], row["Details_HasAquaparks"], row["Details_HasCaterings"], row["Details_HasSanitaryFacilities"],
                row["Details_IsSecured"], row["Details_HasPublicWiFi"], row["Details_WiFiOperator"], row["Details_SheduleTime"], row["Details_PoliceStations"], passport_id);
        }
    }
}
