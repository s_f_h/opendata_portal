﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class IncomeRegional : ImporterBase
    {
        public IncomeRegional(DataContext ctx)
            : base(ctx, 
            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                CreationDate = new DateTime(2015, 04, 02),
                Description = "Месячный отчет по доходам краевого бюджета",
                Title = "Месячный отчет по доходам краевого бюджета",
                SetIdentifier = "regionreportmonthincomes",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Monthly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство финансов Краснодарского края",
                        Description = "Месячный отчет по доходам краевого бюджета на 01.04.2015",
                        IsCurrent = true,
                        PublisherMBox = "minfin@krasnodar.ru",
                        PublisherName = "Кудинов Василий Петрович",
                        PublisherPhone = "+7-861-2145771",
                        Subject = "Месячный отчет по доходам краевого бюджета на 01.04.2015",
                        SourceId = Guid.Parse("C0AC7897-C198-410B-A688-38377D0121F5"),
                        Title = "Месячный отчет по доходам краевого бюджета на 01.04.2015"
                    }
                }
            },
            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                CreationDate = new DateTime(2015, 04, 02),
                Description = "Годовой отчет по доходам краевого бюджета",
                Title = "Годовой отчет по доходам краевого бюджета",
                SetIdentifier = "regionreportyearincomes",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Yearly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство финансов Краснодарского края",
                        Description = "Годовой отчет по доходам краевого бюджета на 01.04.2015",
                        IsCurrent = true,
                        PublisherMBox = "minfin@krasnodar.ru",
                        PublisherName = "Кудинов Василий Петрович",
                        PublisherPhone = "+7-861-2145771",
                        Subject = "Месячный отчет по доходам краевого бюджета на 01.04.2015",
                        SourceId = Guid.Parse("209A20C2-B264-4385-9408-81527DE296B3"),
                        Title = "Годовой отчет по доходам краевого бюджета на 01.04.2015"
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                CreationDate = new DateTime(2015, 04, 02),
                Description = "Месячный отчет по источникам краевого бюджета",
                Title = "Месячный отчет по источникам краевого бюджета",
                SetIdentifier = "regionreportmonthsource",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Monthly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство финансов Краснодарского края",
                        Description = "Месячный отчет по источникам краевого бюджета на 01.04.2015",
                        IsCurrent = true,
                        PublisherMBox = "minfin@krasnodar.ru",
                        PublisherName = "Кудинов Василий Петрович",
                        PublisherPhone = "+7-861-2145771",
                        Subject = "Месячный отчет по источникам краевого бюджета на 01.04.2015",
                        SourceId = Guid.Parse("E098C783-6D51-4A42-970F-E5CD22DA93A5"),
                        Title = "Месячный отчет по источникам краевого бюджета на 01.04.2015"
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                CreationDate = new DateTime(2015, 04, 02),
                Description = "Годовой отчет по источникам краевого",
                Title = "Годовой отчет по источникам краевого",
                SetIdentifier = "regionreporyearsource",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Yearly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство финансов Краснодарского края",
                        Description = "Годовой отчет по источникам краевого бюджета на 01.04.2015",
                        IsCurrent = true,
                        PublisherMBox = "minfin@krasnodar.ru",
                        PublisherName = "Кудинов Василий Петрович",
                        PublisherPhone = "+7-861-2145771",
                        Subject = "Годовой отчет по источникам краевого бюджета на 01.04.2015",
                        SourceId = Guid.Parse("04147998-A40C-4490-B9E2-7E71A19101F7"),
                        Title = "Годовой отчет по источникам краевого бюджета на 01.04.2015"
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                CreationDate = new DateTime(2015, 04, 02),
                Description = "Годовой отчет по расходам краевого бюджета",
                Title = "Годовой отчет по расходам краевого бюджета",
                SetIdentifier = "regionreportyearoutcomes",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Yearly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство финансов Краснодарского края",
                        Description = "Годовой отчет по расходам краевого бюджета на 01.04.2015",
                        IsCurrent = true,
                        PublisherMBox = "minfin@krasnodar.ru",
                        PublisherName = "Кудинов Василий Петрович",
                        PublisherPhone = "+7-861-2145771",
                        Subject = "Годовой отчет по расходам краевого бюджета на 01.04.2015",
                        SourceId = Guid.Parse("B05F91E4-77BE-41BA-8495-170BCE199605"),
                        Title = "Годовой отчет по расходам краевого бюджета на 01.04.2015"
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_fin],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.ekon],
                CreationDate = new DateTime(2015, 04, 02),
                Description = "Месячный отчет по расходам краевого бюджета",
                Title = "Месячный отчет по расходам краевого бюджета",
                SetIdentifier = "regionreportmonthoutcomes",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Monthly,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство финансов Краснодарского края",
                        Description = "Месячный отчет по расходам краевого бюджета на 01.04.2015",
                        IsCurrent = true,
                        PublisherMBox = "minfin@krasnodar.ru",
                        PublisherName = "Кудинов Василий Петрович",
                        PublisherPhone = "+7-861-2145771",
                        Subject = "Месячный отчет по расходам краевого бюджета на 01.04.2015",
                        SourceId = Guid.Parse("F8E693A6-7D69-4E34-BA50-4E9AF26AC88F"),
                        Title = "Месячный отчет по расходам краевого бюджета на 01.04.2015"
                    }
                }
            }

            ) { asIs = true; }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("IncomeRegional", new TableDisplayTemplate
            {
                Description = "Месячный отчет по доходам краевого бюджета",
                Name = "IncomeRegional",
                Title = "Месячный отчет по доходам краевого бюджета",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                 {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "NameOfIndicator",
                         DescriptionRu = "НАИМЕНОВАНИЕ ПОКАЗАТЕЛЯ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ ПОКАЗАТЕЛЯ",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "StringCode",
                         DescriptionRu = "КОД СТРОКИ",
                         DescritpionEn = "",
                         Header = "КОД СТРОКИ",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "BudgetCode",
                         DescriptionRu = "КОД ДОХОДА ПО БЮДЖЕТНОЙ КЛАССИФИКАЦИИ",
                         DescritpionEn = "",
                         Header = "КОД ДОХОДА ПО БЮДЖЕТНОЙ КЛАССИФИКАЦИИ	",
                         IncludeIntoFilter = true,
                         Order = 2,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Approved",
                         DescriptionRu = "УТВЕРЖДЕНО",
                         DescritpionEn = "",
                         Header = "УТВЕРЖДЕНО",
                         IncludeIntoFilter = true,
                         Order = 3,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Fulfiled",
                         DescriptionRu = "ИСПОЛНЕНО",
                         DescritpionEn = "",
                         Header = "ИСПОЛНЕНО",
                         IncludeIntoFilter = true,
                         Order = 4,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "NotFulfiled",
                         DescriptionRu = "НЕ ИСПОЛНЕНО",
                         DescritpionEn = "",
                         Header = "НЕ ИСПОЛНЕНО",
                         IncludeIntoFilter = true,
                         Order = 5,
                         Type = ColumnDisplayTemplate.ContentType.String
                     }
                 }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["IncomeRegional"],
                Name = "IncomeRegional",
                Description = "Месячный отчет по доходам",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "IncomeRegional"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (NameOfIndicator, StringCode, BudgetCode, Approved, Fulfiled, NotFulfiled, SetsPassports_Id) " +
                "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')", tableName, row["NameOfIndicator"], row["StringCode"], row["BudgetCode"],
                row["Approved"], row["Fulfiled"], row["NotFulfiled"], passport_id);
        }
    }
}
