﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_2 : ImporterBase
    {
        public HuntResourcesInfoForm5_2(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_vosproizvodstvo",
                Title = "Документированная информация о воспроизводстве охотничьих ресурсов",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        SourceId = Guid.Parse("F508161B-C4DB-4A13-A926-443FE2C958D5"),
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Документированная информация о воспроизводстве охотничьих ресурсов	по состоянию на 1 мая  2014  г.",
                    }
                }

            }) { asIs = true; }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var rsult = new Dictionary<string, Repository.Entities.TableDisplayTemplate>();

            rsult.Add("HuntResourcesInfoForm5_2",
                new Repository.Entities.TableDisplayTemplate
                {
                    Description = "Документированная информация о воспроизводстве охотничьих ресурсов",
                    Name = "HuntResourcesInfoForm5_2",
                    Title = "Документированная информация о воспроизводстве охотничьих ресурсов",
                    Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                            {
                                 new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OrgNameF5_2",
                                    DescriptionRu = "НАИМЕНОВАНИЕ ЮРИДИЧЕСКОГО ЛИЦА ИЛИ ИНДИВИДУАЛЬНОГО ПРЕДПРИНИМАТЕЛЯ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ЮРИДИЧЕСКОГО ЛИЦА ИЛИ ИНДИВИДУАЛЬНОГО ПРЕДПРИНИМАТЕЛЯ",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                 new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "AddressesF5_2",
                                    DescriptionRu = "ПОЧТОВЫЙ АДРЕС, ТЕЛЕФОН,E-MAIL",
                                    DescritpionEn = "",
                                    Header = "ПОЧТОВЫЙ АДРЕС, ТЕЛЕФОН,E-MAIL",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                 new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "HuntResourcesType5_2",
                                    DescriptionRu = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                                    DescritpionEn = "",
                                    Header = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "QuantityF5_2",
                                    DescriptionRu = "КОЛИЧЕСТВО ОСОБЕЙ",
                                    DescritpionEn = "",
                                    Header = "КОЛИЧЕСТВО ОСОБЕЙ",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TerritorySpaceF5_2",
                                    DescriptionRu = "ПЛОЩАДЬ ВОЛЬЕРА, ГА",
                                    DescritpionEn = "",
                                    Header = "ПЛОЩАДЬ ВОЛЬЕРА, ГА",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    Header = "РАЗРЕШЕНИЕ",
                                    Order = 6,
                                    InnerTable = new TableDisplayTemplate
                                    {
                                        Description = "Документированная информация о воспроизводстве охотничьих ресурсов. Разрешение",
                                        Name = "HuntResourcesInfoForm5_2. Permissions",
                                        Title = "Документированная информация о воспроизводстве охотничьих ресурсов. Разрешение",
                                        Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                                        Columns = new ColumnDisplayTemplate[]
                                        {
                                            new ColumnDisplayTemplate
                                            {
                                                BindedCollumn = "PermissionF5_2_GotFromF5_2",
                                                DescriptionRu = "РАЗРЕШЕНИЕ Кем выдано",
                                                DescritpionEn = "",
                                                Header = "Кем выдано",
                                                IncludeIntoFilter = true,
                                                Order = 7,
                                                Type = ColumnDisplayTemplate.ContentType.String
                                            },

                                            new ColumnDisplayTemplate
                                            {
                                                BindedCollumn = "PermissionF5_2_DateF5_2",
                                                DescriptionRu = "РАЗРЕШЕНИЕ Дата выдачи",
                                                DescritpionEn = "",
                                                Header = "Дата выдачи",
                                                IncludeIntoFilter = true,
                                                Order = 8,
                                                Type = ColumnDisplayTemplate.ContentType.String
                                            },

                                            new ColumnDisplayTemplate
                                            {
                                                BindedCollumn = "PermissionF5_2_NumberF5_2",
                                                DescriptionRu = "РАЗРЕШЕНИЕ Серия и номер",
                                                DescritpionEn = "",
                                                Header = "Серия и номер",
                                                IncludeIntoFilter = true,
                                                Order = 9,
                                                Type = ColumnDisplayTemplate.ContentType.String
                                            },

                                            new ColumnDisplayTemplate
                                            {
                                                BindedCollumn = "PermissionF5_2_RunOfValidityF5_2",
                                                DescriptionRu = "РАЗРЕШЕНИЕ Срок действия",
                                                DescritpionEn = "",
                                                Header = "Срок действия",
                                                IncludeIntoFilter = true,
                                                Order = 10,
                                                Type = ColumnDisplayTemplate.ContentType.String
                                            },
                                        }
                                    }
                                }
                            }
                });
            return rsult;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_2"],
                Name = "HuntResourcesInfoForm5_2",
                Description = "Содержание и разведение охотничьих ресурсов в полувольных условиях и искусственно созданной среде обитания",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_2"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (OrgNameF5_2, AddressesF5_2, HuntResourcesType5_2, QuantityF5_2, TerritorySpaceF5_2, " +
                "PermissionF5_2_GotFromF5_2, PermissionF5_2_DateF5_2, PermissionF5_2_NumberF5_2, PermissionF5_2_RunOfValidityF5_2, SetsPassports_Id) " +
                "VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')", tableName, row["OrgNameF5_2"], row["AddressesF5_2"],
                row["HuntResourcesType5_2"], row["QuantityF5_2"], row["TerritorySpaceF5_2"], row["PermissionF5_2_GotFromF5_2"], row["PermissionF5_2_DateF5_2"],
                row["PermissionF5_2_NumberF5_2"], row["PermissionF5_2_RunOfValidityF5_2"], passport_id);
        }
    }
}
