﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class RegistryEducationApproved : ImporterBase
    {
        public RegistryEducationApproved(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_obr],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.obraz],
                CreationDate = DateTime.Now,
                Description = "Реестр аккредитованных образовательных учреждений, аккредитованных министерством образования и науки  Краснодарского края",
                SetIdentifier = "akkr001",
                Title = "Реестр аккредитованных образовательных учреждений Краснодарского края",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство образования и науки Краснодарского края",
                        Description = "Реестр аккредитованных образовательных учреждений, аккредитованных министерством образования и науки  Краснодарского края",
                        IsCurrent = true,
                        PublisherMBox = "nadzor@des.kubannet.ru",
                        PublisherName = "Акиньшин Вадим Валерьевич",
                        PublisherPhone = "+7-861-23165703",
                        Subject = "-;-",
                        Title = "Реестр аккредитованных образовательных учреждений Краснодарского края",
                        SourceId =  Guid.Parse("716C3C79-B246-4AF9-AA60-950995693E56")
                    }
                }
            }) { }
        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["RegistryEducationApproved"],
                Name = "RegistryEducationApproved",
                Description = "Реестр аккредитованных образовательных учреждений Краснодарского края",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "RegistryEducationApproved"
            };
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("RegistryEducationApproved", new TableDisplayTemplate
            {
                Description = "Реестр аккредитованных образовательных учреждений Краснодарского края",
                Name = "RegistryEducationApproved",
                Title = "Реестр аккредитованных образовательных учреждений Краснодарского края",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                 {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "INN",
                         DescriptionRu = "ИНН",
                         DescritpionEn = "",
                         Header = "ИНН",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Name",
                         DescriptionRu = "НАИМЕНОВАНИЕ",
                         DescritpionEn = "",
                         Header = "НАИМЕНОВАНИЕ",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     },
            
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "Address",
                         DescriptionRu = "АДРЕС",
                         DescritpionEn = "",
                         Header = "АДРЕС",
                         IncludeIntoFilter = true,
                         Order = 2,
                         Type = ColumnDisplayTemplate.ContentType.PostAddress
                     },
            
                     new ColumnDisplayTemplate
                     {
                         Header = "Свидетельство",
                         Order = 3,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Реестр аккредитованных образовательных учреждений Краснодарского края Свидетельство",
                             Name = "RegistryEducationApproved_Svidetelstvo",
                             Title = "Реестр аккредитованных образовательных учреждений Краснодарского края Свидетельство",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "CertificateRegNumber",
                                     DescriptionRu = "Свидетельство Регистрационный номер",
                                     DescritpionEn = "",
                                     Header = "Регистрационный номер",
                                     IncludeIntoFilter = true,
                                     Order = 4,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "CertificateSerialNumber",
                                     DescriptionRu = "Свидетельство Серия и номер",
                                     DescritpionEn = "",
                                     Header = "Серия и номер",
                                     IncludeIntoFilter = true,
                                     Order = 5,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
                             }
                         }
                     },
            
                     new ColumnDisplayTemplate
                     {
                         Header = "Приказ",
                         Order = 6,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Реестр аккредитованных образовательных учреждений Краснодарского края Свидетельство",
                             Name = "RegistryEducationApproved_Order",
                             Title = "Реестр аккредитованных образовательных учреждений Краснодарского края Свидетельство",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "OrderDate",
                                     DescriptionRu = "Приказ Дата",
                                     DescritpionEn = "",
                                     Header = "Дата",
                                     IncludeIntoFilter = true,
                                     Order = 7,
                                     Type = ColumnDisplayTemplate.ContentType.Date
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "OrderNumber",
                                     DescriptionRu = "Приказ Номер",
                                     DescritpionEn = "",
                                     Header = "Номер",
                                     IncludeIntoFilter = true,
                                     Order = 8,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "RunOfValidity",
                                     DescriptionRu = "Приказ Срок действия",
                                     DescritpionEn = "",
                                     Header = "Срок действия",
                                     IncludeIntoFilter = true,
                                     Order = 9,
                                     Type = ColumnDisplayTemplate.ContentType.Date
                                 },
            
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "State",
                                     DescriptionRu = "Приказ Состояние",
                                     DescritpionEn = "",
                                     Header = "Состояние",
                                     IncludeIntoFilter = true,
                                     Order = 10,
                                     Type = ColumnDisplayTemplate.ContentType.String
                                 },
            
                                 new ColumnDisplayTemplate
                                 {
                                     Header = "Приложение",
                                     Order = 11,
                                     SubColumns = new ColumnDisplayTemplate[]
                                     {
                                         new ColumnDisplayTemplate
                                         {
                                             BindedCollumn = "AppendixNumber",
                                             DescriptionRu = "Приказ Приложение Номер",
                                             DescritpionEn = "",
                                             Header = "Номер",
                                             IncludeIntoFilter = true,
                                             Order = 12,
                                             Type = ColumnDisplayTemplate.ContentType.String
                                         },
                                         new ColumnDisplayTemplate
                                         {
                                             BindedCollumn = "AppendixSerialNumber",
                                             DescriptionRu = "Приказ Приложение Серия",
                                             DescritpionEn = "",
                                             Header = "Серия",
                                             IncludeIntoFilter = true,
                                             Order = 13,
                                             Type = ColumnDisplayTemplate.ContentType.String
                                         },
                                     }
                                 }
                             }
                         }
                     },
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "ProgrammsList",
                         DescriptionRu = "ПЕРЕЧЕНЬ ПРОГРАММ",
                         DescritpionEn = "",
                         Header = "ПЕРЕЧЕНЬ ПРОГРАММ",
                         IncludeIntoFilter = true,
                         Order = 14,
                         Type = ColumnDisplayTemplate.ContentType.Text
                     }
                 }
            });
            return result;
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (INN, Name, Address, CertificateRegNumber, CertificateSerialNumber, OrderDate, " +
             "OrderNumber, RunOfValidity, State, AppendixNumber, AppendixSerialNumber, ProgrammsList, SetsPassports_Id) VALUES (" +
             "'{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}')", tableName,
            row["INN"], row["Name"], row["Address"], row["CertificateRegNumber"], row["CertificateSerialNumber"],
            row["OrderDate"] != null ? ((DateTime)row["OrderDate"]).ToString("yyyy-MM-dd") : null,
            row["OrderNumber"], row["RunOfValidity"] != null ? ((DateTime)row["RunOfValidity"]).ToString("yyyy-MM-dd") : null,
            row["State"], row["AppendixNumber"], row["AppendixSerialNumber"], row["ProgrammsList"], passport_id);
        }
    }
}
