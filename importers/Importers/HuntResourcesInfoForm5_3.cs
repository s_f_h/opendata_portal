﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_3 : ImporterBase
    {
        public HuntResourcesInfoForm5_3(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_chislennost",
                Title = "Сведения по регулированию численности охотничьих ресурсов",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения по регулированию численности охотничьих ресурсов",
                        SourceId = Guid.Parse("6A39D9DF-BFBA-413E-B2A2-E572F3ADE4EF")
                    }
                }
            }) { asIs = true; }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var rsult = new Dictionary<string, Repository.Entities.TableDisplayTemplate>();

            rsult.Add("HuntResourcesInfoForm5_3",
                new Repository.Entities.TableDisplayTemplate
                {
                    Description = "Сведения по регулированию численности охотничьих ресурсов",
                    Name = "HuntResourcesInfoForm5_3",
                    Title = "Сведения по регулированию численности охотничьих ресурсов",
                    Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                    {
                         new ColumnDisplayTemplate
                        {
                            BindedCollumn = "TerritoryNameF5_3",
                            DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                            DescritpionEn = "",
                            Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                            IncludeIntoFilter = true,
                            Order = 0,
                            Type = ColumnDisplayTemplate.ContentType.String
                        },

                         new ColumnDisplayTemplate
                        {
                            BindedCollumn = "ReasonsF5_3",
                            DescriptionRu = "ПРИЧИНЫ, ОБУСЛОВЛЕННЫЕ НЕОБХОДИМОСТЬЮ РЕГУЛИРОВАНИЯ ЧИСЛЕННОСТИ",
                            DescritpionEn = "",
                            Header = "ПРИЧИНЫ, ОБУСЛОВЛЕННЫЕ НЕОБХОДИМОСТЬЮ РЕГУЛИРОВАНИЯ ЧИСЛЕННОСТИ",
                            IncludeIntoFilter = true,
                            Order = 1,
                            Type = ColumnDisplayTemplate.ContentType.Text
                        },

                         new ColumnDisplayTemplate
                        {
                            BindedCollumn = "GovOrder5_3",
                            DescriptionRu = "РЕШЕНИЕ ОРГАНА ИСПОЛНИТЕЛЬНОЙ ВЛАСТИ",
                            DescritpionEn = "",
                            Header = "РЕШЕНИЕ ОРГАНА ИСПОЛНИТЕЛЬНОЙ ВЛАСТИ",
                            IncludeIntoFilter = true,
                            Order = 2,
                            Type = ColumnDisplayTemplate.ContentType.Text
                        },

                         new ColumnDisplayTemplate
                        {
                            BindedCollumn = "HunterResourcesTypeF5_3",
                            DescriptionRu = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                            DescritpionEn = "",
                            Header = "ВИД ОХОТНИЧЬИХ РЕСУРСОВ",
                            IncludeIntoFilter = true,
                            Order = 3,
                            Type = ColumnDisplayTemplate.ContentType.String
                        },

                        new ColumnDisplayTemplate
                        {
                            Header = "КОЛИЧЕСТВО ДОБЫТЫХ, ОСОБЕЙ",
                            Order = 4,
                            InnerTable = new TableDisplayTemplate
                            {
                                Description = "Сведения по регулированию численности охотничьих ресурсов. КОЛИЧЕСТВО ДОБЫТЫХ, ОСОБЕЙ",
                                Name = "HuntResourcesInfoForm5_3_Number_of_hunted",
                                Title = "КОЛИЧЕСТВО ДОБЫТЫХ, ОСОБЕЙ",
                                Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                                Columns = new ColumnDisplayTemplate[]
                                {
                                    new ColumnDisplayTemplate
                                    {
                                        BindedCollumn = "QuantityHuntedF5_3_TotalF5_3",
                                        DescriptionRu = "КОЛИЧЕСТВО ДОБЫТЫХ, ОСОБЕЙ	Всего",
                                        DescritpionEn = "Всего",
                                        Header = "",
                                        IncludeIntoFilter = true,
                                        Order = 5,
                                        Type = ColumnDisplayTemplate.ContentType.String
                                    },

                                    new ColumnDisplayTemplate
                                    {
                                        BindedCollumn = "QuantityHuntedF5_3_MalesYoungerOneYearF5_3",
                                        DescriptionRu = "КОЛИЧЕСТВО ДОБЫТЫХ, ОСОБЕЙ	Самцов до 1 года",
                                        DescritpionEn = "",
                                        Header = "Самцов до 1 года",
                                        IncludeIntoFilter = true,
                                        Order = 6,
                                        Type = ColumnDisplayTemplate.ContentType.String
                                    },

                                    new ColumnDisplayTemplate
                                    {
                                        BindedCollumn = "QuantityHuntedF5_3_MatureMalesF5_3",
                                        DescriptionRu = "КОЛИЧЕСТВО ДОБЫТЫХ, ОСОБЕЙ	Взрослых самцов",
                                        DescritpionEn = "",
                                        Header = "Взрослых самцов",
                                        IncludeIntoFilter = true,
                                        Order = 7,
                                        Type = ColumnDisplayTemplate.ContentType.String
                                    },

                                    new ColumnDisplayTemplate
                                    {
                                        BindedCollumn = "QuantityHuntedF5_3_FemalesYoungerOneYearF5_3",
                                        DescriptionRu = "КОЛИЧЕСТВО ДОБЫТЫХ, ОСОБЕЙ	Самок до 1 года",
                                        DescritpionEn = "",
                                        Header = "Самок до 1 года",
                                        IncludeIntoFilter = true,
                                        Order = 8,
                                        Type = ColumnDisplayTemplate.ContentType.String
                                    },

                                    new ColumnDisplayTemplate
                                    {
                                        BindedCollumn = "QuantityHuntedF5_3_MatureFemalesF5_3",
                                        DescriptionRu = "КОЛИЧЕСТВО ДОБЫТЫХ, ОСОБЕЙ	Взрослых самок",
                                        DescritpionEn = "",
                                        Header = "Взрослых самок",
                                        IncludeIntoFilter = true,
                                        Order = 9,
                                        Type = ColumnDisplayTemplate.ContentType.String
                                    }
                                }
                            }
                        },

                         new ColumnDisplayTemplate
                        {
                            BindedCollumn = "TimePeriodsF5_3",
                            DescriptionRu = "СРОКИ ПРОВЕДЕНИЯ МЕРОПРИЯТИЙ",
                            DescritpionEn = "",
                            Header = "СРОКИ ПРОВЕДЕНИЯ МЕРОПРИЯТИЙ",
                            IncludeIntoFilter = true,
                            Order = 10,
                            Type = ColumnDisplayTemplate.ContentType.String
                        },

                        new ColumnDisplayTemplate
                        {
                            BindedCollumn = "InformationF5_3",
                            DescriptionRu = "СВЕДЕНИЯ ОБ ИСПОЛЬЗОВАНИИ ПРОДУКЦИИ",
                            DescritpionEn = "",
                            Header = "СВЕДЕНИЯ ОБ ИСПОЛЬЗОВАНИИ ПРОДУКЦИИ",
                            IncludeIntoFilter = true,
                            Order = 11,
                            Type = ColumnDisplayTemplate.ContentType.Text
                        }
                    }
                });

            return rsult;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_3"],
                Name = "HuntResourcesInfoForm5_3",
                Description = "Сведения по регулированию численности охотничьих ресурсов",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_3"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (TerritoryNameF5_3, ReasonsF5_3, GovOrder5_3, HunterResourcesTypeF5_3, "
                + "QuantityHuntedF5_3_TotalF5_3, QuantityHuntedF5_3_MalesYoungerOneYearF5_3, QuantityHuntedF5_3_MatureMalesF5_3, " +
                "QuantityHuntedF5_3_FemalesYoungerOneYearF5_3, QuantityHuntedF5_3_MatureFemalesF5_3, TimePeriodsF5_3, InformationF5_3, " +
                "SetsPassports_Id) VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}')", tableName,
                row["TerritoryNameF5_3"], row["ReasonsF5_3"], row["GovOrder5_3"], row["HunterResourcesTypeF5_3"], row["QuantityHuntedF5_3_TotalF5_3"],
                row["QuantityHuntedF5_3_MalesYoungerOneYearF5_3"], row["QuantityHuntedF5_3_MatureMalesF5_3"], row["QuantityHuntedF5_3_FemalesYoungerOneYearF5_3"],
                row["QuantityHuntedF5_3_MatureFemalesF5_3"], row["TimePeriodsF5_3"], row["InformationF5_3"], passport_id);
        }
    }
}
