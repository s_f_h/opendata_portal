﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;


namespace importers.Importers
{
    internal class  HuntResourcesInfoFrom4_3 : ImporterBase
    {
        public HuntResourcesInfoFrom4_3(DataContext ctx)
            : base(ctx, 
            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_lisuha",
                Title = "Сведения о добыче  лысухи",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче  лысухи  в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("DE6398E0-3A0D-4B01-9687-D882EEB68847")
                    }
                }
            },
            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_fazan",
                Title = "Сведения о добыче  фазанов",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче  фазанов  в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("BCA25AC3-8192-499D-9D26-CA6CC0045B79")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_utka",
                Title = "Сведения о добыче  уток",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче  уток  в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("5CFB4973-1120-4EC1-8387-C3FDA43E653C")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_valdshnep",
                Title = "Сведения о добыче  вальшнепов",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче  вальшнепов  в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("6EE8CDA9-FA37-46B9-A58B-A48B55D6E0C9")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_bolsoy_baklan",
                Title = "Сведения о добыче  большого  баклана",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче  большого  баклана  в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("79EFC544-9D24-4858-B49F-A9139395CD33")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_seraya_kuropatka",
                Title = "Сведения о добыче серой куропатки",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче серой куропатки в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("989BAAEF-8063-4BB3-BB9D-FF1ED28B0B3F")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_nirok",
                Title = "Сведения о добыче нырков",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче нырков в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("2DCC46C0-3277-4CD3-89AC-DF21C612D19E")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_lutok",
                Title = "Сведения о добыче лутков",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче лутков в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("62D54F9E-0CB3-420B-B027-5F53871F195F")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_gus",
                Title = "Сведения о добыче гусей",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче гусей в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("3AFAE2B2-395C-40B3-ACB5-4FE031BE7A75")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_kulik",
                Title = "Сведения о добыче куликов и пастушковых",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче куликов и пастушковых в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("EF18B4BE-6CE9-4849-8AC2-51869B2279F6")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_perepel",
                Title = "Сведения о добыче перепелов",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче перепелов в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("88A3DF97-AFE9-47C7-A856-6EA8D2D082C1")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_golub",
                Title = "Сведения о добыче голубей",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче голубей в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("4B07C5B5-1B68-42EC-A53C-7F451AE432AA")
                    }
                }
            },

            new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2014, 12, 12),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_seraya_vorona",
                Title = "Сведения о добыче серой вороны",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о добыче серой вороны в сезоне охоты 2013 – 2014 гг.",
                        SourceId = Guid.Parse("1E83DF67-3B88-41D2-BE56-837B37503267")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm4_3", new TableDisplayTemplate()
            {
                Title = "Сведения о добыче ",
                Name = "HuntResourcesInfoForm4_3",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                Description = "Сведения о добыче ",
                #region
                Columns = new ColumnDisplayTemplate[]
                {
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TerritoryNameF4_3",
                        DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String,
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "ApprovedNormF4_3",
                        DescriptionRu = "УСТАНОВЛЕННЫЙ НОРМАТИВ ИЗЪЯТИЯ, ОСОБЕЙ",
                        DescritpionEn = "",
                        Header = "УСТАНОВЛЕННЫЙ НОРМАТИВ ИЗЪЯТИЯ, ОСОБЕЙ",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.Integer,
                    },
            
                    new ColumnDisplayTemplate
                    {
                        Header = "В СРОКИ ВЕСЕННЕЙ ОХОТЫ",
                        Order = 2,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения о добыче В СРОКИ ВЕСЕННЕЙ ОХОТЫ	",
                            Name = "HuntResourcesInfoForm4_3_InSpring",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Description = "Сведения о добыче В СРОКИ ВЕСЕННЕЙ ОХОТЫ	",
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "SpringHuntF4_3_PermisionsNumberGivenF4_3",
                                    DescriptionRu = "Количество разрешений на добычу охотничьих птиц выдано, шт.",
                                    DescritpionEn = "",
                                    Header = "Количество разрешений на добычу охотничьих птиц выдано, шт.",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
            
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "SpringHuntF4_3_PermisionsNumberReturnedF4_3",
                                    DescriptionRu = "Количество разрешений на добычу охотничьих птиц возвращено, шт.",
                                    DescritpionEn = "",
                                    Header = "Количество разрешений на добычу охотничьих птиц возвращено, шт.",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
            
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "SpringHuntF4_3_HuntedPermisionsReturnedF4_3",
                                    DescriptionRu = "Добыто охотниками, возвратившими разрешения, особей",
                                    DescritpionEn = "",
                                    Header = "Добыто охотниками, возвратившими разрешения, особей",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                }
                            }
                        }
                    },
                  
                    new ColumnDisplayTemplate
                    {
                        Header = "В СРОКИ ЛЕТНЕ-ОСЕННЕЙ И ОСЕННЕ-ЗИМНЕЙ ОХОТЫ",
                        Order = 7,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения о добыче В СРОКИ ЛЕТНЕ-ОСЕННЕЙ И ОСЕННЕ-ЗИМНЕЙ ОХОТЫ",
                            Name = "HuntResourcesInfoForm4_3_Other",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Description = "Сведения о добыче В СРОКИ ЛЕТНЕ-ОСЕННЕЙ И ОСЕННЕ-ЗИМНЕЙ ОХОТЫ",
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OtherHuntF4_3_PermisionsNumberGivenF4_3",
                                    DescriptionRu = "Количество разрешений на добычу охотничьих птиц выдано, шт.",
                                    DescritpionEn = "",
                                    Header = "Количество разрешений на добычу охотничьих птиц выдано, шт.",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
            
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OtherHuntF4_3_PermisionsNumberReturnedF4_3",
                                    DescriptionRu = "Количество разрешений на добычу охотничьих птиц возвращено, шт.",
                                    DescritpionEn = "",
                                    Header = "Количество разрешений на добычу охотничьих птиц возвращено, шт.",
                                    IncludeIntoFilter = true,
                                    Order = 9,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
            
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "OtherHuntF4_3_HuntedPermisionsReturnedF4_3",
                                    DescriptionRu = "Добыто охотниками, возвратившими разрешения, особей",
                                    DescritpionEn = "",
                                    Header = "Добыто охотниками, возвратившими разрешения, особей",
                                    IncludeIntoFilter = true,
                                    Order = 10,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                }
                            }
                        }
                    },
            
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TotalHuntedF4_3",
                        DescriptionRu = "ВСЕГО ДОБЫТО, ОСОБЕЙ",
                        DescritpionEn = "",
                        Header = "ВСЕГО ДОБЫТО, ОСОБЕЙ",
                        IncludeIntoFilter = true,
                        Order = 11,
                        Type = ColumnDisplayTemplate.ContentType.Integer,
                    },
                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm4_3"],
                Name = "HuntResourcesInfoForm4_3",
                Description = "Сведения о добыче",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm4_3"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (TerritoryNameF4_3, ApprovedNormF4_3, SpringHuntF4_3_PermisionsNumberGivenF4_3, " +
                "SpringHuntF4_3_PermisionsNumberReturnedF4_3, SpringHuntF4_3_HuntedPermisionsReturnedF4_3, OtherHuntF4_3_PermisionsNumberGivenF4_3, " +
                "OtherHuntF4_3_PermisionsNumberReturnedF4_3, OtherHuntF4_3_HuntedPermisionsReturnedF4_3, TotalHuntedF4_3, SetsPassports_Id) " +
                "VALUES ('{1}', {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, '{10}')", tableName, row["TerritoryNameF4_3"],
                row["ApprovedNormF4_3"] ?? "null",
                row["SpringHuntF4_3_PermisionsNumberGivenF4_3"] ?? "null",
                row["SpringHuntF4_3_PermisionsNumberReturnedF4_3"] ?? "null",
                row["SpringHuntF4_3_HuntedPermisionsReturnedF4_3"] ?? "null",
                row["OtherHuntF4_3_PermisionsNumberGivenF4_3"] ?? "null",
                row["OtherHuntF4_3_PermisionsNumberReturnedF4_3"] ?? "null",
                row["OtherHuntF4_3_HuntedPermisionsReturnedF4_3"] ?? "null",
                row["TotalHuntedF4_3"] ?? "null", passport_id);
        }
    }


    //class HuntResourcesInfoFrom4_3 : ImporterSkeleton
    //{
    //     
    //            
    //            , DateTime.Now, 
    //             Guid.Parse("DE6398E0-3A0D-4B01-9687-D882EEB68847")
    //             , Guid.Parse("BCA25AC3-8192-499D-9D26-CA6CC0045B79")
    //             , Guid.Parse("5CFB4973-1120-4EC1-8387-C3FDA43E653C")
    //             , Guid.Parse("79EFC544-9D24-4858-B49F-A9139395CD33")
    //             , Guid.Parse("989BAAEF-8063-4BB3-BB9D-FF1ED28B0B3F")
    //             , Guid.Parse("2DCC46C0-3277-4CD3-89AC-DF21C612D19E")
    //             , Guid.Parse("62D54F9E-0CB3-420B-B027-5F53871F195F")
    //             , Guid.Parse("3AFAE2B2-395C-40B3-ACB5-4FE031BE7A75")
    //             , Guid.Parse("EF18B4BE-6CE9-4849-8AC2-51869B2279F6")
    //             , Guid.Parse("88A3DF97-AFE9-47C7-A856-6EA8D2D082C1")
    //             , Guid.Parse("4B07C5B5-1B68-42EC-A53C-7F451AE432AA")
    //             , Guid.Parse("1E83DF67-3B88-41D2-BE56-837B37503267")) { asIs = false; }

    //    protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passportId)
    //    {
    //       
    //       
    //       
    //       
    //       
    //       
    //       
    //       
    //       
    //       
    //       
    //       
    //    }

    //    protected override SetPassport CreatePassport(Guid ver_Id, Agency agency, string set_identifier, DateTime creationDate)
    //    {
    //        return new SetPassport
    //        {
    //            Agency_Id = agency.Id,
    //            Created = creationDate,
    //            
    //            Identifier = string.Format("{0}-{1}", agency.ITN, set_identifier),
    //            MethodicStandartVersion = "3.1",
    //            Provinance = "-",
    //            
    //            
    //            
    //            SetVersion_Id = ver_Id,
    //            
    //            
    //            Valid = null,
    //            Modified = creationDate
    //        };
    //    }

    //    protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
    //    {
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        

    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        

    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        

    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        

    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
              
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        

    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        

    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        

    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //    }

    //    protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
    //    {
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //    }

    //    protected override Set CreateSet(Guid cat_Id, params Agency[] agencies)
    //    {
    //        return new Set
    //        {
    //            Agencies = agencies,
    //            Category_Id = cat_id,
    //            Description = "Сведения о добыче",
    //            Title = "Сведения о добыче",
    //            UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
    //        };
    //    }

    //    protected override SetVersion CreateSetVersion(Guid set_Id, Guid dataStruct_Id, Guid? parentVer_Id, DateTime creationDate, int verId)
    //    {
    //        return new SetVersion
    //        {
    //            CodeName = creationDate.ToString("yyyyMMdd"),
    //            CreationDate = creationDate,
    //            DataStructure_Id = dataStruct_Id,
    //            IsAproved = true,
    //            IsCurrent = true,
    //            Name = string.Format("ver1{0:d3}", verId),
    //            ParentVersion_Id = parentVer_Id,
    //            Set_Id = set_Id
    //        };
    //    }
    //}
}
