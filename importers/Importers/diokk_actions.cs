﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class diokk_actions : ImporterBase
    {
        public diokk_actions(DataContext ctx)
            : base(ctx,
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.dep_imuszh],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.stroitelstvo],
                    CreationDate = DateTime.Now,
                    Description = "Наименование объекта проверки, местонахождение объекта проверки и (или) имущества, наименование органа проводившего проверку, результат проведения проверки",
                    SetIdentifier = "revisionresult",
                    Title = "Информация о результатах проверок, проведенных департаментом, подведомственными организациями в пределах их полномочий, а также о результатах проверок, проведенных в департаменте, подведомственных организациях",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Департамент имущественных отношений Краснодарского края",
                            Description = "Наименование объекта проверки, местонахождение объекта проверки и (или) имущества, наименование органа проводившего проверку, результат проведения проверки",
                            IsCurrent = true,
                            PublisherMBox = "kru@diok.ru",
                            PublisherName = "Г.Г. Пискова, Е.Е. Скотаренко, А.Г. Карасюк, Е.А. Трегубова, Д.Ю. Голуб",
                            PublisherPhone = "+7-861-2671729",
                            SourceId = Guid.Parse("58553926-AB2C-438A-A6B3-8196159BE6B6"),
                            Title = "Информация о результатах проверок, проведенных департаментом, подведомственными организациями в пределах их полномочий, а также о результатах проверок, проведенных в департаменте, подведомственных организациях",
                            Subject = "-;"
                            #endregion
                        }
                    }
                    #endregion
                }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();

            result.Add("diokk_actions",
                new TableDisplayTemplate
                {
                    Description = "Наименование объекта проверки, местонахождение объекта проверки и (или) имущества, наименование органа проводившего проверку, результат",
                    Title = "Результаты проверок",
                    Name = "diokk_actions",
                    Type = TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "NAME",
                                    DescriptionRu = "НАИМЕНОВАНИЕ ОБЪЕКТА ПРОВЕРКИ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ОБЪЕКТА ПРОВЕРКИ",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ADRES",
                                    DescriptionRu = "МЕСТОНАХОЖДЕНИЕ ОБЪЕКТА ПРОВЕРКИ И (ИЛИ) ИМУЩЕСТВА",
                                    DescritpionEn = "",
                                    Header = "МЕСТОНАХОЖДЕНИЕ ОБЪЕКТА ПРОВЕРКИ И (ИЛИ) ИМУЩЕСТВА",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ORGAN",
                                    DescriptionRu = "НАИМЕНОВАНИЕ ОРГАНА, ПРОВОДИВШЕГО ПРОВЕРКУ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ОРГАНА, ПРОВОДИВШЕГО ПРОВЕРКУ",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "RESULT",
                                    DescriptionRu = "РЕЗУЛЬТАТ ПРОВЕДЕНИЯ ПРОВЕРКИ",
                                    DescritpionEn = "",
                                    Header = "РЕЗУЛЬТАТ ПРОВЕДЕНИЯ ПРОВЕРКИ",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String,
                                }
                                #endregion
                            }
                });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["diokk_actions"],
                Name = "diokk_actions",
                Description = "Информация о результатах проверок, проведенных департаментом, подведомственными организациями в пределах их полномочий",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "diokk_actions"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return
                string.Format(
                    "INSERT INTO [dbo].[{0}] (NAME, ADRES, ORGAN, RESULT, SetsPassports_Id) " +
                    "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}')",
                    tableName, row["NAME"] ?? "null", row["ADRES"] ?? "null",
                    row["ORGAN"] ?? "null", row["RESULT"] ?? "null", passport_id);
        }
    }
}