﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm4_4 : ImporterBase
    {
        public HuntResourcesInfoForm4_4(DataContext ctx)
            : base(ctx,
                new ImportingSetSettings
                {
                    #region
                    AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                    CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                    CreationDate = DateTime.Now,
                    Description = "Документированная информация о добыче волка",
                    SetIdentifier = "gosohotreestr_volk2",
                    Title = "Сведения по добыче волка",
                    UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                    SetVersionSettings = new ImportingSetVersionSettings[]
                    {
                        new ImportingSetVersionSettings
                        {
                            #region
                            Creator = "Министерство природных ресурсов Краснодарского края",
                            Description = "Документированная информация о добыче волка по состоянию на 1 сентября  2014  г.",
                            Provinance = "Обновление данных",
                            PublisherMBox = "mprkk@krasnodar.ru",
                            PublisherName = "Белоусов Игорь Валерьевич",
                            PublisherPhone = "+7-861-2591971",
                            SourceId = Guid.Parse("D59ABD30-281E-4F7B-96B4-ECA246312402"),
                            Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                            Title = "Сведения по добыче волка в сезон добычи 2013 - 2014 гг.",
                            IsCurrent = true
                            #endregion
                        }
                    }
                    #endregion
                }) { }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var rsult = new Dictionary<string, Repository.Entities.TableDisplayTemplate>();

            rsult.Add("HuntResourcesInfoForm4_4",
                new Repository.Entities.TableDisplayTemplate
                {
                    Description = "Сведения по добыче волка в сезон добычи",
                    Name = "HuntResourcesInfoForm4_4",
                    Title = "Сведения по добыче волка в сезон добычи",
                    Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                    Columns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TerritoryNameF4_4",
                                    DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    DescritpionEn = "",
                                    Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "DateF4_4",
                                    DescriptionRu = "ДАТА ДОБЫЧИ",
                                    DescritpionEn = "",
                                    Header = "ДАТА ДОБЫЧИ",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "HuntMethodF4_4",
                                    DescriptionRu = "СПОСОБ ДОБЫЧИ",
                                    DescritpionEn = "",
                                    Header = "СПОСОБ ДОБЫЧИ",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },

                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalHuntedF4_4",
                                    DescriptionRu = "ВСЕГО ДОБЫТО",
                                    DescritpionEn = "",
                                    Header = "ВСЕГО ДОБЫТО",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.Integer
                                },

                                new ColumnDisplayTemplate
                                {
                                    Header = "ВЗРОСЛЫХ",
                                    Order = 4,
                                    SubColumns = new ColumnDisplayTemplate[]
                                    {
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdultF4_4_TotalF4_4",
                                            DescriptionRu = "ВЗРОСЛЫХ Всего",
                                            DescritpionEn = "",
                                            Header = "Всего",
                                            IncludeIntoFilter = true,
                                            Order = 5,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdultF4_4_MaleF4_4",
                                            DescriptionRu = "ВЗРОСЛЫХ самцов",
                                            DescritpionEn = "",
                                            Header = "самцов",
                                            IncludeIntoFilter = true,
                                            Order = 6,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "AdultF4_4_FemaleF4_4",
                                            DescriptionRu = "ВЗРОСЛЫХ самок",
                                            DescritpionEn = "",
                                            Header = "самок",
                                            IncludeIntoFilter = true,
                                            Order = 7,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        }
                                    }
                                },


                                new ColumnDisplayTemplate
                                {
                                    Header = "ДО 1 ГОДА",
                                    Order = 8,
                                    SubColumns = new ColumnDisplayTemplate[]
                                    {
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "YoungerOneYearF4_4_TotalF4_4",
                                            DescriptionRu = "ДО 1 ГОДА Всего",
                                            DescritpionEn = "",
                                            Header = "Всего",
                                            IncludeIntoFilter = true,
                                            Order = 9,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "YoungerOneYearF4_4_MaleF4_4",
                                            DescriptionRu = "ДО 1 ГОДА самцов",
                                            DescritpionEn = "",
                                            Header = "самцов",
                                            IncludeIntoFilter = true,
                                            Order = 10,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            BindedCollumn = "YoungerOneYearF4_4_FemaleF4_4",
                                            DescriptionRu = "ДО 1 ГОДА самок",
                                            DescritpionEn = "",
                                            Header = "самок",
                                            IncludeIntoFilter = true,
                                            Order = 11,
                                            Type = ColumnDisplayTemplate.ContentType.Integer
                                        }
                                    }
                                },
                                #endregion
                            }
                });

            return rsult;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm4_4"],
                Name = "HuntResourcesInfoForm4_4",
                Description = "Документированная информация о добыче волка",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm4_4"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (TerritoryNameF4_4, DateF4_4, HuntMethodF4_4, TotalHuntedF4_4, " +
                "AdultF4_4_TotalF4_4, AdultF4_4_MaleF4_4, AdultF4_4_FemaleF4_4, YoungerOneYearF4_4_TotalF4_4, " +
                "YoungerOneYearF4_4_MaleF4_4 ,YoungerOneYearF4_4_FemaleF4_4, SetsPassports_Id) VALUES('{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, {8}, {9}, {10}, '{11}')",
                tableName, row["TerritoryNameF4_4"] ?? null, row["DateF4_4"] ?? null, row["HuntMethodF4_4"] ?? null, row["TotalHuntedF4_4"] ?? null,
                row["AdultF4_4_TotalF4_4"] ?? null, row["AdultF4_4_MaleF4_4"] ?? null, row["AdultF4_4_FemaleF4_4"] ?? null, row["YoungerOneYearF4_4_TotalF4_4"] ?? null,
                row["YoungerOneYearF4_4_MaleF4_4"] ?? null, row["YoungerOneYearF4_4_FemaleF4_4"] ?? null, passport_id);
        }
    }

    //class HuntResourcesInfoForm4_4 : ImporterSkeleton
    //{

    //    public HuntResourcesInfoForm4_4(DataContext ctx) : base(ctx, 
    //        
    //        
    //        "gosohotreestr_wolf", DateTime.Now, Guid.Parse("D59ABD30-281E-4F7B-96B4-ECA246312402"))
    //    {
    //    }

    //    protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passportId)
    //    {
    //        return string.Format("INSERT INTO [dbo].[{0}] (TerritoryNameF4_4, DateF4_4, HuntMethodF4_4, TotalHuntedF4_4, " +
    //            "AdultF4_4_TotalF4_4, AdultF4_4_MaleF4_4, AdultF4_4_FemaleF4_4, YoungerOneYearF4_4_TotalF4_4, " +
    //            "YoungerOneYearF4_4_MaleF4_4 ,YoungerOneYearF4_4_FemaleF4_4, SetsPassports_Id) VALUES('{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, {8}, {9}, {10}, '{11}')",
    //            tableName, row["TerritoryNameF4_4"] ?? null, row["DateF4_4"] ?? null, row["HuntMethodF4_4"] ?? null, row["TotalHuntedF4_4"] ?? null,
    //            row["AdultF4_4_TotalF4_4"] ?? null, row["AdultF4_4_MaleF4_4"] ?? null, row["AdultF4_4_FemaleF4_4"] ?? null, row["YoungerOneYearF4_4_TotalF4_4"] ?? null,
    //            row["YoungerOneYearF4_4_MaleF4_4"] ?? null, row["YoungerOneYearF4_4_FemaleF4_4"] ?? null, passportId);
    //    }

    //    protected override Repository.Entities.SetPassport CreatePassport(Guid ver_Id, Repository.Entities.Agency agency, string set_identifier, DateTime creationDate)
    //    {
    //        return new Repository.Entities.SetPassport
    //        {
    //            Agency_Id = agency.Id,
    //            Created = creationDate,
    //            Creator = "Министерство природных ресурсов Краснодарского края,  управление по охране, федеральному государственному надзору и регулированию использования объектов животного мира и среды их обитания",
    //            Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
    //            Identifier = string.Format("{0}-{1}", agency.ITN, set_identifier),
    //            MethodicStandartVersion = "3.1",
    //            Provinance = "-",
    //            PublisherMBox = "mprkk@krasnodar.ru",
    //            PublisherName = "Белоусов Игорь Валерьевич",
    //            PublisherPhone = "+7-861-2591971",
    //            SetVersion_Id = ver_Id,
    //            Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
    //            Title = "Сведения по добыче волка в сезон добычи 2013 - 2014 гг.",
    //            Valid = null,
    //            Modified = creationDate
    //        };
    //    }

    //    protected override Dictionary<string, Repository.Entities.TableDisplayTemplate> CreateTableDisplayTemplate()
    //    {
    //       
    //    }

    //    protected override Repository.Entities.DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
    //    {
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //        
    //    }

    //    protected override Repository.Entities.Set CreateSet(Guid cat_Id, params Repository.Entities.Agency[] agencies)
    //    {
    //        return new Set
    //        {
    //            Agencies = agencies,
    //            Category_Id = cat_id,
    //            Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
    //            Title = "Сведения по добыче волка в сезон добычи 2013 - 2014 гг.",
    //            UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
    //        };
    //    }

    //    protected override Repository.Entities.SetVersion CreateSetVersion(Guid set_Id, Guid dataStruct_Id, Guid? parentVer_Id, DateTime creationDate, int verId)
    //    {
    //        return new SetVersion
    //        {
    //            CodeName = creationDate.ToString("yyyyMMdd"),
    //            CreationDate = creationDate,
    //            DataStructure_Id = dataStruct_Id,
    //            IsAproved = true,
    //            IsCurrent = true,
    //            Name = string.Format("ver1{0:d3}", verId),
    //            ParentVersion_Id = parentVer_Id,
    //            Set_Id = set_Id
    //        };
    //    }
    //}
}
