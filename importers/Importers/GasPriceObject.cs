﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class GasPriceObject : ImporterBase
    {
        public GasPriceObject(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.reg_energ_kom],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.promishlenost],
                CreationDate = new DateTime(2015, 4, 1),
                Description =
                    "\"Документированная информация о плодовитости копытных животных, отнесенных к охотничьим ресурсам\",",
                SetIdentifier = "liquidgasprice",
                Title =
                    "Розничные цены на сжиженный газ, реализуемый населению, на 01.04.2015",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Региональная энергетическая комиссия",
                        Description =
                            "Перечень цен на газ",
                            IsCurrent = true,
                        Provinance = "Обновление данных",
                        PublisherMBox = "not@given.ru",
                        PublisherName = "unknown",
                        PublisherPhone = "+7-861-1111111",
                        SourceId = Guid.Parse("6C5D4832-D0FA-4737-99ED-894A77610530"),
                        Subject = "Розничные цены на сжиженный газ, реализуемый населению",
                        Title =
                            "Розничные цены на сжиженный газ, реализуемый населению, на 01.04.2015"
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("GasPriceObject", new TableDisplayTemplate
            {
                Description =
                    "Перечень цен на газ",
                Name = "GasPriceObject",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "MAName",
                        DescriptionRu = "НАИМЕНОВАНИЕ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ КРАЯ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ КРАЯ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "AgencyName",
                        DescriptionRu =
                            "НАИМЕНОВАНИЕ ОКН ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ (СОГЛАСНО НОРМАТИВНО-ПРАВОВОМУ АКТУ ПОСТАНОВКИ НА ГОСУДАСРТВЕННУЮ ОХРАНУ)",
                        DescritpionEn = "",
                        Header =
                            "НАИМЕНОВАНИЕ ОКН ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ (СОГЛАСНО НОРМАТИВНО-ПРАВОВОМУ АКТУ ПОСТАНОВКИ НА ГОСУДАСРТВЕННУЮ ОХРАНУ)",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "BalonDelivery",
                        DescriptionRu = "В БАЛЛОНАХ С ДОСТАВКОЙ ДО ПОТРЕБИТ.",
                        DescritpionEn = "",
                        Header = "В БАЛЛОНАХ С ДОСТАВКОЙ ДО ПОТРЕБИТ.",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "BalonAfterDelivery",
                        DescriptionRu = "В БАЛЛОНАХ БЕЗ ДОСТАВКИ ДО ПОТРЕБ.",
                        DescritpionEn = "",
                        Header = "В БАЛЛОНАХ БЕЗ ДОСТАВКИ ДО ПОТРЕБ.",
                        IncludeIntoFilter = true,
                        Order = 3,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "GasGroup",
                        DescriptionRu = "ИЗ ГРУППОВЫХ ГАЗОВЫХ РЕЗЕРВУАРНЫХ УСТАНОВОК",
                        DescritpionEn = "",
                        Header = "ИЗ ГРУППОВЫХ ГАЗОВЫХ РЕЗЕРВУАРНЫХ УСТАНОВОК",
                        IncludeIntoFilter = true,
                        Order = 4,
                        Type = ColumnDisplayTemplate.ContentType.String
                    }

                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["GasPriceObject"],
                Name = "GasPriceObject",
                Description =
                    "Перечень цен на газ",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "GasPriceObject"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (MAName, AgencyName, " +
                                 "BalonDelivery, BalonAfterDelivery, " +
                                 "GasGroup, SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}')",
                                 tableName,
                                 row["MAName"], row["AgencyName"],
                                 row["BalonDelivery"], row["BalonAfterDelivery"],
                                 row["GasGroup"], passport_id);
        }
    }
}