﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoLand_1 : ImporterBase
    {
        public HuntResourcesInfoLand_1(DataContext ctx)
            : base(ctx, new ImportingSetSettings
             {
                 AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                 CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                 CreationDate = DateTime.Now,
                 Description = "Информация государственного лесного реестра о составе земель лесного фонда и земель иных категорий на которых расположены леса",
                 Title = "Состав земель лесного фонда и земель иных категорий, на которых расположены леса",
                 SetIdentifier = "gosohotreestr_sostav_zemel",
                 UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                 SetVersionSettings = new ImportingSetVersionSettings[]
                 {
                     new ImportingSetVersionSettings
                     {
                         Creator = "Министерство природных ресурсов Краснодарского края отдел государственного лесного реестра и государственной экспертизы проектов освоения лесов",
                         Description = "Информация государственного лесного реестра о составе земель лесного фонда и земель иных категорий на которых расположены леса",
                         IsCurrent = true,
                         PublisherMBox = "dlhkk@krasnodar.ru",
                         PublisherName = "Скворцов Александр Витальевич",
                         PublisherPhone = "+7-861-2980368",
                         Subject = "Государственный лесной реестр, состав земель лесного фонда, государственные реестры",
                         Title = "Состав земель лесного фонда и земель иных категорий, на которых расположены леса на 01.01. 2015 года",
                         SourceId = Guid.Parse("ddc763ec-7818-43fe-8b5c-087c33ebfda5")
                     }
                 }
             }) { }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoLand_1", new TableDisplayTemplate
            {
                Description = "Состав земель лесного фонда и земель иных категорий, на которых расположены леса",
                Name = "HuntResourcesInfoLand_1",
                Title = "Состав земель лесного фонда и земель иных категорий, на которых расположены леса",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                 {
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "LandTypeL_1",
                         DescriptionRu = "СОСТАВ ЗЕМЕЛЬ",
                         DescritpionEn = "",
                         Header = "СОСТАВ ЗЕМЕЛЬ",
                         IncludeIntoFilter = true,
                         Order = 0,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
                     new ColumnDisplayTemplate
                     {
                         BindedCollumn = "ForestPercentL_1",
                         DescriptionRu = "ЛЕСИСТОСТЬ ТЕРРИТОРИИ %",
                         DescritpionEn = "",
                         Header = "ЛЕСИСТОСТЬ ТЕРРИТОРИИ %",
                         IncludeIntoFilter = true,
                         Order = 1,
                         Type = ColumnDisplayTemplate.ContentType.String
                     },
                     new ColumnDisplayTemplate
                     {
                         Header = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА",
                         Order = 2,
                         InnerTable = new TableDisplayTemplate
                         {
                             Description = "Состав земель лесного фонда и земель иных категорий, на которых расположены леса Площадь",
                             Name = "HuntResourcesInfoLand_1_Square",
                             Title = "Состав земель лесного фонда и земель иных категорий, на которых расположены леса Площадь",
                             Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                             Columns = new ColumnDisplayTemplate[]
                             {
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "LandSquareL_1_TotalL_1",
                                     DescriptionRu = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА Всего",
                                     DescritpionEn = "",
                                     Header = "Всего",
                                     IncludeIntoFilter = true,
                                     Order = 3,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "LandSquareL_1_ProtectingL_1",
                                     DescriptionRu = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА По целевому назначению - защитные",
                                     DescritpionEn = "",
                                     Header = "По целевому назначению - защитные",
                                     IncludeIntoFilter = true,
                                     Order = 4,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "LandSquareL_1_InExpluatationL_1",
                                     DescriptionRu = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА По целевому назначению - эксплуатационные",
                                     DescritpionEn = "",
                                     Header = "По целевому назначению - эксплуатационные",
                                     IncludeIntoFilter = true,
                                     Order = 5,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "LandSquareL_1_InReserveL_1",
                                     DescriptionRu = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА По целевому назначению - резервные",
                                     DescritpionEn = "",
                                     Header = "По целевому назначению - резервные",
                                     IncludeIntoFilter = true,
                                     Order = 6,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "LandSquareL_1_ForestLandsL_1",
                                     DescriptionRu = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА Лесные земли",
                                     DescritpionEn = "",
                                     Header = "Лесные земли",
                                     IncludeIntoFilter = true,
                                     Order = 7,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "LandSquareL_1_ForestCovL_1",
                                     DescriptionRu = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА Покрытые лесной растительностью",
                                     DescritpionEn = "",
                                     Header = "Покрытые лесной растительностью",
                                     IncludeIntoFilter = true,
                                     Order = 8,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "LandSquareL_1_MorePineL_1",
                                     DescriptionRu = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА из них лесными насаждениями с преобладанием хвойных древесных пород",
                                     DescritpionEn = "",
                                     Header = "из них лесными насаждениями с преобладанием хвойных древесных пород",
                                     IncludeIntoFilter = true,
                                     Order = 9,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },
                                 new ColumnDisplayTemplate
                                 {
                                     BindedCollumn = "LandSquareL_1_MoreHardLeavedL_1",
                                     DescriptionRu = "ПЛОЩАДЬ ЗЕМЕЛЬ, НА КОТОРЫХ РАСПОЛОЖННЫ ЛЕСА, ГА из них лесными насаждениями с преобладанием твердолиственных древесных пород",
                                     DescritpionEn = "",
                                     Header = "из них лесными насаждениями с преобладанием твердолис-твенных древесных пород",
                                     IncludeIntoFilter = true,
                                     Order = 10,
                                     Type = ColumnDisplayTemplate.ContentType.Integer
                                 },

                             }
                         }
                     }
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoLand_1"],
                Name = "HuntResourcesInfoLand_1",
                Description = "Состав земель лесного фонда и земель иных категорий, на которых расположены леса",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoLand_1"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (LandTypeL_1, ForestPercentL_1, LandSquareL_1_TotalL_1,  LandSquareL_1_ProtectingL_1, " +
                "LandSquareL_1_InExpluatationL_1, LandSquareL_1_InReserveL_1, LandSquareL_1_ForestLandsL_1, LandSquareL_1_ForestCovL_1, " +
                "LandSquareL_1_MorePineL_1, LandSquareL_1_MoreHardLeavedL_1, SetsPassports_Id) VALUES ('{1}', '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, '{11}')",
                tableName, row["LandTypeL_1"], row["ForestPercentL_1"], row["LandSquareL_1_TotalL_1"] ?? "null", row["LandSquareL_1_ProtectingL_1"] ?? "null",
                row["LandSquareL_1_InExpluatationL_1"] ?? "null", row["LandSquareL_1_InReserveL_1"] ?? "null", row["LandSquareL_1_ForestLandsL_1"] ?? "null",
                row["LandSquareL_1_ForestCovL_1"] ?? "null", row["LandSquareL_1_MorePineL_1"] ?? "null", row["LandSquareL_1_MoreHardLeavedL_1"] ?? "null", passport_id);
        }
    }
}
