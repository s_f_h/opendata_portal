﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_4_5 : ImporterBase
    {

        public HuntResourcesInfoForm5_4_5(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now.AddYears(-2).AddMonths(-4),
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничьего хозяйства",
                SetIdentifier = "gosohotreestr_izyatiya",
                Title = "Сведения об изъятиях",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничьего хозяйства",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения об изъятиях",
                        SourceId = Guid.Parse("9BC2667E-696B-428B-A667-FA4D6C372E7B")
                    }
                }
            }) { }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm5_4_5",
            new TableDisplayTemplate
            {
                Title = "Сведения об изъятиях",
                Name = "HuntResourcesInfoForm5_4_5",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TerritoryNameF5_4_5",
                        DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String,
                    },

                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "PeltryF5_4_5",
                        DescriptionRu = "ПУШНИНЫ НА СУММУ, ТЫС. РУБ.",
                        DescritpionEn = "",
                        Header = "ПУШНИНЫ НА СУММУ, ТЫС. РУБ.",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.Real,
                    },

                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "MeatF5_4_5",
                        DescriptionRu = "МЯСА ДИКИХ ЖИВОТНЫХ НА СУММУ, ТЫС. РУБ.",
                        DescritpionEn = "",
                        Header = "МЯСА ДИКИХ ЖИВОТНЫХ НА СУММУ, ТЫС. РУБ.",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.Real,
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "ОРУДИЯ ОХОТЫ, ЕД.",
                        Order = 3,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения об изъятиях, орудия",
                            Name = "HuntResourcesInfoForm5_4_5_Items",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ToolsF5_4_5__TrapF5_4_5",
                                    DescriptionRu = "ОРУДИЯ ОХОТЫ, ЕД.	Капканы",
                                    DescritpionEn = "",
                                    Header = "Капканы",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ToolsF5_4_5__EquipmentF5_4_5",
                                    DescriptionRu = "ОРУДИЯ ОХОТЫ, ЕД.	приборы, оборудование, устройства	",
                                    DescritpionEn = "",
                                    Header = "приборы, оборудование, устройства",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ToolsF5_4_5__OtherF5_4_5",
                                    DescriptionRu = "ОРУДИЯ ОХОТЫ, ЕД.	иные",
                                    DescritpionEn = "",
                                    Header = "иные",
                                    IncludeIntoFilter = true,
                                    Order = 6,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                            }
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "ОРУЖИЯ, ЕД",
                        Order = 7,
                        InnerTable = new TableDisplayTemplate
                        {
                            Title = "Сведения об изъятиях, ОРУЖИЯ, ЕД",
                            Name = "HuntResourcesInfoForm5_4_5_Weapons",
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "GunsF5_4_5__TotalF5_4_5",
                                    DescriptionRu = "ОРУЖИЯ, ЕД Всего",
                                    DescritpionEn = "",
                                    Header = "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "GunsF5_4_5__FirearmsF5_4_5",
                                    DescriptionRu = "ОРУЖИЯ, ЕД Огнестрельного",
                                    DescritpionEn = "",
                                    Header = "Огнестрельного",
                                    IncludeIntoFilter = true,
                                    Order = 9,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "GunsF5_4_5__PneumaticF5_4_5",
                                    DescriptionRu = "ОРУЖИЯ, ЕД Пневматического",
                                    DescritpionEn = "",
                                    Header = "Пневматического",
                                    IncludeIntoFilter = true,
                                    Order = 10,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "GunsF5_4_5__ColdSteelF5_4_5",
                                    DescriptionRu = "ОРУЖИЯ, ЕД Холодного",
                                    DescritpionEn = "",
                                    Header = "Холодного",
                                    IncludeIntoFilter = true,
                                    Order = 11,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "GunsF5_4_5__ConfiscatedF5_4_5",
                                    DescriptionRu = "ОРУЖИЯ, ЕД Конфисковано по решению суда",
                                    DescritpionEn = "",
                                    Header = "Конфисковано по решению суда",
                                    IncludeIntoFilter = true,
                                    Order = 12,
                                    Type = ColumnDisplayTemplate.ContentType.Integer,
                                },
                            }
                        }
                    },
                }
            });

            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_4_5"],
                Name = "HuntResourcesInfoForm5_4_5",
                Description = "Сведения о выявленных административных правонарушениях и уголовных преступлениях",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_4_5"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (TerritoryNameF5_4_5, PeltryF5_4_5, MeatF5_4_5, " +
                "GunsF5_4_5__TotalF5_4_5, GunsF5_4_5__FirearmsF5_4_5, GunsF5_4_5__PneumaticF5_4_5, " +
                "GunsF5_4_5__ColdSteelF5_4_5, GunsF5_4_5__ConfiscatedF5_4_5, ToolsF5_4_5__TrapF5_4_5, " +
                "ToolsF5_4_5__EquipmentF5_4_5, ToolsF5_4_5__OtherF5_4_5, SetsPassports_Id) VALUES ('{1}', {2}, " +
                "{3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, '{12}')", tableName, row["TerritoryNameF5_4_5"],
                row["PeltryF5_4_5"], row["MeatF5_4_5"], row["GunsF5_4_5__TotalF5_4_5"], row["GunsF5_4_5__FirearmsF5_4_5"],
                row["GunsF5_4_5__PneumaticF5_4_5"], row["GunsF5_4_5__ColdSteelF5_4_5"], row["GunsF5_4_5__ConfiscatedF5_4_5"],
                row["ToolsF5_4_5__TrapF5_4_5"], row["ToolsF5_4_5__EquipmentF5_4_5"], row["ToolsF5_4_5__OtherF5_4_5"], passport_id);
        }
    }
}
