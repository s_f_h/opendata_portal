﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers
{
    internal class ForestResourcesForm1_8 : ImporterBase
    {
        public ForestResourcesForm1_8(DataContext ctx) :
            base(ctx, new ImportingSetSettings
            {
                #region
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = new DateTime(2015, 1, 1),
                Description =
                    "Информация государственного лесного реестра о составе земель лесного фонда и земель иных категорий на которых расположены леса",
                SetIdentifier = "goslesreestr",
                Title = "Характеристика лесничества (лесопарка)",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        #region
                        Creator = "Министерство природных ресурсов Краснодарского края",
                        Description =
                            "Информация государственного лесного реестра о составе земель лесного фонда и земель иных категорий на которых расположены леса",
                            IsCurrent = true,
                        Provinance = "Обновление данных",
                        PublisherMBox = "dlhkk@krasnodar.ru",
                        PublisherName =
                            "Скворцов Александр Витальевич ведущий консультант отдела государственного лесного реестра и государственной экспертизы проектов освоения лесов министерства природных ресурсов Краснодарского края",
                        PublisherPhone = "+7-861-2980368",
                        SourceId = Guid.Parse("063B42B6-B94A-4C63-AF00-F8790160862B"),
                        Subject = "Государственный лесной реестр, состав земель лесного фонда, государственные реестры",
                        Title =
                            "Распределение площади лесов и запасов древесины по преобладающим породам и группам возраста"
                        #endregion
                    }
                }
                #endregion
            })
        {
            asIs = true;
        }

        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("ForestResourcesForm1_8", new TableDisplayTemplate
            {
                Description =
                    "Распределение площади лесов и запасов древесины по преобладающим породам и группам возраста",
                Name = "ForestResourcesForm1_8",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "PrevailingKindF1_8",
                        DescriptionRu = "ПРЕОБЛАДАЮЩИЕ ДРЕВЕСНЫЕ И КУСТАРНИКОВЫЕ ПОРОДЫ",
                        DescritpionEn = "",
                        Header = "ПРЕОБЛАДАЮЩИЕ ДРЕВЕСНЫЕ И КУСТАРНИКОВЫЕ ПОРОДЫ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "MinLimitF1_8",
                        DescriptionRu = "НИЖНИЙ ПРЕДЕЛ ВОЗРАСТА РУБКИ (СПЕЛОСТИ)",
                        DescritpionEn = "",
                        Header = "НИЖНИЙ ПРЕДЕЛ ВОЗРАСТА РУБКИ (СПЕЛОСТИ)",
                        IncludeIntoFilter = true,
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ЗЕМЛИ, ПОКРЫТЫЕ ЛЕСНОЙ РАСТИТЕЛЬНОСТЬЮ",
                        Order = 2,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Земли, покрытые лесной растительностью",
                            Name = "ForestLandsF1_8",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ForestLandsF1_8_TotalF1_8",
                                    DescriptionRu = "земли, Всего",
                                    DescritpionEn = "",
                                    Header = "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ForestLandsF1_8_YoungFirstClassF1_8",
                                    DescriptionRu = "земли, молодняки 1 класса",
                                    DescritpionEn = "",
                                    Header = "молодняки 1 класса",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ForestLandsF1_8_YoungSecondClassF1_8",
                                    DescriptionRu = "земли, молодняки 2 класса",
                                    DescritpionEn = "",
                                    Header = "молодняки 2 класса",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ForestLandsF1_8_MiddleTotalF1_8",
                                    DescriptionRu = "земли, средневозрастные всего",
                                    DescritpionEn = "",
                                    Header = "средневозрастные всего",
                                    IncludeIntoFilter = true,
                                    Order = 6,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ForestLandsF1_8_MiddleIncludedF1_8",
                                    DescriptionRu = "земли, средневозрастные включен-ные в расчет главного пользования",
                                    DescritpionEn = "",
                                    Header = "средневозрастные включен-ные в расчет главного пользования",
                                    IncludeIntoFilter = true,
                                    Order = 7,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ForestLandsF1_8_NotOldYetF1_8",
                                    DescriptionRu = "земли, приспевающие",
                                    DescritpionEn = "",
                                    Header = "приспевающие",
                                    IncludeIntoFilter = true,
                                    Order = 8,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ForestLandsF1_8_OldTotalF1_8",
                                    DescriptionRu = "земли, спелые и престойные всего",
                                    DescritpionEn = "",
                                    Header = "спелые и престойные всего",
                                    IncludeIntoFilter = true,
                                    Order = 9,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "ForestLandsF1_8_OldOverStandF1_8",
                                    DescriptionRu = "земли, перестойные",
                                    DescritpionEn = "",
                                    Header = "перестойные",
                                    IncludeIntoFilter = true,
                                    Order = 10,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        Header = "ОБЩИЙ ЗАПАС НАСАЖДЕНИЙ",
                        Order = 11,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Общий запас насаждений",
                            Name = "TotalAmountF1_8",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalAmountF1_8_TotalF1_8",
                                    DescriptionRu = "Всего",
                                    DescritpionEn = "",
                                    Header = "Всего",
                                    IncludeIntoFilter = true,
                                    Order = 12,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalAmountF1_8_YoungFirstClassF1_8",
                                    DescriptionRu = "молодняки 1 класса",
                                    DescritpionEn = "",
                                    Header = "молодняки 1 класса",
                                    IncludeIntoFilter = true,
                                    Order = 13,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalAmountF1_8_YoungSecondClassF1_8",
                                    DescriptionRu = "молодняки 2 класса",
                                    DescritpionEn = "",
                                    Header = "молодняки 2 класса",
                                    IncludeIntoFilter = true,
                                    Order = 14,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalAmountF1_8_MiddleTotalF1_8",
                                    DescriptionRu = "средневозрастные всего ",
                                    DescritpionEn = "",
                                    Header = "средневозрастные всего",
                                    IncludeIntoFilter = true,
                                    Order = 15,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalAmountF1_8_MiddleIncludedF1_8",
                                    DescriptionRu = "средневозрастные включен-ные в расчет главного пользования",
                                    DescritpionEn = "",
                                    Header = "средневозрастные включен-ные в расчет главного пользования",
                                    IncludeIntoFilter = true,
                                    Order = 16,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalAmountF1_8_NotOldYetF1_8",
                                    DescriptionRu = "приспевающие",
                                    DescritpionEn = "",
                                    Header = "приспевающие",
                                    IncludeIntoFilter = true,
                                    Order = 17,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalAmountF1_8_OldTotalF1_8",
                                    DescriptionRu = "спелые и престойные всего",
                                    DescritpionEn = "",
                                    Header = "спелые и престойные всего",
                                    IncludeIntoFilter = true,
                                    Order = 18,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "TotalAmountF1_8_OldOverStandF1_8",
                                    DescriptionRu = "перестойные",
                                    DescritpionEn = "",
                                    Header = "перестойные",
                                    IncludeIntoFilter = true,
                                    Order = 19,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                }
                            }
                        }
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TotalAverageGrowthF1_8",
                        DescriptionRu = "ОБЩИЙ СРЕДНИЙ ПРИРОСТ НАСАЖДЕНИЙ",
                        DescritpionEn = "",
                        Header = "ОБЩИЙ СРЕДНИЙ ПРИРОСТ НАСАЖДЕНИЙ",
                        IncludeIntoFilter = true,
                        Order = 20,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "AverageAgeF1_8",
                        DescriptionRu = "СРЕДНИЙ ВОЗРАСТ НАСАЖДЕНИЙ, ЛЕТ",
                        DescritpionEn = "",
                        Header = "СРЕДНИЙ ВОЗРАСТ НАСАЖДЕНИЙ, ЛЕТ",
                        IncludeIntoFilter = true,
                        Order = 21,
                        Type = ColumnDisplayTemplate.ContentType.String
                    }
                    #endregion
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["ForestResourcesForm1_8"],
                Name = "ForestResourcesForm1_8",
                Description = "Распределение площади лесов и запасов древесины по преобладающим породам и группам возраста",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "ForestResourcesForm1_8"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (PrevailingKindF1_8, MinLimitF1_8, " +
                                 "ForestLandsF1_8_TotalF1_8, ForestLandsF1_8_YoungFirstClassF1_8, " +
                                 "ForestLandsF1_8_YoungSecondClassF1_8, ForestLandsF1_8_MiddleTotalF1_8, " +
                                 "ForestLandsF1_8_MiddleIncludedF1_8, ForestLandsF1_8_NotOldYetF1_8, " +
                                 "ForestLandsF1_8_OldTotalF1_8, ForestLandsF1_8_OldOverStandF1_8, " +
                                 "TotalAmountF1_8_TotalF1_8, TotalAmountF1_8_YoungFirstClassF1_8, " +
                                 "TotalAmountF1_8_YoungSecondClassF1_8, TotalAmountF1_8_MiddleTotalF1_8, " +
                                 "TotalAmountF1_8_MiddleIncludedF1_8, TotalAmountF1_8_NotOldYetF1_8, " +
                                 "TotalAmountF1_8_OldTotalF1_8, TotalAmountF1_8_OldOverStandF1_8, " +
                                 "TotalAverageGrowthF1_8, AverageAgeF1_8, SetsPassports_Id) " +
                                 "VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}','{7}', '{8}', '{9}', '{10}', '{11}', '{12}','{13}', '{14}', " +
                                 "'{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}')",
                                 tableName, 
                                 row["PrevailingKindF1_8"], row["MinLimitF1_8"],
                                 row["ForestLandsF1_8_TotalF1_8"], row["ForestLandsF1_8_YoungFirstClassF1_8"],
                                 row["ForestLandsF1_8_YoungSecondClassF1_8"], row["ForestLandsF1_8_MiddleTotalF1_8"],
                                 row["ForestLandsF1_8_MiddleIncludedF1_8"], row["ForestLandsF1_8_NotOldYetF1_8"],
                                 row["ForestLandsF1_8_OldTotalF1_8"], row["ForestLandsF1_8_OldOverStandF1_8"],
                                 row["TotalAmountF1_8_TotalF1_8"], row["TotalAmountF1_8_YoungFirstClassF1_8"],
                                 row["TotalAmountF1_8_YoungSecondClassF1_8"], row["TotalAmountF1_8_MiddleTotalF1_8"],
                                 row["TotalAmountF1_8_MiddleIncludedF1_8"], row["TotalAmountF1_8_NotOldYetF1_8"],
                                 row["TotalAmountF1_8_OldTotalF1_8"], row["TotalAmountF1_8_OldOverStandF1_8"],
                                 row["TotalAverageGrowthF1_8"], row["AverageAgeF1_8"], passport_id);
        }
    }
}