﻿using importers.ImporterParametrs;
using Repository;
using Repository.Entities;
using System;
using System.Collections.Generic;

namespace importers.Importers
{
    internal class HuntResourcesInfoForm5_4 : ImporterBase
    {
        public HuntResourcesInfoForm5_4(DataContext ctx)
            : base(ctx, new ImportingSetSettings
            {
                AgencyId = AgencyAndCategory.agencise[AgencyAndCategory.Agencies.min_prir],
                CategoryId = AgencyAndCategory.categorise[AgencyAndCategory.Categories.priroda],
                CreationDate = DateTime.Now,
                Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                SetIdentifier = "gosohotreestr_narush",
                Title = "Сведения о категориях нарушителей",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand,
                SetVersionSettings = new ImportingSetVersionSettings[]
                {
                    new ImportingSetVersionSettings
                    {
                        Creator = "Министерство природных ресурсов Краснодарского",
                        Description = "Систематизированный свод документированной информации об охотничьих ресурсах, об их использовании и сохранении, об охотничьих угодьях, об охотниках, о юридических лицах и об индивидуальных предпринимателях, осуществляющих виды деятельности в сфере охотничь",
                        IsCurrent = true,
                        PublisherMBox = "mprkk@krasnodar.ru",
                        PublisherName = "Белоусов Игорь Валерьевич",
                        PublisherPhone = "+7-861-2591971",
                        Subject = "охотничьи ресурсы, добыча, регулирование численности, использование",
                        Title = "Сведения о категориях нарушителей",
                        SourceId = Guid.Parse("b5f73a07-1871-46c4-9fbc-965a1c7b0a11")
                    }
                }
            }) { asIs = false; }
        protected override Dictionary<string, TableDisplayTemplate> CreateTableDisplayTemplate()
        {
            var result = new Dictionary<string, TableDisplayTemplate>();
            result.Add("HuntResourcesInfoForm5_4", new TableDisplayTemplate
            {
                Description = "Сведения о категориях нарушителей",
                Name = "HuntResourcesInfoForm5_4",
                Title = "Сведения о категориях нарушителей",
                Type = Repository.Entities.TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "TerritoryNameF5_4_2",
                        DescriptionRu = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                        DescritpionEn = "",
                        Header = "НАИМЕНОВАНИЕ ОХОТНИЧЬИХ УГОДИЙ ИЛИ ИНЫХ ТЕРРИТОРИЙ",
                        IncludeIntoFilter = true,
                        Order = 0,
                        Type = ColumnDisplayTemplate.ContentType.String
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "КОЛИЧЕЧТВО, ЧЕЛ",
                        Order = 1,
                        InnerTable = new TableDisplayTemplate
                        {
                            Description = "Сведения о категориях нарушителей КОЛИЧЕЧТВО, ЧЕЛ",
                            Name = "HuntResourcesInfoForm5_4_Count",
                            Title = "КОЛИЧЕЧТВО, ЧЕЛ",
                            Type = Repository.Entities.TableDisplayTemplate.TemplateType.InnerTable,
                            Columns = new ColumnDisplayTemplate[]
                            {
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "QuantityF5_4_2_WithHunterLicenseF5_4_2",
                                    DescriptionRu = "КОЛИЧЕЧТВО, ЧЕЛ Лица с охотничьим билетом",
                                    DescritpionEn = "",
                                    Header = "Лица с охотничьим билетом",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    Type = ColumnDisplayTemplate.ContentType.Integer
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "QuantityF5_4_2_WithMemberHunterLicenseF5_4_2",
                                    DescriptionRu = "КОЛИЧЕЧТВО, ЧЕЛ Лица с членским охотничьим билетом",
                                    DescritpionEn = "",
                                    Header = "Лица с членским охотничьим билетом",
                                    IncludeIntoFilter = true,
                                    Order = 3,
                                    Type = ColumnDisplayTemplate.ContentType.Integer
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "QuantityF5_4_2_WithoutHunterLicenseF5_4_2",
                                    DescriptionRu = "КОЛИЧЕЧТВО, ЧЕЛ Лица без удостоверения на право охоты",
                                    DescritpionEn = "",
                                    Header = "Лица без удостоверения на право охоты",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    Type = ColumnDisplayTemplate.ContentType.Integer
                                },
                                new ColumnDisplayTemplate
                                {
                                    BindedCollumn = "QuantityF5_4_2_StaffF5_4_2",
                                    DescriptionRu = "КОЛИЧЕЧТВО, ЧЕЛ Штатные работники юридических лиц или индивидуальных предпринимателей",
                                    DescritpionEn = "",
                                    Header = "Штатные работники юридических лиц или индивидуальных предпринимателей",
                                    IncludeIntoFilter = true,
                                    Order = 5,
                                    Type = ColumnDisplayTemplate.ContentType.Integer
                                },
                            }
                        }
                    }
                }
            });
            return result;
        }

        protected override DataStructure CreateDataStructure(Dictionary<string, Guid> tableDisplayTemplates)
        {
            return new DataStructure
            {
                Template_Id = tableDisplayTemplates["HuntResourcesInfoForm5_4"],
                Name = "HuntResourcesInfoForm5_4",
                Description = "Сведения о категориях нарушителей",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "HuntResourcesInfoForm5_4"
            };
        }

        protected override string GenerateInsertQuery(string tableName, Dictionary<string, object> row, Guid passport_id)
        {
            return string.Format("INSERT INTO [dbo].[{0}] (TerritoryNameF5_4_2, QuantityF5_4_2_WithHunterLicenseF5_4_2, " +
                "QuantityF5_4_2_WithMemberHunterLicenseF5_4_2, QuantityF5_4_2_WithoutHunterLicenseF5_4_2, QuantityF5_4_2_StaffF5_4_2, SetsPassports_Id) " +
                "VALUES ('{1}', {2}, {3}, {4}, {5}, '{6}')", tableName, row["TerritoryNameF5_4_2"], row["QuantityF5_4_2_WithHunterLicenseF5_4_2"],
                row["QuantityF5_4_2_WithMemberHunterLicenseF5_4_2"], row["QuantityF5_4_2_WithoutHunterLicenseF5_4_2"], row["QuantityF5_4_2_StaffF5_4_2"], passport_id);
        }
    }
}
