﻿
namespace Repository.Misc
{
    /// <summary>
    /// Форматы файлов выгружаемых данных
    /// </summary>
    public enum Formats
    {
        csv,
        xml,
        xsd
    }
}
