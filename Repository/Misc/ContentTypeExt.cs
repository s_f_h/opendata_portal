﻿using Repository.Entities;
using System;

namespace Repository.Misc
{
    public static class ContentTypeExt
    {
        /// <summary>
        /// Преобразует системный тип данных ячеек колонок в тип данных в БД
        /// </summary>
        /// <param name="contentType">Тип данных</param>
        /// <returns>Представление типа в БД</returns>
        public static string SQLType(this ColumnDisplayTemplate.ContentType contentType)
        {
            switch (contentType)
            {
                case ColumnDisplayTemplate.ContentType.Audio_Bin:
                case ColumnDisplayTemplate.ContentType.File_Bin:
                case ColumnDisplayTemplate.ContentType.Image_Bin:
                case ColumnDisplayTemplate.ContentType.Video_Bin:
                    return "nvarchar(50)"; // hash sum of file plus extension

                case ColumnDisplayTemplate.ContentType.Audio_Url:
                case ColumnDisplayTemplate.ContentType.File_Url:
                case ColumnDisplayTemplate.ContentType.Image_Url:
                case ColumnDisplayTemplate.ContentType.Video_Url:
                case ColumnDisplayTemplate.ContentType.PostAddress:
                case ColumnDisplayTemplate.ContentType.String:
                case ColumnDisplayTemplate.ContentType.Url:
                    return "nvarchar(max)";

                case ColumnDisplayTemplate.ContentType.Date:
                    return "date";
                case ColumnDisplayTemplate.ContentType.DateTime:
                    return "datetime2";
                case ColumnDisplayTemplate.ContentType.Decimal:
                    return "money";
                case ColumnDisplayTemplate.ContentType.EMail:
                    return "nvarchar(256)";
                case ColumnDisplayTemplate.ContentType.Integer:
                    return "bigint";
                case ColumnDisplayTemplate.ContentType.Real:
                    return "real";
                case ColumnDisplayTemplate.ContentType.Text:
                    return "ntext";
                case ColumnDisplayTemplate.ContentType.Time:
                    return "time";
                case ColumnDisplayTemplate.ContentType.Boolean:
                    return "bit";
                default:
                    return "nvarchar(MAX)";
            }
        }

        public static string CSVType(this ColumnDisplayTemplate.ContentType contentType)
        {
            switch (contentType)
            {
                case ColumnDisplayTemplate.ContentType.Audio_Bin:
                case ColumnDisplayTemplate.ContentType.Video_Bin:
                case ColumnDisplayTemplate.ContentType.Image_Bin:
                    return "base64";
                case ColumnDisplayTemplate.ContentType.Audio_Url:
                case ColumnDisplayTemplate.ContentType.Video_Url:
                case ColumnDisplayTemplate.ContentType.Image_Url:
                case ColumnDisplayTemplate.ContentType.Url:
                case ColumnDisplayTemplate.ContentType.EMail:
                    return "url";
                case ColumnDisplayTemplate.ContentType.Boolean:
                    return "boolean";
                case ColumnDisplayTemplate.ContentType.Date:
                    return "date";
                case ColumnDisplayTemplate.ContentType.DateTime:
                    return "datetime";
                case ColumnDisplayTemplate.ContentType.Decimal:
                    return "decimal";
                case ColumnDisplayTemplate.ContentType.Integer:
                    return "integer";
                case ColumnDisplayTemplate.ContentType.Real:
                    return "real";
                case ColumnDisplayTemplate.ContentType.Time:
                    return "time";
                case ColumnDisplayTemplate.ContentType.String:
                case ColumnDisplayTemplate.ContentType.Text:
                case ColumnDisplayTemplate.ContentType.PostAddress:
                default:
                    return "string";
            }
        }

        public static string CSVValue(this ColumnDisplayTemplate.ContentType type, object value, string urlPrefix = null)
        {
            switch(type){
                case ColumnDisplayTemplate.ContentType.Audio_Bin:
                case ColumnDisplayTemplate.ContentType.File_Bin:
                case ColumnDisplayTemplate.ContentType.Image_Bin:
                case ColumnDisplayTemplate.ContentType.Video_Bin:
                    return string.Format("{0}/bin/{1}", urlPrefix, value);

                case ColumnDisplayTemplate.ContentType.Integer:
                case ColumnDisplayTemplate.ContentType.Audio_Url:
                case ColumnDisplayTemplate.ContentType.File_Url:
                case ColumnDisplayTemplate.ContentType.Image_Url:
                case ColumnDisplayTemplate.ContentType.Url:
                case ColumnDisplayTemplate.ContentType.Video_Url:
                case ColumnDisplayTemplate.ContentType.EMail:
                case ColumnDisplayTemplate.ContentType.String:
                case ColumnDisplayTemplate.ContentType.Text:
                case ColumnDisplayTemplate.ContentType.PostAddress:
                    return value.ToString();

                case ColumnDisplayTemplate.ContentType.Boolean:
                    return (bool)value ? "true" : "false";

                case ColumnDisplayTemplate.ContentType.Date:                //--
                    return string.Format("{0:yyyyMMdd}", value);            // |
                                                                            // \
                case ColumnDisplayTemplate.ContentType.DateTime:            //  >  ISO 8061 DateTime format
                    return string.Format("{0:yyyyMMddhhmmss}", value);      // /
                                                                            // |
                case ColumnDisplayTemplate.ContentType.Time:                //--
                    return string.Format("{0:hhmmss}", value);              //

                case ColumnDisplayTemplate.ContentType.Real:
                case ColumnDisplayTemplate.ContentType.Decimal:
                    return value.ToString().Replace('.', ',');
            }

            return null;
        }

        public static string XMLType(this ColumnDisplayTemplate.ContentType type, string _namespace)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Возвращает часть SQL запроса для указанного типа и значения
        /// </summary>
        /// <param name="type">Тип данных</param>
        /// <param name="field">Поле запроса</param>
        /// <param name="value">Значение поля</param>
        /// <returns>SQL запрос</returns>
        public static string CreateSqlQueryFilterItem(this ColumnDisplayTemplate.ContentType type, string field, string value)
        {
            switch (type)
            {
                case ColumnDisplayTemplate.ContentType.Url:
                case ColumnDisplayTemplate.ContentType.Text:
                case ColumnDisplayTemplate.ContentType.String:
                case ColumnDisplayTemplate.ContentType.PostAddress:
                case ColumnDisplayTemplate.ContentType.EMail:
                    return string.Format("{0} = '{1}'", field, value);
                case ColumnDisplayTemplate.ContentType.Boolean:
                    return string.Format("{0} = {1}", field, SqlFilterQueryBoolVariant(value));
                case ColumnDisplayTemplate.ContentType.Decimal:
                case ColumnDisplayTemplate.ContentType.Integer:
                case ColumnDisplayTemplate.ContentType.Real:
                    return string.Format("{0} = {1}", field, value.Replace(',', '.'));
                case ColumnDisplayTemplate.ContentType.Date:
                case ColumnDisplayTemplate.ContentType.DateTime:
                case ColumnDisplayTemplate.ContentType.Time:
                    return string.Format("{0} = '{1}'", field, value);
                default:
                    return null;
            }
        }

        static int SqlFilterQueryBoolVariant(string variant)
        {
            switch (variant.ToLowerInvariant())
            {
                case "on":
                case "true":
                    return 1;
            }

            return 0;
        }
    }
}
