﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Misc
{
    internal static class SqlCommandGenerator
    {
        internal static class TableNames
        {
            internal const string SetPassport = "SetsPassports";
            internal const string SetVersion = "SetVersions";
            internal const string Agency = "Agencies";
        }

        const string POIFielsd = @"POI_Latitude, POI_Longitude, POI_MarkerName";

        /// <summary>
        /// Возвращает SQL комманду создающую в БД новую таблицу
        /// </summary>
        /// <param name="tableName">Наименование таблицы</param>
        /// <param name="hasPOI">Добавить раздел гео-данных</param>
        /// <param name="dataColumns">Список колонок таблицы; <Key> - наименование колонки в БД, <Value> - тип данных</param>
        /// <param name="referencedTables">Список таблиц внешних таблиц; <Key> - наименование внешней таблицы, <Value> - тип данных внешнего ключа</param>
        /// <returns>SQL комманда</returns>
        internal static string CreateNewPlainTable(string tableName, bool hasPOI,
            Dictionary<string, string> dataColumns, Dictionary<string, string> referencedTables)
        {
            if (dataColumns == null || dataColumns.Count == 0)
                throw new ArgumentException("Parametr can't be null or empty.", "columns");

            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentException("Parametr can't be null or empty.", "tableName");

            StringBuilder cmd = new StringBuilder();

            if (referencedTables != null)
                foreach (var referencedTable in referencedTables)
                    cmd.AppendFormat("ALTER TABLE [dbo].[{0}] SET (LOCK_ESCALATION = TABLE);{1}", referencedTable.Key, Environment.NewLine);

            cmd.AppendFormat(@"CREATE TABLE [dbo].[{0}] ([Id] uniqueidentifier NOT NULL DEFAULT(NEWID()), ", tableName);

            foreach (var column in dataColumns)
                cmd.AppendFormat("[{0}] {1} NULL, ", column.Key, column.Value);

            if (hasPOI)
                cmd.Append(@"[POI_Latitude] float NOT NULL, [POI_Longitude] float NOT NULL, [POI_MarkerName] nvarchar(20) NOT NULL, ");

            // внешние ключи на ссылаемые таблицы
            if (referencedTables != null)
                for (int i = 0; i < referencedTables.Count; ++i)
                {
                    var referencedTable = referencedTables.ElementAt(i);
                    cmd.AppendFormat("[{0}_Id] {1} NOT NULL{2}", referencedTable.Key,
                        referencedTable.Value, i != referencedTables.Count - 1 ? ", " : "");
                }

            cmd.AppendFormat(");{0}", Environment.NewLine);

            // первичный ключ для таблицы tableName
            cmd.AppendFormat(@"ALTER TABLE [dbo].[{0}] ADD CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED (Id) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON);{1}", tableName, Environment.NewLine);

            // связи с внешними таблицами
            if (referencedTables != null)
                foreach (var referencedTable in referencedTables)
                    cmd.AppendFormat("ALTER TABLE [dbo].[{0}] ADD CONSTRAINT [FK_{0}_{1}] FOREIGN KEY ([{1}_Id]) REFERENCES [dbo].[{1}] ([Id]) ON UPDATE  NO ACTION ON DELETE CASCADE;{2}", tableName, referencedTable.Key, Environment.NewLine);

            return cmd.ToString();
        }


        /// <summary>
        /// Возвращает SQL запрос на данные версии набора из главной таблицы
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="versionId">Идентефикатор версии</param>
        /// <param name="filterQuery">Дополнительный фильтр данных</param>
        /// <returns>Строка с запросом</returns>
        internal static string GetTableDataQuery(string tableName, Guid versionId, string filterQuery = null)
        {
            if (string.IsNullOrWhiteSpace(filterQuery))
                return string.Format("SELECT * FROM [dbo].[{0}] WHERE [SetsPassports_Id] IN " +
                    "(SELECT [Id] FROM [dbo].[SetsPassports] WHERE [SetVersion_Id] = '{1}')",
                    tableName, versionId);
            else
                return string.Format("SELECT * FROM [dbo].[{0}] WHERE {2} AND [SetsPassports_Id] IN " +
                "(SELECT [Id] FROM [dbo].[SetsPassports] WHERE [SetVersion_Id] = '{1}')",
                tableName, versionId, filterQuery);

        }

        /// <summary>
        /// Возвращает SQL запрос на данные версии набора из главной таблицы для определенного поставщика
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="versionId">Идентефикатор версии</param>
        /// <param name="passportId">Идентефикатор набора в пасспорте поставщика</param>
        /// <param name="filterQuery">Дополнительный фильтр данных</param>
        /// <returns>Строка с запросом</returns>
        internal static string GetTableDataQuery(string tableName, Guid versionId, string passportId, string filterQuery = null)
        {
            if (string.IsNullOrWhiteSpace(filterQuery))
                return string.Format("SELECT * FROM [dbo].[{0}] WHERE [SetsPassports_Id] IN " +
                    "(SELECT [Id] FROM [dbo].[SetsPassports] WHERE [SetVersion_Id] = '{1}' " +
                    "AND [Identifier] = '{2}')", tableName, versionId, passportId);
            else
                return string.Format("SELECT * FROM [dbo].[{0}] WHERE {3} AND [SetsPassports_Id] IN " +
                "(SELECT [Id] FROM [dbo].[SetsPassports] WHERE [SetVersion_Id] = '{1}' " +
                "AND [Identifier] = '{2}')", tableName, versionId, passportId, filterQuery);
        }

        /// <summary>
        /// Возвращает SQL запрос на данные версии набора из главной таблицы
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="versionId">Идентефикатор версии</param>
        /// <param name="from">Смещение от начала</param>
        /// <param name="count">Количество записей</param>
        /// <param name="filterQuery">Дополнительный фильтр данных</param>
        /// <returns>Строка с запросом</returns>
        internal static string GetTableDataQuery(string tableName, Guid versionId, int from, int count, string filterQuery = null)
        {
            if (string.IsNullOrWhiteSpace(filterQuery))
                return string.Format(";WITH [NumberedTable] AS (SELECT *, ROW_NUMBER() OVER (ORDER BY Id) " +
                    "AS RowNumber FROM [dbo].[{0}] WHERE [SetsPassports_Id] IN (SELECT [Id] FROM [dbo].[SetsPassports]" +
                    " WHERE [SetVersion_Id] = '{1}')) SELECT TOP {3} * FROM NumberedTable WHERE RowNumber >= {2};",
                    tableName, versionId, from, count);
            else
                return string.Format(";WITH [NumberedTable] AS (SELECT *, ROW_NUMBER() OVER (ORDER BY Id) " +
                "AS RowNumber FROM [dbo].[{0}] WHERE {4} AND [SetsPassports_Id] IN (SELECT [Id] FROM [dbo].[SetsPassports]" +
                " WHERE [SetVersion_Id] = '{1}')) SELECT TOP {3} * FROM NumberedTable WHERE RowNumber >= {2};",
                tableName, versionId, from, count, filterQuery);
        }

        /// <summary>
        /// Возвращает SQL запрос на данные версии набора из главной таблицы для определенного поставщика
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="versionId">Идентефикатор версии</param>
        /// <param name="passportId">Идентефикатор набора в пасспорте поставщика</param>
        /// <param name="from">Смещение от начала</param>
        /// <param name="count">Количество записей</param>
        /// <param name="filterQuery">Дополнительный фильтр данных</param>
        /// <returns>Строка с запросом</returns>
        internal static string GetTableDataQuery(string tableName, Guid versionId, string passportId, int from, int count, string filterQuery = null)
        {
            if (string.IsNullOrWhiteSpace(filterQuery))
                return string.Format(";WITH [NumberedTable] AS (SELECT *, ROW_NUMBER() OVER (ORDER BY Id) " +
                    "AS RowNumber FROM [dbo].[{0}] WHERE [SetsPassports_Id] IN (SELECT [Id] FROM [dbo].[SetsPassports]" +
                    " WHERE [SetVersion_Id] = '{1}' AND [Identifier] = '{4}')) SELECT TOP {3} * FROM NumberedTable WHERE RowNumber >= {2};",
                    tableName, versionId, from, count, passportId);
            else
                return string.Format(";WITH [NumberedTable] AS (SELECT *, ROW_NUMBER() OVER (ORDER BY Id) " +
                "AS RowNumber FROM [dbo].[{0}] WHERE {5} AND [SetsPassports_Id] IN (SELECT [Id] FROM [dbo].[SetsPassports]" +
                " WHERE [SetVersion_Id] = '{1}' AND [Identifier] = '{4}')) SELECT TOP {3} * FROM NumberedTable WHERE RowNumber >= {2};",
                tableName, versionId, from, count, passportId, filterQuery);
        }

        /// <summary>
        /// Возвращает SQL запрос на количество данных версии набора
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="versionId">Идентефикатор версии</param>
        /// <param name="filterQuery">Дополнительный фильтр данных</param>
        /// <returns>Строка с запросом</returns>
        internal static string GetTableDataCountQuery(string tableName, Guid versionId, string filterQuery = null)
        {
            if (string.IsNullOrWhiteSpace(filterQuery))
                return string.Format("SELECT COUNT(1) FROM [dbo].[{0}] WHERE [SetsPassports_Id] IN (SELECT [Id] FROM [dbo].[SetsPassports]" +
                    " WHERE [SetVersion_Id] = '{1}')", tableName, versionId);
            else
                return string.Format("SELECT COUNT(1) FROM [dbo].[{0}] WHERE {2} AND [SetsPassports_Id] IN (SELECT [Id] FROM [dbo].[SetsPassports]" +
                " WHERE [SetVersion_Id] = '{1}')", tableName, versionId, filterQuery);
        }

        /// <summary>
        /// Возвращает SQL запрос на количество данных версии набора для определенного поставщика
        /// </summary>
        /// <param name="tableName">Имя таблицы</param>
        /// <param name="versionId">Идентефикатор версии</param>
        /// <param name="passportId">Идентефикатор набора в пасспорте поставщика</param>
        /// <param name="filterQuery">Дополнительный фильтр данных</param>
        /// <returns>Строка с запросом</returns>
        internal static string GetTableDataCount(string tableName, Guid versionId, string passportId, string filterQuery = null)
        {
            if (string.IsNullOrWhiteSpace(filterQuery))
                return string.Format("SELECT COUNT(1) FROM [dbo].[{0}] WHERE [SetsPassports_Id] IN (SELECT [Id] FROM [dbo].[SetsPassports]" +
                    " WHERE [SetVersion_Id] = '{1}' AND [Identifier] = '{2}')", tableName, versionId, passportId);
            else
                return string.Format("SELECT COUNT(1) FROM [dbo].[{0}] WHERE {3} AND [SetsPassports_Id] IN (SELECT [Id] FROM [dbo].[SetsPassports]" +
                " WHERE [SetVersion_Id] = '{1}' AND [Identifier] = '{2}')", tableName, versionId, passportId, filterQuery);
        }

        internal static string GetFullViewItemTableDataQuery(string tableName, Guid fullViewItemId)
        {
            return string.Format("SELECT * FROM [dbo].[{0}] WHERE Id = '{1}'", tableName, fullViewItemId);
        }

        internal static string GetAdditionalSetsItemDataQuery(string tableName, string parentTableName, Guid parentItemId)
        {
            return string.Format("SELECT * FROM [dbo].[{0}] WHERE {1}_Id = '{2}'", tableName, parentTableName, parentItemId);
        }
    }
}
