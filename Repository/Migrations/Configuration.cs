namespace Repository.Migrations
{
    using Repository.Abstract;
    using Repository.Concreate;
    using Repository.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<Repository.DataContext>
    {
        Guid agency1Id = Guid.NewGuid();
        Guid category1Id = Guid.NewGuid();
        Random rand = new Random();

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        internal void _seed(Repository.DataContext context)
        {
            this.Seed(context);
        }

        protected override void Seed(Repository.DataContext context)
        {
            //TODO: ���������������� ����� �������� �������� �������� ������
            //if (!Common.Configuration.Base.AddTestData) return;
            return;
            var agency1 = new Agency
            {
                Id = agency1Id,
                ITN = "1234567890",
                Keyword = "agency1",
                Name = "���� � ������",
                ShortName = "���"
            };



            context.Set<Agency>().AddRange(new Agency[] { agency1 });


            var category1 = new Category
            {
                Id = category1Id,
                Keyword = "category1",
                Name = "��������� 1",
            };



            context.Set<Category>().AddRange(new Category[] { category1 });



            var template1 = new TableDisplayTemplate
            {
                #region
                Description = "������ 1",
                HasMapPOIs = true,
                Name = "template1",
                Title = "������ 1. �������������� ���� ������",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[] 
                {
                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "audiobin_t",
                        Header = "�������� ���������",
                        DescriptionRu = "�������� ���������, ������������� �� �������.",
                        DescritpionEn = "Binary audiofile located on server",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Audio_Bin,
                        Order = 0
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "audiourl_t",
                        Header = "������ �� ���������",
                        DescriptionRu = "������ �� ��������� � ���� ��������",
                        DescritpionEn = "Internel link to audiofile",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Audio_Url,
                        Order = 1
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "bool_t",
                        Header = "������� ���",
                        DescriptionRu = "���������� ��� ������",
                        DescritpionEn = "Logical data type",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Boolean,
                        Order = 2
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "date_t",
                        DescritpionEn = "���� � ������� ISO6081",
                        DescriptionRu = "Date in ISO6081 format",
                        Header = "����",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Date,
                        Order = 3
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "datetime_t",
                        DescriptionRu="���� � ����� � ������� ISO6081",
                        DescritpionEn = "Date and time in ISO6081 format",
                        Header = "���� � �����",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.DateTime,
                        Order = 4
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "decimal_t",
                        DescriptionRu = "��� ������ ��� �������� �������� ������",
                        DescritpionEn = "Money value storage data type",
                        Header = "��������",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Decimal,
                        Order = 5
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "email_t",
                        Header = "������ ����������� �����",
                        DescriptionRu="������ ����������� �����",
                        DescritpionEn = "An E-Mail address",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.EMail,
                        Order = 6
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "filebin_t",
                        DescriptionRu="�������� ����, ������������� �� �������",
                        DescritpionEn = "Binary file, located on the server",
                        Header = "���� � �������",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.File_Bin,
                        Order = 7
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "fileurl_t",
                        Header = "������ �� ����",
                        DescriptionRu="������ �� ���� � ���� ��������",
                        DescritpionEn = "Link to the file in Internet",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.File_Url,
                        Order = 8
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "imagebin_t",
                        DescriptionRu="�����������, ���������� �� �������",
                        DescritpionEn = "An image located on server",
                        Header = "�����������",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Image_Bin,
                        Order = 9
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "imageurl_t",
                        DescriptionRu="������ �� ����������� � ���� ��������",
                        DescritpionEn = "The link to image in internet",
                        Header = "������ �� �����������",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Image_Url,
                        Order = 10
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "int_t",
                        DescriptionRu="����� �����",
                        DescritpionEn = "An Integer value",
                        Header = "����� �����",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Integer,
                        Order = 11
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "postaddress_t",
                        DescriptionRu="�������� ������",
                        DescritpionEn = "Postal address",
                        Header = "�������� ������",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.PostAddress,
                        Order = 12
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "real_t",
                        DescriptionRu="������������ �����",
                        DescritpionEn = "The Real value",
                        Header = "������������ ��� ������",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Real,
                        Order = 13
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "string_t",
                        DescriptionRu="������������ �����",
                        DescritpionEn = "Single line text",
                        Header = "������",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.String,
                        Order = 14
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "text_t",
                        DescriptionRu="������������� �����",
                        DescritpionEn = "Multiline text",
                        Header = "������������� �����",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Text,
                        Order = 15
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "time_t",
                        DescriptionRu="����� � ������� ISO6081",
                        DescritpionEn = "Time in ISO6081",
                        Header = "�����",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Time,
                        Order = 16
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "url_t",
                        DescriptionRu="������ �� ������� ������ � ���� ��������",
                        DescritpionEn = "Link to external source in internet",
                        Header = "������",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Url,
                        Order = 17
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "videobin_t",
                        DescriptionRu="���������, ������������� �� �������",
                        DescritpionEn = "Videofile located on server",
                        Header = "���� � �����",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Video_Bin,
                        Order = 18
                    },

                    new ColumnDisplayTemplate 
                    {
                        BindedCollumn = "videourl_t",
                        DescriptionRu="������ �� ��������� � ���� ��������",
                        DescritpionEn = "The link to the videofile in Internet",
                        Header = "������ �� ���� � �����",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        ShowInPOIsPopup = true,
                        Type = ColumnDisplayTemplate.ContentType.Video_Url,
                        Order = 19
                    },
                }
                #endregion
            };


            var template2 = new TableDisplayTemplate
            {
                #region
                Description = "������ 2",
                HasMapPOIs = false,
                Name = "template2",
                Title = "������ 2. ���������������������� �������",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[] 
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        BindedCollumn = "cell1",
                        Header = "A",
                        DescriptionRu = "����� ���� ������� ������.",
                        DescritpionEn = "Simple cell",
                        IncludeIntoFilter = true,
                        ShowInPreview = true,
                        Type = ColumnDisplayTemplate.ContentType.Text,
                        Order = 0
                    },

                    new ColumnDisplayTemplate 
                    {
                        Header = "B",
                        ShowInPreview = true,
                        Order = 1,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            #region
                            new ColumnDisplayTemplate
                            {
                                Header = "B_1",
                                BindedCollumn = "cell2",
                                DescriptionRu = "��������� ������ 1",
                                DescritpionEn = "Subcell 1",
                                IncludeIntoFilter = true,
                                ShowInPreview = true,
                                Type = ColumnDisplayTemplate.ContentType.Integer,
                                Order = 2
                            },

                            new ColumnDisplayTemplate
                            {
                                Header = "B_2",
                                BindedCollumn = "cell3",
                                DescriptionRu = "��������� ������ 2",
                                DescritpionEn = "Subcell 2",
                                IncludeIntoFilter = true,
                                ShowInPreview = true,
                                Type = ColumnDisplayTemplate.ContentType.Real,
                                Order = 3
                            }
                            #endregion
                        }
                    },

                    new ColumnDisplayTemplate 
                    {
                        Header = "C",
                        ShowInPreview = true,
                        Order = 4,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            #region
                            new ColumnDisplayTemplate 
                            {
                                Header = "C_1",
                                BindedCollumn = "cell4",
                                DescriptionRu = "��������� ������ 1",
                                DescritpionEn = "Subcell 1",
                                IncludeIntoFilter = true,
                                ShowInPreview = true,
                                Type = ColumnDisplayTemplate.ContentType.Url,
                                Order = 5
                            },

                            new ColumnDisplayTemplate
                            {
                                Header = "C_2",
                                ShowInPreview = true,
                                Order = 6,
                                SubColumns = new ColumnDisplayTemplate[]
                                {
                                    #region
                                    new ColumnDisplayTemplate
                                    {
                                        Header = "C_2_1",
                                        BindedCollumn = "cell5",
                                        DescriptionRu = "��������� ������ 2.1",
                                        DescritpionEn = "Subcell 2.1",
                                        IncludeIntoFilter = true,
                                        ShowInPreview = true,
                                        Type = ColumnDisplayTemplate.ContentType.EMail,
                                        Order = 7
                                    },

                                    new ColumnDisplayTemplate
                                    {
                                        Header = "C_2_2",
                                        BindedCollumn = "cell6",
                                        DescriptionRu = "��������� ������ 2.2",
                                        DescritpionEn = "Subcell 2.2",
                                        IncludeIntoFilter = true,
                                        ShowInPreview = true,
                                        Type = ColumnDisplayTemplate.ContentType.Image_Url,
                                        Order = 8
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "D",
                        Order = 9,
                        ShowInPreview = true,
                        InnerTable = new TableDisplayTemplate
                        {
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            HasMapPOIs = false,
                            Description = "�������� ��������� ������� 1 ��� ������� 2",
                            Name = "template3",
                            Title = "��������� ������� 1",
                            Columns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    Header = "D_1",
                                    BindedCollumn = "cell7",
                                    DescriptionRu = "������� 1, ������ 2",
                                    DescritpionEn = "Table 1, Cell 2",
                                    IncludeIntoFilter = true,
                                    ShowInPreview = true,
                                    Type = ColumnDisplayTemplate.ContentType.Date,
                                    Order = 10
                                },

                                new ColumnDisplayTemplate
                                {
                                    Header = "D_2",
                                    BindedCollumn = "cell8",
                                    DescriptionRu = "������� 1, ������ 2",
                                    DescritpionEn = "Table 1, Cell 2",
                                    IncludeIntoFilter = true,
                                    ShowInPreview = true,
                                    Type = ColumnDisplayTemplate.ContentType.DateTime,
                                    Order = 11
                                },

                                new ColumnDisplayTemplate
                                {
                                    Header = "D_3",
                                    BindedCollumn = "cell9",
                                    DescriptionRu = "������� 1, ������ 3",
                                    DescritpionEn = "Table 1, Cell 3",
                                    IncludeIntoFilter = true,
                                    ShowInPreview = true,
                                    Type = ColumnDisplayTemplate.ContentType.Time,
                                    Order = 12
                                }
                                #endregion
                            }
                        }
                    },

                    new ColumnDisplayTemplate
                    {
                        Header = "E",
                        ShowInPreview = true,
                        Order = 13,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            #region
                            new ColumnDisplayTemplate
                            {
                                Header = "E_1",
                                BindedCollumn = "cell10",
                                DescriptionRu = "������ 1",
                                DescritpionEn = "Cell 1",
                                IncludeIntoFilter = true,
                                ShowInPreview = true,
                                Type = ColumnDisplayTemplate.ContentType.Decimal,
                                Order = 13
                            },

                            new ColumnDisplayTemplate
                            {
                                Header = "E_2",
                                Order = 14,
                                IncludeIntoFilter = true,
                                ShowInPreview = true,
                                SubColumns = new ColumnDisplayTemplate[]
                                {
                                    #region
                                    new ColumnDisplayTemplate
                                    {
                                        Header = "E_2_1",
                                        BindedCollumn = "cell11",
                                        DescriptionRu = "������ 2.1",
                                        DescritpionEn = "Cell 2.1",
                                        IncludeIntoFilter = true,
                                        ShowInPreview = true,
                                        Type = ColumnDisplayTemplate.ContentType.Video_Url,
                                        Order = 15
                                    },

                                    new ColumnDisplayTemplate
                                    {
                                        Header = "E_2_2",
                                        IncludeIntoFilter = true,
                                        ShowInPreview = true,
                                        Order = 16,
                                        InnerTable = new TableDisplayTemplate
                                        {
                                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                                            HasMapPOIs = false,
                                            Description = "�������� ��������� ������� 2 ��� ������� 2",
                                            Name = "template4",
                                            Title = "��������� ������� 2",
                                            Columns = new ColumnDisplayTemplate[]
                                            {
                                                #region
                                                new ColumnDisplayTemplate
                                                {
                                                    Header = "E_2_2_1",
                                                    BindedCollumn = "cell12",
                                                    DescriptionRu = "����� ������ 1",
                                                    DescritpionEn = "Supercell 1",
                                                    IncludeIntoFilter = true,
                                                    ShowInPreview = true,
                                                    Type = ColumnDisplayTemplate.ContentType.PostAddress,
                                                    Order = 17
                                                },

                                                new ColumnDisplayTemplate
                                                {
                                                    Header = "E_2_2_2",
                                                    ShowInPreview = true,
                                                    Order = 18,
                                                    SubColumns = new ColumnDisplayTemplate[]
                                                    {
                                                        #region
                                                        new ColumnDisplayTemplate
                                                        {
                                                            Header = "E_2_2_2_1",
                                                            BindedCollumn = "cell13",
                                                            DescriptionRu = "����� ������ 2.1",
                                                            DescritpionEn = "Supercell 2.1",
                                                            IncludeIntoFilter = true,
                                                            ShowInPreview = true,
                                                            Type = ColumnDisplayTemplate.ContentType.File_Url,
                                                            Order = 19
                                                        },

                                                        new ColumnDisplayTemplate
                                                        {
                                                            Header = "E_2_2_2_2",
                                                            BindedCollumn = "cell14",
                                                            DescriptionRu = "����� ������ 2.2",
                                                            DescritpionEn = "Supercell 2.2",
                                                            IncludeIntoFilter = true,
                                                            ShowInPreview = true,
                                                            Type = ColumnDisplayTemplate.ContentType.Boolean,
                                                            Order = 20
                                                        }
                                                        #endregion
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion  
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            };

            #region Extended Structure
            var orgStructTemplate = new TableDisplayTemplate
            {
                #region
                Description = "��������� �����������",
                Name = "organizations",
                Title = "�����������",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                HasMapPOIs = true,
                Columns = new ColumnDisplayTemplate[] 
                {
                    #region
                    new ColumnDisplayTemplate 
                    {
                        #region
                        BindedCollumn = "name",
                        DescriptionRu = "������������",
                        DescritpionEn = "Name",
                        Header = "������������",
                        IncludeIntoFilter = true,
                        IsFullViewLink = true,
                        Order = 0,
                        ShowInPOIsPopup = true,
                        ShowInPreview = true,
                        Type = ColumnDisplayTemplate.ContentType.String
                        #endregion
                    },
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "description",
                        DescriptionRu = "��������",
                        DescritpionEn = "Description",
                        Header = "��������",
                        Order = 1,
                        ShowInPreview = false,
                        Type = ColumnDisplayTemplate.ContentType.Text
                        #endregion
                    },
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "url",
                        DescriptionRu = "������� � ���������",
                        DescritpionEn = "Website",
                        Header = "���-����",
                        Order = 2,
                        ShowInPreview = true,
                        Type = ColumnDisplayTemplate.ContentType.Text
                        #endregion
                    },

                    new ColumnDisplayTemplate
                    {
                        #region
                        Header = "��������",
                        DescritpionEn = "Contacts",
                        DescriptionRu = "��������",
                        Order = 3,
                        ShowInPreview = true,
                        InnerTable = new TableDisplayTemplate
                        {
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Description = "�������� �����",
                            Name = "contacts",
                            Title = "��������",
                            Columns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    #region
                                    BindedCollumn = "address",
                                    DescriptionRu = "������",
                                    DescritpionEn = "address",
                                    Header = "������",
                                    IncludeIntoFilter = true,
                                    Order = 4,
                                    ShowInPreview = true,
                                    ShowInPOIsPopup = true,
                                    Type = ColumnDisplayTemplate.ContentType.PostAddress
                                    #endregion
                                },
                                new ColumnDisplayTemplate
                                {
                                    #region
                                    BindedCollumn = "phone",
                                    DescriptionRu = "�������",
                                    DescritpionEn = "phone",
                                    Header = "�������",
                                    IncludeIntoFilter = true,
                                    ShowInPOIsPopup = true,
                                    Order = 5,
                                    ShowInPreview = true,
                                    Type = ColumnDisplayTemplate.ContentType.Text
                                    #endregion
                                },
                                new ColumnDisplayTemplate
                                {
                                    #region
                                    BindedCollumn = "email",
                                    DescriptionRu = "������ ����������� �����",
                                    DescritpionEn = "e-mail",
                                    Header = "������ ����������� �����",
                                    IncludeIntoFilter = true,
                                    ShowInPOIsPopup = true,
                                    Order = 6,
                                    ShowInPreview = true,
                                    Type = ColumnDisplayTemplate.ContentType.EMail
                                    #endregion
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            };

            var departmentsTemplate = new TableDisplayTemplate
            {
                #region
                Description = "������ �����������",
                Name = "departments",
                Title = "������ �����������",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "name",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "������������",
                        IncludeIntoFilter = true,
                        IsFullViewLink = true,
                        Order = 0,
                        ShowInPreview = true,
                        Type = ColumnDisplayTemplate.ContentType.String
                        #endregion
                    },
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "description",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "��������",
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.Text
                        #endregion
                    },
                    new ColumnDisplayTemplate
                    {
                        #region
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "������������",
                        Order = 2,
                        SubColumns = new ColumnDisplayTemplate[]
                        {
                            #region
                            new ColumnDisplayTemplate
                            {
                                #region
                                DescriptionRu = "",
                                DescritpionEn = "",
                                Header = "��������",
                                Order = 3,
                                ShowInPreview = true,
                                InnerTable = new TableDisplayTemplate
                                {
                                    #region
                                    Description = "�������� � ������������ ������",
                                    Name = "department chief details",
                                    Title = "�������� � ������������ ������",
                                    Type = TableDisplayTemplate.TemplateType.InnerTable,
                                    Columns = new ColumnDisplayTemplate[]
                                    {
                                        #region
                                        new ColumnDisplayTemplate
                                        {
                                            #region
                                            BindedCollumn = "chief_name",
                                            DescriptionRu = "",
                                            DescritpionEn = "",
                                            Header = "�.�.�.",
                                            IncludeIntoFilter = true,
                                            Order = 4,
                                            ShowInPreview = true,
                                            Type = ColumnDisplayTemplate.ContentType.String
                                            #endregion
                                        },
                                        new ColumnDisplayTemplate
                                        {
                                            #region
                                            DescriptionRu = "",
                                            DescritpionEn = "",
                                            Header = "��������",
                                            SubColumns = new ColumnDisplayTemplate[]
                                            {
                                                #region
                                                new ColumnDisplayTemplate
                                                {
                                                    #region
                                                    BindedCollumn = "phone",
                                                    DescriptionRu = "",
                                                    DescritpionEn = "",
                                                    Header = "�������",
                                                    IncludeIntoFilter = true,
                                                    Order = 5,
                                                    Type = ColumnDisplayTemplate.ContentType.String
                                                    #endregion
                                                },
                                                new ColumnDisplayTemplate
                                                {
                                                    #region
                                                    BindedCollumn = "email",
                                                    DescriptionRu = "",
                                                    DescritpionEn = "",
                                                    Header = "E-Mail",
                                                    IncludeIntoFilter = true,
                                                    Order = 6,
                                                    Type = ColumnDisplayTemplate.ContentType.EMail
                                                    #endregion
                                                },
                                                new ColumnDisplayTemplate
                                                {
                                                    #region
                                                    BindedCollumn = "other_contacts",
                                                    DescriptionRu = "",
                                                    DescritpionEn = "",
                                                    Header = "������",
                                                    Order = 7,
                                                    Type = ColumnDisplayTemplate.ContentType.Text
                                                    #endregion
                                                }
                                                #endregion
                                            }
                                            #endregion
                                        }
                                        #endregion
                                    }
                                    #endregion
                                }
                                #endregion
                            },
                            new ColumnDisplayTemplate
                            {
                                #region
                                BindedCollumn = "photo",
                                DescriptionRu = "",
                                DescritpionEn = "",
                                Header = "����������",
                                Order = 4,
                                ShowInPreview = false,
                                Type = ColumnDisplayTemplate.ContentType.Image_Url
                                #endregion
                            },
                            #endregion
                        }
                        #endregion
                    },
                    #endregion
                }
                #endregion
            };

            var projectsTemplate = new TableDisplayTemplate
            {
                #region
                Description = "������� �����������",
                Name = "projects",
                Title = "������� �����������",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "name",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "������������",
                        IncludeIntoFilter = true,
                        Order = 0,
                        IsFullViewLink = true,
                        Type = ColumnDisplayTemplate.ContentType.String
                        #endregion
                    },
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "description",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "��������",
                        Order = 1,
                        Type = ColumnDisplayTemplate.ContentType.Text
                        #endregion
                    },
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "date",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "���� ����������",
                        IncludeIntoFilter = true,
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.Date
                        #endregion
                    },
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "presentation",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "����������� �������",
                        Order = 2,
                        Type = ColumnDisplayTemplate.ContentType.File_Url
                        #endregion
                    }
                    #endregion
                }
                #endregion
            };

            var employesTemplate = new TableDisplayTemplate
            {
                #region
                Description = "����������",
                Name = "employers",
                Title = "����������",
                Type = TableDisplayTemplate.TemplateType.RootTable,
                Columns = new ColumnDisplayTemplate[]
                {
                    #region
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "name",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "�.�.�.",
                        IncludeIntoFilter = true,
                        Order = 0,
                        ShowInPreview = true,
                        IsFullViewLink = true,
                        Type = ColumnDisplayTemplate.ContentType.String
                        #endregion
                    },
                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "post",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "���������",
                        IncludeIntoFilter = false,
                        Order = 1,
                        ShowInPreview = true,
                        Type = ColumnDisplayTemplate.ContentType.String
                        #endregion
                    },

                    new ColumnDisplayTemplate
                    {
                        #region
                        BindedCollumn = "photo",
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "����������",
                        IncludeIntoFilter = false,
                        Order = 2,
                        ShowInPreview = false,
                        Type = ColumnDisplayTemplate.ContentType.Image_Url
                        #endregion
                    },

                    new ColumnDisplayTemplate
                    {
                        #region
                        DescriptionRu = "",
                        DescritpionEn = "",
                        Header = "��������",
                        IncludeIntoFilter = false,
                        Order = 3,
                        ShowInPreview = true,
                        InnerTable = new TableDisplayTemplate
                        {
                            Type = TableDisplayTemplate.TemplateType.InnerTable,
                            Description = "�������� ����������",
                            Title = "�������� ����������",
                            Name = "employe_contacts",
                            Columns = new ColumnDisplayTemplate[]
                            {
                                #region
                                new ColumnDisplayTemplate
                                {
                                    #region
                                    BindedCollumn = "phone",
                                    DescriptionRu = "",
                                    DescritpionEn = "",
                                    Header = "�������",
                                    IncludeIntoFilter = true,
                                    Order = 0,
                                    ShowInPreview = true,
                                    Type = ColumnDisplayTemplate.ContentType.String
                                    #endregion
                                },
                                new ColumnDisplayTemplate
                                {
                                    #region
                                    BindedCollumn = "email",
                                    DescriptionRu = "",
                                    DescritpionEn = "",
                                    Header = "E-Mail",
                                    IncludeIntoFilter = true,
                                    Order = 1,
                                    ShowInPreview = true,
                                    Type = ColumnDisplayTemplate.ContentType.EMail
                                    #endregion
                                },
                                new ColumnDisplayTemplate
                                {
                                    #region
                                    BindedCollumn = "other",
                                    DescriptionRu = "",
                                    DescritpionEn = "",
                                    Header = "Other",
                                    IncludeIntoFilter = true,
                                    Order = 2,
                                    ShowInPreview = false,
                                    Type = ColumnDisplayTemplate.ContentType.Text
                                    #endregion
                                }
                                #endregion
                            }

                        }
                        #endregion
                    },
                    #endregion
                }
                #endregion
            };

            #endregion

            context.Set<TableDisplayTemplate>().AddRange(new TableDisplayTemplate[] { template1, template2, orgStructTemplate, departmentsTemplate, projectsTemplate, employesTemplate });

            context.SaveChanges();

            var dataStruct1 = new DataStructure
            {
                Template_Id = template1.Id,
                Name = "��������� 1",
                Description = "��� ��������� ���� � �� ��������",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "structure1"
            };

            var dataStruct2 = new DataStructure
            {
                Template_Id = template2.Id,
                Name = "��������� 2",
                Description = "������� ������� ���������: ��������� � ������������ ������, ��������� �������",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "structure2"
            };

            var dataStruct3 = new DataStructure
            {
                Template_Id = orgStructTemplate.Id,
                Name = "��������� �����������",
                Description = "�������� ��������� �����������",
                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                TableName = "organizations",
                Children = new DataStructure[]
                {
                    new DataStructure
                    {
                        Template_Id = departmentsTemplate.Id,
                        Name = "������ �����������",
                        Description = "������ �����������",
                        CodeName = DateTime.Now.ToString("yyyyMMdd"),
                        TableName = "departments",
                        Children = new DataStructure[]
                        {
                            new DataStructure
                            {
                                Template_Id = employesTemplate.Id,
                                Name = "���������� �����������",
                                Description = "���������� �����������",
                                CodeName = DateTime.Now.ToString("yyyyMMdd"),
                                TableName = "employers",
                            }
                        }
                    },

                    new DataStructure
                    {
                        Template_Id = projectsTemplate.Id,
                        Name = "������� �����������",
                        Description = "������� �����������",
                        CodeName = DateTime.Now.ToString("yyyyMMdd"),
                        TableName = "projects"
                    }
                }
            };

            IRepositoryExtend<DataStructure, Guid> repo = new DataStructureRepository(context);
            repo.Add(dataStruct1);
            repo.Add(dataStruct2);
            repo.Add(dataStruct3);

            var set1 = new Set
            {
                Agencies = new Agency[] { agency1 },
                Category = category1,
                Description = "����� � ������������ ���� ��������� �������� � ������",
                Title = "����� �1",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Yearly
            };

            var set2 = new Set
            {
                Agencies = new Agency[] { agency1 },
                Category = category1,
                Description = "����� � ������������ ���������� �������� ���� ������� � �������",
                Title = "����� �2",
                UpdatePeriodicity = Set.UpdatingPeriodicity.Yearly
            };

            var set3 = new Set
            {
                Agencies = new Agency[] { agency1 },
                Category = category1,
                Description = "������ ��������� �����������",
                Title = "��������� �����������",
                UpdatePeriodicity = Set.UpdatingPeriodicity.OnDemand
            };

            context.Set<Set>().AddRange(new Set[] { set1, set2, set3 });
            context.SaveChanges();

            CreateDataSet1(set1, dataStruct1, context);
            CreateDataSet2(set2, dataStruct2, context);
            CreateDataSet3(set3, dataStruct3, context);
        }

        #region CreateDataSet1
        private void CreateDataSet1(Set set, DataStructure dataStruct, DataContext context)
        {
            List<Guid> versions = new List<Guid>();

            var counter = 2;

            for (int i = 0; i < counter; ++i)
            {
                var creationDate = DateTime.Now.AddYears(i - counter);

                var setVersion = new SetVersion
                {
                    Id = Guid.NewGuid(),
                    Set_Id = set.Id,
                    DataStructure_Id = dataStruct.Id,
                    IsAproved = true,
                    ParentVersion_Id = versions.Any() ? versions[rand.Next(versions.Count)] : (Guid?)null,
                    Name = string.Format("ver10{0}0", i),
                    CreationDate = creationDate,
                    CodeName = creationDate.ToString("yyyyMMdd"),
                    IsCurrent = (counter / 2) == i
                };

                context.Set<SetVersion>().Add(setVersion);
                context.SaveChanges();
                versions.Add(setVersion.Id);

                var ag = set.Agencies.First();

                var passportId = CreatePassport1(setVersion, context, ag, "setNumOne", i);


                for (int j = 0; j < rand.Next(100, 1000); j++)
                {
                    string cmd = @"INSERT INTO [dbo].[" + dataStruct.TableName + "] (audiobin_t, audiourl_t, bool_t, " +
                        "date_t, datetime_t, decimal_t, email_t, filebin_t, fileurl_t, imagebin_t, imageurl_t, " +
                        "int_t, postaddress_t, real_t, string_t, text_t, time_t, url_t, videobin_t, videourl_t, " +
                        "POI_Latitude, POI_Longitude, POI_MarkerName, SetsPassports_Id) VALUES (@p0, @p1, @p2, @p3, " +
                        "@p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, " +
                        "@p20, @p21, @p22, @p23)";

                    context.Database.ExecuteSqlCommand(cmd,
                        new SqlParameter("@p0", "cf23df22sdsa23d23twerdsf32f4ads86ff.mp3"),
                        new SqlParameter("@p1", "http://soundcloud.com/1.mp3"),
                        new SqlParameter("@p2", (j % 2) == 0),
                        new SqlParameter("@p3", DateTime.Now.AddDays(rand.Next(j * i + 1))),
                        new SqlParameter("@p4", DateTime.Now.AddDays(rand.Next(j * i + 1))),
                        new SqlParameter("@p5", (decimal)rand.Next(j * i + 1)),
                        new SqlParameter("@p6", string.Format("example_{0}@example{1}.com", j, i)),
                        new SqlParameter("@p7", "cf23df22sdsa23d23twerdsf32f4ads86ff.doc"),
                        new SqlParameter("@p8", "https://github.com/ArhiChief/ooc/ooc.pdf"),
                        new SqlParameter("@p9", "cf23df22sdsa23d23twerdsf32f4ads86ff.png"),
                        new SqlParameter("@p10", "http://1.bp.blogspot.com/_XE0TDW07Noo/TOSVQXZtgAI/AAAAAAAAELo/aG80jZ7u_fo/s1600/aptitude_test.gif"),
                        new SqlParameter("@p11", rand.Next(int.MaxValue)),
                        new SqlParameter("@p12", "350000, ������, �. ���������, ��. ������, �.97"),
                        new SqlParameter("@p13", rand.NextDouble() * rand.Next(int.MaxValue)),
                        new SqlParameter("@p14", "lorem ipsum"),
                        new SqlParameter("@p15", "lorem ipsum 350000, ������, �. ���������, ��. ������, �.97 cf23df22sdsa23d23twerdsf32f4ads86ff.png http://1.bp.blogspot.com/_XE0TDW07Noo/TOSVQXZtgAI/AAAAAAAAELo/aG80jZ7u_fo/s1600/aptitude_test.gif new SqlParameter(\"@p4\", DateTime.Now.AddDays(rand.Next(j*i+1))),"),
                        new SqlParameter("@p16", DateTime.Now.AddHours(rand.Next(j * i + 1))),
                        new SqlParameter("@p17", string.Format("http://example{1}.com/example_{0}", j, i)),
                        new SqlParameter("@p18", "cf23df22sdsa23d23twerdsf32f4ads86ff.mp4"),
                        new SqlParameter("@p19", "cf23df22sdsa23d23twerdsf32f4ads86ff.png"),
                        new SqlParameter("@p20", 90 * rand.NextDouble()),
                        new SqlParameter("@p21", 90 * rand.NextDouble()),
                        new SqlParameter("@p22", "default"),
                        new SqlParameter("@p23", passportId));
                }

            }
        }

        private Guid CreatePassport1(SetVersion ver, DataContext ctx, Agency agency, string identifier, int i)
        {
            var setPassport = new SetPassport
            {
                Id = Guid.NewGuid(),
                Agency_Id = agency.Id,
                Created = DateTime.Now,
                Creator = "�������� ������",
                Description = "�������� ����� � ������������� ���� �������������� ����� ������",
                Identifier = string.Format("{0}-{1}", agency.ITN, identifier),
                MethodicStandartVersion = "3.1",
                Provinance = "������ ������ ������" + i.ToString(),
                PublisherMBox = string.Format("{0}@example.com", identifier),
                PublisherName = "������ ���� ��������",
                PublisherPhone = "+1-234-5678901",
                SetVersion = ver,
                Subject = string.Format("�������� �����; �������� �����; {0}", identifier),
                Title = "����� 1",
                Valid = DateTime.Now.AddYears(1 + i),
                Modified = DateTime.Now.AddYears(i)
            };

            ctx.Set<SetPassport>().Add(setPassport);
            ctx.SaveChanges();
            return setPassport.Id;
        }
        #endregion


        #region CreateDataSet2
        private void CreateDataSet2(Set set, DataStructure dataStruct, DataContext context)
        {
            List<Guid> versions = new List<Guid>();

            var counter = 3;

            for (int i = 0; i < counter; ++i)
            {
                var creationDate = DateTime.Now.AddYears(i - counter);

                var setVersion = new SetVersion
                {
                    Id = Guid.NewGuid(),
                    Set_Id = set.Id,
                    DataStructure_Id = dataStruct.Id,
                    IsAproved = true,
                    ParentVersion_Id = versions.Any() ? versions[rand.Next(versions.Count)] : (Guid?)null,
                    Name = string.Format("ver10{0}0", i),
                    CreationDate = creationDate,
                    CodeName = creationDate.ToString("yyyyMMdd"),
                    IsCurrent = (counter / 3) == i
                };

                context.Set<SetVersion>().Add(setVersion);
                context.SaveChanges();
                versions.Add(setVersion.Id);

                var ag = set.Agencies.First();

                var passportId = CreatePassport2(setVersion, context, ag, "setNumTwo", i);


                for (int j = 0; j < rand.Next(100, 1000); j++)
                {
                    string cmd = @"INSERT INTO [dbo].[" + dataStruct.TableName + "] (cell1, cell2, cell3, cell4, cell5, cell6, cell7, " +
                        "cell8, cell9, cell10, cell11, cell12, cell13, cell14, SetsPassports_Id) VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, " +
                        "@p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14)";

                    context.Database.ExecuteSqlCommand(cmd,
                        new SqlParameter("@p0", j.ToString() + "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
                        new SqlParameter("@p1", rand.Next(100, 1000)),
                        new SqlParameter("@p2", rand.Next(100, 1000) * rand.NextDouble()),
                        new SqlParameter("@p3", "http://example.com/" + j.ToString()),
                        new SqlParameter("@p4", "example" + j.ToString() + "@example.com"),
                        new SqlParameter("@p5", "http://foro.putalocura.com/customavatars/avatar121301_2.gif"),
                        new SqlParameter("@p6", DateTime.Now),
                        new SqlParameter("@p7", DateTime.Now),
                        new SqlParameter("@p8", DateTime.Now),
                        new SqlParameter("@p9", (decimal)(rand.Next(100, 1000))),
                        new SqlParameter("@p10", "https://youtu.be/25NzynvJFFQ"),
                        new SqlParameter("@p11", "350000, ������, ������������� ����, �. ���������, ��. ������ 97, ��. 23"),
                        new SqlParameter("@p12", "https://github.com/ArhiChief/ooc/raw/master/ooc.pdf"),
                        new SqlParameter("@p13", (j % 2 == 0)),
                        new SqlParameter("@p14", passportId));
                }

            }
        }

        private object CreatePassport2(SetVersion ver, DataContext ctx, Agency agency, string identifier, int i)
        {
            var setPassport = new SetPassport
            {
                Id = Guid.NewGuid(),
                Agency_Id = agency.Id,
                Created = DateTime.Now,
                Creator = "��� \"������\"",
                Description = "����� � ������������ ���������� �������� ���� ������� � �������",
                Identifier = string.Format("{0}-{1}", agency.ITN, identifier),
                MethodicStandartVersion = "3.1",
                Provinance = "������ ������ ������" + i.ToString(),
                PublisherMBox = string.Format("{0}@example.com", identifier),
                PublisherName = "������ ���� ��������",
                PublisherPhone = "+1-234-5678901",
                SetVersion = ver,
                Subject = string.Format("�������� �����; �������� �����; {0}", identifier),
                Title = "����� 2",
                Valid = DateTime.Now.AddYears(1 + i),
                Modified = DateTime.Now.AddYears(i)
            };

            ctx.Set<SetPassport>().Add(setPassport);
            ctx.SaveChanges();
            return setPassport.Id;
        }
        #endregion


        #region CreateDataSet3
        private void CreateDataSet3(Set set, DataStructure dataStruct, DataContext context)
        {
            List<Guid> versions = new List<Guid>();

            var counter = 1;

            for (int i = 0; i < counter; ++i)
            {
                var creationDate = DateTime.Now.AddYears(i - counter);

                var setVersion = new SetVersion
                {
                    Id = Guid.NewGuid(),
                    Set_Id = set.Id,
                    DataStructure_Id = dataStruct.Id,
                    IsAproved = true,
                    ParentVersion_Id = versions.Any() ? versions[rand.Next(versions.Count)] : (Guid?)null,
                    Name = string.Format("ver10{0}0", i),
                    CreationDate = creationDate,
                    CodeName = creationDate.ToString("yyyyMMdd"),
                    IsCurrent = true
                };

                context.Set<SetVersion>().Add(setVersion);
                context.SaveChanges();
                versions.Add(setVersion.Id);

                var ag = set.Agencies.First();

                var passportId = CreatePassport3(setVersion, context, ag, "spellsystems", i);

                Guid orgId = Guid.NewGuid();
                Guid dep1Id = Guid.NewGuid();
                Guid dep2Id = Guid.NewGuid();

                #region Organization SQL
                string cmd = @"INSERT INTO [dbo].[organizations] (name, description, url, address, phone, email, " +
                    "POI_Latitude, POI_Longitude, POI_MarkerName, SetsPassports_Id, Id) VALUES (@p0, @p1, @p2, " +
                    "@p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10)";
                context.Database.ExecuteSqlCommand(cmd,
                        new SqlParameter("@p0", "SpellSystems"),
                        new SqlParameter("@p1", "Code monkeys company"),
                        new SqlParameter("@p2", "http://spellsystems.com"),
                        new SqlParameter("@p3", "Lenina st., 97, Krasnodar, Krasnodar Kray, Russia 350000"),
                        new SqlParameter("@p4", "+1234567890"),
                        new SqlParameter("@p5", "info@spellsystem.com"),
                        new SqlParameter("@p6", (double)35.44),
                        new SqlParameter("@p7", (double)45.43),
                        new SqlParameter("@p8", "default"),
                        new SqlParameter("@p9", passportId),
                        new SqlParameter("@p10", orgId));
                #endregion

                #region Projects SQL
                for (int j = 0; j < 40; j++)
                {
                    cmd = @"INSERT INTO [dbo].[projects] (name, description, date, presentation, organizations_Id) " +
                        "VALUES (@p0, @p1, @p2, @p3, @p4)";
                    context.Database.ExecuteSqlCommand(cmd,
                        new SqlParameter("@p0", "project #" + j.ToString()),
                        new SqlParameter("@p1", "description #" + j.ToString()),
                        new SqlParameter("@p2", DateTime.Now.AddMonths(-j)),
                        new SqlParameter("@p3", @"https://www.google.ru/url?sa=t&rct=j&q=&esrc=s&source=web&cd=9&ved=0ahUKEwi8prqHs_jNAhXMWSwKHZOdDzIQFggwMAg&url=http%3A%2F%2Fwww.publishers.org.uk%2F_resources%2Fassets%2Fattachment%2Ffull%2F0%2F2091.pdf&usg=AFQjCNH_l99OZ55Ktd5YdA6Y3k5qOX6EIw&bvm=bv.127521224,d.bGg&cad=rjt"),
                        new SqlParameter("@p4", orgId));
                }
                #endregion

                #region Departments SQL
                cmd = @"INSERT INTO [dbo].[departments] (photo, phone, email, other_contacts, chief_name, description, name, organizations_Id, Id) " +
                    "VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8)";
                context.Database.ExecuteSqlCommand(cmd,
                    new SqlParameter("@p0", "spellsystems.com.opt-images.1c-bitrix-cdn.ru/upload/iblock/51f/DSC_3754.jpg"),
                    new SqlParameter("@p1", "+2345678901"),
                    new SqlParameter("@p2", "nzahar@spellsystems.com"),
                    new SqlParameter("@p3", "test test test test test"),
                    new SqlParameter("@p4", "nzahar"),
                    new SqlParameter("@p5", "Main Code Monkeys department"),
                    new SqlParameter("@p6", "Progers"),
                    new SqlParameter("@p7", orgId),
                    new SqlParameter("@p8", dep1Id));
                

                context.Database.ExecuteSqlCommand(cmd,
                    new SqlParameter("@p0", "http://spellsystems.com.opt-images.1c-bitrix-cdn.ru/upload/iblock/9df/DSC_3675.jpg"),
                    new SqlParameter("@p1", "+3456789012"),
                    new SqlParameter("@p2", "karel@spellsystems.com"),
                    new SqlParameter("@p3", "test test test"),
                    new SqlParameter("@p4", "karel"),
                    new SqlParameter("@p5", "Our Masters"),
                    new SqlParameter("@p6", "Managers"),
                    new SqlParameter("@p7", orgId),
                    new SqlParameter("@p8", dep2Id));
                #endregion  

                #region Employers SQL
                cmd = @"INSERT INTO [dbo].[employers] (name, post, photo, phone, email, other, departments_Id) " +
                    "VALUES(@p0, @p1, @p2, @p3, @p4, @p5, @p6)";
                context.Database.ExecuteSqlCommand(cmd,
                    new SqlParameter("@p0", "sergey"),
                    new SqlParameter("@p1", "Developer"),
                    new SqlParameter("@p2", "spellsystems.com.opt-images.1c-bitrix-cdn.ru/upload/iblock/9c7/DSC_4342.jpg"),
                    new SqlParameter("@p3", "+4567890123"),
                    new SqlParameter("@p4", "kosivchenko@spellsystems.com"),
                    new SqlParameter("@p5", "n/a"),
                    new SqlParameter("@p6", dep1Id));

                context.Database.ExecuteSqlCommand(cmd,
                    new SqlParameter("@p0", "stas"),
                    new SqlParameter("@p1", "Manager"),
                    new SqlParameter("@p2", "spellsystems.com.opt-images.1c-bitrix-cdn.ru/upload/iblock/3e5/DSC_4794.jpg"),
                    new SqlParameter("@p3", "+5678901234"),
                    new SqlParameter("@p4", "stas@spellsystems.com"),
                    new SqlParameter("@p5", "many many many other contacts"),
                    new SqlParameter("@p6", dep2Id));

                #endregion
            }
        }

        private object CreatePassport3(SetVersion ver, DataContext ctx, Agency agency, string identifier, int i)
        {
            var setPassport = new SetPassport
            {
                Id = Guid.NewGuid(),
                Agency_Id = agency.Id,
                Created = DateTime.Now,
                Creator = "SpellSystems",
                Description = "������ ��������� �����������. �������� ������� � ���������� � �������������, �������� � ����������� ���������� �����������.",
                Identifier = string.Format("{0}-{1}", agency.ITN, identifier),
                MethodicStandartVersion = "3.1",
                Provinance = "������ ������ ������ " + i.ToString(),
                PublisherMBox = string.Format("{0}@example.com", identifier),
                PublisherName = "������ ���� ��������",
                PublisherPhone = "+1-234-5678901",
                SetVersion = ver,
                Subject = string.Format("�������� �����; �������� �����; {0}", identifier),
                Title = "��������� ����������� SpellSystems",
                Valid = DateTime.Now.AddYears(1 + i),
                Modified = DateTime.Now.AddYears(i)
            };

            ctx.Set<SetPassport>().Add(setPassport);
            ctx.SaveChanges();
            return setPassport.Id;
        }
        #endregion


    }
}
