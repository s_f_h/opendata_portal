namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agencies",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ShortName = c.String(nullable: false),
                        ITN = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Keyword = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sets",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(storeType: "ntext"),
                        Category_Id = c.Guid(nullable: false),
                        UpdatePeriodicity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Keyword = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SetVersions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Set_Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 10),
                        CreationDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CodeName = c.String(nullable: false),
                        ParentVersion_Id = c.Guid(),
                        IsCurrent = c.Boolean(nullable: false),
                        IsAproved = c.Boolean(nullable: false),
                        DataStructure_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataStructures", t => t.DataStructure_Id, cascadeDelete: true)
                .ForeignKey("dbo.Sets", t => t.Set_Id, cascadeDelete: true)
                .ForeignKey("dbo.SetVersions", t => t.ParentVersion_Id)
                .Index(t => t.Set_Id)
                .Index(t => t.ParentVersion_Id)
                .Index(t => t.DataStructure_Id);
            
            CreateTable(
                "dbo.DataStructures",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Template_Id = c.Guid(nullable: false),
                        Order = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(storeType: "ntext"),
                        CodeName = c.String(nullable: false),
                        TableName = c.String(nullable: false),
                        Parent_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataStructures", t => t.Parent_Id)
                .ForeignKey("dbo.TableDisplayTemplates", t => t.Template_Id, cascadeDelete: true)
                .Index(t => t.Template_Id)
                .Index(t => t.Parent_Id);
            
            CreateTable(
                "dbo.TableDisplayTemplates",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(storeType: "ntext"),
                        Title = c.String(),
                        HasMapPOIs = c.Boolean(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CollumnDisplayTemplates",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        BindedCollumn = c.String(),
                        Header = c.String(),
                        DescritpionEn = c.String(unicode: false, storeType: "text"),
                        DescriptionRu = c.String(unicode: false, storeType: "text"),
                        Type = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        Template_Id = c.Guid(),
                        InnerTable_Id = c.Guid(),
                        IsFullViewLink = c.Boolean(nullable: false),
                        IncludeIntoFilter = c.Boolean(nullable: false),
                        ShowInPOIsPopup = c.Boolean(nullable: false),
                        ShowInPreview = c.Boolean(nullable: false),
                        ColumnDisplayTemplate_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TableDisplayTemplates", t => t.InnerTable_Id)
                .ForeignKey("dbo.CollumnDisplayTemplates", t => t.ColumnDisplayTemplate_Id)
                .ForeignKey("dbo.TableDisplayTemplates", t => t.Template_Id)
                .Index(t => t.Template_Id)
                .Index(t => t.InnerTable_Id)
                .Index(t => t.ColumnDisplayTemplate_Id);
            
            CreateTable(
                "dbo.SetsPassports",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        MethodicStandartVersion = c.String(nullable: false),
                        Identifier = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Description = c.String(storeType: "ntext"),
                        Creator = c.String(nullable: false),
                        PublisherName = c.String(nullable: false),
                        PublisherPhone = c.String(nullable: false),
                        PublisherMBox = c.String(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Subject = c.String(nullable: false, storeType: "ntext"),
                        Valid = c.DateTime(precision: 7, storeType: "datetime2"),
                        Provinance = c.String(unicode: false, storeType: "text"),
                        Agency_Id = c.Guid(nullable: false),
                        SetVersion_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SetVersions", t => t.SetVersion_Id, cascadeDelete: true)
                .ForeignKey("dbo.Agencies", t => t.Agency_Id, cascadeDelete: true)
                .Index(t => t.Agency_Id)
                .Index(t => t.SetVersion_Id);
            
            CreateTable(
                "dbo.DownloadedBy",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserIP = c.String(),
                        SetPassport_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SetsPassports", t => t.SetPassport_Id)
                .Index(t => t.SetPassport_Id);
            
            CreateTable(
                "dbo.RatedBy",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Rating = c.Double(nullable: false),
                        Comment = c.String(storeType: "ntext"),
                        UserIP = c.String(),
                        SetPassport_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SetsPassports", t => t.SetPassport_Id)
                .Index(t => t.SetPassport_Id);
            
            CreateTable(
                "dbo.ViewedBy",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserIP = c.String(),
                        SetPassport_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SetsPassports", t => t.SetPassport_Id)
                .Index(t => t.SetPassport_Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PostedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Page = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QAs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Page = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AgencySets",
                c => new
                    {
                        Agency_Id = c.Guid(nullable: false),
                        Set_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Agency_Id, t.Set_Id })
                .ForeignKey("dbo.Agencies", t => t.Agency_Id, cascadeDelete: true)
                .ForeignKey("dbo.Sets", t => t.Set_Id, cascadeDelete: true)
                .Index(t => t.Agency_Id)
                .Index(t => t.Set_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SetsPassports", "Agency_Id", "dbo.Agencies");
            DropForeignKey("dbo.AgencySets", "Set_Id", "dbo.Sets");
            DropForeignKey("dbo.AgencySets", "Agency_Id", "dbo.Agencies");
            DropForeignKey("dbo.SetVersions", "ParentVersion_Id", "dbo.SetVersions");
            DropForeignKey("dbo.SetsPassports", "SetVersion_Id", "dbo.SetVersions");
            DropForeignKey("dbo.ViewedBy", "SetPassport_Id", "dbo.SetsPassports");
            DropForeignKey("dbo.RatedBy", "SetPassport_Id", "dbo.SetsPassports");
            DropForeignKey("dbo.DownloadedBy", "SetPassport_Id", "dbo.SetsPassports");
            DropForeignKey("dbo.SetVersions", "Set_Id", "dbo.Sets");
            DropForeignKey("dbo.SetVersions", "DataStructure_Id", "dbo.DataStructures");
            DropForeignKey("dbo.DataStructures", "Template_Id", "dbo.TableDisplayTemplates");
            DropForeignKey("dbo.CollumnDisplayTemplates", "Template_Id", "dbo.TableDisplayTemplates");
            DropForeignKey("dbo.CollumnDisplayTemplates", "ColumnDisplayTemplate_Id", "dbo.CollumnDisplayTemplates");
            DropForeignKey("dbo.CollumnDisplayTemplates", "InnerTable_Id", "dbo.TableDisplayTemplates");
            DropForeignKey("dbo.DataStructures", "Parent_Id", "dbo.DataStructures");
            DropForeignKey("dbo.Sets", "Category_Id", "dbo.Categories");
            DropIndex("dbo.AgencySets", new[] { "Set_Id" });
            DropIndex("dbo.AgencySets", new[] { "Agency_Id" });
            DropIndex("dbo.ViewedBy", new[] { "SetPassport_Id" });
            DropIndex("dbo.RatedBy", new[] { "SetPassport_Id" });
            DropIndex("dbo.DownloadedBy", new[] { "SetPassport_Id" });
            DropIndex("dbo.SetsPassports", new[] { "SetVersion_Id" });
            DropIndex("dbo.SetsPassports", new[] { "Agency_Id" });
            DropIndex("dbo.CollumnDisplayTemplates", new[] { "ColumnDisplayTemplate_Id" });
            DropIndex("dbo.CollumnDisplayTemplates", new[] { "InnerTable_Id" });
            DropIndex("dbo.CollumnDisplayTemplates", new[] { "Template_Id" });
            DropIndex("dbo.DataStructures", new[] { "Parent_Id" });
            DropIndex("dbo.DataStructures", new[] { "Template_Id" });
            DropIndex("dbo.SetVersions", new[] { "DataStructure_Id" });
            DropIndex("dbo.SetVersions", new[] { "ParentVersion_Id" });
            DropIndex("dbo.SetVersions", new[] { "Set_Id" });
            DropIndex("dbo.Sets", new[] { "Category_Id" });
            DropTable("dbo.AgencySets");
            DropTable("dbo.QAs");
            DropTable("dbo.News");
            DropTable("dbo.ViewedBy");
            DropTable("dbo.RatedBy");
            DropTable("dbo.DownloadedBy");
            DropTable("dbo.SetsPassports");
            DropTable("dbo.CollumnDisplayTemplates");
            DropTable("dbo.TableDisplayTemplates");
            DropTable("dbo.DataStructures");
            DropTable("dbo.SetVersions");
            DropTable("dbo.Categories");
            DropTable("dbo.Sets");
            DropTable("dbo.Agencies");
        }
    }
}
