﻿using Repository.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.Abstract
{
    /// <summary>
    /// Интерфейс работы с репозиторием версий наборов открытых данных
    /// </summary>
    public interface ISetVersionRepository : IRepositoryExtend<SetVersion, Guid>, IRepositoryMisc
    {
        /// <summary>
        /// Получает рейтинг указанной версии набора данных
        /// </summary>
        /// <param name="versionId">Идентефикатор версии</param>
        /// <returns>Результат операции</returns>
        RepositoryOperationResult<double?> GetAverageRating(Guid versionId);

        /// <summary>
        /// Получает все актуальные версии наборов данных указанной категории и минимальным указанным рейтингом
        /// </summary>
        /// <param name="categoryId">Идентефикатор категории</param>
        /// <param name="rating">Минимальный рейтинг</param>
        /// <param name="predicate">Дополнительное условие выборки</param>
        /// <param name="from">Смещение в выборке</param>
        /// <param name="count">Количество элементов получаемых из выборки</param>
        /// <param name="includes">Дополнительно загружаемые элементы</param>
        /// <returns>Результат операции</returns>
        RepositoryOperationResult<IQueryable<SetVersion>> GetSetVersionsForCategoryAndRating(Guid categoryId, double rating, 
            Expression<Func<SetVersion, bool>> predicate = null, int from = 0, 
            int count = 0, params Expression<Func<SetVersion, object>>[] includes);
    }
}
