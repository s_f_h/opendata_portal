﻿using Repository.Entities;
using System;

namespace Repository.Abstract
{
    /// <summary>
    /// Интерфейс репозитория поставщиков данных
    /// </summary>
    public interface IAgencyRepository : IRepositoryExtend<Agency, Guid>
    {
    }
}
