﻿namespace Repository.Abstract
{
    /// <summary>
    /// Расширенный интерфейс репозитория. Расширяет базовый интерфейс 
    /// методами манипуляции содержимого репозитория.
    /// </summary>
    /// <typeparam name="TEntity">Тип даных объектов репизитория</typeparam>
    /// <typeparam name="TKey">Тип данных первичного ключа объектов репозитория</typeparam>
    public interface IRepositoryExtend<TEntity, TKey> : IRepositoryBase<TEntity, TKey>, IRepositoryMisc
    {
        /// <summary>
        /// Производит удаление объектов из репозитория
        /// </summary>
        /// <param name="withSaveChanges">Синхронизация с БД</param>
        /// <param name="ids">Идентефикаторы удаляемых объектов</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<bool> Remove(bool withSaveChanges = true, params TKey[] ids);

        /// <summary>
        /// Добавляет объект в репозиторий
        /// </summary>
        /// <param name="item">Добавляемый объект</param>
        /// <param name="withSaveChanges">Синхронизация с БД</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<bool> Add(TEntity item, bool withSaveChanges = true);

        /// <summary>
        /// Изменяет состояние объекта в репозитории
        /// </summary>
        /// <param name="id">Идентефикатор изменяемого объекта</param>
        /// <param name="newState">Новое состояние объекта</param>
        /// <param name="withSaveChanges">Синхронизация с БД</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<bool> Update(TKey id, TEntity newState, bool withSaveChanges = true);
    }
}
