﻿using Repository.Misc;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.Abstract
{
    /// <summary>
    /// Интерфейс работы с репозиторием данных набора
    /// </summary>
    public interface ISetDataRepository
    {
        /// <summary>
        /// Возвращает данные набора
        /// </summary>
        /// <param name="verId">Идентефикатор версии набора</param>
        /// <param name="passportId">Идентефикатор набора в пасспорте</param>
        /// <param name="structId">Идентефикатор элемента структуры набора данных</param>
        /// <param name="from">Смещение от начала</param>
        /// <param name="count">Количество записей</param>
        /// <param name="filterQuery">Дополнительный фильтр данных</param>
        /// <returns>Данные</returns>
        List<Dictionary<string, object>> GetTableData(Guid verId, Guid structId, string passportId = null, int from = 0, int count = 0, string filterQuery = null);

        /// <summary>
        /// Производит выгрузку данных в указанный файл для указанного набора и версии структуры и данных
        /// </summary>
        /// <param name="stream">Поток для результата</param>
        /// <param name="passportId">Идентефикатор набора</param>
        /// <param name="setVerId">Версия набора данных</param>
        /// <param name="setStructId">Версия структуры</param>
        /// <param name="format">Формат выгружаемых данных</param>
        void ExportData(Stream stream, string passportId = null, string setVerId = null, string setStructId = null, Formats format = Formats.xml);

        /// <summary>
        /// Производит выгрузку данных в указанный файл для указанного набора
        /// </summary>
        /// <param name="stream">Поток для результата</param>
        /// <param name="setVerId">Идентефикатор версии набора</param>
        /// <param name="format">Формат выгружаемых данных</param>
        void ExportData(Stream stream, Guid setVerId, Formats format);

        /// <summary>
        /// Возвращает количество элементов таблицы набора данных
        /// </summary>
        /// <param name="verId">Версия набора данных</param>
        /// <param name="passportId">Идентефикатор паспорта</param>
        /// <param name="templateId">Идентефикатор элемента структуры данных</param>
        /// <param name="filterQuery">Дополнительный фильтр данных</param>
        /// <returns>Количество элементов</returns>
        int GetElementsCount(Guid verId, Guid structId, string passportId = null, string filterQuery = null);

        /// <summary>
        /// Возвращает данные просматриваемого элемента в режиме FullView
        /// </summary>
        /// <param name="structId">Идентефикатор структуры данных</param>
        /// <param name="fullViewItemId">Идентефикатор элемента в таблице</param>
        /// <returns>Данные</returns>
        List<Dictionary<string, object>> GetFullViewItemTableData(Guid structId, Guid fullViewItemId);

        /// <summary>
        /// Возвращает данные дополнительных наборов в режиме FullView
        /// </summary>
        /// <param name="structId">Идентефикатор структуры</param>
        /// <param name="parentItemId">Идентефикатор родительского элемента в таблице</param>
        /// <returns>Данные</returns>
        List<Dictionary<string, object>> GetAdditionalSetsItemData(Guid structId, Guid parentItemId);
    }
}
