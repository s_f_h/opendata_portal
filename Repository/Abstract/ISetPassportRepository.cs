﻿using Repository.Entities;
using Repository.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.Abstract
{
    /// <summary>
    /// Интерфейс работы с репозиторием пасспортов наборов данных
    /// </summary>
    public interface ISetPassportRepository : IRepositoryExtend<SetPassport, Guid>
    {
        /// <summary>
        /// Выгружает реестр наборов указанных форматов
        /// </summary>
        /// <param name="stream">Поток для результата</param>
        /// <param name="url_prefix">URL аддресс портала</param>
        /// <param name="format">Формат выгружаемых данных</param>
        void ExportList(Stream stream, string url_prefix, Formats format = Formats.xml);

        /// <summary>
        /// Выгружает пасспорт указанного набора в соответствии с версией набора 
        /// </summary>
        /// <param name="stream">Поток для результата</param>
        /// <param name="url_prefix">URL аддресс портала</param>
        /// <param name="identifier">Идентефикатор набора</param>
        /// <param name="version">Версия набора данных</param>
        /// <param name="format">Формат выгружаемых данных</param>
        void ExportMeta(Stream stream, string url_prefix, string identifier, Guid? version = null, Formats format = Formats.xml);

        /// <summary>
        /// Возвращает рейтинг набора паспорта
        /// </summary>
        /// <param name="passportId">Идентефикатор пасспорта</param>
        /// <returns>Результат операции</returns>
        RepositoryOperationResult<double?> GetAverageRating(Guid passportId);
        /// <summary>
        /// Возвращает паспорта указанного поставщика с наборами определенного рейтинга
        /// </summary>
        /// <param name="agencyId">Идентефикатор поставщика</param>
        /// <param name="rating">Минимальный рейтинг набора</param>
        /// <param name="predicate">Дополнительное условие выборки</param>
        /// <param name="from">Смещение в выборке</param>
        /// <param name="count">Количество элементов</param>
        /// <param name="includes">Дополнительно загружаемые элементы</param>
        /// <returns>Результат операции</returns>
        RepositoryOperationResult<IQueryable<SetPassport>> GetPassportsForAgencyAndRating(Guid agencyId, double rating,
            Expression<Func<SetPassport, bool>> predicate = null, int from = 0,
            int count = 0, params Expression<Func<SetPassport, object>>[] includes);

        /// <summary>
        /// Устанавливает рейтинг для набора данных
        /// </summary>
        /// <param name="passportId">Идентефикатор пасспорта набора</param>
        /// <param name="value">Рейтинг</param>
        /// <param name="ip">IP аддресс пользователя</param>
        /// <returns>Результат операции</returns>
        RepositoryOperationResult<bool> AddPassportRating(Guid passportId, int value, string ip);

        /// <summary>
        /// Усьанавливает количество просмотров для набора данных паспорта
        /// </summary>
        /// <param name="passportId">Идентефикатор пасспорта</param>
        /// <param name="ip">IP аддрес пользователя, просмотревшего набор</param>
        /// <returns>Результат операции</returns>
        RepositoryOperationResult<bool> AddPassportViewdBy(Guid passportId, string ip);
    }
}
