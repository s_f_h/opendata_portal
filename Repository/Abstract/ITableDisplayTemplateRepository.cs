﻿using Repository.Entities;
using System;

namespace Repository.Abstract
{
    /// <summary>
    /// Интерфейс работы с шаблонами структур наборов данных
    /// </summary>
    public interface ITableDisplayTemplateRepository : IRepositoryExtend<TableDisplayTemplate, Guid>
    {
        /// <summary>
        /// Возвращает описание структуры таблицы
        /// </summary>
        /// <param name="id">Идентефикатор шаблона</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<TableDisplayTemplate> TableStructure(Guid id);

        /// <summary>
        /// Возвращает описание структуры набора
        /// </summary>
        /// <param name="id">Идентефикатор описания</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<DataStructure> SetStructure(Guid id);
    }
}
