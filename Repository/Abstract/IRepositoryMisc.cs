﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Abstract
{
    /// <summary>
    /// Дополнительный интерфейс для работы с репозиторием
    /// </summary>
    public interface IRepositoryMisc
    {
        /// <summary>
        /// Принудительная синхронизация с БД
        /// </summary>
        RepositoryOperationResult<bool> SaveChanges();
    }
}
