﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.Abstract
{
    /// <summary>
    /// Базовый интерфейс репозитория
    /// </summary>
    /// <typeparam name="TItem">Тип данных элементов репозитория</typeparam>
    /// <typeparam name="TKey">Тип данных первичного ключа элементов репозитория</typeparam>
    public interface IRepositoryBase<TEntity, TKey>
    {
        /// <summary>
        /// Возвращает количество элементов в репозитории
        /// </summary>
        /// <param name="predicate">Условие поиска элемента</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<int> Count(Expression<Func<TEntity, bool>> predicate = null);

        /// <summary>
        /// Возвращает наличие элемента в репозитории
        /// </summary>
        /// <param name="predicate">Условие поиска</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<bool> Any(Expression<Func<TEntity, bool>> predicate = null);

        /// <summary>
        /// Возвращает элемент из репозитория
        /// </summary>
        /// <param name="predicate">Условия поиска</param>
        /// <param name="includes">Дополнительно подгружаемые объекты</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<TEntity> FirstOrDefault(Expression<Func<TEntity, bool>> predicate = null, 
            params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Возвращает коллекцию элементов из репозитория
        /// </summary>
        /// <param name="predicate">Условие поиска</param>
        /// <param name="from">Смещение в коллекции</param>
        /// <param name="count">Количество возвращаемых элементов</param>
        /// <param name="includes">Дополнительно подгружаемые объекты</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        RepositoryOperationResult<IQueryable<TEntity>> Where(Expression<Func<TEntity, bool>> predicate = null,
            int from = 0, int count = 0, params Expression<Func<TEntity, object>>[] includes);
    }
}
