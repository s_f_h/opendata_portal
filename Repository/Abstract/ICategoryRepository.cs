﻿using Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Abstract
{
    /// <summary>
    /// Интерфейс репозитория категорий данных
    /// </summary>
    public interface ICategoryRepository : IRepositoryExtend<Category, Guid>
    {
    }
}
