﻿using Repository.Entities;
using System;

namespace Repository.Abstract
{
    /// <summary>
    /// Интерфейс работы с группами наборов данных
    /// </summary>
    public interface ISetRepository : IRepositoryExtend<Set, Guid>
    {
    }
}
