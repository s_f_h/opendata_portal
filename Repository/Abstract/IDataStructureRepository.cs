﻿using Repository.Entities;
using Repository.Misc;
using System;
using System.IO;

namespace Repository.Abstract
{
    /// <summary>
    /// Интерфейс репозитория структур данных
    /// </summary>
    public interface IDataStructureRepository : IRepositoryExtend<DataStructure, Guid>
    {
        /// <summary>
        /// Выгружает структуру указанного набора в соответствии с идентефикатором набора и версией структуры
        /// </summary>
        /// <param name="stream">Поток для результата</param>
        /// <param name="setId">Идентефикатор набора</param>
        /// <param name="structId">Идентефикатор структуры</param>
        /// <param name="format">Формат выгружаемых данных</param>
        void ExportStructure(Stream stream, string setId, string structId, Formats format);

        /// <summary>
        /// Выгружает структуру указанного набора в соответствии с версией набора 
        /// </summary>
        /// <param name="stream">Поток для результата</param>
        /// <param name="setVerId">Иденетфикатор версии набора данных</param>
        /// <param name="format">Формат выгружаемых данных</param>
        void ExportStructure(Stream stream, Guid setVerId, Formats format);
    }
}
