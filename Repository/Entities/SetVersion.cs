﻿using Common;
using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Repository.Entities
{
    /// <summary>
    /// Описание версии набора данных
    /// </summary>
    [Table("SetVersions")]
    public class SetVersion : UniqueEntity<Guid>
    {
        /// <summary>
        /// Внешний ключ на набор данных
        /// </summary>
        [Required(ErrorMessage="Не задан набор")]
        [ForeignKey("Set")]
        public Guid Set_Id { get; set; }
        /// <summary>
        /// Набор данных
        /// </summary>
        public virtual Set Set { get; set; }
        /// <summary>
        /// Наименование версии набора данных
        /// </summary>
        [Required(ErrorMessage="Не задан идентефикатор версии")]
        [MaxLength(10)]
        [MinLength(3)]
        [RegularExpression(Constants.RegExps.VersionName, ErrorMessage="Не корректный идентефикатор версии")]
        [Display(Name="Идентефикатор версии", Prompt=Constants.Watermarks.VersionName)]
        public string Name { get; set; }

        /// <summary>
        /// Дата создания версии набора
        /// </summary>
        [Required(ErrorMessage="Не задана дата создания версии")]
        [Column(TypeName="datetime2")]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Кодовое имя версии набора. Используется для получения ссылки на версию при скачивании набора
        /// </summary>
        [Required(ErrorMessage = "Не задано кодовое имя версии")]
        //[Column(TypeName = "nvarchar(20)")]
        [RegularExpression(Constants.RegExps.CodeName, ErrorMessage="Не корректный формат кодового имени")]
        public string CodeName { get; set; }

        /// <summary>
        /// Список версий, выходящих из данной
        /// </summary>
        public ICollection<SetVersion> SubVersions { get; set; }

        /// <summary>
        /// Внешний ключ на родительскую версию
        /// </summary>
        [ForeignKey("ParentVersion")]
        public Guid? ParentVersion_Id { get; set; }
        /// <summary>
        /// Родительская версия
        /// </summary>
        public SetVersion ParentVersion { get; set; }
        /// <summary>
        /// Версия набора является текущей
        /// </summary>
        [Display(Name="Текущая")]
        public bool IsCurrent { get; set; }
        /// <summary>
        /// Версия является утвержденной оператором
        /// </summary>
        [Display(Name="Утверждена")]
        public bool IsAproved { get; set; }

        /// <summary>
        /// Внешний ключ на структуру данных набора
        /// </summary>
        [Required(ErrorMessage = "Не задана структура данных версии набора")]
        [ForeignKey("DataStructure")]
        public Guid DataStructure_Id { get; set; }

        /// <summary>
        /// Структура данных версии набора данных
        /// </summary>
        [Display(Name="Структура данных версии набора")]
        public DataStructure DataStructure { get; set; }
        /// <summary>
        /// Список поспартов на данные, содержащиеся в версии
        /// </summary>
        public ICollection<SetPassport> SetsPassports { get; set; }
    }
}