﻿using Common;
using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Repository.Entities
{
    /// <summary>
    /// Описание паспортов данных наборов
    /// </summary>
    [Table("SetsPassports")]
    public class SetPassport : UniqueEntity<Guid>
    {
        /// <summary>
        /// Версия методических рекомендаций соответствующая набору
        /// </summary>
        [Required(ErrorMessage = "Не задана версия методических рекомендаций")]
        [Display(Name = "Версия методических рекомендаций")]
        public string MethodicStandartVersion { get; set; }
        /// <summary>
        /// Идентефикатор набора данных
        /// </summary>
        [Required(ErrorMessage = "Идентефикатор набора")]
        [RegularExpression(Constants.RegExps.SetIdentifier, ErrorMessage = "Идентефикатор не соответствует шаблону")]
        [Display(Name = "Идентефикатор набора", Prompt = Constants.Watermarks.SetIdentifier)]
        public string Identifier { get; set; }
        /// <summary>
        /// Заголовок набора данных
        /// </summary>
        [Required(ErrorMessage = "Не задан заголовок набора")]
        [Display(Name = "Заголовок набора")]
        public string Title { get; set; }
        /// <summary>
        /// Описание содержимого набора данных
        /// </summary>
        [Display(Name = "Описание содержимого набора")]
        [DataType(DataType.MultilineText)]
        [Column(TypeName = "ntext")]
        public string Description { get; set; }
        /// <summary>
        /// Владелец набора данных
        /// </summary>
        [Required(ErrorMessage = "Не задан владельца набора")]
        [Display(Name = "Владелец набора")]
        public string Creator { get; set; }
        /// <summary>
        /// Ответственное лицо
        /// </summary>
        [Required(ErrorMessage = "Не задано ответственное лицо")]
        [Display(Name = "Ответственное лицо")]
        public string PublisherName { get; set; }
        /// <summary>
        /// Телефон ответственного лица
        /// </summary>
        [Required(ErrorMessage = "Не задан телефон ответственного лица")]
        [RegularExpression(Constants.RegExps.PhoneNumber, ErrorMessage = "Телефонный номер не соответствует шаблону")]
        [Display(Name = "Телефон ответственного лица", Prompt = Constants.Watermarks.PhoneNumber)]
        [DataType(DataType.PhoneNumber)]
        public string PublisherPhone { get; set; }
        /// <summary>
        /// Адрес электронной почты ответственного лица
        /// </summary>
        [Required(ErrorMessage = "Не задан E-Mail ответственного лица")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Не соответствует шаблону E-Mail адреса")]
        [Display(Name = "E-Mail ответственного лица", Prompt = Constants.Watermarks.Email)]
        public string PublisherMBox { get; set; }
        /// <summary>
        /// Дата первой публикации набора данных
        /// </summary>
        [Required(ErrorMessage = "Не задана дата публикации набора")]
        [Column(TypeName = "datetime2")]
        [Display(Name = "Дата публикации набора", Prompt = Constants.Watermarks.Date)]
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }
        /// <summary>
        /// Дата последнего изменения набора данных
        /// </summary>
        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date)]
        public DateTime Modified { get; set; }

        /// <summary>
        /// Ключевые слова, соответствующие содержанию набора данных
        /// </summary>
        [Required(ErrorMessage = "Не заданы ключевые слова набора")]
        [Display(Name = "Ключевые слова набора", Prompt = "тест; текстовый набор; что-то еще")]
        [Column(TypeName = "ntext")]
        [DataType(DataType.MultilineText)]
        public string Subject { get; set; }
        /// <summary>
        /// Дата следующего обновления набора данных
        /// </summary>
        [Display(Name = "Действителен до")]
        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date)]
        public DateTime? Valid { get; set; }
        /// <summary>
        /// Содержание изменений в наборе
        /// </summary>
        [Column(TypeName = "text")]
        [Display(Name = "Содержание последнего изменения")]
        [DataType(DataType.MultilineText)]
        public string Provinance { get; set; }

        /// <summary>
        /// Идентефикатор поставщика, предоствившего паспорт
        /// </summary>
        [Required(ErrorMessage = "Не задан поставщик паспорта")]
        [ForeignKey("Agency")]
        public Guid Agency_Id { get; set; }
        /// <summary>
        /// Поставщик, предоствивший паспорт
        /// </summary>
        public virtual Agency Agency { get; set; }

        /// <summary>
        /// Идентефикатор версии, соответствующего набора
        /// </summary>
        [Required(ErrorMessage = "Не задана версия набора")]
        [ForeignKey("SetVersion")]
        public Guid SetVersion_Id { get; set; }
        /// <summary>
        /// Версия, соответствующего набора
        /// </summary>
        public virtual SetVersion SetVersion { get; set; }

        /// <summary>
        /// Лог просмотров данных
        /// </summary>
        public ICollection<ViewedBy> ViewedBy { get; set; }
        /// <summary>
        /// Лог скачиваний данных
        /// </summary>
        public ICollection<DownloadedBy> DownloadedBy { get; set; }
        /// <summary>
        /// Лог установки рейтинга наборов
        /// </summary>
        public ICollection<RatedBy> RatedBy { get; set; }


        /// <summary>
        /// Количество уникальных просмотров содержимого набора данной версии
        /// </summary>
        [NotMapped]
        [Display(Name = "Количество просмотров")]
        public int ViewsCount { get { return ViewedBy.Count; } }
        /// <summary>
        /// Количество скачиваний данных данной версии набора данных
        /// </summary>
        [NotMapped]
        [Display(Name = "Количество скачиваний")]
        public int DownlodsCount { get { return DownloadedBy.Count; } }
        /// <summary>
        /// Средний рейтинг данных данной версии набора данных
        /// </summary>
        [NotMapped]
        [Display(Name = "Средний рейтинг")]
        public double AverageRating { get { return RatedBy.Sum(x => x.Rating) / RatedBy.Count; } }
    }
}
