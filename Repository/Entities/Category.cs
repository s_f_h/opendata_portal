﻿using Repository.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Описание категории наборов данных
    /// </summary>
    [Table("Categories")]
    public class Category : AgencyCategoryBase
    {
    }
}
