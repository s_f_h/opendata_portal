﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Entities.Base
{
    /// <summary>
    /// Базовый класс сущностей
    /// </summary>
    /// <typeparam name="TKey">Тип данных первичного ключа</typeparam>
    public abstract class EntityBase<TKey>
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        [Key]
        public TKey Id { get; set; }
    }
}
