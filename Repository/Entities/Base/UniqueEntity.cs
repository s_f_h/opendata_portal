﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities.Base
{
    /// <summary>
    /// Базовый класс для сущностей с уникальным первичным ключом
    /// </summary>
    public abstract class UniqueEntity<TKey> : EntityBase<TKey>
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public new TKey Id { get; set; }
    }
}
