﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repository.Entities.Base
{
    /// <summary>
    /// Базовый класс для рубрикаций наборов
    /// </summary>
    public abstract class AgencyCategoryBase : UniqueEntity<Guid>
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [Required(ErrorMessage="Не задано наименование")]
        public string Name { get; set; }
        /// <summary>
        /// Ключевое слово
        /// </summary>
        [Required(ErrorMessage="Не задано ключевое слова")]
        public string Keyword { get; set; }

        /// <summary>
        /// Наборы данных
        /// </summary>
        public ICollection<Set> Sets { get; set; }
    }
}
