﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities.Base
{
    /// <summary>
    /// Базовый класс логирования действий пользователя
    /// </summary>
    public abstract class ActionedBy : UniqueEntity<Guid>
    {
        /// <summary>
        /// IP адресс пользователя сгенерировшего действие
        /// </summary>
        public string UserIP { get; set; }
    }
}
