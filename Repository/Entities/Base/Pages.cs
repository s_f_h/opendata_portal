﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities.Base
{
    /// <summary>
    /// Отображения пользовательской информации
    /// </summary>
    public abstract class UserPage : EntityBase<int>
    {
        /// <summary>
        /// Содержимое страницы
        /// </summary>
        [Column(TypeName="xml")]
        [Display(Name="Содержимое")]
        public string Page { get; set; }
    }
}
