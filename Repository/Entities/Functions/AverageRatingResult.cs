﻿using Repository.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities.Function
{
    // The 'Rating' property on 'AverageRating' could not be set to a 'System.Double' value. You must set this property to a non-null value of type 'System.Single'. 
    [ComplexType]
    public class AverageRatingResult //: EntityBase<Guid>
    {
        public Guid Id { get; set; }
        public double Rating { get; set; }
    }
}
