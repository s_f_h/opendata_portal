﻿using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Описание колонки таблицы
    /// </summary>
    [Table("CollumnDisplayTemplates")]
    public class ColumnDisplayTemplate : UniqueEntity<Guid>
    {
        TableDisplayTemplate _innerTable;
        bool _isFullViewLink;

        /// <summary>
        /// Наименование колонки с данными в таблице БД
        /// </summary>
        public string BindedCollumn { get; set; }
        /// <summary>
        /// Заголовок колонки
        /// </summary>
        public string Header { get; set; }
        /// <summary>
        /// Назначение колонки (английский перевод)   
        /// </summary>
        [Column(TypeName = "text")]
        public string DescritpionEn { get; set; }
        /// <summary>
        /// Назначение колонки
        /// </summary>
        [Column(TypeName = "text")]
        public string DescriptionRu { get; set; }
        /// <summary>
        /// Тип данных содержимого ячеек колонки
        /// </summary>
        public ContentType Type { get; set; }
        /// <summary>
        /// Вложенные в заголовок колонки подколонки
        /// </summary>
        public ICollection<ColumnDisplayTemplate> SubColumns { get; set; }
        /// <summary>
        /// Порядок следования колонки в строке
        /// </summary>
        public int Order { get; set; }
        /// <summary>
        /// Внешний ключ на родительский шаблон 
        /// </summary>
        [ForeignKey("Template")]
        public Guid? Template_Id { get; set; }
        /// <summary>
        /// Ссылка на родительский шаблон 
        /// </summary>
        public TableDisplayTemplate Template { get; set; }

        /// <summary>
        /// Внешний ключ на шаблон вложенной в ячейку таблицы
        /// </summary>
        [ForeignKey("InnerTable")]
        public Guid? InnerTable_Id { get; set; }
        /// <summary>
        /// Ссылка на шаблон вложенной в ячейку таблицы
        /// </summary>
        public TableDisplayTemplate InnerTable
        {
            get
            {
                if (_innerTable != null && _innerTable.Type != TableDisplayTemplate.TemplateType.InnerTable)
                    throw new ArgumentException("Template " + _innerTable.Id.ToString() + " must be InnerTemplate type");
                return _innerTable;
            }
            set
            {
                if (value != null && value.Type != TableDisplayTemplate.TemplateType.InnerTable)
                    throw new ArgumentException("Template " + value.Id.ToString() + " must be InnerTemplate type");
                _innerTable = value;
            }
        }

        /// <summary>
        /// Содержимое ячейки является ссылкой подробное описание
        /// </summary>
        public bool IsFullViewLink
        {
            get { return _isFullViewLink; }
            set
            {
                if (value && ((int)Type > 8 || InnerTable != null || SubColumns != null))
                    throw new ArgumentException("Ячейка - ссылка должна быть представлена простым типом данных");

                _isFullViewLink = value;
                if (value) ShowInPreview = value;
            }
        }
        /// <summary>
        /// Включить данную ячейку в фильтр
        /// </summary>
        public bool IncludeIntoFilter { get; set; }
        /// <summary>
        /// Отображать содержимое данной ячейки в геолакоционных метках
        /// </summary>
        public bool ShowInPOIsPopup { get; set; }
        /// <summary>
        /// Данная ячейка будет отображаться в таблицах предварительного просмотра
        /// </summary>
        public bool ShowInPreview { get; set; }

        /// <summary>
        /// Тип данных содержимого ячейки колонки
        /// </summary>
        public enum ContentType
        {
            /// <summary>
            /// Целочисленное значение
            /// </summary>
            Integer = 0,
            /// <summary>
            /// Вещественное значение
            /// </summary>
            Real = 1,
            /// <summary>
            /// Денежный тип
            /// </summary>
            Decimal = 2,
            /// <summary>
            /// Дата
            /// </summary>
            Date = 3,
            /// <summary>
            /// Время
            /// </summary>
            Time = 4,
            /// <summary>
            /// Дата и время
            /// </summary>
            DateTime = 5,
            /// <summary>
            /// Однострочный текст
            /// </summary>
            String = 6,
            /// <summary>
            /// Многострочный текст
            /// </summary>
            Text = 7,
            /// <summary>
            /// Почтовый адресс
            /// </summary>
            PostAddress = 8,
            /// <summary>
            /// Ссылка
            /// </summary>
            Url = 9,
            /// <summary>
            /// Адресс электронной почты
            /// </summary>
            EMail = 10,
            /// <summary>
            /// Видеозапись файл
            /// </summary>
            Video_Bin = 11,
            /// <summary>
            /// Видео на внешнем сервере
            /// </summary>
            Video_Url = 12,
            /// <summary>
            /// Аудиозапись
            /// </summary>
            Audio_Bin = 13,
            /// <summary>
            /// Аудиозапись на внешнем сервере
            /// </summary>
            Audio_Url = 14,
            /// <summary>
            /// Изображение
            /// </summary>
            Image_Bin = 15,
            /// <summary>
            /// Изображение на внешнего сервера
            /// </summary>
            Image_Url = 16,
            /// <summary>
            /// Двоичный файл
            /// </summary>
            File_Bin = 17,
            /// <summary>
            /// Файл на внешнем сервера
            /// </summary>
            File_Url = 18,
            /// <summary>
            /// Логический тип
            /// </summary>
            Boolean = 19,
        }
    }
}
