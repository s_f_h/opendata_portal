﻿using Repository.Entities.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Сущность логирования действий просмотра набора данных
    /// </summary>
    [Table("VieweBy")]
    public class ViewedBy : ActionedBy
    {
    }
}
