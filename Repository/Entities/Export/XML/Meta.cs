// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 4.2.0.15
//    <NameSpace>Common</NameSpace><Collection>List</Collection><codeType>CSharp</codeType><EnableDataBinding>False</EnableDataBinding><GenerateCloneMethod>False</GenerateCloneMethod><GenerateDataContracts>False</GenerateDataContracts><DataMemberNameArg>OnlyIfDifferent</DataMemberNameArg><DataMemberOnXmlIgnore>False</DataMemberOnXmlIgnore><CodeBaseTag>Net45</CodeBaseTag><InitializeFields>All</InitializeFields><GenerateUnusedComplexTypes>False</GenerateUnusedComplexTypes><GenerateUnusedSimpleTypes>False</GenerateUnusedSimpleTypes><GenerateXMLAttributes>True</GenerateXMLAttributes><OrderXMLAttrib>False</OrderXMLAttrib><EnableLazyLoading>False</EnableLazyLoading><VirtualProp>False</VirtualProp><PascalCase>False</PascalCase><AutomaticProperties>False</AutomaticProperties><PropNameSpecified>None</PropNameSpecified><PrivateFieldName>StartWithUnderscore</PrivateFieldName><PrivateFieldNamePrefix></PrivateFieldNamePrefix><EnableRestriction>False</EnableRestriction><RestrictionMaxLenght>False</RestrictionMaxLenght><RestrictionRegEx>False</RestrictionRegEx><RestrictionRange>False</RestrictionRange><ValidateProperty>False</ValidateProperty><ClassNamePrefix></ClassNamePrefix><ClassLevel>Public</ClassLevel><PartialClass>True</PartialClass><ClassesInSeparateFiles>False</ClassesInSeparateFiles><ClassesInSeparateFilesDir></ClassesInSeparateFilesDir><TrackingChangesEnable>False</TrackingChangesEnable><GenTrackingClasses>False</GenTrackingClasses><HidePrivateFieldInIDE>False</HidePrivateFieldInIDE><EnableSummaryComment>False</EnableSummaryComment><EnableAppInfoSettings>False</EnableAppInfoSettings><EnableExternalSchemasCache>False</EnableExternalSchemasCache><EnableDebug>False</EnableDebug><EnableWarn>False</EnableWarn><ExcludeImportedTypes>False</ExcludeImportedTypes><ExpandNesteadAttributeGroup>False</ExpandNesteadAttributeGroup><CleanupCode>False</CleanupCode><EnableXmlSerialization>False</EnableXmlSerialization><SerializeMethodName>Serialize</SerializeMethodName><DeserializeMethodName>Deserialize</DeserializeMethodName><SaveToFileMethodName>SaveToFile</SaveToFileMethodName><LoadFromFileMethodName>LoadFromFile</LoadFromFileMethodName><EnableEncoding>False</EnableEncoding><EnableXMLIndent>False</EnableXMLIndent><Encoder>UTF8</Encoder><Serializer>XmlSerializer</Serializer><sspNullable>False</sspNullable><sspString>False</sspString><sspCollection>False</sspCollection><sspComplexType>False</sspComplexType><sspSimpleType>False</sspSimpleType><sspEnumType>True</sspEnumType><BaseClassName>EntityBase</BaseClassName><UseBaseClass>False</UseBaseClass><GenBaseClass>False</GenBaseClass><CustomUsings></CustomUsings><AttributesToExlude></AttributesToExlude>
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace Repository.Entities.Export.XML
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.Collections;
    using System.Xml.Schema;
    using System.ComponentModel;
    using System.Xml;
    using System.Collections.Generic;
    
    /// <summary>
    /// �������� ������ ������
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1064.2")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute("meta", Namespace="", IsNullable=false)]
    public partial class Meta
    {
        
        private string _standardversion = @"http://opendata.gosmonitor.ru/standart/3.0";
        
        private string _identifier;
        
        private string _title;
        
        private string _description;
        
        private string _creator;
        
        private string _created;
        
        private string _modified;
        
        private string _subject;
        
        private string _format = "xml";
        
        private List<DataVersion> _data;
        
        private List<StructureVersion> _structure;
        
        private Publisher _publisher;
        
        public Meta()
        {
            this._publisher = new Publisher();
            this._structure = new List<StructureVersion>();
            this._data = new List<DataVersion>();
        }
        
        /// <summary>
        /// ������ ������������ ������������
        /// </summary>
        /// <value>3.1</value>
        [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
        public string standardversion
        {
            get
            {
                return this._standardversion;
            }
            set
            {
                this._standardversion = value;
            }
        }
        
        /// <summary>
        /// ������������� ������ ������
        /// </summary>
        public string identifier
        {
            get
            {
                return this._identifier;
            }
            set
            {
                this._identifier = value;
            }
        }
        
        /// <summary>
        /// ������������ ������ ������
        /// </summary>
        public string title
        {
            get
            {
                return this._title;
            }
            set
            {
                this._title = value;
            }
        }
        
        /// <summary>
        /// �������� ������ ������
        /// </summary>
        public string description
        {
            get
            {
                return this._description;
            }
            set
            {
                this._description = value;
            }
        }
        
        /// <summary>
        /// �������� ������ ������
        /// </summary>
        public string creator
        {
            get
            {
                return this._creator;
            }
            set
            {
                this._creator = value;
            }
        }
        
        /// <summary>
        /// ���� �������� ������ ������
        /// </summary>
        public string created
        {
            get
            {
                return this._created;
            }
            set
            {
                this._created = value;
            }
        }
        
        /// <summary>
        /// ���� ��������� ����������� ������ ������
        /// </summary>
        public string modified
        {
            get
            {
                return this._modified;
            }
            set
            {
                this._modified = value;
            }
        }
        
        /// <summary>
        /// �������� �����, ����������� ����� ������
        /// </summary>
        public string subject
        {
            get
            {
                return this._subject;
            }
            set
            {
                this._subject = value;
            }
        }
        
        /// <summary>
        /// ������ ������ ������
        /// </summary>
        /// <value>xml</value>
        public string format
        {
            get
            {
                return this._format;
            }
            set
            {
                this._format = value;
            }
        }
        
        /// <summary>
        /// ������ �� ������ ������ ������
        /// </summary>
        [System.Xml.Serialization.XmlArrayItemAttribute("dataversion", IsNullable=false)]
        public List<DataVersion> data
        {
            get
            {
                return this._data;
            }
            set
            {
                this._data = value;
            }
        }
        
        /// <summary>
        /// ������ �� ��������� ������ ������ ������
        /// </summary>
        [System.Xml.Serialization.XmlArrayItemAttribute("structureversion", IsNullable=false)]
        public List<StructureVersion> structure
        {
            get
            {
                return this._structure;
            }
            set
            {
                this._structure = value;
            }
        }
        
        /// <summary>
        /// ������������� ����
        /// </summary>
        public Publisher publisher
        {
            get
            {
                return this._publisher;
            }
            set
            {
                this._publisher = value;
            }
        }
    }
    
    /// <summary>
    /// �������� ������ �� ����� ������
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1064.2")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DataVersion
    {
        
        private string _source;
        
        private string _created;
        
        private string _provenance;
        
        private string _valid;
        
        private string _structure;
        
        /// <summary>
        /// ������ �� ����� ������
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
        public string source
        {
            get
            {
                return this._source;
            }
            set
            {
                this._source = value;
            }
        }
        
        /// <summary>
        /// ���� �������� ������ ������
        /// </summary>
        public string created
        {
            get
            {
                return this._created;
            }
            set
            {
                this._created = value;
            }
        }
        
        /// <summary>
        /// �������� ��������� � ������ ������
        /// </summary>
        public string provenance
        {
            get
            {
                return this._provenance;
            }
            set
            {
                this._provenance = value;
            }
        }
        
        /// <summary>
        /// ���� ������������ ������ ������
        /// </summary>
        public string valid
        {
            get
            {
                return this._valid;
            }
            set
            {
                this._valid = value;
            }
        }
        

        /// <summary>
        /// ���� �������� ��������� ������ ������
        /// </summary>
        public string structure
        {
            get
            {
                return this._structure;
            }
            set
            {
                this._structure = value;
            }
        }
    }
    
    /// <summary>
    /// ���������� �� ������������� ���� ������ ������
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1064.2")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class Publisher
    {
        
        private string _name;
        
        private string _phone;
        
        private string _mbox;
        
        /// <summary>
        /// �������, ��� � �������� �������������� ����
        /// </summary>
        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }
        
        /// <summary>
        /// ���������� ����� �������������� ����
        /// </summary>
        public string phone
        {
            get
            {
                return this._phone;
            }
            set
            {
                this._phone = value;
            }
        }
        
        /// <summary>
        /// E-Mail �������������� ����
        /// </summary>
        public string mbox
        {
            get
            {
                return this._mbox;
            }
            set
            {
                this._mbox = value;
            }
        }
    }
    
    /// <summary>
    /// �������� ��������� ������ �������� ������
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1064.2")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class StructureVersion
    {
        
        private string _source;
        
        private string _created;
        
        /// <summary>
        /// ������ �� ��������� �������� ������
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
        public string source
        {
            get
            {
                return this._source;
            }
            set
            {
                this._source = value;
            }
        }
        
        /// <summary>
        /// ���� �������� ���������
        /// </summary>
        public string created
        {
            get
            {
                return this._created;
            }
            set
            {
                this._created = value;
            }
        }
    }
}
#pragma warning restore
