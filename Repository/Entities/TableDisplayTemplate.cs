﻿using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Описание шаблона представлнения данных набора
    /// </summary>
    [Table("TableDisplayTemplates")]
    public class TableDisplayTemplate : UniqueEntity<Guid>
    {
        /// <summary>
        /// Наименование шаблона
        /// </summary>
        [Required(ErrorMessage="Не задано наименование")]
        [Display(Name="Наименование")]
        public string Name { get; set; }

        /// <summary>
        /// Описание шаблона
        /// </summary>
        [Column(TypeName="ntext")]
        [Display(Name = "Описание")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        /// <summary>
        /// Заголовок таблицы, описываемой данным шаблоном
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Шаблон содержит геолокационные данные
        /// </summary>
        public bool HasMapPOIs { get; set; }

        /// <summary>
        /// Тип шаблона
        /// </summary>
        [Required(ErrorMessage="Не задан тип шаблона")]
        public TemplateType Type { get; set; }

        /// <summary>
        /// Описание колонок шаблона представления
        /// </summary>
        public ICollection<ColumnDisplayTemplate> Columns { get; set; }

        /// <summary>
        /// Тип шаблона
        /// </summary>
        public enum TemplateType
        {
            /// <summary>
            /// Ссылка на набор во внешнем источнике
            /// </summary>
            Reference = 0,
            /// <summary>
            /// Шаблон данных
            /// </summary>
            RootTable,
            /// <summary>
            /// Вложенная в ячейку таблица
            /// </summary>
            InnerTable
        }
    }
}
