﻿using Common;
using Repository.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Описание поставщика наборов данных
    /// </summary>
    [Table("Agencies")]
    public class Agency : AgencyCategoryBase
    {
        /// <summary>
        /// Абревиатура поставщика
        /// </summary>
        [Required(ErrorMessage = "Не задана абревиатура")]
        [Display(Name = "Абревиатура")]
        public string ShortName { get; set; }

        /// <summary>
        /// ИНН поставщика
        /// </summary>
        [Required(ErrorMessage = "Не задан ИНН")]
        [RegularExpression(Constants.RegExps.ITN, ErrorMessage = "ИНН не соответствует формату")]
        [Display(Name = "ИНН", Prompt = Constants.Watermarks.ITN)]
        public string ITN { get; set; }

        /// <summary>
        /// Паспорта наборов данных предоставленных поставщиком
        /// </summary>
        public ICollection<SetPassport> SetsPassports { get; set; }
    }
}
