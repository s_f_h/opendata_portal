﻿using Repository.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Элемент новостной ленты
    /// </summary>
    [Table("News")]
    public class News : UserPage
    {
        /// <summary>
        /// Дата публикации
        /// </summary>
        [Column(TypeName="datetime2")]
        [Display(Name="Дата публикации")]
        public DateTime PostedDate { get; set; }
    }
}
