﻿using Repository.Entities.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Сущность логирования действий установки рейтинга набора
    /// </summary>
    [Table("RetedBy")]
    public class RatedBy : ActionedBy
    {
        /// <summary>
        /// Установленный рейтинг
        /// </summary>
        [Display(Name="Рейтинг")]
        public double Rating { get; set; }

        /// <summary>
        /// Комментарий пользователя
        /// </summary>
        [Column(TypeName="ntext")]
        [Display(Name="Комментарий")]
        [DataType(DataType.MultilineText)]
        public string Comment { get; set; }
    }
}
