﻿using Repository.Entities.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Сущность логирования действий скачивания набора данных
    /// </summary>
    [Table("DownloadedBy")]
    public class DownloadedBy : ActionedBy
    {
    }
}
