﻿using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Описание набора данных
    /// </summary>
    [Table("Sets")]
    public class Set : UniqueEntity<Guid>
    {
        /// <summary>
        /// Заголовок набора данных
        /// </summary>
        [Required(ErrorMessage = "Не задан заголовок")]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        /// <summary>
        /// Описание содержимого набора данных
        /// </summary>
        [Column(TypeName = "ntext")]
        [Display(Name = "Описание")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        /// <summary>
        /// Внешний ключ на категорию, к которой относится набор данных
        /// </summary>
        [Required(ErrorMessage = "Не задана категория набора")]
        [ForeignKey("Category")]
        public Guid Category_Id { get; set; }
        public Category Category { get; set; }

        [Required(ErrorMessage = "Не задана переодичность обновления данных")]
        [Display(Name = "Переодичность обновления данных")]
        public UpdatingPeriodicity UpdatePeriodicity { get; set; }

        /// <summary>
        /// Список поставщиков данных для набора
        /// </summary>
        [Required(ErrorMessage = "Не заданы поставщики данных для набора")]
        public ICollection<Agency> Agencies { get; set; }

        /// <summary>
        /// Корневая версия набора
        /// </summary>
        public ICollection<SetVersion> RootVersion { get; set; }

        /// <summary>
        /// Переодичность обновления данных набора
        /// </summary>
        public enum UpdatingPeriodicity
        {
            /// <summary>
            /// По требованию
            /// </summary>
            OnDemand = -1,
            /// <summary>
            /// Неизвестно
            /// </summary>
            Unknown = -2,
            /// <summary>
            /// Более раза в день
            /// </summary>
            MoreTimesPerDay = 0,
            /// <summary>
            /// Ежедневно
            /// </summary>
            Dayly = 1,
            /// <summary>
            /// Еженедельно
            /// </summary>
            Weekly = 7,
            /// <summary>
            /// Каждую декаду
            /// </summary>
            Decadely = 12,
            /// <summary>
            /// Ежемесячно
            /// </summary>
            Monthly = 30,
            /// <summary>
            /// Ежеквартально
            /// </summary>,
            Quaterly = 91,
            /// <summary>
            /// Полугодично
            /// </summary>
            HalfYear = 182,
            /// <summary>
            /// Ежегодно
            /// </summary>
            Yearly = 365
        }
    }
}
