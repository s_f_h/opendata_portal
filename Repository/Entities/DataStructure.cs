﻿using Common;
using Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Entities
{
    /// <summary>
    /// Описание структуры данных версии набора
    /// </summary>
    [Table("DataStructures")]
    public class DataStructure : UniqueEntity<Guid>
    {
        /// <summary>
        /// Внешний ключ на шаблон
        /// </summary>
        [Required(ErrorMessage = "Не задан шаблон отображения")]
        [ForeignKey("Template")]
        public Guid Template_Id { get; set; }
        /// <summary>
        /// Шаблон данных
        /// </summary>
        public TableDisplayTemplate Template { get; set; }

        /// <summary>
        /// Порядок следования данных шаблона
        /// </summary>
        [Display(Name = "Порядок следования")]
        public int Order { get; set; }

        /// <summary>
        /// Ниаменование структуры
        /// </summary>
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Не задано наименование")]
        public string Name { get; set; }
        /// <summary>
        /// Описание структуры
        /// </summary>
        [Column(TypeName = "ntext")]
        [Display(Name = "Описание")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        /// <summary>
        /// Кодовое имя версии набора. Используется для получения ссылки на версию при скачивании набора
        /// </summary>
        [Required(ErrorMessage = "Не задано кодовое имя структуры")]
        //[Column(TypeName = "nvarchar(20)")]
        [RegularExpression(Constants.RegExps.CodeName, ErrorMessage = "Не корректный формат кодового имени")]
        public string CodeName { get; set; }

        /// <summary>
        /// Наименование таблицы в БД
        /// </summary>
        [Required]
        public string TableName
        {
            get { return _tableName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value)) return;
                _tableName = value;
            }
        }
        string _tableName = "table_" + Guid.NewGuid().ToString("N");

        /// <summary>
        /// Версии, описанные структурой
        /// </summary>
        public ICollection<SetVersion> Version { get; set; }

        /// <summary>
        /// Внешний ключ на родительский элемент
        /// </summary>
        [ForeignKey("Parent")]
        public Guid? Parent_Id { get; set; }
        /// <summary>
        /// Родительский элемент
        /// </summary>
        public virtual DataStructure Parent { get; set; }

        /// <summary>
        /// Описания для вложенных таблиц
        /// </summary>
        public ICollection<DataStructure> Children { get; set; }
    }
}
