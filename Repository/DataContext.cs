﻿using EntityFramework.Functions;
using Repository.Entities;
using Repository.Entities.Function;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Repository
{
    public partial class DataContext : DbContext
    {
        public DataContext(string conStr) : base(conStr) { }
        public DataContext() : this("OD_ConStr") { }

        internal DbQuery<Agency> Agencies { get { return Set<Agency>().AsNoTracking(); } }
        internal DbQuery<Category> Categories { get { return Set<Category>().AsNoTracking(); } }
        internal DbQuery<DownloadedBy> DownloadedBy { get { return Set<DownloadedBy>().AsNoTracking(); } }
        internal DbQuery<RatedBy> RatedBy { get { return Set<RatedBy>().AsNoTracking(); } }
        internal DbQuery<ViewedBy> ViewedBy { get { return Set<ViewedBy>().AsNoTracking(); } }
        internal DbQuery<SetPassport> SetsPassports { get { return Set<SetPassport>().AsNoTracking(); } }
        internal DbQuery<SetVersion> SetsVersions { get { return Set<SetVersion>().AsNoTracking(); } }
        internal DbQuery<Set> Sets { get { return Set<Set>().AsNoTracking(); } }
        internal DbQuery<TableDisplayTemplate> TableDisplayTemplate { get { return Set<TableDisplayTemplate>().AsNoTracking(); } }
        internal DbQuery<ColumnDisplayTemplate> CollumnDisplayTemplates { get { return Set<ColumnDisplayTemplate>().AsNoTracking(); } }
        internal DbQuery<News> News { get { return Set<News>().AsNoTracking(); } }
        internal DbQuery<QA> QAs { get { return Set<QA>().AsNoTracking(); } }
        internal DbQuery<DataStructure> DataStructures { get { return Set<DataStructure>().AsNoTracking(); } }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Add(new FunctionConvention<DataContext>());
            modelBuilder.ComplexType<AverageRatingResult>();
            modelBuilder.AddFunctions<AverageRatingResult>();
            


            modelBuilder.Entity<Agency>().HasMany(x => x.Sets).WithMany(x => x.Agencies);
            modelBuilder.Entity<Agency>().HasMany(x => x.SetsPassports).WithRequired(x => x.Agency).WillCascadeOnDelete(true);
            
            modelBuilder.Entity<Category>().HasMany(x => x.Sets).WithRequired(x => x.Category).WillCascadeOnDelete(true);
            
            modelBuilder.Entity<SetVersion>().HasMany(x => x.SetsPassports).WithRequired(x => x.SetVersion).WillCascadeOnDelete(true);
            modelBuilder.Entity<SetVersion>().HasMany(x => x.SubVersions).WithOptional(x => x.ParentVersion);

            modelBuilder.Entity<TableDisplayTemplate>().HasMany(x => x.Columns).WithOptional(x => x.Template);

            modelBuilder.Entity<Set>().HasMany(x => x.Agencies).WithMany(x => x.Sets);

            //modelBuilder.Entity<ColumnDisplayTemplate>().HasMany(x => x.SubColumns).WithRequired();
            //modelBuilder.Entity<ColumnDisplayTemplate>().HasOptional(x => x.InnerTable).WithMany(x => x.Columns);


            modelBuilder.Entity<ViewedBy>().Map(x =>
            {
                x.MapInheritedProperties();
                x.ToTable("ViewedBy");
            });

            modelBuilder.Entity<DownloadedBy>().Map(x =>
            {
                x.MapInheritedProperties();
                x.ToTable("DownloadedBy");
            });

            modelBuilder.Entity<RatedBy>().Map(x =>
            {
                x.MapInheritedProperties();
                x.ToTable("RatedBy");
            });
            

            modelBuilder.Entity<News>().HasKey(x => x.Id).ToTable("News");
            modelBuilder.Entity<QA>().HasKey(x => x.Id).ToTable("QAs");
        }
    }
}
