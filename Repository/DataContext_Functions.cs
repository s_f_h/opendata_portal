﻿using EntityFramework.Functions;
using Repository.Entities.Function;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace Repository
{
    public partial class DataContext : DbContext
    {
        [Function(FunctionType.TableValuedFunction, "GetAverageRatingForPassport", "dbo", Schema = "dbo")]
        public IQueryable<AverageRatingResult> GetAverageRatingForPassport(
            [Parameter(DbType = "uniqueidentifier", Name = "id")] Guid id)
        {
            ObjectParameter idParametr = new ObjectParameter("id", id);
            return this.ObjectContext().CreateQuery<AverageRatingResult>("[GetAverageRatingForPassport](@id)", idParametr);
        }


        [Function(FunctionType.TableValuedFunction, "GetAverageRatingForSetVersion", "dbo", Schema = "dbo")]
        public IQueryable<AverageRatingResult> GetAverageRatingForSetVersion(
            [Parameter(DbType = "uniqueidentifier", Name = "id")] Guid id)
        {
            ObjectParameter idParametr = new ObjectParameter("id", id);
            return this.ObjectContext().CreateQuery<AverageRatingResult>("[GetAverageRatingForSetVersion](@id)", idParametr);
        }


        [Function(FunctionType.TableValuedFunction, "GetSetVersionsWithCategoryAndRating", "dbo", Schema = "dbo")]
        public IQueryable<AverageRatingResult> GetSetVersionsWithCategoryAndRating(
            [Parameter(DbType = "uniqueidentifier", Name = "category")] Guid categoryId,
            [Parameter(DbType = "float", Name = "rating")] double rating)
        {
            ObjectParameter categoryParametr = new ObjectParameter("category", categoryId);
            ObjectParameter ratingParametr = new ObjectParameter("rating", rating);
            return this.ObjectContext().CreateQuery<AverageRatingResult>("[GetSetVersionsWithCategoryAndRating](@category, @rating)", categoryParametr, ratingParametr);
        }

        [Function(FunctionType.TableValuedFunction, "GetSetPassporstWithAgencyAndRating", "dbo", Schema = "dbo")]
        public IQueryable<AverageRatingResult> GetSetPassporstWithAgencyAndRating(
            [Parameter(DbType = "uniqueidentifier", Name = "agency")] Guid agencyId,
            [Parameter(DbType = "float", Name = "rating")] double rating)
        {
            ObjectParameter agencyParametr = new ObjectParameter("agency", agencyId);
            ObjectParameter ratingParametr = new ObjectParameter("rating", rating);
            return this.ObjectContext().CreateQuery<AverageRatingResult>("[GetSetPassporstWithAgencyAndRating](@agency, @rating)", agencyParametr, ratingParametr);
        }
    }
}
