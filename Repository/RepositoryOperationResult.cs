﻿namespace Repository
{
    /// <summary>
    /// Представляет результат операции в репозитории
    /// </summary>
    /// <typeparam name="T">Тип данных возвращаемого результата</typeparam>
    public class RepositoryOperationResult<T>
    {
        internal RepositoryOperationResult() { }

        public RepositoryOperationResult(bool isOk, string message, T result)
        {
            IsOK = isOk;
            Message = message;
            Result = result;
        }

        /// <summary>
        /// Состояние операции
        /// </summary>
        public bool IsOK { get; internal set; }
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string Message { get; internal set; }
        /// <summary>
        /// Возвращаемый результат
        /// </summary>
        public T Result { get; internal set; }
    }
}
