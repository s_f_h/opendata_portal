﻿using Repository.Abstract;
using Repository.Concreate;
using Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    /// <summary>
    /// Фабрика репозиториев
    /// </summary>
    public static class RepositoryFactory
    {
        private static readonly Dictionary<Type, Type> repositories = new Dictionary<Type, Type>();

        static RepositoryFactory()
        {
            Register<Agency, Guid, IAgencyRepository, AgencyRepository>();
            Register<Category, Guid, ICategoryRepository, CategoryRepository>();
            Register<TableDisplayTemplate, Guid, ITableDisplayTemplateRepository, TableDisplayTemplateRepository>();
            Register<Set, Guid, ISetRepository, SetRepository>();
            Register<SetPassport, Guid, ISetPassportRepository, SetPasportRepository>();
            Register<SetVersion, Guid, ISetVersionRepository, SetVersionRepository>();
            Register<DataStructure, Guid, IDataStructureRepository, DataStructureRepository>();
            Register<ISetDataRepository, SetDataRepository>();
        }

        /// <summary>
        /// Регистрирует репозиторий в фабрике
        /// </summary>
        /// <typeparam name="TInterface">Интерфейс репозитория</typeparam>
        /// <typeparam name="TInstance">Репозиторий</typeparam>
        internal static void Register<TInterface, TInstance>()
        {
            repositories.Add(typeof(TInterface), typeof(TInstance));
        }

        /// <summary>
        /// Регистрирует репозиторий в фабрике
        /// </summary>
        /// <typeparam name="TEntity">Тип данных элементов репозитория</typeparam>
        /// <typeparam name="TKey">Тип данных первичного ключа элементов репозитория</typeparam>
        /// <typeparam name="TInterface">Тип данных интерфейса репозитория</typeparam>
        /// <typeparam name="TInstance">Тип данных репозитория</typeparam>
        static void Register<TEntity, TKey, TInterface, TInstance>()
        {
            repositories.Add(typeof(IRepositoryBase<TEntity, TKey>), typeof(TInstance));
            repositories.Add(typeof(IRepositoryExtend<TEntity, TKey>), typeof(TInstance));
            Register<TInterface, TInstance>();
        }


        /// <summary>
        /// Возвращает запрашиваемый репозиторий
        /// </summary>
        /// <typeparam name="TInterface">Тип интерфейса репозитория</typeparam>
        /// <returns>Репозиторий</returns>
        public static TInterface Get<TInterface>()
        {
            var repository = repositories.First(x => x.Key.Equals(typeof(TInterface)));
            return (TInterface)Activator.CreateInstance(repository.Value);
        }

        /// <summary>
        /// Возвращает запрашиваемый репозиторий для контекста данных
        /// </summary>
        /// <typeparam name="TInterface">Тип интерфейса репозитория</typeparam>
        /// <param name="ctx">Контекст данных репозитория</param>
        /// <returns>Репозиторий</returns>
        public static TInterface Get<TInterface>(DataContext ctx)
        {
            var repository = repositories.First(x => x.Key.Equals(typeof(TInterface)));
            return (TInterface)Activator.CreateInstance(repository.Value, new object[] { ctx });
        }

        /// <summary>
        /// Создает тестовые данные для БД
        /// </summary>
        public static void GenerateTestData()
        {
            Exception e = null;
            using (var ctx = new DataContext())
            {
                try
                {
                    GenerateTestData(ctx);
                }
                catch(Exception _e)
                {
                    e = _e;
                }
            }

            if (e != null) throw e;
        }

        /// <summary>
        /// Создает тестовые данные для БД
        /// </summary>
        /// <param name="ctx">Контекст данных</param>
        public static void GenerateTestData(DataContext ctx)
        {
            var conf = new Migrations.Configuration();
            conf._seed(ctx);
        }
    }
}
