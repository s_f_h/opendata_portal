﻿using Repository.Abstract;
using Repository.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Repository.Concreate
{
    internal class TableDisplayTemplateRepository : RepositoryBase<TableDisplayTemplate, Guid>, ITableDisplayTemplateRepository
    {
        public TableDisplayTemplateRepository() : base() { }
        public TableDisplayTemplateRepository(DataContext ctx) : base(ctx) { }

        public RepositoryOperationResult<TableDisplayTemplate> TableStructure(Guid id)
        {
            RepositoryOperationResult<TableDisplayTemplate> result = new RepositoryOperationResult<TableDisplayTemplate>();

            try
            {
                var item = ctx.TableDisplayTemplate.Include(x => x.Columns).First(x => x.Id == id);
                LoadTemplate(item);
                result.Result = item;
                result.IsOK = true;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
            }

            return result;
        }
        #region Вспомогательные методы для TableStructure
        void LoadTemplate(TableDisplayTemplate item)
        {
            var columns = item.Columns;
            columns = LoadColumns(columns);
            item.Columns = columns;
        }

        ColumnDisplayTemplate[] LoadColumns(ICollection<ColumnDisplayTemplate> columns)
        {
            ColumnDisplayTemplate[] result = new ColumnDisplayTemplate[columns.Count];
            int i = 0;

            foreach (var column in columns.OrderBy(x => x.Order))
            {
                var _column = ctx.CollumnDisplayTemplates.Include(x => x.SubColumns).Include(x => x.InnerTable.Columns)
                   .First(x => x.Id == column.Id);

                if (_column.InnerTable != null)
                    LoadTemplate(_column.InnerTable);

                if (_column.SubColumns != null && _column.SubColumns.Count > 0)
                    _column.SubColumns = LoadColumns(_column.SubColumns);

                result[i++] = _column;
            }

            return result;
        }
        #endregion

        public RepositoryOperationResult<DataStructure> SetStructure(Guid id)
        {
            RepositoryOperationResult<DataStructure> result = new RepositoryOperationResult<DataStructure>();

            try
            {
                var _item = ctx.DataStructures.FirstOrDefault(x => x.Id == id);

                var item = new DataStructure
                {
                    Id = _item.Id,
                    Parent_Id = _item.Parent_Id,
                    Template_Id = _item.Template_Id,
                    TableName = _item.TableName,
                    Order = _item.Order
                };

                List<DataStructure> children = new List<DataStructure>();
                foreach (var child in ctx.DataStructures.Where(x => x.Parent_Id == id))
                {
                    var _subRes = SetStructure(child.Id);
                    if (_subRes.IsOK)
                    {
                        children.Add(_subRes.Result);
                    }
                }

                result.Result = item;
                result.Result.Children = children;
                result.IsOK = true;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
            }

            return result;
        }
        #region Вспомогательные методы для SetStructure

        #endregion


        public RepositoryOperationResult<bool> Remove(bool withSaveChanges = true, params Guid[] ids)
        {
            throw new NotImplementedException();
        }

        public RepositoryOperationResult<bool> Add(TableDisplayTemplate item, bool withSaveChanges = true)
        {
            // TODO: Compleate
            RepositoryOperationResult<bool> result = new RepositoryOperationResult<bool> { IsOK = true };
            ctx.Set<TableDisplayTemplate>().Add(item);
            result.Result = ctx.SaveChanges() > 0;
            return result;
        }

        public RepositoryOperationResult<bool> Update(Guid id, TableDisplayTemplate newState, bool withSaveChanges = true)
        {
            throw new NotImplementedException();
        }

        protected override RepositoryOperationResult<bool> Remove(Guid id, bool withSaveChanges)
        {
            throw new NotImplementedException();
        }



    }
}
