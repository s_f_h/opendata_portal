﻿using Repository.Abstract;
using Repository.Entities.Base;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.Concreate
{
    /// <summary>
    /// Базовый класс реализации репозиториеа
    /// </summary>
    /// <typeparam name="TEntity">Тип данных элементов репозитория</typeparam>
    /// <typeparam name="TKey">Тип данных первичного ключа элементов репозитория</typeparam>
    internal abstract class RepositoryBase<TEntity, TKey> : IRepositoryMisc, IRepositoryBase<TEntity, TKey> where TEntity : EntityBase<TKey>
    {
        protected readonly DataContext ctx;
        readonly bool disposeOutside;
        protected DbQuery<TEntity> contextItems;

        protected RepositoryBase()
            : this(new DataContext()) { disposeOutside = false; }

        protected RepositoryBase(DataContext ctx)
        {
            this.ctx = ctx;
            disposeOutside = true;
            contextItems = this.ctx.Set<TEntity>().AsNoTracking();
        }

        ~RepositoryBase()
        {
            if (disposeOutside && ctx != null)
                ctx.Dispose();
        }


        public RepositoryOperationResult<int> Count(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null)
        {
            RepositoryOperationResult<int> result = new RepositoryOperationResult<int>();

            try
            {
                if (predicate != null)
                    result.Result = contextItems.Count(predicate);
                else
                    result.Result = contextItems.Count();

                result.IsOK = true;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
            }

            return result;
        }

        public RepositoryOperationResult<bool> Any(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null)
        {
            RepositoryOperationResult<bool> result = new RepositoryOperationResult<bool>();

            try
            {
                if (predicate != null)
                    result.Result = contextItems.Any(predicate);
                else
                    result.Result = contextItems.Any();

                result.IsOK = true;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
            }

            return result;
        }

        public RepositoryOperationResult<TEntity> FirstOrDefault(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null, params System.Linq.Expressions.Expression<Func<TEntity, object>>[] includes)
        {
            RepositoryOperationResult<TEntity> result = new RepositoryOperationResult<TEntity>();

            try
            {
                IQueryable<TEntity> items = AddIncludes(contextItems, includes);

                if (predicate != null)
                    result.Result = items.FirstOrDefault(predicate);
                else
                    result.Result = items.FirstOrDefault();

                result.IsOK = true;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
            }

            return result;
        }

        public RepositoryOperationResult<IQueryable<TEntity>> Where(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null, int from = 0, int count = 0, params System.Linq.Expressions.Expression<Func<TEntity, object>>[] includes)
        {
            RepositoryOperationResult<IQueryable<TEntity>> result = new RepositoryOperationResult<IQueryable<TEntity>>();

            try
            {
                IQueryable<TEntity> items = AddIncludes(contextItems, includes);

                if (predicate != null)
                    items = items.Where(predicate);

                result.Result = TakeRange(items, from, count);

                result.IsOK = true;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
            }

            return result;
        }

        public RepositoryOperationResult<bool> SaveChanges()
        {
            return new RepositoryOperationResult<bool> { Result = ctx.SaveChanges() > 0 };
        }


        protected IQueryable<TEntity> AddIncludes(IQueryable<TEntity> contextItems, params Expression<Func<TEntity, object>>[] includes)
        {
            foreach (var include in includes)
                contextItems = contextItems.Include(include);

            return contextItems;
        }

        protected IQueryable<TEntity> TakeRange(IQueryable<TEntity> contextItems, int from, int count)
        {
            if (from == 0 && count == 0) return contextItems;

            contextItems = contextItems.OrderBy(x => x.Id);

            if (from != 0)
                contextItems = contextItems.Skip(from);

            if (count != 0)
                contextItems = contextItems.Take(count);

            return contextItems;
        }

        /// <summary>
        /// Используется для явного указания процесса удаления элемента.
        /// Все открытые методы реализаций должны явно вызать данный метод внутри себя.
        /// </summary>
        /// <param name="id">Идентефикатор удаляемого элемента</param>
        /// <param name="withSaveChanges">Применить к контектсу данных</param>
        /// <returns><see cref="RepositoryFactory.RepositoryOperationResult"/>Результат операции</returns>
        protected abstract RepositoryOperationResult<bool> Remove(TKey id, bool withSaveChanges);
    }
}
