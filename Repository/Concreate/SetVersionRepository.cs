﻿using Repository.Abstract;
using Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Concreate
{
    internal class SetVersionRepository : RepositoryBase<SetVersion, Guid>, ISetVersionRepository
    {
        public SetVersionRepository() : base()
        {
        }

        public SetVersionRepository(DataContext ctx) : base(ctx)
        {
        }


        public RepositoryOperationResult<bool> Remove(bool withSaveChanges = true, params Guid[] ids)
        {
            throw new NotImplementedException();
        }

        public RepositoryOperationResult<bool> Add(SetVersion item, bool withSaveChanges = true)
        {
            RepositoryOperationResult<bool> result = new RepositoryOperationResult<bool> { IsOK = true };
            ctx.Set<SetVersion>().Add(item);
            result.Result = ctx.SaveChanges() > 0;
            return result;
        }

        public RepositoryOperationResult<bool> Update(Guid id, SetVersion newState, bool withSaveChanges = true)
        {
            throw new NotImplementedException();
        }

        protected override RepositoryOperationResult<bool> Remove(Guid id, bool withSaveChanges)
        {
            throw new NotImplementedException();
        }

        public RepositoryOperationResult<double?> GetAverageRating(Guid versionId)
        {
            var result = new RepositoryOperationResult<double?> { IsOK = true };
            try
            {
                var rating = ctx.GetAverageRatingForSetVersion(versionId);
                if (rating.Any())
                {
                    result.Result = rating.First().Rating;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.IsOK = false;
            }

            return result;
        }

        public RepositoryOperationResult<IQueryable<SetVersion>> GetSetVersionsForCategoryAndRating(Guid categoryId, double rating,
             Expression<Func<SetVersion, bool>> predicate = null, int from = 0,
            int count = 0, params Expression<Func<SetVersion, object>>[] includes)
        {
            var result = new RepositoryOperationResult<IQueryable<SetVersion>> { IsOK = true };

            try
            {
                var setVersions = ctx.GetSetVersionsWithCategoryAndRating(categoryId, rating);
                var _result = this.Where(predicate, from, count, includes);
                result.Result = _result.Result.Where(x => setVersions.Any(y => y.Id == x.Id));
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.IsOK = false;
            }

            return result;
        }
    }
}