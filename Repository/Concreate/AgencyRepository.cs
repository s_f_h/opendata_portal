﻿using Repository.Abstract;
using Repository.Entities;
using System;

namespace Repository.Concreate
{
    internal class AgencyRepository : RepositoryBase<Agency, Guid>, IAgencyRepository
    {
        public AgencyRepository() : base() { }
        public AgencyRepository(DataContext ctx) : base(ctx) { }

        public RepositoryOperationResult<bool> Remove(bool withSaveChanges = true, params Guid[] ids)
        {
            throw new NotImplementedException();
        }

        public RepositoryOperationResult<bool> Add(Agency item, bool withSaveChanges = true)
        {
            try
            {
                //TODO: Complete
                ctx.Set<Agency>().Add(item);
                ctx.SaveChanges();
            }
            catch (Exception e)
            {

            }
            return null;

        }

        public RepositoryOperationResult<bool> Update(Guid id, Agency newState, bool withSaveChanges = true)
        {
            throw new NotImplementedException();
        }

        protected override RepositoryOperationResult<bool> Remove(Guid id, bool withSaveChanges)
        {
            throw new NotImplementedException();
        }
    }
}
