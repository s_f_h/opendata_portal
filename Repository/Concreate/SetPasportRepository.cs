﻿using Repository.Abstract;
using Repository.Entities;
using Repository.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml.Serialization;

namespace Repository.Concreate
{
    internal class SetPasportRepository : RepositoryBase<SetPassport, Guid>, ISetPassportRepository
    {
        public SetPasportRepository() : base()
        {
        }

        public SetPasportRepository(DataContext ctx) : base(ctx)
        {
        }


        public RepositoryOperationResult<bool> Remove(bool withSaveChanges = true, params Guid[] ids)
        {
            throw new NotImplementedException();
        }

        public RepositoryOperationResult<bool> Add(SetPassport item, bool withSaveChanges = true)
        {
            RepositoryOperationResult<bool> result = new RepositoryOperationResult<bool> {IsOK = true};
            ctx.Set<SetPassport>().Add(item);
            result.Result = ctx.SaveChanges() > 0;
            return result;
        }

        public RepositoryOperationResult<bool> Update(Guid id, SetPassport newState, bool withSaveChanges = true)
        {
            throw new NotImplementedException();
        }

        protected override RepositoryOperationResult<bool> Remove(Guid id, bool withSaveChanges)
        {
            throw new NotImplementedException();
        }


        public void ExportList(Stream stream, string url_prefix, Formats format = Formats.xml)
        {
            switch (format)
            {
                case Formats.xml:
                    ExportListXML(stream, url_prefix);
                    break;
                case Formats.csv:
                    ExportListCSV(stream, url_prefix);
                    break;
                default:
                    throw new ArgumentException("Неизвестный формат");
            }
        }

        #region ExportList

        void ExportListXML(Stream stream, string url_prefix)
        {
            var setPassports = this.Where(x => x.SetVersion.IsCurrent);

            Repository.Entities.Export.XML.List result = new Repository.Entities.Export.XML.List();

            foreach (var setPassport in setPassports.Result)
            {
                var listItem = new Entities.Export.XML.MetaItem();
                listItem.identifier = setPassport.Identifier;
                listItem.title = setPassport.Title;
                listItem.link = string.Format("{0}/{1}/meta.xml", url_prefix, setPassport.Identifier);

                result.meta.Add(listItem);
            }

            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(Repository.Entities.Export.XML.List));
                ser.Serialize(stream, result);
            }
            catch
            {
            }
        }

        void ExportListCSV(Stream stream, string url_prefix)
        {
            var setPassports = this.Where(x => x.SetVersion.IsCurrent);

            StringBuilder str = new StringBuilder();
            str.AppendLine("property,title,value,format");
            str.AppendLine(
                "standardversion,Версия методических рекомендаций, http://opendata.gosmonitor.ru/standard/3.0,");

            foreach (var setPassport in setPassports.Result)
            {
                str.AppendFormat("{0},\"{1}\",{2},{3}{4}", setPassport.Identifier, setPassport.Title,
                    string.Format("{0}/{1}/meta.csv", url_prefix, setPassport.Identifier), "csv",
                    Environment.NewLine);
            }

            byte[] arr = Encoding.UTF8.GetBytes(str.ToString());

            stream.Write(arr, 0, arr.Length);
        }

        #endregion

        public void ExportMeta(Stream stream, string url_prefix, string identifier, Guid? version = null,
            Formats format = Formats.xml)
        {
            switch (format)
            {
                case Formats.csv:
                    ExportMetaCSV(stream, url_prefix, identifier, version);
                    break;
                case Formats.xml:
                    ExportMetaXML(stream, url_prefix, identifier, version);
                    break;
                default:
                    throw new ArgumentException("Неизвестный формат");
            }
        }

        #region ExportMeta

        void ExportMetaXML(Stream stream, string url_prefix, string identifier, Guid? version = null)
        {
            SetPassport passport = version.HasValue
                ? this.FirstOrDefault(x => x.SetVersion_Id == version.Value && x.Identifier == identifier).Result
                : this.FirstOrDefault(x => x.Identifier == identifier && x.SetVersion.IsCurrent).Result;

            if (passport == null) throw new NullReferenceException();

            Repository.Entities.Export.XML.Meta result = new Repository.Entities.Export.XML.Meta
            {
                created = passport.Created.ToUniversalTime().ToString("o"),
                creator = passport.Creator,
                data = CreatePassportDataField(passport, url_prefix),
                description = passport.Description,
                identifier = passport.Identifier,
                modified = passport.Modified != DateTime.MinValue
                    ? passport.Modified.ToUniversalTime().ToString("o")
                    : passport.Created.ToUniversalTime().ToString("o"),
                publisher = CreatePassportPublisherField(passport),
                structure = CreatePassportStructureField(passport, url_prefix),
                subject = passport.Subject,
                title = passport.Title
            };


            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(Repository.Entities.Export.XML.Meta));
                ser.Serialize(stream, result);
            }
            catch
            {
            }
        }

        #region ExportMeta routine

        class SetVerDistinctStruct : IEqualityComparer<SetVersion>
        {
            public bool Equals(SetVersion x, SetVersion y)
            {
                return x.DataStructure_Id == y.DataStructure_Id;
            }

            public int GetHashCode(SetVersion obj)
            {
                return obj.Id.GetHashCode();
            }
        }

        private List<Repository.Entities.Export.XML.StructureVersion> CreatePassportStructureField(SetPassport passport,
            string url_prefix)
        {
            var result = new List<Repository.Entities.Export.XML.StructureVersion>();

            IRepositoryBase<SetVersion, Guid> setVersRepo = new SetVersionRepository(this.ctx);
            var setVers = setVersRepo.Where(x => x.SetsPassports.Any(y => y
                .Identifier == passport.Identifier), 0, 0, x => x.DataStructure)
                .Result.ToArray().Distinct(new SetVerDistinctStruct());

            foreach (var ver in setVers)
            {
                result.Add(new Repository.Entities.Export.XML.StructureVersion
                {
                    created = ver.CreationDate.ToUniversalTime().ToString("o"),
                    source = string.Format("{0}/{1}/structure-{2}.xsd", url_prefix,
                        passport.Identifier, ver.DataStructure.CodeName)
                });
            }

            return result;
        }

        private Entities.Export.XML.Publisher CreatePassportPublisherField(SetPassport passport)
        {
            return new Entities.Export.XML.Publisher
            {
                name = passport.PublisherName,
                phone = passport.PublisherPhone,
                mbox = passport.PublisherMBox
            };
        }

        private List<Entities.Export.XML.DataVersion> CreatePassportDataField(SetPassport passport, string url_prefix)
        {
            var result = new List<Entities.Export.XML.DataVersion>();

            var passports =
                this.Where(x => x.SetVersion.SetsPassports.Any(y => y.Identifier == passport.Identifier), 0, 0,
                    x => x.SetVersion.DataStructure).Result;

            foreach (var _passport in passports)
            {
                result.Add(new Entities.Export.XML.DataVersion
                {
                    created = _passport.Modified != DateTime.MinValue
                        ? _passport.Modified.ToUniversalTime().ToString("o")
                        : passport.Created.ToUniversalTime().ToString("o"),
                    provenance = _passport.Provinance,
                    valid = _passport.Valid.HasValue
                        ? _passport.Valid.Value.ToUniversalTime().ToString("o")
                        : null,
                    structure = _passport.SetVersion.CodeName,
                    source = string.Format("{0}/{1}/data-{2}-structure-{3}.xml", url_prefix,
                        passport.Identifier, _passport.SetVersion.CodeName, _passport.SetVersion.DataStructure.CodeName)
                });
            }

            return result;
        }

        #endregion

        void ExportMetaCSV(Stream stream, string url_prefix, string identifier, Guid? version = null)
        {
            var str = new StringBuilder();
            str.AppendLine("property,value");
            str.AppendLine("standardversion,http://opendata.gosmonitor.ru/standard/3.0");

            SetPassport passport = version.HasValue
                ? this.FirstOrDefault(x => x.SetVersion_Id == version.Value && x.Identifier == identifier).Result
                : this.FirstOrDefault(x => x.Identifier == identifier && x.SetVersion.IsCurrent).Result;

            if (passport == null) throw new NullReferenceException();

            str.AppendFormat("identifier,\"{0}\"{1}", passport.Identifier, Environment.NewLine);
            str.AppendFormat("title,\"{0}\"{1}", passport.Title, Environment.NewLine);
            str.AppendFormat("description,\"{0}\"{1}", passport.Description, Environment.NewLine);
            str.AppendFormat("creator,\"{0}\"{1}", passport.Creator, Environment.NewLine);
            str.AppendFormat("publishername,\"{0}\"{1}", passport.PublisherName, Environment.NewLine);
            str.AppendFormat("publisherphone,\"{0}\"{1}", passport.PublisherPhone, Environment.NewLine);
            str.AppendFormat("publishermbox,\"{0}\"{1}", passport.PublisherMBox, Environment.NewLine);
            str.AppendFormat("format,{0}{1}", "csv", Environment.NewLine);
            str.AppendFormat("created,{0}{1}", passport.Created.ToUniversalTime().ToString("yyyyMMdd"),
                Environment.NewLine);
            str.AppendFormat("modified,{0}{1}", passport.Modified != DateTime.MinValue
                ? passport.Modified.ToUniversalTime().ToString("yyyyMMdd")
                : passport.Created.ToUniversalTime().ToString("yyyyMMdd"), Environment.NewLine);
            str.AppendFormat("provenance,\"{0}\"{1}", passport.Provinance, Environment.NewLine);
            str.AppendFormat("valid,{0}{1}", passport.Valid.HasValue && passport.Valid.Value != DateTime.MinValue
                ? passport.Valid.Value.ToUniversalTime().ToString("yyyyMMdd")
                : passport.Created.AddYears(1).ToUniversalTime().ToString("yyyyMMdd"), Environment.NewLine);
            str.AppendFormat("subject,\"{0}\"{1}", passport.Subject.Replace(";", ","), Environment.NewLine);


            var passports =
                this.Where(x => x.SetVersion.SetsPassports.Any(y => y.Identifier == passport.Identifier), 0, 0,
                    x => x.SetVersion.DataStructure).Result;

            foreach (var _passport in passports)
            {
                str.AppendFormat("data-{0}-structure-{1},{2}{3}",
                    _passport.SetVersion.CodeName,
                    _passport.SetVersion.DataStructure.CodeName,
                    string.Format("{0}/{1}/data-{2}-structure-{3}.csv", url_prefix,
                        passport.Identifier, _passport.SetVersion.CodeName,
                        _passport.SetVersion.DataStructure.CodeName),
                    Environment.NewLine);
            }

            foreach (var _passport in passports)
            {
                str.AppendFormat("structure-{0},{1}{2}",
                    _passport.SetVersion.DataStructure.CodeName,
                    string.Format("{0}/{1}/structure-{2}.csv", url_prefix,
                        passport.Identifier, _passport.SetVersion.DataStructure.CodeName),
                    Environment.NewLine);
            }

            byte[] arr = Encoding.UTF8.GetBytes(str.ToString());

            stream.Write(arr, 0, arr.Length);
        }

        #endregion


        public RepositoryOperationResult<double?> GetAverageRating(Guid passportId)
        {
            var result = new RepositoryOperationResult<double?> { IsOK = true };
            try
            {
                var rating = ctx.GetAverageRatingForPassport(passportId);
                if (rating.Any())
                {
                    result.Result = rating.First().Rating;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.IsOK = false;
            }

            return result;
        }

        public RepositoryOperationResult<IQueryable<SetPassport>> GetPassportsForAgencyAndRating(Guid agencyId, double rating,
             Expression<Func<SetPassport, bool>> predicate = null, int from = 0,
            int count = 0, params Expression<Func<SetPassport, object>>[] includes)
        {
            var result = new RepositoryOperationResult<IQueryable<SetPassport>> { IsOK = true };

            try
            {
                var passports = ctx.GetSetPassporstWithAgencyAndRating(agencyId, rating);
                var _result = this.Where(predicate, from, count, includes);
                result.Result = _result.Result.Where(x => passports.Any(y => y.Id == x.Id));

            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.IsOK = false;
            }

            return result;
        }


        public RepositoryOperationResult<bool> AddPassportRating(Guid passportId, int value, string ip)
        {
            var result = new RepositoryOperationResult<bool> { IsOK = true };

            try
            {
                result.Result = ctx.Database.ExecuteSqlCommand(string
                    .Format("INSERT INTO [dbo].[RatedBy] (Rating, UserIP, SetPassport_Id) VALUES ({0}, '{1}', '{2}')",
                    value, ip, passportId)) > 0;
            }
            catch (Exception e)
            {
                result.IsOK = false;
                result.Message = e.Message;
            }

            return result;
        }


        public RepositoryOperationResult<bool> AddPassportViewdBy(Guid passportId, string ip)
        {
            var result = new RepositoryOperationResult<bool>{IsOK = true};
            try
            {
                var passport = ctx.SetsPassports.FirstOrDefault(x => x.Id == passportId);
                if (passport != null)
                {
                    string cmd = string.Format("IF NOT EXISTS(SELECT * FROM [dbo]" +
                        ".[ViewedBy] WHERE [SetPassport_Id] = '{0}') INSERT INTO " +
                        "[dbo].[ViewedBy] ([UserIP], [SetPassport_Id]) VALUES " +
                        "('{1}', '{0}')", passportId, ip);

                    result.Result = ctx.Database.ExecuteSqlCommand(cmd) > 0;
                }
            }
            catch (Exception e)
            {
                result.IsOK = false;
                result.Message = e.Message;
            }

            return result;
        }
    }
}