﻿using Repository.Abstract;
using Repository.Entities;
using Repository.Misc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;

namespace Repository.Concreate
{
    internal class DataStructureRepository : RepositoryBase<DataStructure, Guid>, IDataStructureRepository
    {

        public DataStructureRepository() : base() { }
        public DataStructureRepository(DataContext ctx) : base(ctx) { }

        public RepositoryOperationResult<bool> Remove(bool withSaveChanges = true, params Guid[] ids)
        {
            throw new NotImplementedException();
        }

        public RepositoryOperationResult<bool> Add(DataStructure item, bool withSaveChanges = true)
        {
            if (!withSaveChanges)
                throw new ArgumentException("Must be true for this repository", "withSaveChanges");

            RepositoryOperationResult<bool> result = new RepositoryOperationResult<bool>();

            try
            {
                string createTablesCmd = GenerateDataStructureQuery(item);

                using (var transaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        ctx.Database.ExecuteSqlCommand(createTablesCmd.ToString());
                        ctx.Set<DataStructure>().Add(item);
                        result.Result = ctx.SaveChanges() > 0;

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();

                        result.IsOK = false;
                        result.Message = e.Message;
                    }
                }

                result.IsOK = true;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
            }


            return result;
        }

        #region Вспомогательные методы для Add
        //TODO: попытаться распараллелить данную операцию
        private string GenerateDataStructureQuery(DataStructure item, string referencedTable = null)
        {
            var fields = ParseTableDisplayTemplate(item.Template_Id);
            var template = ctx.TableDisplayTemplate.First(x => x.Id == item.Template_Id);

            StringBuilder cmd = new StringBuilder();
            Dictionary<string, string> referenceTables;
            // предполагается, что если referenceTable пустой, то, создается таблица верхнего уровня
            if (string.IsNullOrWhiteSpace(referencedTable))
            {
                referenceTables = new Dictionary<string, string>()
                {
                    { SqlCommandGenerator.TableNames.SetPassport, "uniqueidentifier" },
                };
            }
            else
            {
                referenceTables = new Dictionary<string, string>()
                {
                    { referencedTable, "uniqueidentifier" }
                };
            }

            cmd.AppendLine(SqlCommandGenerator.CreateNewPlainTable(item.TableName, template.HasMapPOIs,
                    fields, referenceTables));

            if (item.Children != null)
                foreach (var child in item.Children)
                    cmd.Append(GenerateDataStructureQuery(child, item.TableName));

            return cmd.ToString();
        }

        private Dictionary<string, string> ParseTableDisplayTemplate(Guid template_id)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            var columns = ctx.CollumnDisplayTemplates.Include(x => x.SubColumns)
                .Where(x => x.Template_Id == template_id);

            foreach (var column in columns)
                result = result.Concat(ParseColumnDisplayTemplate(column))
                    .ToDictionary(x => x.Key, x => x.Value);

            return result;
        }

        private Dictionary<string, string> ParseColumnDisplayTemplate(ColumnDisplayTemplate column)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            column = ctx.CollumnDisplayTemplates.Include(x => x.SubColumns).First(x => x.Id == column.Id);

            if (column.InnerTable_Id.HasValue)
            {
                result = result.Concat(ParseTableDisplayTemplate(column.InnerTable_Id.Value))
                    .ToDictionary(x => x.Key, x => x.Value);
                return result;
            }

            if (column.SubColumns != null && column.SubColumns.Any())
            {
                foreach (var _column in column.SubColumns)
                    result = result.Concat(ParseColumnDisplayTemplate(_column))
                        .ToDictionary(x => x.Key, x => x.Value);

                return result;
            }

            result.Add(column.BindedCollumn, column.Type.SQLType());

            return result;
        }

        #endregion


        public RepositoryOperationResult<bool> Update(Guid id, DataStructure newState, bool withSaveChanges = true)
        {
            throw new NotImplementedException();
        }

        protected override RepositoryOperationResult<bool> Remove(Guid id, bool withSaveChanges)
        {
            throw new NotImplementedException();
        }

        // TODO: Compleate structure select query for structId
        public void ExportStructure(Stream stream, string setId, string structId, Formats format)
        {
            var structure = this.FirstOrDefault(x => x.Version.Any(y => y
                .SetsPassports.Any(z => z.Identifier == setId)) &&
                x.Parent == null && x.CodeName == structId , x => x.Children).Result;

            if (structure == null) return;

            switch (format)
            {
                case Formats.csv:
                    ExportStructureCSV(stream, structure);
                    break;
                case Formats.xml:
                case Formats.xsd:
                    ExportStructureXML(stream, structure);
                    break;
            }
        }

        #region ExportStructure
        void ExportStructureCSV(Stream stream, DataStructure structure)
        {
            if (structure.Children.Any())
                throw new NotSupportedException();

            StringBuilder csv = new StringBuilder("field name,english description,russian description,format" + Environment.NewLine);
            TableDisplayTemplateRepository repo = new TableDisplayTemplateRepository(this.ctx);

            var template = repo.TableStructure(structure.Template_Id).Result;

            ParseTemplateCSV(template, csv);

            if (template.HasMapPOIs)
            {
                csv.AppendLine("POI_Latitude,\"The latitude coordinates of an object in data set\",\"Координаты широты объекта в наборе данных\",double");
                csv.AppendLine("POI_Longitude,\"The longitude coordinates of an object in data set\",\"Координаты долготы объекта в наборе данных\",double");
            }

            var streamData = Encoding.UTF8.GetBytes(csv.ToString());

            stream.Write(streamData, 0, streamData.Length);
        }

        #region ExportStructureCSV
        void ParseTemplateCSV(TableDisplayTemplate template, StringBuilder csv)
        {
            foreach (var column in template.Columns)
                ParseColumnCSV(column, csv);
        }

        void ParseColumnCSV(ColumnDisplayTemplate column, StringBuilder csv)
        {
            if (column.InnerTable!=null)
                ParseTemplateCSV(column.InnerTable, csv);

            if (column.SubColumns != null )
                foreach (var _column in column.SubColumns)
                    ParseColumnCSV(_column, csv);

            if (!string.IsNullOrEmpty(column.BindedCollumn))
                csv.AppendFormat("{0},\"{1}\",\"{2}\",{3}{4}", column.BindedCollumn, 
                    column.DescritpionEn, column.DescriptionRu, column.Type
                        .CSVType(), Environment.NewLine);

        }
        #endregion

        
        void ExportStructureXML(Stream stream, DataStructure structure)
        {

        }

        #region ExportStructureXML
        #endregion
        #endregion


        public void ExportStructure(Stream stream, Guid setVerId, Formats format)
        {
            var structure = ctx.DataStructures.FirstOrDefault(x => x.Version.Any(y => y.Id == setVerId));
            var passport = ctx.SetsPassports.FirstOrDefault(x => x.SetVersion_Id == setVerId);

            if (structure != null && passport != null)
                ExportStructure(stream, passport.Identifier, structure.CodeName, format);
        }
    }
}
