﻿using Repository.Abstract;
using Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.Concreate
{
    internal class SetRepository : RepositoryBase<Set, Guid>, ISetRepository
    {
         public SetRepository() : base() { }
         public SetRepository(DataContext ctx) : base(ctx) { }

         public RepositoryOperationResult<bool> Remove(bool withSaveChanges = true, params Guid[] ids)
         {
             throw new NotImplementedException();
         }

         public RepositoryOperationResult<bool> Add(Set item, bool withSaveChanges = true)
         {
             //TODO: Complete
             RepositoryOperationResult<bool> result = new RepositoryOperationResult<bool> { IsOK = true };
             var id = item.Agencies.First().Id;
             var agency = ctx.Set<Agency>().Include(x=>x.Sets).First(x => x.Id == id);
             item.Agencies = null;
             agency.Sets.Add(item);

             result.Result = ctx.SaveChanges() > 0;
             return result;
         }

         public RepositoryOperationResult<bool> Update(Guid id, Set newState, bool withSaveChanges = true)
         {
             throw new NotImplementedException();
         }

         protected override RepositoryOperationResult<bool> Remove(Guid id, bool withSaveChanges)
         {
             throw new NotImplementedException();
         }
    }
}
