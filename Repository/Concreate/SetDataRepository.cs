﻿using Repository.Abstract;
using Repository.Entities;
using Repository.Misc;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace Repository.Concreate
{
    internal class SetDataRepository : ISetDataRepository
    {
        const string POIFielsd = @"POI_Latitude, POI_Longitude, POI_MarkerName";

        #region
        protected readonly DataContext ctx;
        readonly bool disposeOutside;

        public SetDataRepository()
        {
            disposeOutside = false;
        }

        public SetDataRepository(DataContext ctx)
        {
            this.ctx = ctx;
            disposeOutside = true;
        }

        ~SetDataRepository()
        {
            if (!disposeOutside && ctx != null)
                ctx.Dispose();
        }
        #endregion

        public List<Dictionary<string, object>> GetTableData(Guid verId, Guid structId, string passportId = null, int from = 0, int count = 0, string filterQuery = null)
        {
            var setStruct = ctx.DataStructures.FirstOrDefault(x => x.Id == structId);
            if (setStruct == null) return null;

            string cmd;
            string tableName = setStruct.TableName;
            List<Dictionary<string, object>> result = (count == 0)
                ? new List<Dictionary<string, object>>()
                : new List<Dictionary<string, object>>(count);

            if (string.IsNullOrWhiteSpace(passportId))
                if (count == 0)
                    cmd = SqlCommandGenerator.GetTableDataQuery(tableName, verId, filterQuery);
                else
                    cmd = SqlCommandGenerator.GetTableDataQuery(tableName, verId, from, count, filterQuery);
            else
                if (count == 0)
                    cmd = SqlCommandGenerator.GetTableDataQuery(tableName, verId, passportId, filterQuery);
                else
                    cmd = SqlCommandGenerator.GetTableDataQuery(tableName, verId, passportId, from, count, filterQuery);

            return LoadData(cmd);
        }

        public List<Dictionary<string, object>> GetFullViewItemTableData(Guid structId, Guid fullViewItemId)
        {
            var setStruct = ctx.DataStructures.FirstOrDefault(x => x.Id == structId);
            if (setStruct == null) return null;
            
            string tableName = setStruct.TableName;
            string cmd = SqlCommandGenerator.GetFullViewItemTableDataQuery(tableName, fullViewItemId);

            return LoadData(cmd);
        }

        public List<Dictionary<string, object>> GetAdditionalSetsItemData(Guid structId, Guid parentItemId)
        {
            var setStruct = ctx.DataStructures.Include(x=>x.Parent).FirstOrDefault(x => x.Id == structId);
            if (setStruct == null) return null;

            string cmd = SqlCommandGenerator.GetAdditionalSetsItemDataQuery(setStruct.TableName, setStruct.Parent.TableName, parentItemId);

            return LoadData(cmd);
        }

        #region Load Data
        List<Dictionary<string, object>> LoadData(string cmd)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            using (var sqlCon = new SqlConnection(ctx.Database.Connection.ConnectionString))
            {
                try
                {
                    sqlCon.Open();
                    using (var sqlCmd = new SqlCommand(cmd, sqlCon))
                    {
                        try
                        {
                            var reader = sqlCmd.ExecuteReader();
                            while (reader.Read())
                            {
                                var row = new Dictionary<string, object>(reader.VisibleFieldCount);
                                for (int i = 0; i < reader.VisibleFieldCount; ++i)
                                    row.Add(reader.GetName(i), reader.GetValue(i));

                                result.Add(row);
                            }
                        }
                        catch
                        {
                        }
                    }
                }
                catch
                {
                    result = null;
                }
                finally
                {
                    sqlCon.Close();
                }

            }

            return result;
        }
        #endregion

        public int GetElementsCount(Guid verId, Guid structId, string passportId = null, string filterQuery = null)
        {
            int result = 0;

            var structure = ctx.DataStructures.FirstOrDefault(x => x.Id == structId);
            if (structure != null)
            {
                string cmd;
                if (string.IsNullOrWhiteSpace(passportId))
                    cmd = SqlCommandGenerator.GetTableDataCountQuery(structure.TableName, verId, filterQuery);
                else
                    cmd = SqlCommandGenerator.GetTableDataCount(structure.TableName, verId, passportId, filterQuery);

                using (var sqlCon = new SqlConnection(ctx.Database.Connection.ConnectionString))
                {
                    try
                    {
                        sqlCon.Open();
                        using (var sqlCmd = new SqlCommand(cmd, sqlCon))
                        {
                            try
                            {
                                result = (int)sqlCmd.ExecuteScalar();
                            }
                            catch
                            {
                            }
                        }
                    }
                    catch
                    {
                    }
                }

            }

            return result;
        }

        public void ExportData(Stream stream, string passportId = null, string setVerId = null, string setStructId = null, Formats format = Formats.xml)
        {
            var setVer = string.IsNullOrEmpty(setVerId)
                ? ctx.SetsVersions.FirstOrDefault(x => x.SetsPassports.Any(y =>
                    y.Identifier == passportId) && x.IsCurrent)
                : ctx.SetsVersions.FirstOrDefault(x => x.SetsPassports.Any(y =>
                    y.Identifier == passportId) && x.CodeName == setVerId &&
                    x.DataStructure.CodeName == setStructId);

            if (setVer == null) return;

            switch (format)
            {
                case Formats.csv:
                    ExportDataCSV(stream, setVer, passportId);
                    break;
                case Formats.xml:
                    ExportDataXML(stream, setVer, passportId);
                    break;
            }
        }

        public void ExportData(Stream stream, Guid setVerId, Formats format)
        {
            var setVer = ctx.SetsVersions.Include(x => x.SetsPassports)
                .FirstOrDefault(x => x.Id == setVerId);

            if (setVer == null) return;

            switch (format)
            {
                case Formats.csv:
                    ExportDataCSV(stream, setVer, setVer.SetsPassports.Select(x => x.Identifier).ToArray());
                    break;
                case Formats.xml:
                    ExportDataXML(stream, setVer, setVer.SetsPassports.Select(x=>x.Identifier).ToArray());
                    break;
            }
        }

        #region ExportData

        private void ExportDataCSV(Stream stream, SetVersion setVer, params string[] passportsId)
        {
            var dataStruct = ctx.DataStructures.FirstOrDefault(x => x.Parent == null && x.Version.Any(y => y.Id == setVer.Id));

            // Создание CSV таблиц невозможно для наборов со сложной структурой
            if (ctx.DataStructures.Any(x=>x.Parent_Id == dataStruct.Id))
                throw new NotSupportedException();

            StringBuilder csv = new StringBuilder();
            bool firstIteration = true;
            List<string> keys = new List<string>();
            foreach (var passportId in passportsId)
            {
                var data = GetTableData(setVer.Id, dataStruct.Id, passportId);
                if (data.Count == 0) continue;
                if (firstIteration)
                {
                    var firstLine = data.First();
                    foreach (var key in firstLine.Keys)
                    {
                        if (key == "Id" || key == "POI_MarkerName" || key == "SetsPassports_Id")
                        {
                            if (key == "SetsPassports_Id")
                            {
                                csv.Remove(csv.Length - 1, 1);
                                csv.Append(Environment.NewLine);
                            }
                            continue;
                        }

                        csv.AppendFormat("{0},", key);
                        keys.Add(key);
                    }
                    firstIteration = false;
                }

                foreach (var line in data)
                {
                    StringBuilder csvLine = new StringBuilder();

                    int pos = 0;

                    foreach (var key in keys)
                    {
                        //TODO: Create correct conversation
                        var field = line[key];

                        csvLine.AppendFormat("\"{0}\"{1}", field.ToString().Replace("\n", "").Replace("\"", ""), (pos == (keys.Count - 1) ? "" : ","));
                        pos++;
                    }

                    csv.AppendLine(csvLine.ToString());
                }
            }
            var csvData = Encoding.UTF8.GetBytes(csv.ToString());

            stream.Write(csvData, 0, csvData.Length);
        }


        private void ExportDataXML(Stream stream, SetVersion setVer, params string[] passportsId)
        {
            throw new NotImplementedException();
        }
        #region ExportDataXML
        #endregion
        #endregion





        
    }
}
