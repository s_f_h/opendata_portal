﻿using Repository.Abstract;
using Repository.Entities;
using System;

namespace Repository.Concreate
{
    internal class CategoryRepository : RepositoryBase<Category, Guid>, ICategoryRepository
    {
        public CategoryRepository() : base() { }
        public CategoryRepository(DataContext ctx) : base(ctx) { }


        public RepositoryOperationResult<bool> Remove(bool withSaveChanges = true, params Guid[] ids)
        {
            throw new NotImplementedException();
        }

        public RepositoryOperationResult<bool> Add(Category item, bool withSaveChanges = true)
        {
            //TODO: Complete
            ctx.Set<Category>().Add(item);
            ctx.SaveChanges();
            return null;
        }

        public RepositoryOperationResult<bool> Update(Guid id, Category newState, bool withSaveChanges = true)
        {
            throw new NotImplementedException();
        }


        protected override RepositoryOperationResult<bool> Remove(Guid id, bool withSaveChanges)
        {
            throw new NotImplementedException();
        }
    }
}
