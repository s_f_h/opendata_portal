﻿using System.Reflection;
using System.Web.Configuration;

namespace Common
{
    /// <summary>
    /// Параметры приложения
    /// </summary>
    public static class Configuration
    {
        static T GetValue<T>(string key)
        {
            T result = default(T);

            try
            {
                object val = WebConfigurationManager.AppSettings[key];

                var T_t = typeof(T);

                if (T_t.GUID != typeof(string).GUID)
                {
                    result = (T)T_t.InvokeMember("Parse", BindingFlags.InvokeMethod, null, result, new object[] { val });
                }
                else
                    result = (T)val;
            }
            catch { }

            return result;
        }

        /// <summary>
        /// Базовые настройки
        /// </summary>
        public static class Base
        {
            static Base()
            {
                _connectionStringName = GetValue<string>("connectionStringName");
                _addTestData = GetValue<bool>("addTestData");
                _itemsPerPage = GetValue<int>("itemsPerPage");
                _itemsPerPage = (_itemsPerPage == 0) ? 15 : _itemsPerPage;
                _baseUrl = GetValue<string>("baseURL");
            }

            readonly static string _connectionStringName;
            readonly static bool _addTestData;
            readonly static int _itemsPerPage;
            readonly static string _baseUrl;

            /// <summary>
            /// Строка подключения к БД
            /// </summary>
            public static string ConnectionStringName { get { return _connectionStringName; } }
            /// <summary>
            /// Создать тестовые данные
            /// </summary>
            public static bool AddTestData { get { return _addTestData; } }

            /// <summary>
            /// Определяет количество элементов на странице
            /// </summary>
            public static int ItemsPerPage { get { return _itemsPerPage; } }

            public static string BaseURL { get { return _baseUrl; } }
        }

        /// <summary>
        /// Параметры WebAPI OData
        /// </summary>
        public static class OData
        {
        }

        public static class SuppurtInformation
        {
            static SuppurtInformation()
            {
                _email = GetValue<string>("supportEMail");
                _smtpPort = GetValue<int>("smtpPort");
                _smtpHost = GetValue<string>("smtpHost");
                _smtpPassword = GetValue<string>("smtpPassword");
                _smtpFrom = GetValue<string>("smtpFrom");
            }

            readonly static string _email;
            readonly static int _smtpPort;
            readonly static string _smtpHost;
            readonly static string _smtpPassword;
            readonly static string _smtpFrom;

            public static string SMTPFrom { get { return _smtpFrom; } }
            public static string EMail { get { return _email; } }
            public static int SMTPPort { get { return _smtpPort; } }
            public static string SMTPHost { get { return _smtpHost; } }
            public static string SMTPPassword { get { return _smtpPassword; } }
        }
    }
}
