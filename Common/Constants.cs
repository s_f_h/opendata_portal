﻿namespace Common
{
    /// <summary>
    /// Константы
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Регулярные выражения
        /// </summary>
        public static class RegExps
        {
            /// <summary>
            /// ИНН
            /// </summary>
            public const string ITN = @"\d{10}";
            /// <summary>
            /// Идентефикатор набора:
            /// <ИНН>-<имя набора>
            /// </summary>
            public const string SetIdentifier = ITN + @"\-\w+";
            /// <summary>
            /// Телефонный номер:
            /// +<код страны>-<код оператора>-<телефон>
            /// </summary>
            public const string PhoneNumber = @"\+\d+\-\d+\-\d+";
            /// <summary>
            /// Guid
            /// </summary>
            public const string Guid = @"\b[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}\b";
            /// <summary>
            /// Наименование версии
            /// </summary>
            public const string VersionName = @"ver[a-z0-9]{2,10}";
            /// <summary>
            /// Код версии или версии структуры для набора данных
            /// </summary>
            public const string CodeName = @"^(\d{4})\D?(0[1-9]|1[0-2])\D?([12]\d|0[1-9]|3[01])(\D?([01]\d|2[0-3])\D?([0-5]\d)\D?([0-5]\d)?\D?(\d{3})?)?$";
        }
        /// <summary>
        /// Шаблоны
        /// </summary>
        public static class Watermarks
        {
            /// <summary>
            /// ИНН
            /// </summary>
            public const string ITN = "1234567980";
            /// <summary>
            /// Идентефикатор набора
            /// </summary>
            public const string SetIdentifier = "1234567890-showrooms";
            /// <summary>
            /// Телефонный номер
            /// </summary>
            public const string PhoneNumber = "+1-234-5678901";
            /// <summary>
            /// Почтовый адресс
            /// </summary>
            public const string PostAdress = @"";
            /// <summary>
            /// Наименование версии
            /// </summary>
            public const string VersionName = "ver334";
            /// <summary>
            /// E-Mail
            /// </summary>
            public const string Email = "test@example.com";
            /// <summary>
            /// Дата
            /// </summary>
            public const string Date = "20.03.2008";
            /// <summary>
            /// Префикс заголовков страниц
            /// </summary>
            public const string PagePrefix = "Портал ОД: ";
            /// <summary>
            /// Код версии
            /// </summary>
            public const string VersionCode = "19991111";
            /// <summary>
            /// Код структуры
            /// </summary>
            public const string StructureCode = "20001111";
        }
        /// <summary>
        /// Форматы строк
        /// </summary>
        public static class StringFormats
        {
            /// <summary>
            /// Строка формата даты в ISO 6081
            /// </summary>
            public const string Date = "yyyyMMdd";
            /// <summary>
            /// Строка формата времени (до секунды) в ISO 6081
            /// </summary>
            public const string Time = "hhmmss";
            /// <summary>
            /// Строка формата даты и времени (до секунды) в ISO 6081
            /// </summary>
            public const string DateTime = Date + Time;
        }
    }
}
