﻿using System;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Linq;
using System.Web.Caching;

namespace Common
{
    /// <summary>
    /// Accepts only AJAX requests for controller actions
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class AjaxRequestAttribute : ActionMethodSelectorAttribute
    {
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            return controllerContext.RequestContext.HttpContext.Request.IsAjaxRequest();
        }
    }

    public class PreventSpamAttribute : ActionFilterAttribute
    {
        public int DelayRequest = 60;
        public string ErrorMessage = "Excessive Request Attempts Detected.";
        public string ReturnURL;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            var originationInfo = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.UserHostAddress;
            originationInfo += request.UserAgent;
            var cache = filterContext.HttpContext.Cache;

            var targetInfo = request.RawUrl + request.QueryString;

            string hashValue;

            using (var md5 = MD5.Create())
            {
                hashValue = string.Join("", md5.ComputeHash(Encoding.ASCII.GetBytes(originationInfo + targetInfo)).Select(s => s.ToString("x2")));
            }

            if (cache[hashValue] != null)
            {
                filterContext.Controller.ViewData.ModelState.AddModelError("ExcessiveRequests", ErrorMessage);
            }
            else
            {
                cache.Add(hashValue, null, null, DateTime.Now.AddSeconds(DelayRequest), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
